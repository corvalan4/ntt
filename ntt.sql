-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 25-11-2020 a las 03:24:03
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ntt`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ajuste`
--

DROP TABLE IF EXISTS `ajuste`;
CREATE TABLE IF NOT EXISTS `ajuste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `importe` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance`
--

DROP TABLE IF EXISTS `balance`;
CREATE TABLE IF NOT EXISTS `balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `notacredito_id` int(11) DEFAULT NULL,
  `consignacion_id` int(11) DEFAULT NULL,
  `remitooficial_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verifarma_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `amountBalance` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ACF41FFEA76ED395` (`user_id`),
  KEY `IDX_ACF41FFEDCD6110` (`stock_id`),
  KEY `IDX_ACF41FFEF04F795F` (`factura_id`),
  KEY `IDX_ACF41FFED3F32E95` (`notacredito_id`),
  KEY `IDX_ACF41FFEA564AB8B` (`consignacion_id`),
  KEY `IDX_ACF41FFE8AD3CF3B` (`remitooficial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

DROP TABLE IF EXISTS `banco`;
CREATE TABLE IF NOT EXISTS `banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

DROP TABLE IF EXISTS `caja`;
CREATE TABLE IF NOT EXISTS `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `saldoInicial` double DEFAULT NULL,
  `saldoFinal` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E465F405C7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `unidadnegocio_id`, `fecha`, `saldoInicial`, `saldoFinal`, `estado`) VALUES
(1, 1, '2020-11-21 16:07:56', 0, 0, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cheque`
--

DROP TABLE IF EXISTS `cheque`;
CREATE TABLE IF NOT EXISTS `cheque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origen_id` int(11) DEFAULT NULL,
  `destino_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `cobranza_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `banco_id` int(11) DEFAULT NULL,
  `tipocheque_id` int(11) DEFAULT NULL,
  `fechaemision` datetime DEFAULT NULL,
  `fechacobro` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `nrocheque` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firmantes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conciliacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A0BBFDE993529ECD` (`origen_id`),
  KEY `IDX_A0BBFDE9E4360615` (`destino_id`),
  KEY `IDX_A0BBFDE9C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_A0BBFDE9A87A7CB6` (`cobranza_id`),
  KEY `IDX_A0BBFDE963FB8380` (`pago_id`),
  KEY `IDX_A0BBFDE9E96CF38D` (`gasto_id`),
  KEY `IDX_A0BBFDE9CC04A73E` (`banco_id`),
  KEY `IDX_A0BBFDE9C5D6C4E2` (`tipocheque_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clienteproveedor`
--

DROP TABLE IF EXISTS `clienteproveedor`;
CREATE TABLE IF NOT EXISTS `clienteproveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listaprecio_id` int(11) DEFAULT NULL,
  `condicioniva_id` int(11) DEFAULT NULL,
  `tipoproveedor_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `idanterior` int(11) DEFAULT NULL,
  `razonSocial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `denominacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dni` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domiciliocomercial` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `porcentajeiva` double DEFAULT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clienteProveedor` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `gln` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `informacion_adicional` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `es_banco` tinyint(1) DEFAULT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigopostal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8231AA30FAA00F8B` (`listaprecio_id`),
  KEY `IDX_8231AA30F1A0F41C` (`condicioniva_id`),
  KEY `IDX_8231AA30A0A9C175` (`tipoproveedor_id`),
  KEY `IDX_8231AA30DB38439E` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clienteproveedorsucursal`
--

DROP TABLE IF EXISTS `clienteproveedorsucursal`;
CREATE TABLE IF NOT EXISTS `clienteproveedorsucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8B8A85F1471C747B` (`clienteproveedor_id`),
  KEY `IDX_8B8A85F1279A5D5E` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranza`
--

DROP TABLE IF EXISTS `cobranza`;
CREATE TABLE IF NOT EXISTS `cobranza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaRegistracion` datetime DEFAULT NULL,
  `importe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nrocomprobante` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AE20EF3DC7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_AE20EF3D471C747B` (`clienteproveedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranzafactura`
--

DROP TABLE IF EXISTS `cobranzafactura`;
CREATE TABLE IF NOT EXISTS `cobranzafactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cobranza_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `notacreditodebito_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BB388CFEA87A7CB6` (`cobranza_id`),
  KEY `IDX_BB388CFEF04F795F` (`factura_id`),
  KEY `IDX_BB388CFED8731886` (`notacreditodebito_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicioniva`
--

DROP TABLE IF EXISTS `condicioniva`;
CREATE TABLE IF NOT EXISTS `condicioniva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `condicioniva`
--

INSERT INTO `condicioniva` (`id`, `descripcion`, `estado`) VALUES
(1, 'Consumidor Final', 'A'),
(2, 'Responsable Inscripto', 'A'),
(3, 'Responsable Monotributo', 'A'),
(4, 'Exento', 'A'),
(5, 'No Responsable', 'A'),
(6, 'No Aclarado', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conotrolhorario`
--

DROP TABLE IF EXISTS `conotrolhorario`;
CREATE TABLE IF NOT EXISTS `conotrolhorario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `usuariocargaingreso_id` int(11) DEFAULT NULL,
  `usuariocargaegreso_id` int(11) DEFAULT NULL,
  `fechaIngreso` datetime DEFAULT NULL,
  `fechaEgreso` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_37E731BDDB38439E` (`usuario_id`),
  KEY `IDX_37E731BD3B1AB47D` (`usuariocargaingreso_id`),
  KEY `IDX_37E731BD8F192E4A` (`usuariocargaegreso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consignacion`
--

DROP TABLE IF EXISTS `consignacion`;
CREATE TABLE IF NOT EXISTS `consignacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaVencimiento` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D0D6BBE3C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_D0D6BBE3F04F795F` (`factura_id`),
  KEY `IDX_D0D6BBE3471C747B` (`clienteproveedor_id`),
  KEY `IDX_D0D6BBE3279A5D5E` (`sucursal_id`),
  KEY `IDX_D0D6BBE38361A8B8` (`vendedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

DROP TABLE IF EXISTS `cotizacion`;
CREATE TABLE IF NOT EXISTS `cotizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` double DEFAULT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentabancaria`
--

DROP TABLE IF EXISTS `cuentabancaria`;
CREATE TABLE IF NOT EXISTS `cuentabancaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banco_id` int(11) DEFAULT NULL,
  `descripcion` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nrocuenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_893324D9CC04A73E` (`banco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallepedido`
--

DROP TABLE IF EXISTS `detallepedido`;
CREATE TABLE IF NOT EXISTS `detallepedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `precio` double NOT NULL,
  `cantidad` double NOT NULL,
  `productodesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_90A2B0064854653A` (`pedido_id`),
  KEY `IDX_90A2B0067645698E` (`producto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diseno`
--

DROP TABLE IF EXISTS `diseno`;
CREATE TABLE IF NOT EXISTS `diseno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod_ref` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A760C58A7645698E` (`producto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dolar`
--

DROP TABLE IF EXISTS `dolar`;
CREATE TABLE IF NOT EXISTS `dolar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` double NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabrica`
--

DROP TABLE IF EXISTS `fabrica`;
CREATE TABLE IF NOT EXISTS `fabrica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabricaproducto`
--

DROP TABLE IF EXISTS `fabricaproducto`;
CREATE TABLE IF NOT EXISTS `fabricaproducto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `fabrica_id` int(11) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_98D3D7827645698E` (`producto_id`),
  KEY `IDX_98D3D78276A13029` (`fabrica_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

DROP TABLE IF EXISTS `factura`;
CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `fechaRegistracion` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `condicionpago` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipofactura` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cae` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechavtocae` datetime DEFAULT NULL,
  `nroremito` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nrofactura` int(11) DEFAULT NULL,
  `ptovta` int(11) DEFAULT NULL,
  `observacion` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mesanio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidademision` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bonificacion` double DEFAULT NULL,
  `percepcionib` double DEFAULT NULL,
  `percepcioniva` double DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `jurisdiccion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F9EBA009279A5D5E` (`sucursal_id`),
  KEY `IDX_F9EBA009471C747B` (`clienteproveedor_id`),
  KEY `IDX_F9EBA009C7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaimportacion`
--

DROP TABLE IF EXISTS `facturaimportacion`;
CREATE TABLE IF NOT EXISTS `facturaimportacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `importe` double DEFAULT NULL,
  `tipofactura` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cae` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechavtocae` datetime DEFAULT NULL,
  `nroremito` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nrofactura` int(11) DEFAULT NULL,
  `ptovta` int(11) DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7ED1800D279A5D5E` (`sucursal_id`),
  KEY `IDX_7ED1800D471C747B` (`clienteproveedor_id`),
  KEY `IDX_7ED1800DC7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formapago`
--

DROP TABLE IF EXISTS `formapago`;
CREATE TABLE IF NOT EXISTS `formapago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

DROP TABLE IF EXISTS `gasto`;
CREATE TABLE IF NOT EXISTS `gasto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaRegistracion` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `efectivo` double DEFAULT NULL,
  `puntoventa` int(11) DEFAULT NULL,
  `nrocomprobante` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `bonificacion` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `tipoGasto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AE43DA14C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_AE43DA14471C747B` (`clienteproveedor_id`),
  KEY `IDX_AE43DA1477AB5106` (`tipoGasto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresosbrutos`
--

DROP TABLE IF EXISTS `ingresosbrutos`;
CREATE TABLE IF NOT EXISTS `ingresosbrutos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notacreditodebito_id` int(11) DEFAULT NULL,
  `tipoingresosbrutos_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_35542342D8731886` (`notacreditodebito_id`),
  KEY `IDX_35542342CBEDE488` (`tipoingresosbrutos_id`),
  KEY `IDX_35542342E96CF38D` (`gasto_id`),
  KEY `IDX_3554234263FB8380` (`pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_nota`
--

DROP TABLE IF EXISTS `item_nota`;
CREATE TABLE IF NOT EXISTS `item_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notacreditodebito_id` int(11) NOT NULL,
  `productofactura_id` int(11) DEFAULT NULL,
  `cantidad` double NOT NULL,
  `precio` double NOT NULL,
  `descuento` double DEFAULT NULL,
  `pertenece` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E07AF6D8731886` (`notacreditodebito_id`),
  KEY `IDX_E07AF64B426D68` (`productofactura_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iva`
--

DROP TABLE IF EXISTS `iva`;
CREATE TABLE IF NOT EXISTS `iva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notacreditodebito_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `tipoiva_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `facturaimportacion_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `importeFactura` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FB97A603D8731886` (`notacreditodebito_id`),
  KEY `IDX_FB97A603F04F795F` (`factura_id`),
  KEY `IDX_FB97A6037EB5104E` (`tipoiva_id`),
  KEY `IDX_FB97A603E96CF38D` (`gasto_id`),
  KEY `IDX_FB97A603A4CE2C14` (`facturaimportacion_id`),
  KEY `IDX_FB97A60363FB8380` (`pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaprecio`
--

DROP TABLE IF EXISTS `listaprecio`;
CREATE TABLE IF NOT EXISTS `listaprecio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iva` tinyint(1) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

DROP TABLE IF EXISTS `localidad`;
CREATE TABLE IF NOT EXISTS `localidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provincia_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4F68E0104E7121AF` (`provincia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

DROP TABLE IF EXISTS `marca`;
CREATE TABLE IF NOT EXISTS `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` longblob,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medio`
--

DROP TABLE IF EXISTS `medio`;
CREATE TABLE IF NOT EXISTS `medio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `medio`
--

INSERT INTO `medio` (`id`, `descripcion`, `estado`) VALUES
(1, 'EFECTIVO', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mediodocumento`
--

DROP TABLE IF EXISTS `mediodocumento`;
CREATE TABLE IF NOT EXISTS `mediodocumento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cobranza_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `medio_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `numerolote` int(11) DEFAULT NULL,
  `numerocupon` int(11) DEFAULT NULL,
  `cuotas` int(11) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_89D975A6A87A7CB6` (`cobranza_id`),
  KEY `IDX_89D975A663FB8380` (`pago_id`),
  KEY `IDX_89D975A6E96CF38D` (`gasto_id`),
  KEY `IDX_89D975A6A40AA46` (`medio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo`
--

DROP TABLE IF EXISTS `motivo`;
CREATE TABLE IF NOT EXISTS `motivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientobancario`
--

DROP TABLE IF EXISTS `movimientobancario`;
CREATE TABLE IF NOT EXISTS `movimientobancario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cheque_id` int(11) DEFAULT NULL,
  `cuentabancaria_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `tipoMovimiento` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EC73C5763DD3DB4B` (`cheque_id`),
  KEY `IDX_EC73C576AA214BB6` (`cuentabancaria_id`),
  KEY `IDX_EC73C576C7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientocaja`
--

DROP TABLE IF EXISTS `movimientocaja`;
CREATE TABLE IF NOT EXISTS `movimientocaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caja_id` int(11) DEFAULT NULL,
  `cobranza_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `ajuste_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FA5AA07DA87A7CB6` (`cobranza_id`),
  UNIQUE KEY `UNIQ_FA5AA07D63FB8380` (`pago_id`),
  UNIQUE KEY `UNIQ_FA5AA07D4676E0C5` (`ajuste_id`),
  KEY `IDX_FA5AA07D2D82B651` (`caja_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientodeposito`
--

DROP TABLE IF EXISTS `movimientodeposito`;
CREATE TABLE IF NOT EXISTS `movimientodeposito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposito_desde` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposito_hasta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3770D991F9E584F8` (`motivo_id`),
  KEY `IDX_3770D991DCD6110` (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notacreditodebito`
--

DROP TABLE IF EXISTS `notacreditodebito`;
CREATE TABLE IF NOT EXISTS `notacreditodebito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaRegistracion` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `nrocomprobante` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiponota` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigonota` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cae` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptovta` int(11) DEFAULT NULL,
  `fechavtocae` datetime DEFAULT NULL,
  `descripcion` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_74547C77C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_74547C77471C747B` (`clienteproveedor_id`),
  KEY `IDX_74547C77279A5D5E` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota_factura`
--

DROP TABLE IF EXISTS `nota_factura`;
CREATE TABLE IF NOT EXISTS `nota_factura` (
  `nota_id` int(11) NOT NULL,
  `factura_id` int(11) NOT NULL,
  PRIMARY KEY (`nota_id`,`factura_id`),
  KEY `IDX_25A1359FA98F9F02` (`nota_id`),
  KEY `IDX_25A1359FF04F795F` (`factura_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numeracionrecibo`
--

DROP TABLE IF EXISTS `numeracionrecibo`;
CREATE TABLE IF NOT EXISTS `numeracionrecibo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `nrorecibo` int(11) NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A4A1088C7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

DROP TABLE IF EXISTS `pago`;
CREATE TABLE IF NOT EXISTS `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaRegistracion` datetime DEFAULT NULL,
  `importe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nrocomprobante` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efectivo` double DEFAULT NULL,
  `puntoventa` int(11) DEFAULT NULL,
  `descripcion` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `bonificacion` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F4DF5F3E471C747B` (`clienteproveedor_id`),
  KEY `IDX_F4DF5F3EC7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoasignacion`
--

DROP TABLE IF EXISTS `pagoasignacion`;
CREATE TABLE IF NOT EXISTS `pagoasignacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pago_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `cobranza_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `consignacion_id` int(11) DEFAULT NULL,
  `notacreditodebito_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B1BFD42763FB8380` (`pago_id`),
  KEY `IDX_B1BFD427E96CF38D` (`gasto_id`),
  KEY `IDX_B1BFD427A87A7CB6` (`cobranza_id`),
  KEY `IDX_B1BFD427F04F795F` (`factura_id`),
  KEY `IDX_B1BFD427A564AB8B` (`consignacion_id`),
  KEY `IDX_B1BFD427D8731886` (`notacreditodebito_id`),
  KEY `IDX_B1BFD427471C747B` (`clienteproveedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagofactura`
--

DROP TABLE IF EXISTS `pagofactura`;
CREATE TABLE IF NOT EXISTS `pagofactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pago_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `notacreditodebito_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FECEAD5C63FB8380` (`pago_id`),
  KEY `IDX_FECEAD5CF04F795F` (`factura_id`),
  KEY `IDX_FECEAD5CD8731886` (`notacreditodebito_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadorigen_id` int(11) DEFAULT NULL,
  `unidaddestino_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `observacion` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C4EC16CE3B715D39` (`unidadorigen_id`),
  KEY `IDX_C4EC16CE5E4E13D3` (`unidaddestino_id`),
  KEY `IDX_C4EC16CEDB38439E` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `percepcionfactura`
--

DROP TABLE IF EXISTS `percepcionfactura`;
CREATE TABLE IF NOT EXISTS `percepcionfactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factura_id` int(11) DEFAULT NULL,
  `tipopercepcion_id` int(11) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_414B1A21F04F795F` (`factura_id`),
  KEY `IDX_414B1A2167D68E9` (`tipopercepcion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio`
--

DROP TABLE IF EXISTS `precio`;
CREATE TABLE IF NOT EXISTS `precio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `lista_precio_id` int(11) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_16A9C1A27645698E` (`producto_id`),
  KEY `IDX_16A9C1A22CFB04B0` (`lista_precio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

DROP TABLE IF EXISTS `presupuesto`;
CREATE TABLE IF NOT EXISTS `presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `formapago_id` int(11) DEFAULT NULL,
  `remitooficial_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaVencimiento` datetime DEFAULT NULL,
  `validez` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paciente` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `plazoEntrega` datetime DEFAULT NULL,
  `plazo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adjunto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1B6368D3C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_1B6368D3471C747B` (`clienteproveedor_id`),
  KEY `IDX_1B6368D3279A5D5E` (`sucursal_id`),
  KEY `IDX_1B6368D38361A8B8` (`vendedor_id`),
  KEY `IDX_1B6368D3C2D0A2F4` (`formapago_id`),
  KEY `IDX_1B6368D38AD3CF3B` (`remitooficial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

DROP TABLE IF EXISTS `producto`;
CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `subcategoria_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gtin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medida` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipoiva` double DEFAULT NULL,
  `detalle` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facturable` tinyint(1) DEFAULT NULL,
  `trazable` tinyint(1) DEFAULT NULL,
  `codigoOriginal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A7BB0615CB305D73` (`proveedor_id`),
  KEY `IDX_A7BB06153397707A` (`categoria_id`),
  KEY `IDX_A7BB061588D3B71A` (`subcategoria_id`),
  KEY `IDX_A7BB0615DB38439E` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoconsignacion`
--

DROP TABLE IF EXISTS `productoconsignacion`;
CREATE TABLE IF NOT EXISTS `productoconsignacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `consignacion_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `observaciones` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paciente` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechauso` datetime DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A0B74A77645698E` (`producto_id`),
  KEY `IDX_5A0B74A7DCD6110` (`stock_id`),
  KEY `IDX_5A0B74A7A564AB8B` (`consignacion_id`),
  KEY `IDX_5A0B74A78361A8B8` (`vendedor_id`),
  KEY `IDX_5A0B74A7DE734E51` (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productofactura`
--

DROP TABLE IF EXISTS `productofactura`;
CREATE TABLE IF NOT EXISTS `productofactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `renglon` int(11) DEFAULT NULL,
  `cantidad` double NOT NULL,
  `precio` double NOT NULL,
  `descuento` double DEFAULT NULL,
  `pertenece` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ADFFE8C4DCD6110` (`stock_id`),
  KEY `IDX_ADFFE8C4F04F795F` (`factura_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productopedido`
--

DROP TABLE IF EXISTS `productopedido`;
CREATE TABLE IF NOT EXISTS `productopedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EF3F238DCD6110` (`stock_id`),
  KEY `IDX_EF3F2384854653A` (`pedido_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productopresupuesto`
--

DROP TABLE IF EXISTS `productopresupuesto`;
CREATE TABLE IF NOT EXISTS `productopresupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `presupuesto_id` int(11) DEFAULT NULL,
  `renglon` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A365C0C87645698E` (`producto_id`),
  KEY `IDX_A365C0C890119F0F` (`presupuesto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoremito`
--

DROP TABLE IF EXISTS `productoremito`;
CREATE TABLE IF NOT EXISTS `productoremito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `productoconsignacion_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `consignacion_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `renglon` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_74F10EE87645698E` (`producto_id`),
  KEY `IDX_74F10EE866F7EBF5` (`productoconsignacion_id`),
  KEY `IDX_74F10EE8DCD6110` (`stock_id`),
  KEY `IDX_74F10EE8A564AB8B` (`consignacion_id`),
  KEY `IDX_74F10EE8DE734E51` (`cliente_id`),
  KEY `IDX_74F10EE8DB38439E` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

DROP TABLE IF EXISTS `provincia`;
CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `remitooficial`
--

DROP TABLE IF EXISTS `remitooficial`;
CREATE TABLE IF NOT EXISTS `remitooficial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_161D5ABEC7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_161D5ABEF04F795F` (`factura_id`),
  KEY `IDX_161D5ABE471C747B` (`clienteproveedor_id`),
  KEY `IDX_161D5ABE279A5D5E` (`sucursal_id`),
  KEY `IDX_161D5ABE8361A8B8` (`vendedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_188D2E3BDCD6110` (`stock_id`),
  KEY `IDX_188D2E3BA76ED395` (`user_id`),
  KEY `IDX_188D2E3B471C747B` (`clienteproveedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retencion`
--

DROP TABLE IF EXISTS `retencion`;
CREATE TABLE IF NOT EXISTS `retencion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiporetencion_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `gasto_id` int(11) DEFAULT NULL,
  `cobranza_id` int(11) DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL,
  `facturaimportacion_id` int(11) DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `observacion` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E0B161AA19B7589C` (`tiporetencion_id`),
  KEY `IDX_E0B161AAF04F795F` (`factura_id`),
  KEY `IDX_E0B161AAE96CF38D` (`gasto_id`),
  KEY `IDX_E0B161AAA87A7CB6` (`cobranza_id`),
  KEY `IDX_E0B161AA63FB8380` (`pago_id`),
  KEY `IDX_E0B161AAA4CE2C14` (`facturaimportacion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubro`
--

DROP TABLE IF EXISTS `rubro`;
CREATE TABLE IF NOT EXISTS `rubro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

DROP TABLE IF EXISTS `stock`;
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `cotizacion_id` int(11) DEFAULT NULL,
  `realizo_id` int(11) DEFAULT NULL,
  `controlo_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `vencimiento` datetime DEFAULT NULL,
  `lote` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `despacho` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposito` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `procedencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etiqueta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carga` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `higiene` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4B3656607645698E` (`producto_id`),
  KEY `IDX_4B365660C7FF8B89` (`unidadnegocio_id`),
  KEY `IDX_4B365660307090AA` (`cotizacion_id`),
  KEY `IDX_4B36566031492207` (`realizo_id`),
  KEY `IDX_4B3656601A34C6C2` (`controlo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stockentregado`
--

DROP TABLE IF EXISTS `stockentregado`;
CREATE TABLE IF NOT EXISTS `stockentregado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `stockpedido_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `fechaEntrega` datetime DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68FD6FE64854653A` (`pedido_id`),
  KEY `IDX_68FD6FE629527418` (`stockpedido_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stockpedido`
--

DROP TABLE IF EXISTS `stockpedido`;
CREATE TABLE IF NOT EXISTS `stockpedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D2C4E6A3DCD6110` (`stock_id`),
  KEY `IDX_D2C4E6A34854653A` (`pedido_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

DROP TABLE IF EXISTS `subcategoria`;
CREATE TABLE IF NOT EXISTS `subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DA7FB9143397707A` (`categoria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE IF NOT EXISTS `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clienteproveedor_id` int(11) DEFAULT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigopostal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E99C6D56471C747B` (`clienteproveedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocheque`
--

DROP TABLE IF EXISTS `tipocheque`;
CREATE TABLE IF NOT EXISTS `tipocheque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipogasto`
--

DROP TABLE IF EXISTS `tipogasto`;
CREATE TABLE IF NOT EXISTS `tipogasto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoingresosbrutos`
--

DROP TABLE IF EXISTS `tipoingresosbrutos`;
CREATE TABLE IF NOT EXISTS `tipoingresosbrutos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoiva`
--

DROP TABLE IF EXISTS `tipoiva`;
CREATE TABLE IF NOT EXISTS `tipoiva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopercepcion`
--

DROP TABLE IF EXISTS `tipopercepcion`;
CREATE TABLE IF NOT EXISTS `tipopercepcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoproveedor`
--

DROP TABLE IF EXISTS `tipoproveedor`;
CREATE TABLE IF NOT EXISTS `tipoproveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporetencion`
--

DROP TABLE IF EXISTS `tiporetencion`;
CREATE TABLE IF NOT EXISTS `tiporetencion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `importe` double DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadnegocio`
--

DROP TABLE IF EXISTS `unidadnegocio`;
CREATE TABLE IF NOT EXISTS `unidadnegocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listaprecio_id` int(11) DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `responsable` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `punto` int(11) DEFAULT NULL,
  `cuit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iibb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6DA43D15FAA00F8B` (`listaprecio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `unidadnegocio`
--

INSERT INTO `unidadnegocio` (`id`, `listaprecio_id`, `descripcion`, `responsable`, `direccion`, `telefono`, `celular`, `punto`, `cuit`, `iibb`, `mail`, `estado`) VALUES
(1, NULL, 'KLOSEWICZ CLAUDIO A Y KLOSEWICZ GABRIEL G SOC DE HECHO N T T', 'Gabriel y Claudio', 'Camargo 775 - Ciudad de Buenos Aires', '-', '-', 2, '30688437578', '901 30688437578', NULL, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadnegocio_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `apellidonombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sector` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2265B05D92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_2265B05DA0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_2265B05DC05FB297` (`confirmation_token`),
  KEY `IDX_2265B05DC7FF8B89` (`unidadnegocio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `unidadnegocio_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `apellidonombre`, `sector`, `estado`) VALUES
(1, NULL, 'admin', 'admin', 'corvalan4@gmail.com', 'corvalan4@gmail.com', 1, NULL, '$2y$13$QqokFbcv4wk/UL.p5U/DM./DugCqh87r8nKdwa1uOAC5BZSIgS6JG', '2020-11-25 02:58:23', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 'Corvalan Wechsler Nicolas', 'ADMINISTRACION', 'A');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `balance`
--
ALTER TABLE `balance`
  ADD CONSTRAINT `FK_ACF41FFE8AD3CF3B` FOREIGN KEY (`remitooficial_id`) REFERENCES `remitooficial` (`id`),
  ADD CONSTRAINT `FK_ACF41FFEA564AB8B` FOREIGN KEY (`consignacion_id`) REFERENCES `consignacion` (`id`),
  ADD CONSTRAINT `FK_ACF41FFEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_ACF41FFED3F32E95` FOREIGN KEY (`notacredito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_ACF41FFEDCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`),
  ADD CONSTRAINT `FK_ACF41FFEF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `caja`
--
ALTER TABLE `caja`
  ADD CONSTRAINT `FK_E465F405C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `cheque`
--
ALTER TABLE `cheque`
  ADD CONSTRAINT `FK_A0BBFDE963FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE993529ECD` FOREIGN KEY (`origen_id`) REFERENCES `cuentabancaria` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9A87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9C5D6C4E2` FOREIGN KEY (`tipocheque_id`) REFERENCES `tipocheque` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9CC04A73E` FOREIGN KEY (`banco_id`) REFERENCES `banco` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9E4360615` FOREIGN KEY (`destino_id`) REFERENCES `cuentabancaria` (`id`),
  ADD CONSTRAINT `FK_A0BBFDE9E96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`);

--
-- Filtros para la tabla `clienteproveedor`
--
ALTER TABLE `clienteproveedor`
  ADD CONSTRAINT `FK_8231AA30A0A9C175` FOREIGN KEY (`tipoproveedor_id`) REFERENCES `tipoproveedor` (`id`),
  ADD CONSTRAINT `FK_8231AA30DB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_8231AA30F1A0F41C` FOREIGN KEY (`condicioniva_id`) REFERENCES `condicioniva` (`id`),
  ADD CONSTRAINT `FK_8231AA30FAA00F8B` FOREIGN KEY (`listaprecio_id`) REFERENCES `listaprecio` (`id`);

--
-- Filtros para la tabla `clienteproveedorsucursal`
--
ALTER TABLE `clienteproveedorsucursal`
  ADD CONSTRAINT `FK_8B8A85F1279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_8B8A85F1471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`);

--
-- Filtros para la tabla `cobranza`
--
ALTER TABLE `cobranza`
  ADD CONSTRAINT `FK_AE20EF3D471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_AE20EF3DC7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `cobranzafactura`
--
ALTER TABLE `cobranzafactura`
  ADD CONSTRAINT `FK_BB388CFEA87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`),
  ADD CONSTRAINT `FK_BB388CFED8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_BB388CFEF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `conotrolhorario`
--
ALTER TABLE `conotrolhorario`
  ADD CONSTRAINT `FK_37E731BD3B1AB47D` FOREIGN KEY (`usuariocargaingreso_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_37E731BD8F192E4A` FOREIGN KEY (`usuariocargaegreso_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_37E731BDDB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `consignacion`
--
ALTER TABLE `consignacion`
  ADD CONSTRAINT `FK_D0D6BBE3279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_D0D6BBE3471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_D0D6BBE38361A8B8` FOREIGN KEY (`vendedor_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_D0D6BBE3C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`),
  ADD CONSTRAINT `FK_D0D6BBE3F04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `cuentabancaria`
--
ALTER TABLE `cuentabancaria`
  ADD CONSTRAINT `FK_893324D9CC04A73E` FOREIGN KEY (`banco_id`) REFERENCES `banco` (`id`);

--
-- Filtros para la tabla `detallepedido`
--
ALTER TABLE `detallepedido`
  ADD CONSTRAINT `FK_90A2B0064854653A` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
  ADD CONSTRAINT `FK_90A2B0067645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `diseno`
--
ALTER TABLE `diseno`
  ADD CONSTRAINT `FK_A760C58A7645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `fabricaproducto`
--
ALTER TABLE `fabricaproducto`
  ADD CONSTRAINT `FK_98D3D7827645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK_98D3D78276A13029` FOREIGN KEY (`fabrica_id`) REFERENCES `fabrica` (`id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `FK_F9EBA009279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_F9EBA009471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_F9EBA009C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `facturaimportacion`
--
ALTER TABLE `facturaimportacion`
  ADD CONSTRAINT `FK_7ED1800D279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_7ED1800D471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_7ED1800DC7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD CONSTRAINT `FK_AE43DA14471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_AE43DA1477AB5106` FOREIGN KEY (`tipoGasto_id`) REFERENCES `tipogasto` (`id`),
  ADD CONSTRAINT `FK_AE43DA14C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `ingresosbrutos`
--
ALTER TABLE `ingresosbrutos`
  ADD CONSTRAINT `FK_3554234263FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_35542342CBEDE488` FOREIGN KEY (`tipoingresosbrutos_id`) REFERENCES `tipoingresosbrutos` (`id`),
  ADD CONSTRAINT `FK_35542342D8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_35542342E96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`);

--
-- Filtros para la tabla `item_nota`
--
ALTER TABLE `item_nota`
  ADD CONSTRAINT `FK_E07AF64B426D68` FOREIGN KEY (`productofactura_id`) REFERENCES `productofactura` (`id`),
  ADD CONSTRAINT `FK_E07AF6D8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`);

--
-- Filtros para la tabla `iva`
--
ALTER TABLE `iva`
  ADD CONSTRAINT `FK_FB97A60363FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_FB97A6037EB5104E` FOREIGN KEY (`tipoiva_id`) REFERENCES `tipoiva` (`id`),
  ADD CONSTRAINT `FK_FB97A603A4CE2C14` FOREIGN KEY (`facturaimportacion_id`) REFERENCES `facturaimportacion` (`id`),
  ADD CONSTRAINT `FK_FB97A603D8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_FB97A603E96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`),
  ADD CONSTRAINT `FK_FB97A603F04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD CONSTRAINT `FK_4F68E0104E7121AF` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `mediodocumento`
--
ALTER TABLE `mediodocumento`
  ADD CONSTRAINT `FK_89D975A663FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_89D975A6A40AA46` FOREIGN KEY (`medio_id`) REFERENCES `medio` (`id`),
  ADD CONSTRAINT `FK_89D975A6A87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`),
  ADD CONSTRAINT `FK_89D975A6E96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`);

--
-- Filtros para la tabla `movimientobancario`
--
ALTER TABLE `movimientobancario`
  ADD CONSTRAINT `FK_EC73C5763DD3DB4B` FOREIGN KEY (`cheque_id`) REFERENCES `cheque` (`id`),
  ADD CONSTRAINT `FK_EC73C576AA214BB6` FOREIGN KEY (`cuentabancaria_id`) REFERENCES `cuentabancaria` (`id`),
  ADD CONSTRAINT `FK_EC73C576C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `movimientocaja`
--
ALTER TABLE `movimientocaja`
  ADD CONSTRAINT `FK_FA5AA07D2D82B651` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `FK_FA5AA07D4676E0C5` FOREIGN KEY (`ajuste_id`) REFERENCES `ajuste` (`id`),
  ADD CONSTRAINT `FK_FA5AA07D63FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_FA5AA07DA87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`);

--
-- Filtros para la tabla `movimientodeposito`
--
ALTER TABLE `movimientodeposito`
  ADD CONSTRAINT `FK_3770D991DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`),
  ADD CONSTRAINT `FK_3770D991F9E584F8` FOREIGN KEY (`motivo_id`) REFERENCES `motivo` (`id`);

--
-- Filtros para la tabla `notacreditodebito`
--
ALTER TABLE `notacreditodebito`
  ADD CONSTRAINT `FK_74547C77279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_74547C77471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_74547C77C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `nota_factura`
--
ALTER TABLE `nota_factura`
  ADD CONSTRAINT `FK_25A1359FA98F9F02` FOREIGN KEY (`nota_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_25A1359FF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `numeracionrecibo`
--
ALTER TABLE `numeracionrecibo`
  ADD CONSTRAINT `FK_5A4A1088C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `FK_F4DF5F3E471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_F4DF5F3EC7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `pagoasignacion`
--
ALTER TABLE `pagoasignacion`
  ADD CONSTRAINT `FK_B1BFD427471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_B1BFD42763FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_B1BFD427A564AB8B` FOREIGN KEY (`consignacion_id`) REFERENCES `consignacion` (`id`),
  ADD CONSTRAINT `FK_B1BFD427A87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`),
  ADD CONSTRAINT `FK_B1BFD427D8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_B1BFD427E96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`),
  ADD CONSTRAINT `FK_B1BFD427F04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `pagofactura`
--
ALTER TABLE `pagofactura`
  ADD CONSTRAINT `FK_FECEAD5C63FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_FECEAD5CD8731886` FOREIGN KEY (`notacreditodebito_id`) REFERENCES `notacreditodebito` (`id`),
  ADD CONSTRAINT `FK_FECEAD5CF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `FK_C4EC16CE3B715D39` FOREIGN KEY (`unidadorigen_id`) REFERENCES `unidadnegocio` (`id`),
  ADD CONSTRAINT `FK_C4EC16CE5E4E13D3` FOREIGN KEY (`unidaddestino_id`) REFERENCES `unidadnegocio` (`id`),
  ADD CONSTRAINT `FK_C4EC16CEDB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `percepcionfactura`
--
ALTER TABLE `percepcionfactura`
  ADD CONSTRAINT `FK_414B1A2167D68E9` FOREIGN KEY (`tipopercepcion_id`) REFERENCES `tipopercepcion` (`id`),
  ADD CONSTRAINT `FK_414B1A21F04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `precio`
--
ALTER TABLE `precio`
  ADD CONSTRAINT `FK_16A9C1A22CFB04B0` FOREIGN KEY (`lista_precio_id`) REFERENCES `listaprecio` (`id`),
  ADD CONSTRAINT `FK_16A9C1A27645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `FK_1B6368D3279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_1B6368D3471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_1B6368D38361A8B8` FOREIGN KEY (`vendedor_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_1B6368D38AD3CF3B` FOREIGN KEY (`remitooficial_id`) REFERENCES `remitooficial` (`id`),
  ADD CONSTRAINT `FK_1B6368D3C2D0A2F4` FOREIGN KEY (`formapago_id`) REFERENCES `formapago` (`id`),
  ADD CONSTRAINT `FK_1B6368D3C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `FK_A7BB06153397707A` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `FK_A7BB061588D3B71A` FOREIGN KEY (`subcategoria_id`) REFERENCES `subcategoria` (`id`),
  ADD CONSTRAINT `FK_A7BB0615CB305D73` FOREIGN KEY (`proveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_A7BB0615DB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `productoconsignacion`
--
ALTER TABLE `productoconsignacion`
  ADD CONSTRAINT `FK_5A0B74A77645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK_5A0B74A78361A8B8` FOREIGN KEY (`vendedor_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_5A0B74A7A564AB8B` FOREIGN KEY (`consignacion_id`) REFERENCES `consignacion` (`id`),
  ADD CONSTRAINT `FK_5A0B74A7DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`),
  ADD CONSTRAINT `FK_5A0B74A7DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `clienteproveedor` (`id`);

--
-- Filtros para la tabla `productofactura`
--
ALTER TABLE `productofactura`
  ADD CONSTRAINT `FK_ADFFE8C4DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`),
  ADD CONSTRAINT `FK_ADFFE8C4F04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `productopedido`
--
ALTER TABLE `productopedido`
  ADD CONSTRAINT `FK_EF3F2384854653A` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
  ADD CONSTRAINT `FK_EF3F238DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`);

--
-- Filtros para la tabla `productopresupuesto`
--
ALTER TABLE `productopresupuesto`
  ADD CONSTRAINT `FK_A365C0C87645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK_A365C0C890119F0F` FOREIGN KEY (`presupuesto_id`) REFERENCES `presupuesto` (`id`);

--
-- Filtros para la tabla `productoremito`
--
ALTER TABLE `productoremito`
  ADD CONSTRAINT `FK_74F10EE866F7EBF5` FOREIGN KEY (`productoconsignacion_id`) REFERENCES `productoconsignacion` (`id`),
  ADD CONSTRAINT `FK_74F10EE87645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK_74F10EE8A564AB8B` FOREIGN KEY (`consignacion_id`) REFERENCES `remitooficial` (`id`),
  ADD CONSTRAINT `FK_74F10EE8DB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_74F10EE8DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`),
  ADD CONSTRAINT `FK_74F10EE8DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `clienteproveedor` (`id`);

--
-- Filtros para la tabla `remitooficial`
--
ALTER TABLE `remitooficial`
  ADD CONSTRAINT `FK_161D5ABE279A5D5E` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  ADD CONSTRAINT `FK_161D5ABE471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_161D5ABE8361A8B8` FOREIGN KEY (`vendedor_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_161D5ABEC7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`),
  ADD CONSTRAINT `FK_161D5ABEF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `FK_188D2E3B471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`),
  ADD CONSTRAINT `FK_188D2E3BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_188D2E3BDCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`);

--
-- Filtros para la tabla `retencion`
--
ALTER TABLE `retencion`
  ADD CONSTRAINT `FK_E0B161AA19B7589C` FOREIGN KEY (`tiporetencion_id`) REFERENCES `tiporetencion` (`id`),
  ADD CONSTRAINT `FK_E0B161AA63FB8380` FOREIGN KEY (`pago_id`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `FK_E0B161AAA4CE2C14` FOREIGN KEY (`facturaimportacion_id`) REFERENCES `facturaimportacion` (`id`),
  ADD CONSTRAINT `FK_E0B161AAA87A7CB6` FOREIGN KEY (`cobranza_id`) REFERENCES `cobranza` (`id`),
  ADD CONSTRAINT `FK_E0B161AAE96CF38D` FOREIGN KEY (`gasto_id`) REFERENCES `gasto` (`id`),
  ADD CONSTRAINT `FK_E0B161AAF04F795F` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `FK_4B3656601A34C6C2` FOREIGN KEY (`controlo_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_4B365660307090AA` FOREIGN KEY (`cotizacion_id`) REFERENCES `cotizacion` (`id`),
  ADD CONSTRAINT `FK_4B36566031492207` FOREIGN KEY (`realizo_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_4B3656607645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FK_4B365660C7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);

--
-- Filtros para la tabla `stockentregado`
--
ALTER TABLE `stockentregado`
  ADD CONSTRAINT `FK_68FD6FE629527418` FOREIGN KEY (`stockpedido_id`) REFERENCES `stockpedido` (`id`),
  ADD CONSTRAINT `FK_68FD6FE64854653A` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`);

--
-- Filtros para la tabla `stockpedido`
--
ALTER TABLE `stockpedido`
  ADD CONSTRAINT `FK_D2C4E6A34854653A` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
  ADD CONSTRAINT `FK_D2C4E6A3DCD6110` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`);

--
-- Filtros para la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `FK_DA7FB9143397707A` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`);

--
-- Filtros para la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD CONSTRAINT `FK_E99C6D56471C747B` FOREIGN KEY (`clienteproveedor_id`) REFERENCES `clienteproveedor` (`id`);

--
-- Filtros para la tabla `unidadnegocio`
--
ALTER TABLE `unidadnegocio`
  ADD CONSTRAINT `FK_6DA43D15FAA00F8B` FOREIGN KEY (`listaprecio_id`) REFERENCES `listaprecio` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_2265B05DC7FF8B89` FOREIGN KEY (`unidadnegocio_id`) REFERENCES `unidadnegocio` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
