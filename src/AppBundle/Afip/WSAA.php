<?php
namespace AppBundle\Afip;
use AppBundle\Afip\Exceptionhandler;
# Autor: Nicolas Corvalan Wechsler

class WSAA {
    const TA   = "/xmlgenerados/TA.xml";      # Archivo con el Token y Sign
    const PASSPHRASE = "";         		    # The passphrase (if any) to sign
    const PROXY_ENABLE = false;

# TESTING
    private $WSDL = "/wsaa.wsdl";              # The WSDL corresponding to WSAA PROD
    private $URL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"; // testing
    private $CERT = "//keys//cwcert.crt";          # The X.509 certificate in PEM format
    private $PRIVATEKEY = "//keys//cwkey.key";   # The private key correspoding to CERT (PEM)
# PRODUCCION
    private $WSDL_PROD = "/wsaaprod.wsdl";                      	# The WSDL corresponding to WSAA PROD
    private $URL_PROD = "https://wsaa.afip.gov.ar/ws/services/LoginCms"; // produccion
    private $CERT_PROD = "//keys//AFIP-Certificados-2020//cw-facturacion-prod.crt";           		# The X.509 certificate in PEM format
    private $PRIVATEKEY_PROD = "//keys//AFIP-Certificados-2020//cw-facturacion-prod.key";         	# La clave privada

    /*
     * el path relativo, terminado en /
     */
    private $path = './';

    /*
     * manejo de errores
     */
    public $error = '';

    /**
     * Cliente SOAP
     */
    private $client;

    /*
     * servicio del cual queremos obtener la autorizacion
     */
    private $service;

    private $producccion;

    /*
     * Constructor
     */
    public function __construct($produccion = false, $service = 'wsfe')
    {
        $this->producccion = $produccion;
        if(!is_dir(dirname(__FILE__).'/xmlgenerados')){
            mkdir(dirname(__FILE__).'/xmlgenerados', 777);
        }

        if($this->producccion){
            $this->WSDL = $this->WSDL_PROD;
            $this->PRIVATEKEY = $this->PRIVATEKEY_PROD;
            $this->URL = $this->URL_PROD;
            $this->CERT = $this->CERT_PROD;
        }

        $this->path = dirname(__FILE__);
        $this->service = $service;

        // seteos en php
        ini_set("soap.wsdl_cache_enabled", "0");

        // validar archivos necesarios
        if (!file_exists(dirname(__FILE__).$this->CERT)) $this->error .= " Failed to open ".$this->CERT;
        if (!file_exists(dirname(__FILE__).$this->PRIVATEKEY)) $this->error .= " Failed to open ".$this->PRIVATEKEY;
        if (!file_exists(dirname(__FILE__).$this->WSDL)) $this->error .= " Failed to open ".$this->WSDL;

        if(!empty($this->error)) {
            throw new \Exception('WSAA class. Faltan archivos necesarios para el funcionamiento'. $this->error);
        }

        $this->client = new \SoapClient(dirname(__FILE__).$this->WSDL, array(
                'soap_version'   => SOAP_1_2,
                'location'       => $this->URL,
                'trace'          => 1,
                'exceptions'     => 0
            )
        );
    }

    /**
     * Crea el archivo xml de TRA
     */
    private function create_TRA()
    {
        if(!file_exists(dirname(__FILE__).'/xmlgenerados/TRA.xml')){
            $tra_file = fopen(dirname(__FILE__).'/xmlgenerados/TRA.xml', "w") or die("Unable to open file!");
            fwrite($tra_file, '');
            fclose($tra_file);
        }

        $TRA = new \SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?>' .
            '<loginTicketRequest version="1.0">'.
            '</loginTicketRequest>');
        $TRA->addChild('header');
        $TRA->header->addChild('uniqueId', date('U'));
        $TRA->header->addChild('generationTime', date('c',date('U')-120));
        $TRA->header->addChild('expirationTime', date('c',date('U')+120));
        $TRA->addChild('service', $this->service);
        $TRA->asXML(dirname(__FILE__).'/xmlgenerados/TRA.xml');
    }

    /*
     * This functions makes the PKCS#7 signature using TRA as input file, CERT and
     * PRIVATEKEY to sign. Generates an intermediate file and finally trims the
     * MIME heading leaving the final CMS required by WSAA.
     *
     * devuelve el CMS
     */
    private function sign_TRA()
    {
        if(!file_exists(dirname(__FILE__)."/xmlgenerados/TRA.xml")){
            $tra_file = fopen(dirname(__FILE__).'/xmlgenerados/TRA.xml', "w") or die("Unable to open file!");
            fwrite($tra_file, '');
            fclose($tra_file);
        }

        if(!file_exists(dirname(__FILE__)."/xmlgenerados/TRA.tmp")){
            $tra_file_tmep = fopen(dirname(__FILE__).'/xmlgenerados/TRA.tmp', "w") or die("Unable to open file!");
            fwrite($tra_file_tmep, '');
            fclose($tra_file_tmep);
        }

        $STATUS = openssl_pkcs7_sign(dirname(__FILE__)."/xmlgenerados/TRA.xml", dirname(__FILE__)."/xmlgenerados/TRA.tmp", "file://".dirname(__FILE__).$this->CERT,
            array('file://'.dirname(__FILE__).$this->PRIVATEKEY, self::PASSPHRASE),
            array(),
            !PKCS7_DETACHED
        );

        if (!$STATUS)
            throw new \Exception("ERROR generating PKCS#7 signature");

        $inf = fopen(dirname(__FILE__)."/xmlgenerados/TRA.tmp", "r");
        $i = 0;
        $CMS = "";
        while (!feof($inf)) {
            $buffer = fgets($inf);
            if ( $i++ >= 4 ) $CMS .= $buffer;
        }

        fclose($inf);

        return $CMS;
    }

    /**
     * Conecta con el web service y obtiene el token y sign
     */
    private function call_WSAA($cms)
    {
        $results = $this->client->loginCms(array('in0' => $cms));

        if (is_soap_fault($results))
            throw new \Exception("SOAP Fault: ".$results->faultcode.': '.$results->faultstring);

        $ta_xml = simplexml_load_string($results->loginCmsReturn);
        $TOKEN = $ta_xml->credentials->token;
        $SIGN = $ta_xml->credentials->sign;

        // para logueo
        file_put_contents(dirname(__FILE__)."/request-loginCms.xml", $this->client->__getLastRequest());
        file_put_contents(dirname(__FILE__)."/response-loginCms.xml", $this->client->__getLastResponse());

        return $results->loginCmsReturn;
    }

    /*
     * Convertir un XML a Array
     */
    private function xml2array($xml) {
        $json = json_encode( simplexml_load_string($xml));
        return json_decode($json, TRUE);
    }

    /**
     * funcion principal que llama a las demas para generar el archivo TA.xml
     * que contiene el token y sign
     */
    public function generar_TA()
    {
        $this->create_TRA();
        $TA = $this->call_WSAA( $this->sign_TRA() );
        if(!file_exists(dirname(__FILE__).self::TA)){
            $tra_file = fopen(dirname(__FILE__).self::TA, "w") or die("Unable to open file!");
            fwrite($tra_file, '');
            fclose($tra_file);
        }
        if (!file_put_contents(dirname(__FILE__).self::TA, $TA))
            throw new \Exception("Error al generar al archivo TA.xml");

        $this->TA = $this->xml2Array($TA);

        return true;
    }

    /**
     * Obtener la fecha de expiracion del TA
     * si no existe el archivo, devuelve false
     */
    public function get_expiration()
    {
        if(empty($this->TA)) {
            if(!file_exists(dirname(__FILE__).self::TA)){
                $ta_file = fopen(dirname(__FILE__).self::TA, "w") or die("Unable to open file!");
                fwrite($ta_file, '');
                fclose($ta_file);
            }
            $TA_file = file(dirname(__FILE__).self::TA, FILE_IGNORE_NEW_LINES);

            if($TA_file) {
                $TA_xml = '';
                for($i=0; $i < sizeof($TA_file); $i++)
                    $TA_xml.= $TA_file[$i];
                $this->TA = $this->xml2Array($TA_xml);
                $r = $this->TA['header']['expirationTime'];
            } else {
                $r = null;
            }
        } else {
            $r = $this->TA['header']['expirationTime'];
        }

        return !empty($r) ? new \DateTime($r) : $r;
    }

}

?>
