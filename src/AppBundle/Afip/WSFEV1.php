<?php
namespace AppBundle\Afip;
use AppBundle\Afip\Exceptionhandler;
# Autor: Nicolas Corvalan Wechsler

class WSFEV1{
    const TA   = "/xmlgenerados/TA.xml";          # Archivo con el Token y Sign
    const PASSPHRASE = "";                        # The passphrase (if any) to sign
    const PROXY_ENABLE = false;
    const LOG_XMLS = false;

#TESTING
    private $WSDL = "/wsfev1test.wsdl";                # WSDL TESTING
    private $WSFEURL = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx"; // testing

#PRODUCCION
    private $WSDL_PROD = "/wsfev1prod.wsdl";                # WSDL PRODUCCION
    private $WSFEURL_PROD = "https://servicios1.afip.gov.ar/wsfev1/service.asmx"; // PRODUCCION

    /*
     * el path relativo, terminado en /
     */
    private $path = './';

    /*
     * manejo de errores
     */
    public $error = '';

    /**
     * Cliente SOAP
     */
    private $client;

    /**
     * objeto que va a contener el xml de TA
     */
    private $TA;

    private $producccion;

    /*
     * Constructor
     */
    public function __construct($produccion = false)
    {
        $this->producccion = $produccion;

        if($this->producccion){
            $this->WSDL = $this->WSDL_PROD;
            $this->WSFEURL = $this->WSFEURL_PROD;
        }
        $this->path = dirname(__FILE__);

        // seteos en php
        ini_set("soap.wsdl_cache_enabled", "0");

        // validar archivos necesarios
        if (!file_exists($this->path.$this->WSDL)) $this->error .= " Failed to open ".$this->WSDL;

        if(!empty($this->error)) {
            throw new \Exception('WSFEv1 class. Faltan archivos para el funcionamiento de la clase');
        }

        $this->client = new \SoapClient($this->path.$this->WSDL, array(
                'soap_version' => SOAP_1_2,
                'location'     => $this->WSFEURL,
                'exceptions'   => 0,
                'trace'        => 1)
        );
    }

    /**
     * Chequea los errores en la operacion, si encuentra algun error falta lanza una exepcion
     * si encuentra un error no fatal, loguea lo que paso en $this->error
     */
    private function _checkErrors($results, $method)
    {
        if (self::LOG_XMLS) {
            file_put_contents("xmlgenerados/request-".$method.".xml",$this->client->__getLastRequest());
            file_put_contents("xmlgenerados/response-".$method.".xml",$this->client->__getLastResponse());
        }

        if (is_soap_fault($results)) {
            throw new \Exception('WSFE class. FaultString: ' . $results->faultcode.' '.$results->faultstring);
        }

        if ($method == 'FEDummy') {return;}

        $XXX=$method.'Result';
        if ($results->$XXX->RError->percode != 0) {
            $this->error = "Method=$method errcode=".$results->$XXX->RError->percode." errmsg=".$results->$XXX->RError->perrmsg;
        }

        return $results->$XXX->RError->percode != 0 ? true : false;
    }

    /**
     * Abre el archivo de TA xml,
     * si hay algun problema devuelve false
     */
    public function openTA()
    {
        $this->TA = simplexml_load_file($this->path.self::TA);

        return $this->TA == false ? false : true;
    }

    /**
     * Retorna la cantidad maxima de registros de detalle que
     * puede tener una invocacion al FEAutorizarRequest
     */
    public function recuperaQTY()
    {
        $results = $this->client->FERecuperaQTYRequest(
            array('argAuth'=>array('Token' => $this->TA->credentials->token,
                'Sign' => $this->TA->credentials->sign,
                'cuit' => self::CUIT)));

        $e = $this->_checkErrors($results, 'FERecuperaQTYRequest');

        return $e == false ? $results->FERecuperaQTYRequestResult->qty->value : false;
    }

    /*
     * Retorna el Ticket de Acceso.
     */
    public function getTA()
    {
        return $this->TA;
    }

    /*
     * Retorna el ultimo número de Request.
     */
    public function FECompUltimoAutorizado($tipo_cbte,$punto_vta, $cuit)
    {
        //Castea el cuit para ser aceptado en el Request (Pide LONG)
        $cuit = (float)$cuit;

        $results = $this->client->FECompUltimoAutorizado(
            array('Auth' =>  array('Token'    => $this->TA->credentials->token,
                'Sign'     => $this->TA->credentials->sign,
                'Cuit'     => $cuit),
                'PtoVta' => $punto_vta,
                'CbteTipo' => $tipo_cbte));

        return $results;
    }

    /*
     * Retorna el ultimo comprobante autorizado para el tipo de comprobante /cuit / punto de venta ingresado.
     */
    public function recuperaLastCMP ($ptovta, $cuit)
    {
        $results = $this->client->FERecuperaLastCMPRequest(
            array('argAuth' =>  array('Token'    => $this->TA->credentials->token,
                'Sign'     => $this->TA->credentials->sign,
                'cuit'     => $cuit),
                'argTCMP' => array('PtoVta'   => $ptovta,
                    'TipoCbte' => $this->tipo_cbte)));

        $e = $this->_checkErrors($results, 'FERecuperaLastCMPRequest');

        return $e == false ? $results->FERecuperaLastCMPRequestResult->cbte_nro : false;
    }

    /**
     * Setea el tipo de comprobante
     * A = 1
     * B = 6
     */
    public function setTipoCbte($tipo)
    {
        switch($tipo) {
            case 'a': case 'A': case '1':
            $this->tipo_cbte = 1;
            break;

            case 'b': case 'B': case 'c': case 'C': case '6':
            $this->tipo_cbte = 6;
            break;

            default:
                return false;
        }
        return true;
    }

    public function armadoFacturaUnica($tipofactura, $puntoventa, $nc = '', $datosfactura, $cuit, $CbteTipo) {
        $FeCabReq = array('CantReg' => 1, 'PtoVta' => $puntoventa, 'CbteTipo' => $CbteTipo);
        $tipodoc = $datosfactura['tipodocumento'];
        if ($tipodoc == 96) {
            $nrodoc = (float)$datosfactura['numerodocumento'];
        } elseif ($tipodoc == 80) {
            $nrodoc = (float)$datosfactura['cuit'];
        } else {
            $tipodoc = 99;
            if ($datosfactura['numerodocumento'] != 0) {
                $nrodoc = (float)$datosfactura['numerodocumento'];
                $tipodoc = 96;
            } elseif ($tipofactura == 'B' and $datosfactura['importetotal'] < 10000) {
                $nrodoc = 0;
            } elseif ($tipofactura == 'B' and $datosfactura['importetotal'] > 10000 and $datosfactura['cuit'] != '') {
                $nrodoc = (float)str_replace('-', '', $datosfactura['cuit']);
                $tipodoc = 80;
            } else {
                $nrodoc = str_replace('-', '', $datosfactura['cuit']);
                if ($tipofactura == 'A')
                    $tipodoc = 80;
            }
        }

        $neto = $datosfactura['importeneto'];

        /*if ($datosfactura['importeneto'] != '0') {
            $baseimp = round($datosfactura['importeneto'], 2);
        }*/

        // si el iva es 0 no informo nada
        // 4=> 10.5%
        // 3=> 0%
        if ($datosfactura['importeiva'] > 0) {
            /*$detalleiva = array(
                'AlicIva' => array(
                    'Id' => 5,
                    'BaseImp' => 100,                   
                    'Importe' =>21
                    //'BaseImp' => $baseimp,                   
                    //'BaseImp' => $datosfactura['baseimp'],                   
                    //'Importe' => round($datosfactura['importeiva'], 2)
                )
            );*/           
           
        /*var_dump($datosfactura['detalleiva']);
        die;*/
       /*  var_dump(round($neto, 2));
         die;*/
           
            
            $FECAEDetRequest = array
            (
                'Concepto' => $datosfactura['concepto'],
                'DocTipo' => $tipodoc,
                'DocNro' => $nrodoc,
                'CbteDesde' => $datosfactura['nrofactura'],
                'CbteHasta' => $datosfactura['nrofactura'],
                'CbteFch' => $datosfactura['fechaemision'],
                'ImpTotal' => round($datosfactura['importetotal'], 2),
                'ImpTotConc' => round($datosfactura['capitalafinanciar'], 2),
                'ImpNeto' => round($neto, 2),
                'ImpOpEx' => 0.00,
                'ImpTrib' => 0.00,
                'ImpIVA' => round($datosfactura['importeiva'], 2),
                'FchServDesde' => $datosfactura['fechadesde'],
                'FchServHasta' => $datosfactura['fechahasta'],
                'FchVtoPago' => $datosfactura['fecha_venc_pago'],
                'MonId' => "PES",
                'MonCotiz' => "1.00",
                //'Iva' => $detalleiva
                'Iva' => $datosfactura['detalleiva']
            );
        }else{
            $FECAEDetRequest = array
            (
                'Concepto' => $datosfactura['concepto'],
                'DocTipo' => $tipodoc,
                'DocNro' => $nrodoc,
                'CbteDesde' => $datosfactura['nrofactura'],
                'CbteHasta' => $datosfactura['nrofactura'],
                'CbteFch' => $datosfactura['fechaemision'],
                'ImpTotal' => round($datosfactura['importetotal'], 2),
                'ImpTotConc' => round($datosfactura['capitalafinanciar'], 2),
                'ImpNeto' => round($neto, 2),
                'ImpOpEx' => 0.00,
                'ImpTrib' => 0.00,
                'ImpIVA' => round($datosfactura['importeiva'], 2),
                'FchServDesde' => $datosfactura['fechadesde'],
                'FchServHasta' => $datosfactura['fechahasta'],
                'FchVtoPago' => $datosfactura['fecha_venc_pago'],
                'MonId' => "PES",
                'MonCotiz' => "1.00",
            );
        }

        if(!empty($datosfactura['PeriodoAsoc'])){
            $FECAEDetRequest['PeriodoAsoc'] = $datosfactura['PeriodoAsoc'];
        }

        if(!empty($datosfactura['CbtesAsoc'])){
            $FECAEDetRequest['CbtesAsoc'] = $datosfactura['CbtesAsoc'];
        }

        if(!empty($datosfactura['opcionales'])){
            $FECAEDetRequest['Opcionales'] = ['Opcional'=>$datosfactura['opcionales']];
        }
//        var_dump($FECAEDetRequest);
//        var_dump($FeCabReq);
//        die();
        $fedetreq = array('FECAEDetRequest' => $FECAEDetRequest);
        $params = array
        (
            'FeCabReq' => $FeCabReq,
            'FeDetReq' => $fedetreq
        );
        return $params;
    }

    public function llamarmetodo($metodo, $params) {
        $cuit = (float)self::CUIT;

        switch ($metodo) {
            case 'FEDummy':
                $resu = $this->client->FEDummy();
                break;
            case 'FECAESolicitar':
                $resu = $this->client->FECAESolicitar($params);
                break;
            case 'FECompUltimoAutorizado':
                $resu = $this->client->FECompUltimoAutorizado($params);
                break;
            case 'FEParamGetPtosVenta':
                $resu = $this->client->FEParamGetPtosVenta(
                    array('Auth' =>  array('Token'   => $this->TA->credentials->token,
                        'Sign'     => $this->TA->credentials->sign,
                        'Cuit'     => $cuit)));
                break;
            case 'FEParamGetTiposMonedas':
                $resu = $this->client->FEParamGetTiposMonedas($params);
                break;
            case 'FEParamGetTiposCbte':
                $resu = $this->client->FEParamGetTiposCbte($params);
                break;
            case 'FEParamGetTiposDoc':
                $params->Auth->Token = $this->TA->credentials->token;
                $params->Auth->Sign = $this->TA->credentials->sign;
                $params->Auth->Cuit = self::CUIT;
                $resu = $this->client->FEParamGetTiposDoc($params);
                break;

            default: echo "falta definir metodo";
                break;
        }
        return $resu;
    }

    public function solicitarCAE($params, $cuit)
    {
        $cuit = (float)$cuit;
        $results = $this->client->FECAESolicitar(
            array('Auth'    =>  array('Token'   => $this->TA->credentials->token,
                'Sign'     => $this->TA->credentials->sign,
                'Cuit'     => $cuit),
                'FeCAEReq' 	=> $params
            )
        );
        return $results;
    }

}

?>
