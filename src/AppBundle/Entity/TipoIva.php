<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TipoIvaRepository")
 * @ORM\Table(name="tipoiva")
 */
class TipoIva {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $porcentaje;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="tipoIva")
     */
    protected $ivas;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
    $this->ivas = new ArrayCollection();    
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoIva
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return TipoIva
     */
    public function setPorcentaje($porcentaje) {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje() {
        return $this->porcentaje;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoIva
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Add ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     * @return TipoIva
     */
    public function addIva(\AppBundle\Entity\Iva $ivas)
    {
        $this->ivas[] = $ivas;
    
        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     */
    public function removeIva(\AppBundle\Entity\Iva $ivas)
    {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIvas()
    {
        return $this->ivas;
    }
}
