<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProductoPresupuestoRepository")
 * @ORM\Table(name="productopresupuesto")
 */
class ProductoPresupuesto {

    const PRODUCTO_PRESUPUESTO_ACEPTADO = 'ACEPTADO';
    const PRODUCTO_PRESUPUESTO_PENDIENTE = 'PENDIENTE';
    const PRODUCTO_PRESUPUESTO_NO_ACEPTADO = 'NO ACEPTADO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $renglon;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosPresupuesto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Presupuesto", inversedBy="productosPresupuesto", cascade={"persist"})
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id")
     */
    protected $presupuesto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $estado = ProductoPresupuesto::PRODUCTO_PRESUPUESTO_PENDIENTE;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'Presupuesto Nro.' . $this->id;
    }

    public function getSubtotal() {
        return round($this->precio * $this->cantidad, 2);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ProductoPresupuesto
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ProductoPresupuesto
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ProductoPresupuesto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return ProductoPresupuesto
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto 
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set presupuesto
     *
     * @param \AppBundle\Entity\Presupuesto $presupuesto
     * @return ProductoPresupuesto
     */
    public function setPresupuesto(\AppBundle\Entity\Presupuesto $presupuesto = null) {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \AppBundle\Entity\Presupuesto 
     */
    public function getPresupuesto() {
        return $this->presupuesto;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ProductoPresupuesto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }


    /**
     * Set renglon
     *
     * @param integer $renglon
     * @return ProductoPresupuesto
     */
    public function setRenglon($renglon)
    {
        $this->renglon = $renglon;

        return $this;
    }

    /**
     * Get renglon
     *
     * @return integer 
     */
    public function getRenglon()
    {
        return $this->renglon;
    }
}
