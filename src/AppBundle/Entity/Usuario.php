<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UsuarioRepository")
 * @ORM\Table(name="usuario")
 * @UniqueEntity(
 * 		fields = {"username", "email"},
 * 			message = "LOGIN o MAIL existentes, elija otro."
 * 		)
 */
class Usuario extends BaseUser {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $apellidonombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sector;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="usuarios")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\OneToMany(targetEntity="ControlHorario", mappedBy="usuario")
     */
    protected $controlesHorario;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        parent::__construct();
        $this->controlesUsuario = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        if (null != $this->getApellidonombre()) {
            return $this->getApellidonombre();
        } else {
            return '';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Usuario
     */
    public function setClave($clave) {
        $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        $this->clave = base64_encode(mcrypt_encrypt(MCRYPT_BLOWFISH, $cryptKey, $clave, MCRYPT_MODE_ECB, "\0"));

        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave() {
        if (!empty($this->clave)) {
            $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
            $b64data = base64_decode($this->clave);
            $qDecoded = mcrypt_decrypt(MCRYPT_BLOWFISH, $cryptKey, $b64data, MCRYPT_MODE_ECB, "\0");
            return trim($qDecoded);
        }
        return $this->clave;
    }

    public function getClaveEnc() {
        return $this->clave;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Usuario
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Usuario
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Usuario
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set apellidonombre
     *
     * @param string $apellidonombre
     * @return Usuario
     */
    public function setApellidonombre($apellidonombre) {
        $this->apellidonombre = $apellidonombre;

        return $this;
    }

    /**
     * Get apellidonombre
     *
     * @return string 
     */
    public function getApellidonombre() {
        return $this->apellidonombre;
    }

    /**
     * Set sector
     *
     * @param string $sector
     * @return Usuario
     */
    public function setSector($sector) {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return string 
     */
    public function getSector() {
        return $this->sector;
    }

    /**
     * Add controlesHorario
     *
     * @param \AppBundle\Entity\ControlHorario $controlesHorario
     * @return Usuario
     */
    public function addControlesHorario(\AppBundle\Entity\ControlHorario $controlesHorario) {
        $this->controlesHorario[] = $controlesHorario;

        return $this;
    }

    /**
     * Remove controlesHorario
     *
     * @param \AppBundle\Entity\ControlHorario $controlesHorario
     */
    public function removeControlesHorario(\AppBundle\Entity\ControlHorario $controlesHorario) {
        $this->controlesHorario->removeElement($controlesHorario);
    }

    /**
     * Get controlesHorario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getControlesHorario() {
        return $this->controlesHorario;
    }

}
