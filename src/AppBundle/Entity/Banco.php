<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BancoRepository")
 * @ORM\Table(name="banco")
 */
class Banco {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="CuentaBancaria", mappedBy="banco")
     */
    protected $cuentasBancarias;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="banco")
     */
    protected $cheques;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->cuentasBancarias = new ArrayCollection();
        $this->cheques = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion

     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Banco
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add cuentasBancarias
     *
     * @param \AppBundle\Entity\CuentaBancaria $cuentasBancarias
     * @return Banco
     */
    public function addCuentasBancaria(\AppBundle\Entity\CuentaBancaria $cuentasBancarias) {
        $this->cuentasBancarias[] = $cuentasBancarias;

        return $this;
    }

    /**
     * Remove cuentasBancarias
     *
     * @param \AppBundle\Entity\CuentaBancaria $cuentasBancarias
     */
    public function removeCuentasBancaria(\AppBundle\Entity\CuentaBancaria $cuentasBancarias) {
        $this->cuentasBancarias->removeElement($cuentasBancarias);
    }

    /**
     * Get cuentasBancarias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCuentasBancarias() {
        return $this->cuentasBancarias;
    }


    /**
     * Add cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     * @return Banco
     */
    public function addCheque(\AppBundle\Entity\Cheque $cheques)
    {
        $this->cheques[] = $cheques;
    
        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\AppBundle\Entity\Cheque $cheques)
    {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques()
    {
        return $this->cheques;
    }
}
