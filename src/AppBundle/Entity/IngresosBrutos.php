<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\IngresosBrutosRepository")
 * @ORM\Table(name="ingresosbrutos")
 */
class IngresosBrutos {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="ingresosbrutos")
     * @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
     */
    protected $notaCreditoDebito;

    /**
     * @ORM\ManyToOne(targetEntity="TipoIngresosBrutos", inversedBy="ingresosBrutos")
     * @ORM\JoinColumn(name="tipoingresosbrutos_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoIngresosBrutos;

    /**
     * @ORM\ManyToOne(targetEntity="Gasto", inversedBy="ingresosbrutos")
     * @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
     */
    protected $gasto;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="ingresosbrutos")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    
    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getValor();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return IngresosBrutos
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return IngresosBrutos
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return IngresosBrutos
     */
    public function setNotaCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null) {
        $this->notaCreditoDebito = $notaCreditoDebito;

        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito
     */
    public function getNotaCreditoDebito() {
        return $this->notaCreditoDebito;
    }

    /**
     * Set tipoIngresosBrutos
     *
     * @param \AppBundle\Entity\TipoIngresosBrutos $tipoIngresosBrutos
     * @return IngresosBrutos
     */
    public function setTipoIngresosBrutos(\AppBundle\Entity\TipoIngresosBrutos $tipoIngresosBrutos = null) {
        $this->tipoIngresosBrutos = $tipoIngresosBrutos;

        return $this;
    }

    /**
     * Get tipoIngresosBrutos
     *
     * @return \AppBundle\Entity\TipoIngresosBrutos
     */
    public function getTipoIngresosBrutos() {
        return $this->tipoIngresosBrutos;
    }

    /**
     * Set gasto
     *
     * @param \AppBundle\Entity\Gasto $gasto
     * @return IngresosBrutos
     */
    public function setGasto(\AppBundle\Entity\Gasto $gasto = null) {
        $this->gasto = $gasto;

        return $this;
    }

    /**
     * Get gasto
     *
     * @return \AppBundle\Entity\Gasto
     */
    public function getGasto() {
        return $this->gasto;
    }


    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return IngresosBrutos
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null)
    {
        $this->pago = $pago;
    
        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago
     */
    public function getPago()
    {
        return $this->pago;
    }
}
