<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RemitoOficialRepository")
 * @ORM\Table(name="remitooficial")
 */
class RemitoOficial {

    const IN_PROCESS = 'PENDIENTE';
    const INVOICED = 'FACTURADO';
    const CANCEL = 'ANULADO';
    const NO_FACTURAR = 'CANCELADO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $numero;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\OneToMany(targetEntity="ProductoRemito", mappedBy="remitooficial", cascade={"persist"})
     */
    protected $productosRemito;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Factura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="vendedor_id", referencedColumnName="id",nullable=true)
     */
    protected $vendedor;

   
    /**
     * @ORM\OneToMany(targetEntity="Presupuesto", mappedBy="remitoOficial")
     */
    protected $presupuestos;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $estado = RemitoOficial::IN_PROCESS;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */
    public function __construct() {
        $this->productosRemito = new ArrayCollection();
        $this->presupuestos = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $nro = $this->numero;
        return "" . str_pad($nro, 6, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return RemitoOficial
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return RemitoOficial
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return RemitoOficial
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return RemitoOficial
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return RemitoOficial
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosRemito
     *
     * @param \AppBundle\Entity\ProductoRemito $productosRemito
     * @return RemitoOficial
     */
    public function addProductosRemito(\AppBundle\Entity\ProductoRemito $productosRemito) {
        $this->productosRemito[] = $productosRemito;

        return $this;
    }

    /**
     * Remove productosRemito
     *
     * @param \AppBundle\Entity\ProductoRemito $productosRemito
     */
    public function removeProductosRemito(\AppBundle\Entity\ProductoRemito $productosRemito) {
        $this->productosRemito->removeElement($productosRemito);
    }

    /**
     * Get productosRemito
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosRemito() {
        return $this->productosRemito;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return RemitoOficial
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return RemitoOficial
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor 
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return RemitoOficial
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal 
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * Set vendedor
     *
     * @param \AppBundle\Entity\Usuario $vendedor
     * @return RemitoOficial
     */
    public function setVendedor(\AppBundle\Entity\Usuario $vendedor = null) {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getVendedor() {
        return $this->vendedor;
    }


    /**
     * Add presupuestos
     *
     * @param \AppBundle\Entity\Presupuestos $presupuestos
     * @return RemitoOficial
     */
    public function addPresupuesto(\AppBundle\Entity\Presupuesto $presupuestos)
    {
        $this->presupuestos[] = $presupuestos;

        return $this;
    }

    /**
     * Remove presupuestos
     *
     * @param \AppBundle\Entity\Presupuestos $presupuestos
     */
    public function removePresupuesto(\AppBundle\Entity\Presupuesto $presupuestos)
    {
        $this->presupuestos->removeElement($presupuestos);
    }

    /**
     * Get presupuestos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresupuestos()
    {
        return $this->presupuestos;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return RemitoOficial
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }
}
