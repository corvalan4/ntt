<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ItemNotaPRepository")
 * @ORM\Table(name="item_nota")
 */
class ItemNota {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="itemsNota")
     * @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=false)
     */
    protected $notadebitocredito;

    /**
     * @ORM\ManyToOne(targetEntity="ProductoFactura")
     * @ORM\JoinColumn(name="productofactura_id", referencedColumnName="id")
     */
    protected $productofactura;

    /**
     * @ORM\Column(type="float")
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float")
     */
    protected $precio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $pertenece;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;
    
    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $tipoiva = 21;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    public function __toString() {
        return $this->descripcion;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ItemNota
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ItemNota
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return ItemNota
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set pertenece
     *
     * @param string $pertenece
     * @return ItemNota
     */
    public function setPertenece($pertenece) {
        $this->pertenece = $pertenece;

        return $this;
    }

    /**
     * Get pertenece
     *
     * @return string 
     */
    public function getPertenece() {
        return $this->pertenece;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ItemNota
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ItemNota
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set notadebitocredito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notadebitocredito
     * @return ItemNota
     */
    public function setNotadebitocredito(\AppBundle\Entity\NotaCreditoDebito $notadebitocredito) {
        $this->notadebitocredito = $notadebitocredito;

        return $this;
    }

    /**
     * Get notadebitocredito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito
     */
    public function getNotadebitocredito() {
        return $this->notadebitocredito;
    }

    /**
     * Set productofactura
     *
     * @param \AppBundle\Entity\ProductoFactura $productofactura
     * @return ItemNota
     */
    public function setProductofactura(\AppBundle\Entity\ProductoFactura $productofactura = null) {
        $this->productofactura = $productofactura;

        return $this;
    }

    /**
     * Get productofactura
     *
     * @return \AppBundle\Entity\ProductoFactura
     */
    public function getProductofactura() {
        return $this->productofactura;
    }


    /**
     * Set tipoiva
     *
     * @param float $tipoiva
     * @return ItemNota
     */
    public function setTipoiva($tipoiva)
    {
        $this->tipoiva = $tipoiva;
    
        return $this;
    }

    /**
     * Get tipoiva
     *
     * @return float 
     */
    public function getTipoiva()
    {
        return $this->tipoiva;
    }
}
