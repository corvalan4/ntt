<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StockPedidoRepository")
 * @ORM\Table(name="stockpedido")
 */
class StockPedido{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Stock", inversedBy="stocksPedidos")
	* @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
	*/
    protected $stock;

	/**
	* @ORM\ManyToOne(targetEntity="Pedido", inversedBy="stocksPedidos")
	* @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
	*/
    protected $pedido;

	/**
	* @ORM\OneToMany(targetEntity="StockEntregado", mappedBy="stockpedido")
	*/
	protected $stocksEntregados;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return StockPedido
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     * @return StockPedido
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return StockPedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set stock
     *
     * @param \AppBundle\Entity\Stock $stock
     * @return StockPedido
     */
    public function setStock(\AppBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \AppBundle\Entity\Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     * @return StockPedido
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Add stocksEntregados
     *
     * @param \AppBundle\Entity\StockEntregado $stocksEntregados
     * @return StockPedido
     */
    public function addStocksEntregado(\AppBundle\Entity\StockEntregado $stocksEntregados)
    {
        $this->stocksEntregados[] = $stocksEntregados;

        return $this;
    }

    /**
     * Remove stocksEntregados
     *
     * @param \AppBundle\Entity\StockEntregado $stocksEntregados
     */
    public function removeStocksEntregado(\AppBundle\Entity\StockEntregado $stocksEntregados)
    {
        $this->stocksEntregados->removeElement($stocksEntregados);
    }

    /**
     * Get stocksEntregados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocksEntregados()
    {
        return $this->stocksEntregados;
    }

    public function getCantidadEntregada()
    {
        $cantidad = 0;
        foreach ($this->stocksEntregados as $value) {
            $cantidad+=$value->getCantidad();
        }
        return $cantidad;
    }
}
