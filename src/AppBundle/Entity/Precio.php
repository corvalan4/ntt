<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PrecioRepository")
 * @ORM\Table(name="precio")
 */
class Precio{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="precios")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio", inversedBy="precios")
     * @ORM\JoinColumn(name="lista_precio_id", referencedColumnName="id")
     */
    protected $listaprecio;
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return "<b>".$this->listaprecio."</b> $ ".$this->valor;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Precio
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Precio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return Precio
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set listaprecio
     *
     * @param \AppBundle\Entity\ListaPrecio $listaprecio
     * @return Precio
     */
    public function setListaprecio(\AppBundle\Entity\ListaPrecio $listaprecio = null)
    {
        $this->listaprecio = $listaprecio;

        return $this;
    }

    /**
     * Get listaprecio
     *
     * @return \AppBundle\Entity\ListaPrecio
     */
    public function getListaprecio()
    {
        return $this->listaprecio;
    }
}
