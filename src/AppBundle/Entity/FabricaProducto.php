<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FabricaProductoRepository")
 * @ORM\Table(name="fabricaproducto")
 */
class FabricaProducto{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Producto", inversedBy="fabricaProductos")
	* @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
	*/
    protected $producto;

	/**
	* @ORM\ManyToOne(targetEntity="Fabrica", inversedBy="fabricaProductos")
	* @ORM\JoinColumn(name="fabrica_id", referencedColumnName="id")
	*/
    protected $fabrica;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return FabricaProducto
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return FabricaProducto
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set fabrica
     *
     * @param \AppBundle\Entity\Fabrica $fabrica
     * @return FabricaProducto
     */
    public function setFabrica(\AppBundle\Entity\Fabrica $fabrica = null)
    {
        $this->fabrica = $fabrica;
    
        return $this;
    }

    /**
     * Get fabrica
     *
     * @return \AppBundle\Entity\Fabrica 
     */
    public function getFabrica()
    {
        return $this->fabrica;
    }
}
