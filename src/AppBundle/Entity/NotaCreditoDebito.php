<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\NotaCreditoDebitoRepository")
 * @ORM\Table(name="notacreditodebito")
 */
class NotaCreditoDebito {
    const TIPO_DEBITO = 'DEBITO';
    const TIPO_CREDITO = 'CREDITO';
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadnegocio;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="notasCreditoDebito")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="facturas")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $importe=0;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tiponota; // DEBITO O CREDITO

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $codigonota = "B"; // A, B, C

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cae;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ptovta;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechavtocae;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="notaCreditoDebito", cascade={"persist"})
     */
    protected $ivas;

    /**
     * @ORM\OneToMany(targetEntity="IngresosBrutos", mappedBy="notaCreditoDebito", cascade={"persist"})
     */
    protected $ingresosbrutos;

    /**
     * @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="notaCreditoDebito", cascade={"persist"})
     */
    protected $cobranzasfactura;

    /**
     * @ORM\OneToMany(targetEntity="PagoFactura", mappedBy="factura")
     */
    protected $pagosfactura;

    /**
     * @ORM\OneToMany(targetEntity="ItemNota", mappedBy="notadebitocredito", cascade={"persist", "remove"} )
     */
    protected $itemsNota;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="notaCreditoDebito" )
     */
    protected $pagosAsignacion;

    /**
     * @ORM\ManyToMany(targetEntity="Factura")
     * @ORM\JoinTable(name="nota_factura",
     *      joinColumns={@ORM\JoinColumn(name="nota_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="factura_id", referencedColumnName="id")}
     *      )
     */
    private $facturas;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fechaRegistracion = new \DateTime();
        $this->ivas = new ArrayCollection();
        $this->ingresosbrutos = new ArrayCollection();
        $this->itemsNota = new ArrayCollection();
        $this->pagosAsignacions = new ArrayCollection();
        $this->facturas = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return "NOTA " . $this->tiponota . " " . $this->codigonota . " - NRO: " . $this->nrocomprobante;
    }

      /**
     * Get importe
     *
     * @return float
     */
    public function getTotal() {
        $importesIva = 0;
        foreach ($this->ivas as $iva) {
            $importesIva += $iva->getValor();
        }
        return $this->importe + $importesIva;
    }
    
    
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    
    
    
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return NotaCreditoDebito
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return NotaCreditoDebito
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return NotaCreditoDebito
     */
    public function setNrocomprobante($nrocomprobante) {
        $this->nrocomprobante = $nrocomprobante;

        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string 
     */
    public function getNrocomprobante() {
        return $this->nrocomprobante;
    }

    public function getNumero() {
        $nrocomprobante = !empty($this->nrocomprobante) ? str_pad($this->nrocomprobante, 5, "0", STR_PAD_LEFT) : '';
        return $nrocomprobante;
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return NotaCreditoDebito
     */
    public function setCae($cae) {
        $this->cae = $cae;

        return $this;
    }

    /**
     * Get cae
     *
     * @return string 
     */
    public function getCae() {
        return $this->cae;
    }

    /**
     * Set ptovta
     *
     * @param integer $ptovta
     * @return NotaCreditoDebito
     */
    public function setPtovta($ptovta) {
        $this->ptovta = $ptovta;

        return $this;
    }

    /**
     * Get ptovta
     *
     * @return integer 
     */
    public function getPtovta() {
        return $this->ptovta;
    }

    public function getPtovtaPad() {
        return str_pad($this->ptovta, 3, "0", STR_PAD_LEFT);
    }

    /**
     * Set fechavtocae
     *
     * @param string $fechavtocae
     * @return NotaCreditoDebito
     */
    public function setFechavtocae($fechavtocae) {
        $this->fechavtocae = $fechavtocae;

        return $this;
    }

    /**
     * Get fechavtocae
     *
     * @return string 
     */
    public function getFechavtocae() {
        return $this->fechavtocae;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return NotaCreditoDebito
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getObservacionLimpia() {
        $obsevacion = sizeof(explode(' - ERROR: ', $this->descripcion)) > 0 ? explode(' - ERROR: ', $this->descripcion)[0] : $this->descripcion;

        return $obsevacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return NotaCreditoDebito
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set unidadnegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadnegocio
     * @return NotaCreditoDebito
     */
    public function setUnidadnegocio(\AppBundle\Entity\UnidadNegocio $unidadnegocio = null) {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get unidadnegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadnegocio() {
        return $this->unidadnegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return NotaCreditoDebito
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Add ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     * @return NotaCreditoDebito
     */
    public function addIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas[] = $ivas;

        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     */
    public function removeIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIvas() {
        return $this->ivas;
    }

    /**
     * Add ingresosbrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosbrutos
     * @return NotaCreditoDebito
     */
    public function addIngresosbruto(\AppBundle\Entity\IngresosBrutos $ingresosbrutos) {
        $this->ingresosbrutos[] = $ingresosbrutos;

        return $this;
    }

    /**
     * Remove ingresosbrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosbrutos
     */
    public function removeIngresosbruto(\AppBundle\Entity\IngresosBrutos $ingresosbrutos) {
        $this->ingresosbrutos->removeElement($ingresosbrutos);
    }

    /**
     * Get ingresosbrutos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngresosbrutos() {
        return $this->ingresosbrutos;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return NotaCreditoDebito
     */
    public function addCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura[] = $cobranzasfactura;

        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzasfactura() {
        return $this->cobranzasfactura;
    }

    /**
     * Set tiponota
     *
     * @param string $tiponota
     * @return NotaCreditoDebito
     */
    public function setTiponota($tiponota) {
        $this->tiponota = $tiponota;

        return $this;
    }

    /**
     * Get tiponota
     *
     * @return string 
     */
    public function getTiponota() {
        return $this->tiponota;
    }

    /**
     * Set codigonota
     *
     * @param string $codigonota
     * @return NotaCreditoDebito
     */
    public function setCodigonota($codigonota) {
        $this->codigonota = $codigonota;

        return $this;
    }

    /**
     * Get codigonota
     *
     * @return string 
     */
    public function getCodigonota() {
        return $this->codigonota;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return NotaCreditoDebito
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * Add pagosfactura
     *
     * @param \AppBundle\Entity\PagoFactura $pagosfactura
     * @return NotaCreditoDebito
     */
    public function addPagosfactura(\AppBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura[] = $pagosfactura;

        return $this;
    }

    /**
     * Remove pagosfactura
     *
     * @param \AppBundle\Entity\PagoFactura $pagosfactura
     */
    public function removePagosfactura(\AppBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura->removeElement($pagosfactura);
    }

    /**
     * Get pagosfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosfactura() {
        return $this->pagosfactura;
    }

    /**
     * Add itemsNota
     *
     * @param \AppBundle\Entity\ItemNota $itemsNota
     * @return NotaCreditoDebito
     */
    public function addItemsNota(\AppBundle\Entity\ItemNota $itemsNota) {
        $this->itemsNota[] = $itemsNota;

        return $this;
    }

    /**
     * Remove itemsNota
     *
     * @param \AppBundle\Entity\ItemNota $itemsNota
     */
    public function removeItemsNota(\AppBundle\Entity\ItemNota $itemsNota) {
        $this->itemsNota->removeElement($itemsNota);
    }

    /**
     * Get itemsNota
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemsNota() {
        return $this->itemsNota;
    }

    /**
     * Add itemsNota
     *
     * @param \AppBundle\Entity\ItemNota $itemsNota
     * @return NotaCreditoDebito
     */
    public function addItemsNotum(\AppBundle\Entity\ItemNota $itemsNota) {
        $this->itemsNota[] = $itemsNota;

        return $this;
    }

    /**
     * Remove itemsNota
     *
     * @param \AppBundle\Entity\ItemNota $itemsNota
     */
    public function removeItemsNotum(\AppBundle\Entity\ItemNota $itemsNota) {
        $this->itemsNota->removeElement($itemsNota);
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return NotaCreditoDebito
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion() {
        return
                $this->pagosAsignacion;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return 'cobranza';
    }


    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return NotaCreditoDebito
     */
    public function setFechaRegistracion($fechaRegistracion)
    {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion()
    {
        return $this->fechaRegistracion;
    }

    /**
     * Add facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     * @return NotaCreditoDebito
     */
    public function addFactura(\AppBundle\Entity\Factura $facturas)
    {
        $this->facturas[] = $facturas;

        return $this;
    }

    /**
     * Remove facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     */
    public function removeFactura(\AppBundle\Entity\Factura $facturas)
    {
        $this->facturas->removeElement($facturas);
    }

    /**
     * Get facturas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFacturas()
    {
        return $this->facturas;
    }
}
