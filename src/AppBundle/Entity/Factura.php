<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FacturaRepository")
 * @ORM\Table(name="factura")
 */
class Factura {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="facturas")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal; // Para F

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="facturas")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor; // Para FP

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipo = "F"; // Puede ser F por FACTURA o FP por FACTURAPROVEEDOR o FC por Factura de Credito 

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="factura", cascade={"persist", "remove"} )
     */
    protected $productosFactura;

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="factura")
     */
    protected $ivas;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $condicionpago;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipofactura; // A o B

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $cae;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechavtocae;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nroremito;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nrofactura;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ptovta;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mesanio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidademision;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion=0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $percepcionib;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $percepcioniva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento=0;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\ManyToOne(targetEntity="TipoGasto", inversedBy="gastos")
     * @ORM\JoinColumn(name="tipoGasto_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoGasto;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $jurisdiccion;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="factura")
     */
    protected $pagosAsignacion;

    /**
     * @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="factura")
     */
    protected $cobranzasfactura;

    /**
     * @ORM\OneToMany(targetEntity="PagoFactura", mappedBy="pago")
     */
    protected $pagosfactura;

    /**
     * @ORM\OneToMany(targetEntity="Retencion", mappedBy="factura")
     */
    protected $retenciones;

    /*     * ********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->fechaRegistracion = new \DateTime();
        $this->productosFactura = new ArrayCollection();
        $this->ivas = new ArrayCollection();
        $this->cobranzasfactura = new ArrayCollection();
        $this->pagosfactura = new ArrayCollection();
        $this->pagosAsignacion = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $tipo = ($this->tipo == "F" or $this->tipo == "FP") ? "FACTURA" : "FACTURA DE CREDITO";
        $nro = !empty($this->nrofactura) ? $this->nrofactura : $this->id;
        if ($this->tipoGasto) {
            $gasto = '(gasto)';
        } else {
            $gasto = '';
        }
        return "<strong>$tipo " . $this->getTipofactura() . " " . $gasto . "</strong>" . " #" . str_pad($nro, 5, "0", STR_PAD_LEFT) . " - Pto." . $this->ptovta;
    }

        /**
        * Get importe
        *
        * @return float
        */
       public function getTotal() {
           $importesIva = 0;
           foreach ($this->ivas as $iva) {
               $importesIva += $iva->getValor();
           }
           return $this->importe + $importesIva;
       }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Factura
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    public function getTipoFacturaStr() {
        $tipo = ($this->tipo == "F" or $this->tipo == "FP") ? "FACTURA" : "FACTURA DE CREDITO";

        return $tipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Factura
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Factura
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Get importe
     *
     * @return float
     */
   /* public function getImporteNeto() {
        $importesIva = 0;
        foreach ($this->ivas as $iva) {
            $importesIva += $iva->getValor();
        }
        return $this->importe - $importesIva;
    }*/

    /**
     * Set tipofactura
     *
     * @param string $tipofactura
     * @return Factura
     */
    public function setTipofactura($tipofactura) {
        $this->tipofactura = $tipofactura;

        return $this;
    }

    /**
     * Get tipofactura
     *
     * @return string
     */
    public function getTipofactura() {
        return $this->tipofactura;
    }

    public function getTipocomprobante() {
        return ($this->tipofactura == 'A') ? 1 : (($this->tipofactura == 'B') ? 6 : 11);
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return Factura
     */
    public function setCae($cae) {
        $this->cae = $cae;

        return $this;
    }

    /**
     * Get cae
     *
     * @return string
     */
    public function getCae() {
        return $this->cae;
    }

    /**
     * Set fechavtocae
     *
     * @param \DateTime $fechavtocae
     * @return Factura
     */
    public function setFechavtocae($fechavtocae) {
        $this->fechavtocae = $fechavtocae;

        return $this;
    }

    /**
     * Get fechavtocae
     *
     * @return \DateTime
     */
    public function getFechavtocae() {
        return $this->fechavtocae;
    }

    /**
     * Set nroremito
     *
     * @param string $nroremito
     * @return Factura
     */
    public function setNroremito($nroremito) {
        $this->nroremito = $nroremito;

        return $this;
    }

    /**
     * Get nroremito
     *
     * @return string
     */
    public function getNroremito() {
        return $this->nroremito;
    }

    /**
     * Set nrofactura
     *
     * @param integer $nrofactura
     * @return Factura
     */
    public function setNrofactura($nrofactura) {
        $this->nrofactura = $nrofactura;

        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return integer
     */
    public function getNrofactura() {
        $nro = !empty($this->nrofactura) ? str_pad($this->nrofactura, 5, "0", STR_PAD_LEFT) : '';
        return $nro;
    }

    /**
     * Get nrofactura
     *
     * @return integer
     */
    public function getNumero() {
        $nro = !empty($this->nrofactura) ? str_pad($this->nrofactura, 5, "0", STR_PAD_LEFT) : '';
        return $nro;
    }

    /**
     * Set ptovta
     *
     * @param integer $ptovta
     * @return Factura
     */
    public function setPtovta($ptovta) {
        $this->ptovta = $ptovta;

        return $this;
    }

    /**
     * Get ptovta
     *
     * @return integer
     */
    public function getPtovta() {
        return $this->ptovta;
    }

    /**
     * Get ptovta
     *
     * @return integer
     */
    public function getPtovtaPad() {
        return str_pad($this->ptovta, 3, "0", STR_PAD_LEFT);
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Factura
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion() {
        return $this->observacion;
    }

    public function getObservacionLimpia() {
        $obsevacion = sizeof(explode(' - ERROR: ', $this->observacion)) > 0 ? explode(' - ERROR: ', $this->observacion)[0] : $this->observacion;

        return $obsevacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Factura
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return Factura
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Factura
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Factura
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Add productosFactura
     *
     * @param \AppBundle\Entity\ProductoFactura $productosFactura
     * @return Factura
     */
    public function addProductosFactura(\AppBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \AppBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\AppBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Add ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     * @return Factura
     */
    public function addIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas[] = $ivas;

        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     */
    public function removeIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIvas() {
        return $this->ivas;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return Factura
     */
    public function addCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura[] = $cobranzasfactura;

        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCobranzasfactura() {
        return $this->cobranzasfactura;
    }

    /**
     * @return mixed
     */
    public function getMesanio() {
        return $this->mesanio;
    }

    /**
     * @param mixed $mesanio
     */
    public function setMesanio($mesanio) {
        $this->mesanio = $mesanio;
    }

    /**
     * Set localidademision
     *
     * @param string $localidademision
     * @return Factura
     */
    public function setLocalidademision($localidademision) {
        $this->localidademision = $localidademision;

        return $this;
    }

    /**
     * Get localidademision
     *
     * @return string 
     */
    public function getLocalidademision() {
        return $this->localidademision;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Factura
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set percepcionib
     *
     * @param float $percepcionib
     * @return Factura
     */
    public function setPercepcionib($percepcionib) {
        $this->percepcionib = $percepcionib;

        return $this;
    }

    /**
     * Get percepcionib
     *
     * @return float 
     */
    public function getPercepcionib() {
        return $this->percepcionib;
    }

    /**
     * Set percepcioniva
     *
     * @param float $percepcioniva
     * @return Factura
     */
    public function setPercepcioniva($percepcioniva) {
        $this->percepcioniva = $percepcioniva;

        return $this;
    }

    /**
     * Get percepcioniva
     *
     * @return float 
     */
    public function getPercepcioniva() {
        return $this->percepcioniva;
    }

    /**
     * Add pagosfactura
     *
     * @param \AppBundle\Entity\PagoFactura $pagosfactura
     * @return Factura
     */
    public function addPagosfactura(\AppBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura[] = $pagosfactura;

        return $this;
    }

    /**
     * Remove pagosfactura
     *
     * @param \AppBundle\Entity\PagoFactura $pagosfactura
     */
    public function removePagosfactura(\AppBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura->removeElement($pagosfactura);
    }

    /**
     * Get pagosfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosfactura() {
        return $this->pagosfactura;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Factura
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return Factura
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion() {
        return $this->pagosAsignacion;
    }

    /**
     * Add retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     * @return Factura
     */
    public function addRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones[] = $retenciones;

        return $this;
    }

    /**
     * Remove retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     */
    public function removeRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones->removeElement($retenciones);
    }

    /**
     * Get retenciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRetenciones() {
        return $this->retenciones;
    }

    /**
     * Set jurisdiccion
     *
     * @param string $jurisdiccion
     * @return Factura
     */
    public function setJurisdiccion($jurisdiccion) {
        $this->jurisdiccion = $jurisdiccion;

        return $this;
    }

    /**
     * Get jurisdiccion
     *
     * @return string 
     */
    public function getJurisdiccion() {
        return $this->jurisdiccion;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return Factura
     */
    public function setFechaRegistracion($fechaRegistracion) {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion() {
        return $this->fechaRegistracion;
    }

    /**
     * Set condicionpago
     *
     * @param string $condicionpago
     * @return Factura
     */
    public function setCondicionpago($condicionpago) {
        $this->condicionpago = $condicionpago;

        return $this;
    }

    /**
     * Get condicionpago
     *
     * @return string 
     */
    public function getCondicionpago() {
        return $this->condicionpago;
    }

    /**
     * Set tipoGasto
     *
     * @param \AppBundle\Entity\TipoGasto $tipoGasto
     * @return Factura
     */
    public function setTipoGasto(\AppBundle\Entity\TipoGasto $tipoGasto = null) {
        $this->tipoGasto = $tipoGasto;

        return $this;
    }

    /**
     * Get tipoGasto
     *
     * @return \AppBundle\Entity\TipoGasto 
     */
    public function getTipoGasto() {
        return $this->tipoGasto;
    }

}
