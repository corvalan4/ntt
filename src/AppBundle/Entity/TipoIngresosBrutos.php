<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TipoIngresosBrutosRepository")
 * @ORM\Table(name="tipoingresosbrutos")
 */
class TipoIngresosBrutos {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $porcentaje;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="IngresosBrutos", mappedBy="tipoIngresosBrutos")
     */
    protected $ingresosBrutos;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->ingresosBrutos = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoIngresosBrutos
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return TipoIngresosBrutos
     */
    public function setPorcentaje($porcentaje) {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje() {
        return $this->porcentaje;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoIngresosBrutos
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Add ingresosBrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosBrutos
     * @return TipoIngresosBrutos
     */
    public function addIngresosBruto(\AppBundle\Entity\IngresosBrutos $ingresosBrutos)
    {
        $this->ingresosBrutos[] = $ingresosBrutos;
    
        return $this;
    }

    /**
     * Remove ingresosBrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosBrutos
     */
    public function removeIngresosBruto(\AppBundle\Entity\IngresosBrutos $ingresosBrutos)
    {
        $this->ingresosBrutos->removeElement($ingresosBrutos);
    }

    /**
     * Get ingresosBrutos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngresosBrutos()
    {
        return $this->ingresosBrutos;
    }
}
