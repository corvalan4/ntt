<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CotizacionRepository")
 * @ORM\Table(name="cotizacion")
 */
class Cotizacion {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;
    
    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="cotizacion")
     */
    protected $stocks;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->stocks = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getValor();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Cotizacion
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cotizacion
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }


    /**
     * Add stocks
     *
     * @param \AppBundle\Entity\Stock $stocks
     * @return Cotizacion
     */
    public function addStock(\AppBundle\Entity\Stock $stocks)
    {
        $this->stocks[] = $stocks;
    
        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \AppBundle\Entity\Stock $stocks
     */
    public function removeStock(\AppBundle\Entity\Stock $stocks)
    {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStocks()
    {
        return $this->stocks;
    }
}
