<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CondicionIvaRepository")
 * @ORM\Table(name="condicioniva")
 */
class CondicionIva {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="condicioniva")
     */
    protected $clientesProveedores;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->clientesProveedores = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return CondicionIva
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CondicionIva
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Add clientesProveedores
     *
     * @param \AppBundle\Entity\ClienteProveedor $clientesProveedores
     * @return CondicionIva
     */
    public function addClientesProveedore(\AppBundle\Entity\ClienteProveedor $clientesProveedores)
    {
        $this->clientesProveedores[] = $clientesProveedores;
    
        return $this;
    }

    /**
     * Remove clientesProveedores
     *
     * @param \AppBundle\Entity\ClienteProveedor $clientesProveedores
     */
    public function removeClientesProveedore(\AppBundle\Entity\ClienteProveedor $clientesProveedores)
    {
        $this->clientesProveedores->removeElement($clientesProveedores);
    }

    /**
     * Get clientesProveedores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientesProveedores()
    {
        return $this->clientesProveedores;
    }
}
