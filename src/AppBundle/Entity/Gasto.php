<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GastoRepository")
 * @ORM\Table(name="gasto")
 */
class Gasto {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="gastos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadnegocio;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="gastos")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="TipoGasto", inversedBy="gastos")
     * @ORM\JoinColumn(name="tipoGasto_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoGasto;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $efectivo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $puntoventa;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="gasto", cascade={"persist"})
     */
    protected $ivas;

    /**
     * @ORM\OneToMany(targetEntity="IngresosBrutos", mappedBy="gasto", cascade={"persist"})
     */
    protected $ingresosbrutos;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="gasto", cascade={"persist"})
     */
    protected $pagosAsignacion;

    /**
     * @ORM\OneToMany(targetEntity="MedioDocumento", mappedBy="gasto", cascade={"persist"})
     */
    protected $medios;

    /**
     * @ORM\OneToMany(targetEntity="Retencion", mappedBy="gasto", cascade={"persist"})
     */
    protected $retenciones;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="gasto", cascade={"persist"})
     */
    protected $cheques;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->fechaRegistracion = new \DateTime();
        $this->medios = new ArrayCollection();
        $this->ivas = new ArrayCollection();
        $this->ingresosbrutos = new ArrayCollection();
        $this->pagosAsignacion = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
        $this->fechaRegistracion = new \DateTime('NOW');
        $this->cheques = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $nro = !empty($this->nrocomprobante) ? $this->nrocomprobante : $this->id;
        return "GASTO " . str_pad($nro, 5, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Gasto
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return Gasto
     */
    public function setFechaRegistracion($fechaRegistracion) {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime
     */
    public function getFechaRegistracion() {
        return $this->fechaRegistracion;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Gasto
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return Gasto
     */
    public function setNrocomprobante($nrocomprobante) {
        $this->nrocomprobante = $nrocomprobante;

        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string
     */
    public function getNrocomprobante() {
        return $this->nrocomprobante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Gasto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Gasto
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Gasto
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Gasto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set unidadnegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadnegocio
     * @return Gasto
     */
    public function setUnidadnegocio(\AppBundle\Entity\UnidadNegocio $unidadnegocio = null) {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get unidadnegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadnegocio() {
        return $this->unidadnegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Gasto
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set tipoGasto
     *
     * @param \AppBundle\Entity\TipoGasto $tipoGasto
     * @return Gasto
     */
    public function setTipoGasto(\AppBundle\Entity\TipoGasto $tipoGasto = null) {
        $this->tipoGasto = $tipoGasto;

        return $this;
    }

    /**
     * Get tipoGasto
     *
     * @return \AppBundle\Entity\TipoGasto
     */
    public function getTipoGasto() {
        return $this->tipoGasto;
    }

    /**
     * Add ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     * @return Gasto
     */
    public function addIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas[] = $ivas;

        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \AppBundle\Entity\Iva $ivas
     */
    public function removeIva(\AppBundle\Entity\Iva $ivas) {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIvas() {
        return $this->ivas;
    }

    /**
     * Add ingresosbrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosbrutos
     * @return Gasto
     */
    public function addIngresosbruto(\AppBundle\Entity\IngresosBrutos $ingresosbrutos) {
        $this->ingresosbrutos[] = $ingresosbrutos;

        return $this;
    }

    /**
     * Remove ingresosbrutos
     *
     * @param \AppBundle\Entity\IngresosBrutos $ingresosbrutos
     */
    public function removeIngresosbruto(\AppBundle\Entity\IngresosBrutos $ingresosbrutos) {
        $this->ingresosbrutos->removeElement($ingresosbrutos);
    }

    /**
     * Get ingresosbrutos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngresosbrutos() {
        return $this->ingresosbrutos;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return Gasto
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPagosAsignacion() {
        return $this->pagosAsignacion;
    }

    /**
     * Add retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     * @return Gasto
     */
    public function addRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones[] = $retenciones;

        return $this;
    }

    /**
     * Remove retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     */
    public function removeRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones->removeElement($retenciones);
    }

    /**
     * Get retenciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRetenciones() {
        return $this->retenciones;
    }

    /**
     * Set efectivo
     *
     * @param string $efectivo
     * @return Gasto
     */
    public function setEfectivo($efectivo) {
        $this->efectivo = $efectivo;

        return $this;
    }

    /**
     * Get efectivo
     *
     * @return string
     */
    public function getEfectivo() {
        return $this->efectivo;
    }

    /**
     * Set puntoventa
     *
     * @param integer $puntoventa
     * @return Gasto
     */
    public function setPuntoventa($puntoventa) {
        $this->puntoventa = $puntoventa;

        return $this;
    }

    /**
     * Get puntoventa
     *
     * @return integer
     */
    public function getPuntoventa() {
        return $this->puntoventa;
    }

    /**
     * Add medios
     *
     * @param \AppBundle\Entity\MedioDocumento $medios
     * @return Gasto
     */
    public function addMedio(\AppBundle\Entity\MedioDocumento $medios) {
        $this->medios[] = $medios;

        return $this;
    }

    /**
     * Remove medios
     *
     * @param \AppBundle\Entity\MedioDocumento $medios
     */
    public function removeMedio(\AppBundle\Entity\MedioDocumento $medios) {
        $this->medios->removeElement($medios);
    }

    /**
     * Get medios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedios() {
        return $this->medios;
    }

    /**
     * Add cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     * @return Gasto
     */
    public function addCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques[] = $cheques;

        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques() {
        return $this->cheques;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return 'gasto';
    }

}
