<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MovimientoBancarioRepository")
 * @ORM\Table(name="movimientobancario")
 */
class MovimientoBancario {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cheque", inversedBy="movimientosBancarios")
     * @ORM\JoinColumn(name="cheque_id", referencedColumnName="id", nullable=true)
     * */
    private $cheque;

    /**
     * @ORM\ManyToOne(targetEntity="CuentaBancaria", inversedBy="movimientos")
     * @ORM\JoinColumn(name="cuentabancaria_id", referencedColumnName="id", nullable=true)
     */
    private $cuentabancaria;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="movimientoscc")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $tipoMovimiento;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fecha = new \DateTime('NOW');
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getEstadoActual() {
         if ($this->estado == 'U') {
            return 'USADO';
        }
        if ($this->estado == 'Z') {
            return 'RECHAZADO';
        }
        if ($this->estado == 'A') {
            return 'ACTIVO';
        }
        if ($this->estado == 'E') {
            return 'ELIMINADO';
        }
        if ($this->estado == 'C') {
            return 'CONCILIADO';
        }
        if ($this->estado == 'D') {
            return 'DEPOSITADO';
        }
         if ($this->estado == 'R') {
            return 'RETIRADO';
        }
        return $this->estado;
    }

    public function getColor() {
         if ($this->estado == 'C') {
            return 'blue';
        }
        if ($this->estado == 'D') {
            return 'green';
        }
        if ($this->estado == 'U') {
            return 'yellow';
        }
        if ($this->estado == 'Z') {
            return 'red';
        }
        if ($this->estado == 'E') {
            return 'grey';
        }
        if ($this->estado == 'R') {
            return 'brown';
        }
        return 'black';

    
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipoMovimiento
     *
     * @param string $tipoMovimiento
     * @return MovimientoBancario
     */
    public function setTipoMovimiento($tipoMovimiento) {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

    /**
     * Get tipoMovimiento
     *
     * @return string 
     */
    public function getTipoMovimiento() {
        return $this->tipoMovimiento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return MovimientoBancario
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return MovimientoBancario
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return MovimientoBancario
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set cheque
     *
     * @param \AppBundle\Entity\Cheque $cheque
     * @return MovimientoBancario
     */
    public function setCheque(\AppBundle\Entity\Cheque $cheque = null) {
        $this->cheque = $cheque;

        return $this;
    }

    /**
     * Get cheque
     *
     * @return \AppBundle\Entity\Cheque
     */
    public function getCheque() {
        return $this->cheque;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return MovimientoBancario
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set CuentaBancaria
     *
     * @param \AppBundle\Entity\CuentaBancaria $cuentabancaria
     * @return MovimientoBancario
     */
    public function setCuentabancaria(\AppBundle\Entity\CuentaBancaria $cuentabancaria = null) {
        $this->cuentabancaria = $cuentabancaria;

        return $this;
    }

    /**
     * Get cuentabancaria
     *
     * @return \AppBundle\Entity\CuentaBancaria
     */
    public function getCuentabancaria() {
        return $this->cuentabancaria;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return MovimientoBancario
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion() {
        return $this->observacion;
    }

}
