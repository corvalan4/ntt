<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProductoConsignacionRepository")
 * @ORM\Table(name="productoconsignacion")
 */
class ProductoConsignacion {

    const PRODUCTO_CONSIGNACION_OK = 'UTILIZADO';
    const PRODUCTO_CONSIGNACION_ERROR = 'DEVUELTO';
    const PRODUCTO_CONSIGNACION_IN_CLIENT = 'EN BANCO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observaciones;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $paciente;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechauso;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosConsignacion")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Stock", inversedBy="stocksConsignacion")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @ORM\ManyToOne(targetEntity="Consignacion", inversedBy="productosConsignacion", cascade={"persist"})
     * @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id")
     */
    protected $consignacion;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="vendedor_id", referencedColumnName="id")
     */
    protected $vendedor;
    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    protected $cliente;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $estado = ProductoConsignacion::PRODUCTO_CONSIGNACION_IN_CLIENT;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'consignacion';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ProductoConsignacion
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ProductoConsignacion
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ProductoConsignacion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return ProductoConsignacion
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto 
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set consignacion
     *
     * @param \AppBundle\Entity\Consignacion $consignacion
     * @return ProductoConsignacion
     */
    public function setConsignacion(\AppBundle\Entity\Consignacion $consignacion = null) {
        $this->consignacion = $consignacion;

        return $this;
    }

    /**
     * Get consignacion
     *
     * @return \AppBundle\Entity\Consignacion 
     */
    public function getConsignacion() {
        return $this->consignacion;
    }

    /**
     * Set stock
     *
     * @param \AppBundle\Entity\Stock $stock
     * @return ProductoConsignacion
     */
    public function setStock(\AppBundle\Entity\Stock $stock = null) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \AppBundle\Entity\Stock 
     */
    public function getStock() {
        return $this->stock;
    }



    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\ClienteProveedor $cliente
     * @return ProductoConsignacion
     */
    public function setCliente(\AppBundle\Entity\ClienteProveedor $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\ClienteProveedor 
     */
    public function getCliente()
    {
        return $this->cliente;
    }



    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return ProductoConsignacion
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fechauso
     *
     * @param \DateTime $fechauso
     * @return ProductoConsignacion
     */
    public function setFechauso($fechauso)
    {
        $this->fechauso = $fechauso;

        return $this;
    }

    /**
     * Get fechauso
     *
     * @return \DateTime 
     */
    public function getFechauso()
    {
        return $this->fechauso;
    }

    /**
     * Set vendedor
     *
     * @param \AppBundle\Entity\Usuario $vendedor
     * @return ProductoConsignacion
     */
    public function setVendedor(\AppBundle\Entity\Usuario $vendedor = null)
    {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * Set paciente
     *
     * @param string $paciente
     * @return ProductoConsignacion
     */
    public function setPaciente($paciente)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return string 
     */
    public function getPaciente()
    {
        return $this->paciente;
    }
}
