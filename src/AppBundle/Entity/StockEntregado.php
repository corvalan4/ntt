<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StockEntregadoRepository")
 * @ORM\Table(name="stockentregado")
 */
class StockEntregado{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Pedido", inversedBy="stocksEntregado")
	* @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
	*/
    protected $pedido;

	/**
	* @ORM\ManyToOne(targetEntity="StockPedido", inversedBy="stocksEntregado")
	* @ORM\JoinColumn(name="stockpedido_id", referencedColumnName="id")
	*/
    protected $stockpedido;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaEntrega;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
        $this->fechaEntrega = new \DateTime('NOW');
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return StockEntregado
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     * @return StockEntregado
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return StockEntregado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     * @return StockEntregado
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set stockpedido
     *
     * @param \AppBundle\Entity\StockPedido $stockpedido
     * @return StockEntregado
     */
    public function setStockpedido(\AppBundle\Entity\StockPedido $stockpedido = null)
    {
        $this->stockpedido = $stockpedido;

        return $this;
    }

    /**
     * Get stockpedido
     *
     * @return \AppBundle\Entity\StockPedido
     */
    public function getStockpedido()
    {
        return $this->stockpedido;
    }
}
