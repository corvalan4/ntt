<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\DetallePedidoRepository")
 * @ORM\Table(name="detallepedido")
 */
class DetallePedido{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
		
	/**
	* @ORM\ManyToOne(targetEntity="Pedido", inversedBy="detallespedido")
	* @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
	*/
    protected $pedido;
		
	/**
	* @ORM\ManyToOne(targetEntity="Producto", inversedBy="detallespedido")
	* @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
	*/
    protected $producto;

    /**
     * @ORM\Column(type="float")
     */
    protected $precio;

    /**
     * @ORM\Column(type="float")
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $productodesc;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/ 
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return DetallePedido
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return DetallePedido
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set productodesc
     *
     * @param string $productodesc
     * @return DetallePedido
     */
    public function setProductodesc($productodesc)
    {
        $this->productodesc = $productodesc;
    
        return $this;
    }

    /**
     * Get productodesc
     *
     * @return string 
     */
    public function getProductodesc()
    {
        return $this->productodesc;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return DetallePedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     * @return DetallePedido
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;
    
        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return DetallePedido
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
