<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PagoFacturaRepository")
 * @ORM\Table(name="pagofactura")
 */
class PagoFactura {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="pagosfactura")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="cobranzasfactura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="cobranzasfactura")
     * @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
     */
    protected $notaCreditoDebito;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return "$ " . $this->getImporte();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return CobranzaFactura
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CobranzaFactura
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return CobranzaFactura
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return CobranzaFactura
     */
    public function setNotaCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null) {
        $this->notaCreditoDebito = $notaCreditoDebito;

        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito
     */
    public function getNotaCreditoDebito() {
        return $this->notaCreditoDebito;
    }

    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return PagoFactura
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago
     */
    public function getPago() {
        return $this->pago;
    }

}
