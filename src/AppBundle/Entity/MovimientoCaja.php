<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MovimientoCajaRepository")
 * @ORM\Table(name="movimientocaja")
 */
class MovimientoCaja {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\ManyToOne(targetEntity="Caja", inversedBy="movimientosCaja")
     * @ORM\JoinColumn(name="caja_id", referencedColumnName="id")
     */
    protected $caja;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToOne(targetEntity="Cobranza", inversedBy="movimientoCaja")
     * @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id", nullable=true)
     */
    protected $cobranza;

    /**
     * @ORM\OneToOne(targetEntity="Pago", inversedBy="movimientoCaja")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\OneToOne(targetEntity="Ajuste", inversedBy="movimientoCaja")
     * @ORM\JoinColumn(name="ajuste_id", referencedColumnName="id", nullable=true)
     */
    protected $ajuste;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get FechaActual
     *
     *
     */
    public function getFecha() {
        $cobranza = $this->getCobranza();
        $pago = $this->getPago();
        $ajuste = $this->getAjuste();
        if (isset($cobranza)) {
            return $cobranza->getFecha();
        }
        if (isset($pago)) {
            return $pago->getFecha();
        }
        if (isset($ajuste)) {
            return $ajuste->getFecha();
        }
        return false;
    }

    /**
     * Get Documento
     *
     *
     */
    public function getDocumento() {
        $cobranza = $this->getCobranza();
        $pago = $this->getPago();
        $ajuste = $this->getAjuste();
        if (isset($cobranza)) {
            return $cobranza;
        }
        if (isset($pago)) {
            return $pago;
        }
        if (isset($ajuste)) {
            return $ajuste;
        }
        return false;
    }

     /**
     * Get importe
     *
     * @return float
     */
    public function getImporte() {        
        return $this->importe;;
    }

    public function getStyle() {
        if ($this->getImporte() > 0) {
            return 'green';
        } else if ($this->getImporte() < 0) {
            return 'red';
        } else {
            return 'black';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return MovimientoCaja
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set caja
     *
     * @param \AppBundle\Entity\Caja $caja
     * @return MovimientoCaja
     */
    public function setCaja(\AppBundle\Entity\Caja $caja = null) {
        $this->caja = $caja;

        return $this;
    }

    /**
     * Get caja
     *
     * @return \AppBundle\Entity\Caja 
     */
    public function getCaja() {
        return $this->caja;
    }

    /**
     * Set cobranza
     *
     * @param \AppBundle\Entity\Cobranza $cobranza
     * @return MovimientoCaja
     */
    public function setCobranza(\AppBundle\Entity\Cobranza $cobranza = null) {
        $this->cobranza = $cobranza;

        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \AppBundle\Entity\Cobranza 
     */
    public function getCobranza() {
        return $this->cobranza;
    }

    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return MovimientoCaja
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago 
     */
    public function getPago() {
        return $this->pago;
    }

    /**
     * Set ajuste
     *
     * @param \AppBundle\Entity\Ajuste $ajuste
     * @return MovimientoCaja
     */
    public function setAjuste(\AppBundle\Entity\Ajuste $ajuste = null) {
        $this->ajuste = $ajuste;

        return $this;
    }

    /**
     * Get ajuste
     *
     * @return \AppBundle\Entity\Ajuste 
     */
    public function getAjuste() {
        return $this->ajuste;
    }


    /**
     * Set importe
     *
     * @param float $importe
     * @return MovimientoCaja
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }
}
