<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ClienteProveedorSucursalRepository")
 * @ORM\Table(name="clienteproveedorsucursal")
 */
class ClienteProveedorSucursal{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="clienteproveedorsucursales")
	* @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
	*/
    protected $clienteproveedor;

	/**
	* @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="clienteproveedorsucursales")
	* @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
	*/
    protected $sucursal;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ClienteProveedorSucursal
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set clienteproveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteproveedor
     * @return ClienteProveedorSucursal
     */
    public function setClienteproveedor(\AppBundle\Entity\ClienteProveedor $clienteproveedor = null)
    {
        $this->clienteproveedor = $clienteproveedor;
    
        return $this;
    }

    /**
     * Get clienteproveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteproveedor()
    {
        return $this->clienteproveedor;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return ClienteProveedorSucursal
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null)
    {
        $this->sucursal = $sucursal;
    
        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }
}
