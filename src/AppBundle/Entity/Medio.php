<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MedioRepository")
 * @ORM\Table(name="medio")
 */
class Medio {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="MedioDocumento", mappedBy="medio")
     */
    protected $medioDocumentos;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion

     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Medio
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Add medioDocumentos
     *
     * @param \AppBundle\Entity\MedioDocumento $medioDocumentos
     * @return Medio
     */
    public function addMedioDocumento(\AppBundle\Entity\MedioDocumento $medioDocumentos)
    {
        $this->medioDocumentos[] = $medioDocumentos;
    
        return $this;
    }

    /**
     * Remove medioDocumentos
     *
     * @param \AppBundle\Entity\MedioDocumento $medioDocumentos
     */
    public function removeMedioDocumento(\AppBundle\Entity\MedioDocumento $medioDocumentos)
    {
        $this->medioDocumentos->removeElement($medioDocumentos);
    }

    /**
     * Get medioDocumentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedioDocumentos()
    {
        return $this->medioDocumentos;
    }
}
