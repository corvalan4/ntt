<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * GastoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GastoRepository extends EntityRepository {

    public function filtro($desde, $hasta, $tipoGasto, $proveedor, $unidad = null) {

        $queryStr = "
        SELECT g
        FROM AppBundle:Gasto g
        WHERE g.fecha >= :desde AND g.fecha <= :hasta ";

        if (!empty($unidad)) {
            $queryStr .= "AND g.unidadnegocio = $unidad ";
        }
        if (!empty($tipoGasto)) {
            $queryStr .= "AND g.tipoGasto = $tipoGasto ";
        }
        if (!empty($proveedor)) {
            $queryStr .= "AND g.clienteProveedor = $proveedor ";
        }
        $queryStr .= " ORDER BY g.id DESC";

        $query = $this->getEntityManager()->createQuery($queryStr)
                ->setParameter(':desde', $desde)
                ->setParameter(':hasta', $hasta);


        return $query->getResult();
    }

    /*
     * Filtro de búsqueda 
     */

    public function filtroBusquedaInformeGasto($fechaDesde, $fechaHasta, $proveedor, $medio, $unidadNegocio) {
        $query = $this->createQueryBuilder('e')->where('e.estado=:estado');
        $query->setParameter(':estado', 'A');
        if (isset($fechaDesde) && $fechaDesde != '') {           
            $query->andWhere(':fechaDesde <= e.fecha');
            $query->setParameter(':fechaDesde', $fechaDesde);
        }
        if (isset($fechaHasta) && $fechaHasta != '') {        
            $query->andWhere('e.fecha <=  :fechaHasta');
            $query->setParameter(':fechaHasta', $fechaHasta);
        }

        if (isset($proveedor) && $proveedor != '') {
            $query->andWhere('e.clienteProveedor =:proveedor');
            $query->setParameter(':proveedor', $proveedor);
        }

        if (isset($unidadNegocio) && $unidadNegocio != '') {
            $query->andWhere('e.unidadnegocio =:unidadNegocio');
            $query->setParameter(':unidadNegocio', $unidadNegocio);
        }

        if (isset($medio) && $medio != '') {
            if ($medio == 'cheque') {
                $query->leftJoin('e.cheques', 'c');
            } else {

                $query->leftJoin('e.medios', 'm');
                $query->andWhere('m.medio =:medio');
                $query->setParameter(':medio', $medio);
            }
        }
        $query->orderBy('e.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    /*
     * Filtro de búsqueda 
     */

    public function totalInformeGasto($fechaDesde, $fechaHasta, $medio, $clienteProveedor, $tipoGasto, $unidadNegocio) {
        $query = $this->createQueryBuilder('e')->where('e.estado=:estado');
        $query->setParameter(':estado', 'A');
        $query->select('SUM(e.importe) AS total');

        if (isset($fechaDesde) && $fechaDesde != '') {          
            $query->andWhere(':fechaDesde <= e.fecha');
            $query->setParameter(':fechaDesde', $fechaDesde);
        }
        if (isset($fechaHasta) && $fechaHasta != '') {           
            $query->andWhere('e.fecha <=  :fechaHasta');
            $query->setParameter(':fechaHasta', $fechaHasta);
        }

        if (isset($clienteProveedor) && $clienteProveedor != '') {
            $query->andWhere('e.clienteProveedor =:clienteProveedor');
            $query->setParameter(':clienteProveedor', $clienteProveedor);
        }
        if (isset($tipoGasto) && $tipoGasto != '') {
            $query->andWhere('e.tipoGasto =:tipoGasto');
            $query->setParameter(':tipoGasto', $tipoGasto);
        }
        if (isset($unidadNegocio) && $unidadNegocio != '') {
            $query->andWhere('e.unidadnegocio =:unidadNegocio');
            $query->setParameter(':unidadNegocio', $unidadNegocio);
        }
        if (isset($medio) && $medio != '') {
            if ($medio == 'cheque') {
                $query->leftJoin('e.cheques', 'c');
            } else {
                $query->leftJoin('e.medios', 'm');
                $query->andWhere('m.medio =:medio');
                $query->setParameter(':medio', $medio);
            }
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function getUltimoGasto( $proveedor,$unidad = NULL) {

        $query = $this->createQueryBuilder('f');
        $query->leftJoin('f.clienteProveedor', 'c');
        $query->select('f, MAX(f.fecha) AS fechaGasto');
        $query->where('1 = 1');

        if (isset($unidad) && $unidad != '') {
            $query->andWhere('f.unidadNegocio = :unidadNegocio')->setParameter('unidadNegocio', $unidad);
        }
                if (isset($proveedor) && $proveedor != '') {
                $query->andWhere('f.clienteProveedor = :clienteProveedor')->setParameter('clienteProveedor', $proveedor);
        }
       
        $query->groupBy('f.clienteProveedor');

        $query->orderBy('c.razonSocial', 'DESC');

        return $query->getQuery()->getResult();
    }

}
