<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\LocalidadRepository")
 * @ORM\Table(name="localidad")
 */
class Localidad{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

	/**
	* @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="localidad" )
	*/
	protected $clientesProveedores;

	/**
	* @ORM\ManyToOne(targetEntity="Provincia", inversedBy="localidades")
	* @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
	*/
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getDescripcion();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoRetencion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add clientesProveedores
     *
     * @param \AppBundle\Entity\ClienteProveedor $clientesProveedores
     * @return Localidad
     */
    public function addClientesProveedore(\AppBundle\Entity\ClienteProveedor $clientesProveedores)
    {
        $this->clientesProveedores[] = $clientesProveedores;
    
        return $this;
    }

    /**
     * Remove clientesProveedores
     *
     * @param \AppBundle\Entity\ClienteProveedor $clientesProveedores
     */
    public function removeClientesProveedore(\AppBundle\Entity\ClienteProveedor $clientesProveedores)
    {
        $this->clientesProveedores->removeElement($clientesProveedores);
    }

    /**
     * Get clientesProveedores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientesProveedores()
    {
        return $this->clientesProveedores;
    }

    /**
     * Set provincia
     *
     * @param \AppBundle\Entity\Provincia $provincia
     * @return Localidad
     */
    public function setProvincia(\AppBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \AppBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}
