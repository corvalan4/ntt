<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Stock;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BalanceRepository")
 * @ORM\Table(name="balance")
 */
class Balance {
    const TYPE_POSITIVE = 'TYPE_POSITIVE';
    const TYPE_NEGATIVE = 'TYPE_NEGATIVE';
    const VERIFARMA_ALTA = '#VF_ALTALOTE';
    const VERIFARMA_DESPACHAR = '#VF_DESPACHAR';
    const VERIFARMA_DEVOLUCION = '#VF_DEVOLUCION';
    const VERIFARMA_IMPLANTACION = '#VF_IMPLANTACION';
    const CREACION_BALANCE  = 'Creación de Balance';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $verifarma_status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $amountBalance;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Stock")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @ORM\ManyToOne(targetEntity="Factura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito")
     * @ORM\JoinColumn(name="notacredito_id", referencedColumnName="id")
     */
    protected $notacredito;

    /**
     * @ORM\ManyToOne(targetEntity="Consignacion")
     * @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id")
     */
    protected $consignacion;

    /**
     * @ORM\ManyToOne(targetEntity="RemitoOficial")
     * @ORM\JoinColumn(name="remitooficial_id", referencedColumnName="id")
     */
    protected $remitooficial;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    public function __construct() {
        $this->created = new \DateTime("NOW");
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(?string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getAmount(): ?float {
        return $this->amount;
    }

    public function setAmount(?float $amount): self {
        $this->amount = $amount;

        return $this;
    }

    public function getPrice(): ?float {
        return $this->price;
    }

    public function setPrice(?float $price): self {
        $this->price = $price;

        return $this;
    }

    public function getAmountBalance(): ?float {
        return $this->amountBalance;
    }

    public function setAmountBalance(?float $amountBalance): self {
        $this->amountBalance = $amountBalance;

        return $this;
    }

    public function getUser(): ?Usuario {
        return $this->user;
    }

    public function setUser(?Usuario $user): self {
        $this->user = $user;

        return $this;
    }

    public function getStock(): ?Stock {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return Balance
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura 
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Balance
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getTipoMovimiento() {
        if ($this->getType()=='TYPE_POSITIVE'){
            return 'Incremento';
        }else {
            return 'Decremento';
        }        
    }


    /**
     * Set verifarma_status
     *
     * @param string $verifarmaStatus
     * @return Balance
     */
    public function setVerifarmaStatus($verifarmaStatus)
    {
        $this->verifarma_status = $verifarmaStatus;

        return $this;
    }

    /**
     * Get verifarma_status
     *
     * @return string 
     */
    public function getVerifarmaStatus()
    {
        return $this->verifarma_status;
    }

    /**
     * Set consignacion
     *
     * @param \AppBundle\Entity\Consignacion $consignacion
     * @return Balance
     */
    public function setConsignacion(\AppBundle\Entity\Consignacion $consignacion = null)
    {
        $this->consignacion = $consignacion;

        return $this;
    }

    /**
     * Get consignacion
     *
     * @return \AppBundle\Entity\Consignacion 
     */
    public function getConsignacion()
    {
        return $this->consignacion;
    }

    /**
     * Set remitooficial
     *
     * @param \AppBundle\Entity\RemitoOficial $remitooficial
     * @return Balance
     */
    public function setRemitooficial(\AppBundle\Entity\RemitoOficial $remitooficial = null)
    {
        $this->remitooficial = $remitooficial;

        return $this;
    }

    /**
     * Get remitooficial
     *
     * @return \AppBundle\Entity\RemitoOficial 
     */
    public function getRemitooficial()
    {
        return $this->remitooficial;
    }

    /**
     * Set notacredito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notacredito
     * @return Balance
     */
    public function setNotacredito(\AppBundle\Entity\NotaCreditoDebito $notacredito = null)
    {
        $this->notacredito = $notacredito;

        return $this;
    }

    /**
     * Get notacredito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito 
     */
    public function getNotacredito()
    {
        return $this->notacredito;
    }
}
