<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProductoRemitoRepository")
 * @ORM\Table(name="productoremito")
 */
class ProductoRemito {

    const PRODUCTO_REMITO_INVOICED = 'FACTURADO';
    const PRODUCTO_REMITO_IN_CLIENT = 'EN CLIENTE';
    const PRODUCTO_REMITO_CANCEL = 'CANCELADO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $renglon;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosConsignacion")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="ProductoConsignacion")
     * @ORM\JoinColumn(name="productoconsignacion_id", referencedColumnName="id")
     */
    protected $productoconsignacion;

    /**
     * @ORM\ManyToOne(targetEntity="Stock", inversedBy="stocksConsignacion")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @ORM\ManyToOne(targetEntity="RemitoOficial", inversedBy="productosRemito", cascade={"persist"})
     * @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id")
     */
    protected $remitooficial;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $vendedor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $estado = ProductoRemito::PRODUCTO_REMITO_IN_CLIENT;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'producto - remito oficial';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return ProductoRemito
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ProductoRemito
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ProductoRemito
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return ProductoRemito
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto 
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set stock
     *
     * @param \AppBundle\Entity\Stock $stock
     * @return ProductoRemito
     */
    public function setStock(\AppBundle\Entity\Stock $stock = null) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \AppBundle\Entity\Stock 
     */
    public function getStock() {
        return $this->stock;
    }

    /**
     * Set remitooficial
     *
     * @param \AppBundle\Entity\RemitoOficial $remitooficial
     * @return ProductoRemito
     */
    public function setRemitooficial(\AppBundle\Entity\RemitoOficial $remitooficial = null) {
        $this->remitooficial = $remitooficial;

        return $this;
    }

    /**
     * Get remitooficial
     *
     * @return \AppBundle\Entity\RemitoOficial 
     */
    public function getRemitooficial() {
        return $this->remitooficial;
    }


    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\ClienteProveedor $cliente
     * @return ProductoRemito
     */
    public function setCliente(\AppBundle\Entity\ClienteProveedor $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\ClienteProveedor 
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Set vendedor
     *
     * @param \AppBundle\Entity\Usuario $vendedor
     * @return ProductoRemito
     */
    public function setVendedor(\AppBundle\Entity\Usuario $vendedor = null) {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getVendedor() {
        return $this->vendedor;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ProductoRemito
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }


    /**
     * Set renglon
     *
     * @param integer $renglon
     * @return ProductoRemito
     */
    public function setRenglon($renglon)
    {
        $this->renglon = $renglon;

        return $this;
    }

    /**
     * Get renglon
     *
     * @return integer 
     */
    public function getRenglon()
    {
        return $this->renglon;
    }

    /**
     * Set productoconsignacion
     *
     * @param \AppBundle\Entity\ProductoConsignacion $productoconsignacion
     * @return ProductoRemito
     */
    public function setProductoconsignacion(\AppBundle\Entity\ProductoConsignacion $productoconsignacion = null)
    {
        $this->productoconsignacion = $productoconsignacion;

        return $this;
    }

    /**
     * Get productoconsignacion
     *
     * @return \AppBundle\Entity\ProductoConsignacion 
     */
    public function getProductoconsignacion()
    {
        return $this->productoconsignacion;
    }
}
