<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PedidoRepository")
 * @ORM\Table(name="pedido")
 */
class Pedido{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="pedidos")
	* @ORM\JoinColumn(name="unidadorigen_id", referencedColumnName="id")
	*/
    protected $unidadOrigen;

	/**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="pedidos")
	* @ORM\JoinColumn(name="unidaddestino_id", referencedColumnName="id")
	*/
    protected $unidadDestino;

	/**
	* @ORM\ManyToOne(targetEntity="Usuario", inversedBy="pedidos")
	* @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
	*/
    protected $usuario;

    /**
     * @ORM\Column(type="string", length=3000, nullable=true)
     */
    protected $observacion;

    /**
	 * @ORM\Column(type="datetime")
     */
    protected $fecha;

	/**
	* @ORM\OneToMany(targetEntity="StockPedido", mappedBy="pedido", cascade={"persist", "remove"} )
	*/
	protected $stocksPedidos;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
		$this->stocksPedidos = new ArrayCollection();
	}


	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
        return "Pedido ".$this->id;
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observacion
     *
     * @param float $observacion
     * @return Pedido
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return float
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Pedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }


    public function getEstadostr()
    {
        return ( $this->estado == 'E' ) ? '<strong style="color:green;">Entregado<strong>' : (( $this->estado == 'P' ) ? '<strong style="color:orange;">En Proceso<strong>' : (( $this->estado == 'C' ) ? '<strong style="color:red;">Cancelado<strong>' : '<strong>Activo<strong>'));
    }

    /**
     * Set unidadOrigen
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadOrigen
     * @return Pedido
     */
    public function setUnidadOrigen(\AppBundle\Entity\UnidadNegocio $unidadOrigen = null)
    {
        $this->unidadOrigen = $unidadOrigen;

        return $this;
    }

    /**
     * Get unidadOrigen
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadOrigen()
    {
        return $this->unidadOrigen;
    }

    /**
     * Set unidadDestino
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadDestino
     * @return Pedido
     */
    public function setUnidadDestino(\AppBundle\Entity\UnidadNegocio $unidadDestino = null)
    {
        $this->unidadDestino = $unidadDestino;

        return $this;
    }

    /**
     * Get unidadDestino
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadDestino()
    {
        return $this->unidadDestino;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return Pedido
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add stocksPedidos
     *
     * @param \AppBundle\Entity\StockPedido $stocksPedidos
     * @return Pedido
     */
    public function addStocksPedido(\AppBundle\Entity\StockPedido $stocksPedidos)
    {
        $this->stocksPedidos[] = $stocksPedidos;

        return $this;
    }

    /**
     * Remove stocksPedidos
     *
     * @param \AppBundle\Entity\StockPedido $stocksPedidos
     */
    public function removeStocksPedido(\AppBundle\Entity\StockPedido $stocksPedidos)
    {
        $this->stocksPedidos->removeElement($stocksPedidos);
    }

    /**
     * Get stocksPedidos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocksPedidos()
    {
        return $this->stocksPedidos;
    }
}
