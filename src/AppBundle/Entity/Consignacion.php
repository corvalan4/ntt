<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ConsignacionRepository")
 * @ORM\Table(name="consignacion")
 */
class Consignacion {

    const CONSIGNACION_OK = 'COMPLETA';
    const CONSIGNACION_ERROR = 'DEVUELTA';
    const CONSIGNACION_PARCIAL = 'PARCIAL EN PROCESO';
    const CONSIGNACION_MIXTA = 'MIXTA';
    const CONSIGNACION_IN_PROCESS = 'EN PROCESO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaVencimiento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $numero;

    /**
     * @ORM\OneToMany(targetEntity="ProductoConsignacion", mappedBy="consignacion", cascade={"persist"})
     */
    protected $productosConsignacion;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="consignaciones")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Factura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="consignaciones")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="vendedor_id", referencedColumnName="id",nullable=true)
     */
    protected $vendedor;
   

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $estado = Consignacion::CONSIGNACION_IN_PROCESS;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="consignacion")
     */
    protected $pagosAsignacion;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosConsignacion = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $nro = $this->numero ? $this->numero : $this->id;
        return "CONS" . str_pad($nro, 5, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Consignacion
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return Consignacion
     */
    public function setFechaVencimiento($fechaVencimiento) {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento() {
        return $this->fechaVencimiento;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Consignacion
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Consignacion
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Consignacion
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Consignacion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosConsignacion
     *
     * @param \AppBundle\Entity\ProductoConsignacion $productosConsignacion
     * @return Consignacion
     */
    public function addProductosConsignacion(\AppBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion[] = $productosConsignacion;

        return $this;
    }

    /**
     * Remove productosConsignacion
     *
     * @param \AppBundle\Entity\ProductoConsignacion $productosConsignacion
     */
    public function removeProductosConsignacion(\AppBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion->removeElement($productosConsignacion);
    }

    /**
     * Get productosConsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosConsignacion() {
        return $this->productosConsignacion;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Consignacion
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Consignacion
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return Consignacion
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion() {
        return $this->pagosAsignacion;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Consignacion
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return Consignacion
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal 
     */
    public function getSucursal() {
        return $this->sucursal;
    }


    /**
     * Set vendedor
     *
     * @param \AppBundle\Entity\Usuario $vendedor
     * @return Consignacion
     */
    public function setVendedor(\AppBundle\Entity\Usuario $vendedor = null)
    {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero): void
    {
        $this->numero = $numero;
    }



    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return Consignacion
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }
}
