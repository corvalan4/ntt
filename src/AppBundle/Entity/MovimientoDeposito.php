<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MovimientoDepositoRepository")
 * @ORM\Table(name="movimientodeposito")
 */
class MovimientoDeposito {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Motivo")
     * @ORM\JoinColumn(name="motivo_id", referencedColumnName="id")
     */
    protected $motivo;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $deposito_desde;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $deposito_hasta;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @ORM\ManyToOne(targetEntity="Stock")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /** ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->created = new \DateTime('NOW');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return MovimientoDeposito
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set deposito_desde
     *
     * @param string $depositoDesde
     * @return MovimientoDeposito
     */
    public function setDepositoDesde($depositoDesde)
    {
        $this->deposito_desde = $depositoDesde;

        return $this;
    }

    /**
     * Get deposito_desde
     *
     * @return string 
     */
    public function getDepositoDesde()
    {
        return $this->deposito_desde;
    }

    /**
     * Set deposito_hasta
     *
     * @param string $depositoHasta
     * @return MovimientoDeposito
     */
    public function setDepositoHasta($depositoHasta)
    {
        $this->deposito_hasta = $depositoHasta;

        return $this;
    }

    /**
     * Get deposito_hasta
     *
     * @return string 
     */
    public function getDepositoHasta()
    {
        return $this->deposito_hasta;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return MovimientoDeposito
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return MovimientoDeposito
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set motivo
     *
     * @param \AppBundle\Entity\Motivo $motivo
     * @return MovimientoDeposito
     */
    public function setMotivo(\AppBundle\Entity\Motivo $motivo = null)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo
     *
     * @return \AppBundle\Entity\Motivo 
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set stock
     *
     * @param \AppBundle\Entity\Stock $stock
     * @return MovimientoDeposito
     */
    public function setStock(\AppBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \AppBundle\Entity\Stock 
     */
    public function getStock()
    {
        return $this->stock;
    }
}
