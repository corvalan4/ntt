<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ClienteProveedorRepository")
 * @ORM\Table(name="clienteproveedor")
 */
class ClienteProveedor {

    const CLIENTE = 'Cliente';
    const PROVEEDOR = 'Proveedor';
    const CLIENTE_COD = '1';
    const PROVEEDOR_COD = '2';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idanterior;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $razonSocial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $denominacion;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $dni;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $domiciliocomercial;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $celular;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $saldo = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $porcentajeiva;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $clienteProveedor = self::CLIENTE_COD;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $gln;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigo;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $informacion_adicional;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $es_banco = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigopostal;

    /**
     * @ORM\OneToMany(targetEntity="Sucursal", mappedBy="clienteproveedor")
     */
    protected $sucursales;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="clienteProveedor")
     */
    protected $pagosAsignacion;

    /**
     * @ORM\OneToMany(targetEntity="Cobranza", mappedBy="clienteProveedor")
     */
    protected $cobranzas;

    /**
     * @ORM\OneToMany(targetEntity="Gasto", mappedBy="clienteProveedor")
     */
    protected $gastos;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaprecio_id", referencedColumnName="id")
     */
    protected $listaprecio;

    /**
     * @ORM\ManyToOne(targetEntity="CondicionIva", inversedBy="clientesProveedores")
     * @ORM\JoinColumn(name="condicioniva_id", referencedColumnName="id")
     */
    protected $condicioniva;

    /**
     * @ORM\ManyToOne(targetEntity="Localidad", inversedBy="clientesproveedores")
     * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
      protected $localidad;
     */

    /**
     * @ORM\ManyToOne(targetEntity="TipoProveedor", inversedBy="clientesProveedores")
     * @ORM\JoinColumn(name="tipoproveedor_id", referencedColumnName="id")
     */
    protected $tipoProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="Pago", mappedBy="clienteProveedor")
     */
    protected $pagos;

    /**
     * @ORM\OneToMany(targetEntity="Factura", mappedBy="clienteProveedor")
     */
    protected $facturas;

    /**
     * @ORM\OneToMany(targetEntity="NotaCreditoDebito", mappedBy="clienteProveedor")
     */
    protected $notasCreditoDebito;

    /**
     * @ORM\OneToMany(targetEntity="Consignacion", mappedBy="clienteProveedor")
     */
    protected $consignaciones;

    /**
     * @ORM\OneToMany(targetEntity="Presupuesto", mappedBy="clienteProveedor")
     */
    protected $presupuestos;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->cobranzas = new ArrayCollection();
        $this->gastos = new ArrayCollection();
        $this->pagos = new ArrayCollection();
        $this->sucursales = new ArrayCollection();
        $this->pagosAsignacion = new ArrayCollection();
        $this->facturas = new ArrayCollection();
        $this->notasCreditoDebito = new ArrayCollection();
        $this->consignaciones = new ArrayCollection();
        $this->presupuestos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getRazonSocial() . " - " . $this->getDenominacion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return ClienteProveedor
     */
    public function setRazonSocial($razonSocial) {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial() {
        return $this->razonSocial;
    }

    /**
     * Set denominacion
     *
     * @param string $denominacion
     * @return ClienteProveedor
     */
    public function setDenominacion($denominacion) {
        $this->denominacion = $denominacion;

        return $this;
    }

    /**
     * Get denominacion
     *
     * @return string 
     */
    public function getDenominacion() {
        return $this->denominacion;
    }

    /**
     * Set cuit
     *
     * @param integer $cuit
     * @return ClienteProveedor
     */
    public function setCuit($cuit) {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return integer
     */
    public function getCuit() {
        return $this->cuit;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return ClienteProveedor
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set domiciliocomercial
     *
     * @param string $domiciliocomercial
     * @return ClienteProveedor
     */
    public function setDomiciliocomercial($domiciliocomercial) {
        $this->domiciliocomercial = $domiciliocomercial;

        return $this;
    }

    /**
     * Get domiciliocomercial
     *
     * @return string 
     */
    public function getDomiciliocomercial() {
        return $this->domiciliocomercial;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return ClienteProveedor
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return ClienteProveedor
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular() {
        return $this->celular;
    }

    /**
     * Set saldo
     *
     * @param float $saldo
     * @return ClienteProveedor
     */
    public function setSaldo($saldo) {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get saldo
     *
     * @return float 
     */
    public function getSaldo() {
        return $this->saldo;
    }

    /**
     * Set coeficientePrecio
     *
     * @param float $coeficientePrecio
     * @return ClienteProveedor
     */
    public function setCoeficientePrecio($coeficientePrecio) {
        $this->coeficientePrecio = $coeficientePrecio;

        return $this;
    }

    /**
     * Get coeficientePrecio
     *
     * @return float 
     */
    public function getCoeficientePrecio() {
        return $this->coeficientePrecio;
    }

    /**
     * Set porcentajeiva
     *
     * @param float $porcentajeiva
     * @return ClienteProveedor
     */
    public function setPorcentajeiva($porcentajeiva) {
        $this->porcentajeiva = $porcentajeiva;

        return $this;
    }

    /**
     * Get porcentajeiva
     *
     * @return float 
     */
    public function getPorcentajeiva() {
        return $this->porcentajeiva;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return ClienteProveedor
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return ClienteProveedor
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set clienteProveedor
     *
     * @param string $clienteProveedor
     * @return ClienteProveedor
     */
    public function setClienteProveedor($clienteProveedor) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return string 
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ClienteProveedor
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add sucursales
     *
     * @param \AppBundle\Entity\Sucursal $sucursales
     * @return ClienteProveedor
     */
    public function addSucursale(\AppBundle\Entity\Sucursal $sucursales) {
        $this->sucursales[] = $sucursales;

        return $this;
    }

    /**
     * Remove sucursales
     *
     * @param \AppBundle\Entity\Sucursal $sucursales
     */
    public function removeSucursale(\AppBundle\Entity\Sucursal $sucursales) {
        $this->sucursales->removeElement($sucursales);
    }

    /**
     * Get sucursales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSucursales() {
        return $this->sucursales;
    }

    /**
     * Add cobranzas
     *
     * @param \AppBundle\Entity\Cobranza $cobranzas
     * @return ClienteProveedor
     */
    public function addCobranza(\AppBundle\Entity\Cobranza $cobranzas) {
        $this->cobranzas[] = $cobranzas;

        return $this;
    }

    /**
     * Remove cobranzas
     *
     * @param \AppBundle\Entity\Cobranza $cobranzas
     */
    public function removeCobranza(\AppBundle\Entity\Cobranza $cobranzas) {
        $this->cobranzas->removeElement($cobranzas);
    }

    /**
     * Get cobranzas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzas() {
        return $this->cobranzas;
    }

    /**
     * Set condicioniva
     *
     * @param \AppBundle\Entity\CondicionIva $condicioniva
     * @return ClienteProveedor
     */
    public function setCondicioniva(\AppBundle\Entity\CondicionIva $condicioniva = null) {
        $this->condicioniva = $condicioniva;

        return $this;
    }

    /**
     * Get condicioniva
     *
     * @return \AppBundle\Entity\CondicionIva
     */
    public function getCondicioniva() {
        return $this->condicioniva;
    }

    /**
     * Set localidad
     *
     * @param \AppBundle\Entity\Localidad $localidad
     * @return ClienteProveedor
     */
    public function setLocalidad($localidad) {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return \AppBundle\Entity\Localidad
     */
    public function getLocalidad() {
        return $this->localidad;
    }

    /**
     * Set tipoProveedor
     *
     * @param \AppBundle\Entity\TipoProveedor $tipoProveedor
     * @return ClienteProveedor
     */
    public function setTipoProveedor(\AppBundle\Entity\TipoProveedor $tipoProveedor = null) {
        $this->tipoProveedor = $tipoProveedor;

        return $this;
    }

    /**
     * Get tipoProveedor
     *
     * @return \AppBundle\Entity\TipoProveedor
     */
    public function getTipoProveedor() {
        return $this->tipoProveedor;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return ClienteProveedor
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion() {
        return $this->pagosAsignacion;
    }

    /**
     * @return mixed
     */
    public function getIdanterior() {
        return $this->idanterior;
    }

    /**
     * @param mixed $idanterior
     */
    public function setIdanterior($idanterior) {
        $this->idanterior = $idanterior;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     * @return ClienteProveedor
     */
    public function setProvincia($provincia) {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string 
     */
    public function getProvincia() {
        return $this->provincia;
    }

    /**
     * Set codigopostal
     *
     * @param string $codigopostal
     * @return ClienteProveedor
     */
    public function setCodigopostal($codigopostal) {
        $this->codigopostal = $codigopostal;

        return $this;
    }

    /**
     * Get codigopostal
     *
     * @return string 
     */
    public function getCodigopostal() {
        return $this->codigopostal;
    }

    /**
     * Add pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     * @return ClienteProveedor
     */
    public function addPago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     */
    public function removePago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }

    /**
     * Add facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     * @return ClienteProveedor
     */
    public function addFactura(\AppBundle\Entity\Factura $facturas) {
        $this->facturas[] = $facturas;

        return $this;
    }

    /**
     * Remove facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     */
    public function removeFactura(\AppBundle\Entity\Factura $facturas) {
        $this->facturas->removeElement($facturas);
    }

    /**
     * Get facturas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFacturas() {
        return $this->facturas;
    }

    /**
     * Add notasCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notasCreditoDebito
     * @return ClienteProveedor
     */
    public function addNotasCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notasCreditoDebito) {
        $this->notasCreditoDebito[] = $notasCreditoDebito;

        return $this;
    }

    /**
     * Remove notasCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notasCreditoDebito
     */
    public function removeNotasCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notasCreditoDebito) {
        $this->notasCreditoDebito->removeElement($notasCreditoDebito);
    }

    /**
     * Get notasCreditoDebito
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotasCreditoDebito() {
        return $this->notasCreditoDebito;
    }

    /**
     * Add gastos
     *
     * @param \AppBundle\Entity\Gasto $gastos
     * @return ClienteProveedor
     */
    public function addGasto(\AppBundle\Entity\Gasto $gastos) {
        $this->gastos[] = $gastos;

        return $this;
    }

    /**
     * Remove gastos
     *
     * @param \AppBundle\Entity\Gasto $gastos
     */
    public function removeGasto(\AppBundle\Entity\Gasto $gastos) {
        $this->gastos->removeElement($gastos);
    }

    /**
     * Get gastos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGastos() {
        return $this->gastos;
    }

    /**
     * Set listaprecio
     *
     * @param \AppBundle\Entity\ListaPrecio $listaprecio
     * @return ClienteProveedor
     */
    public function setListaprecio(\AppBundle\Entity\ListaPrecio $listaprecio = null) {
        $this->listaprecio = $listaprecio;

        return $this;
    }

    /**
     * Get listaprecio
     *
     * @return \AppBundle\Entity\ListaPrecio 
     */
    public function getListaprecio() {
        return $this->listaprecio;
    }

    /**
     * Set gln
     *
     * @param string $gln
     * @return ClienteProveedor
     */
    public function setGln($gln) {
        $this->gln = $gln;

        return $this;
    }

    /**
     * Get gln
     *
     * @return string 
     */
    public function getGln() {
        return $this->gln;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return ClienteProveedor
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return ClienteProveedor
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set es_banco
     *
     * @param boolean $esBanco
     * @return ClienteProveedor
     */
    public function setEsBanco($esBanco) {
        $this->es_banco = $esBanco;

        return $this;
    }

    /**
     * Get es_banco
     *
     * @return boolean 
     */
    public function getEsBanco() {
        return $this->es_banco;
    }

    /**
     * Add consignaciones
     *
     * @param \AppBundle\Entity\Consignacion $consignaciones
     * @return ClienteProveedor
     */
    public function addConsignacione(\AppBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones[] = $consignaciones;

        return $this;
    }

    /**
     * Remove consignaciones
     *
     * @param \AppBundle\Entity\Consignacion $consignaciones
     */
    public function removeConsignacione(\AppBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones->removeElement($consignaciones);
    }

    /**
     * Get consignaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsignaciones() {
        return $this->consignaciones;
    }

    /**
     * Add presupuestos
     *
     * @param \AppBundle\Entity\Presupuesto $presupuestos
     * @return ClienteProveedor
     */
    public function addPresupuesto(\AppBundle\Entity\Presupuesto $presupuestos) {
        $this->presupuestos[] = $presupuestos;

        return $this;
    }

    /**
     * Remove presupuestos
     *
     * @param \AppBundle\Entity\Presupuesto $presupuestos
     */
    public function removePresupuesto(\AppBundle\Entity\Presupuesto $presupuestos) {
        $this->presupuestos->removeElement($presupuestos);
    }

    /**
     * Get presupuestos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresupuestos() {
        return $this->presupuestos;
    }

    /**
     * Set informacion_adicional
     *
     * @param string $informacionAdicional
     * @return ClienteProveedor
     */
    public function setInformacionAdicional($informacionAdicional) {
        $this->informacion_adicional = $informacionAdicional;

        return $this;
    }

    /**
     * Get informacion_adicional
     *
     * @return string 
     */
    public function getInformacionAdicional() {
        return $this->informacion_adicional;
    }

}
