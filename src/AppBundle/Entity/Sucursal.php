<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SucursalRepository")
 * @ORM\Table(name="sucursal")
 */
class Sucursal {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="sucursales")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteproveedor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigopostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $contacto;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="Factura", mappedBy="sucursal")
     */
    protected $facturas;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->facturas = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->direccion;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Sucursal
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Sucursal
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Sucursal
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set contacto
     *
     * @param string $contacto
     * @return Sucursal
     */
    public function setContacto($contacto) {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return string 
     */
    public function getContacto() {
        return $this->contacto;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Sucursal
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set clienteproveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteproveedor
     * @return Sucursal
     */
    public function setClienteproveedor(\AppBundle\Entity\ClienteProveedor $clienteproveedor = null) {
        $this->clienteproveedor = $clienteproveedor;

        return $this;
    }

    /**
     * Get clienteproveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteproveedor() {
        return $this->clienteproveedor;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     * @return Sucursal
     */
    public function setProvincia($provincia) {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string 
     */
    public function getProvincia() {
        return $this->provincia;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     * @return Sucursal
     */
    public function setLocalidad($localidad) {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string 
     */
    public function getLocalidad() {
        return $this->localidad;
    }

    /**
     * Set codigopostal
     *
     * @param string $codigopostal
     * @return Sucursal
     */
    public function setCodigopostal($codigopostal) {
        $this->codigopostal = $codigopostal;

        return $this;
    }

    /**
     * Get codigopostal
     *
     * @return string 
     */
    public function getCodigopostal() {
        return $this->codigopostal;
    }

     /**
     * Add facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     * @return Sucursal
     */
    public function addFactura(\AppBundle\Entity\Factura $facturas)
    {
        $this->facturas[] = $facturas;
    
        return $this;
    }

    /**
     * Remove facturas
     *
     * @param \AppBundle\Entity\Factura $facturas
     */
    public function removeFactura(\AppBundle\Entity\Factura $facturas)
    {
        $this->facturas->removeElement($facturas);
    }

    /**
     * Get facturas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFacturas()
    {
        return $this->facturas;
    }
}
