<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\IvaRepository")
 * @ORM\Table(name="iva")
 */
class Iva {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importeFactura;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="ivas")
     * @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
     */
    protected $notaCreditoDebito;

    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="ivas")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="TipoIva", inversedBy="ivas")
     * @ORM\JoinColumn(name="tipoiva_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoIva;

    /**
     * @ORM\ManyToOne(targetEntity="Gasto", inversedBy="ivas")
     * @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
     */
    protected $gasto;

    /**
     * @ORM\ManyToOne(targetEntity="FacturaImportacion", inversedBy="ivasFacturaImportacion")
     * @ORM\JoinColumn(name="facturaimportacion_id", referencedColumnName="id", nullable=true)
     */
    protected $facturaImportacion;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="ivas")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getValor();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Iva
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Iva
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return Iva
     */
    public function setNotaCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null) {
        $this->notaCreditoDebito = $notaCreditoDebito;

        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito 
     */
    public function getNotaCreditoDebito() {
        return $this->notaCreditoDebito;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return Iva
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura 
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set tipoIva
     *
     * @param \AppBundle\Entity\TipoIva $tipoIva
     * @return Iva
     */
    public function setTipoIva(\AppBundle\Entity\TipoIva $tipoIva = null) {
        $this->tipoIva = $tipoIva;

        return $this;
    }

    /**
     * Get tipoIva
     *
     * @return \AppBundle\Entity\TipoIva 
     */
    public function getTipoIva() {
        return $this->tipoIva;
    }

    /**
     * Set gasto
     *
     * @param \AppBundle\Entity\Gasto $gasto
     * @return Iva
     */
    public function setGasto(\AppBundle\Entity\Gasto $gasto = null) {
        $this->gasto = $gasto;

        return $this;
    }

    /**
     * Get gasto
     *
     * @return \AppBundle\Entity\Gasto 
     */
    public function getGasto() {
        return $this->gasto;
    }

    /**
     * Set facturaImportacion
     *
     * @param \AppBundle\Entity\FacturaImportacion $facturaImportacion
     * @return Iva
     */
    public function setFacturaImportacion(\AppBundle\Entity\FacturaImportacion $facturaImportacion = null) {
        $this->facturaImportacion = $facturaImportacion;

        return $this;
    }

    /**
     * Get facturaImportacion
     *
     * @return \AppBundle\Entity\FacturaImportacion 
     */
    public function getFacturaImportacion() {
        return $this->facturaImportacion;
    }

    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return Iva
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago 
     */
    public function getPago() {
        return $this->pago;
    }


    /**
     * Set importeFactura
     *
     * @param float $importeFactura
     * @return Iva
     */
    public function setImporteFactura($importeFactura)
    {
        $this->importeFactura = $importeFactura;
    
        return $this;
    }

    /**
     * Get importeFactura
     *
     * @return float 
     */
    public function getImporteFactura()
    {
        return $this->importeFactura;
    }
}
