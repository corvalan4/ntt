<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProductoRepository")
 */
class Producto {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *                  message = "Debe agregar una nombre."
     *              )
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $gtin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $marca;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $modelo;
	
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $medida;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *                  message = "Debe agregar una descripcion."
     *              )
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio; //Precio base.

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $tipoiva = 21;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $detalle;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $facturable = true;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $trazable = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigoOriginal;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="producto")
     */
    protected $stocks;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="producto")
     */
    protected $precios;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor")
     * @ORM\JoinColumn(name="proveedor_id", referencedColumnName="id")
     */
    protected $proveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="productos")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    protected $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategoria", inversedBy="productos")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;
    
    /**
     * @ORM\OneToMany(targetEntity="ProductoPresupuesto", mappedBy="producto", cascade={"persist"})
     */
    protected $productosPresupuesto;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->precios = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->productosPresupuesto = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre() . ' - ' . $this->getCodigo();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Producto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock($unidadNegocio='') {

        $stocktotal = 0;
        foreach ($this->stocks as $stock) {
            if ($unidadNegocio == '') {
                $stocktotal += $stock->getStock();
            }
            if ($unidadNegocio != '' && $stock->getUnidadnegocio()->getId() == $unidadNegocio) {
                $stocktotal += $stock->getStock();
            }
        }
        return $stocktotal;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Producto
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return Producto
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set tipoiva
     *
     * @param float $tipoiva
     * @return Producto
     */
    public function setTipoiva($tipoiva) {
        $this->tipoiva = $tipoiva;

        return $this;
    }

    /**
     * Get tipoiva
     *
     * @return float 
     */
    public function getTipoiva() {
        return $this->tipoiva;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Producto
     */
    public function setDetalle($detalle) {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle() {
        return $this->detalle;
    }

    /**
     * Set facturable
     *
     * @param boolean $facturable
     * @return Producto
     */
    public function setFacturable($facturable) {
        $this->facturable = $facturable;

        return $this;
    }

    /**
     * Get facturable
     *
     * @return boolean 
     */
    public function getFacturable() {
        return $this->facturable;
    }

    /**
     * Set codigoOriginal
     *
     * @param string $codigoOriginal
     * @return Producto
     */
    public function setCodigoOriginal($codigoOriginal) {
        $this->codigoOriginal = $codigoOriginal;

        return $this;
    }

    /**
     * Get codigoOriginal
     *
     * @return string 
     */
    public function getCodigoOriginal() {
        return $this->codigoOriginal;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Producto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add stocks
     *
     * @param \AppBundle\Entity\Producto $stocks
     * @return Producto
     */
    public function addStock(\AppBundle\Entity\Producto $stocks) {
        $this->stocks[] = $stocks;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \AppBundle\Entity\Producto $stocks
     */
    public function removeStock(\AppBundle\Entity\Producto $stocks) {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStocks() {
        return $this->stocks;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     * @return Producto
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null) {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria 
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * Set subcategoria
     *
     * @param \AppBundle\Entity\Subcategoria $subcategoria
     * @return Producto
     */
    public function setSubcategoria(\AppBundle\Entity\Subcategoria $subcategoria = null) {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    /**
     * Get subcategoria
     *
     * @return \AppBundle\Entity\Subcategoria 
     */
    public function getSubcategoria() {
        return $this->subcategoria;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return Producto
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Add precios
     *
     * @param \AppBundle\Entity\Precio $precios
     * @return Producto
     */
    public function addPrecio(\AppBundle\Entity\Precio $precios) {
        $this->precios[] = $precios;

        return $this;
    }

    /**
     * Remove precios
     *
     * @param \AppBundle\Entity\Precio $precios
     */
    public function removePrecio(\AppBundle\Entity\Precio $precios) {
        $this->precios->removeElement($precios);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios() {
        return $this->precios;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void {
        $this->slug = $slug;
    }



    /**
     * Set gtin
     *
     * @param string $gtin
     * @return Producto
     */
    public function setGtin($gtin)
    {
        $this->gtin = $gtin;

        return $this;
    }

    /**
     * Get gtin
     *
     * @return string 
     */
    public function getGtin()
    {
        return $this->gtin;
    }

    /**
     * Set pm
     *
     * @param string $pm
     * @return Producto
     */
    public function setPm($pm)
    {
        $this->pm = $pm;

        return $this;
    }

    /**
     * Get pm
     *
     * @return string 
     */
    public function getPm()
    {
        return $this->pm;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Producto
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Producto
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set medida
     *
     * @param string $medida
     * @return Producto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string 
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set trazable
     *
     * @param boolean $trazable
     * @return Producto
     */
    public function setTrazable($trazable)
    {
        $this->trazable = $trazable;

        return $this;
    }

    /**
     * Get trazable
     *
     * @return boolean 
     */
    public function getTrazable()
    {
        return $this->trazable;
    }

    /**
     * Set proveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $proveedor
     * @return Producto
     */
    public function setProveedor(\AppBundle\Entity\ClienteProveedor $proveedor = null)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Add productosPresupuesto
     *
     * @param \AppBundle\Entity\ProductoPresupuesto $productosPresupuesto
     * @return Producto
     */
    public function addProductosPresupuesto(\AppBundle\Entity\ProductoPresupuesto $productosPresupuesto)
    {
        $this->productosPresupuesto[] = $productosPresupuesto;

        return $this;
    }

    /**
     * Remove productosPresupuesto
     *
     * @param \AppBundle\Entity\ProductoPresupuesto $productosPresupuesto
     */
    public function removeProductosPresupuesto(\AppBundle\Entity\ProductoPresupuesto $productosPresupuesto)
    {
        $this->productosPresupuesto->removeElement($productosPresupuesto);
    }

    /**
     * Get productosPresupuesto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosPresupuesto()
    {
        return $this->productosPresupuesto;
    }
}
