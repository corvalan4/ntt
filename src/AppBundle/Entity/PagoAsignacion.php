<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\ClienteProveedor;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PagoAsignacionRepository")
 * @ORM\Table(name="pagoasignacion")
 */
class PagoAsignacion {

    CONST FACTURA = 'Factura';
    CONST FACTURA_PROVEEDOR = 'Factura proveedor';
    CONST NOTA_CREDITO = 'Nota de crédito';
    CONST NOTA_DEBITO = 'Nota de débito';
    CONST RECIBO_CLIENTE = 'Recibo cliente';
    CONST ORDEN_PAGO = 'Orden de pago';
    CONST GASTO = 'Gasto';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\ManyToOne(targetEntity="Gasto", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
     */
    protected $gasto;

    /**
     * @ORM\ManyToOne(targetEntity="Cobranza", inversedBy="pagosAsignacionCobranza")
     * @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id", nullable=true)
     */
    protected $cobranza;

    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="Consignacion", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id", nullable=true)
     */
    protected $consignacion;

    /**
     * @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
     */
    protected $notaCreditoDebito;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="pagosAsignacion")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->gasto . $this->pago . $this->factura . $this->cobranza . $this->notaCreditoDebito;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function getDocumentName() {
        if (!is_null($this->factura)) {
            if ($this->factura->getTipo() == "F") {
                return self::FACTURA;
            } else {
                return self::FACTURA_PROVEEDOR;
            }
        }

        if (!is_null($this->notaCreditoDebito)) {

            if ($this->notaCreditoDebito->getTiponota() == NotaCreditoDebito::TIPO_CREDITO) {
                return self::NOTA_CREDITO;
            } else {
                return self::NOTA_DEBITO;
            }
        }
        if (!is_null($this->pago)) {
            return self::ORDEN_PAGO;
        }
        if (!is_null($this->cobranza)) {
            return self::RECIBO_CLIENTE;
        }

        if (!is_null($this->gasto)) {
            return self::GASTO;
        }
    }

    public function getDocument() {

        if (!is_null($this->factura)) {
            if ($this->factura->getTipo() == "F") {
                return (object) ['documento' => $this->factura, 'tipo' => self::FACTURA];
            } else {
                return (object) ['documento' => $this->factura, 'tipo' => self::FACTURA_PROVEEDOR];
            }
        }
        if (!is_null($this->notaCreditoDebito)) {
            if ($this->notaCreditoDebito->getTiponota() == NotaCreditoDebito::TIPO_CREDITO) {
                return (object) ['documento' => $this->notaCreditoDebito, 'tipo' => self::NOTA_CREDITO];
            } else {
                return (object) ['documento' => $this->notaCreditoDebito, 'tipo' => self::NOTA_DEBITO];
            }
        }
        if (!is_null($this->pago)) {
            return (object) ['documento' => $this->pago, 'tipo' => self::ORDEN_PAGO];
        }
        if (!is_null($this->cobranza)) {
            return (object) ['documento' => $this->cobranza, 'tipo' => self::RECIBO_CLIENTE];
        }
        if (!is_null($this->gasto)) {
            return (object) ['documento' => $this->gasto, 'tipo' => self::GASTO];
        }
    }

    /*
     * 
     */

    public function getBaseDocumentPath() {

        if (!is_null($this->factura)) {
            if ($this->factura->getTipo() == "F") {
                return 'factura';
            } else {
                return 'facturaproveedor';
            }
        }

        if (!is_null($this->notaCreditoDebito)) {
            if ($this->notaCreditoDebito->getClienteProveedor()->getClienteProveedor()==ClienteProveedor::CLIENTE_COD){
                return 'notacreditodebito';
            }else{
                return 'notacreditodebito_proveedor';
            }
            
        }
        if (!is_null($this->pago)) {
            return 'pago';
        }
        if (!is_null($this->cobranza)) {
            return 'cobranza';
        }
        if (!is_null($this->gasto)) {
            return 'gasto';
        }
    }

    /*
     * 
     */

    public function isDebit() {
        switch ($this->getDocumentName()) {
            case self::FACTURA:
                return true;
            case self::FACTURA_PROVEEDOR:
                return false;
            case self::GASTO:
                return false;
            case self::ORDEN_PAGO:
                return true;
            case self::NOTA_CREDITO:
                if ($this->clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {
                    return false;
                } else {
                    return true;
                }
            case self::NOTA_DEBITO:
                if ($this->clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {
                    return true;
                } else {
                    return false;
                }
            case self::RECIBO_CLIENTE:
                return false;
        }
    }

    /* public function getDocumento() {
      if ($this->factura) {
      $positivo = ($this->factura->getTipo() == "F") ? false : true;
      $tipo = ($this->factura->getTipo() == "FP") ? "facturaproveedor" : "factura";
      return array("entity" => $this->factura, "tipo" => $tipo, "positivo" => $positivo);
      }
      if ($this->consignacion) {
      return array("entity" => $this->consignacion, "tipo" => "consignacion", "positivo" => true);
      }
      if ($this->cobranza) {
      return array("entity" => $this->cobranza, "tipo" => "cobranza", "positivo" => true);
      }
      if ($this->pago) {
      return array("entity" => $this->pago, "tipo" => "pago", "positivo" => false);
      }
      if ($this->gasto) {
      return array("entity" => $this->gasto, "tipo" => "gasto", "positivo" => false);
      }
      if ($this->notaCreditoDebito) {
      $positivo = $this->notaCreditoDebito->getTiponota() == NotaCreditoDebito::TIPO_CREDITO ? true : false;
      return array("entity" => $this->notaCreditoDebito, "tipo" => "notaCreditoDebito", "positivo" => $positivo);
      }
      } */

    /**
     * Set importe
     *
     * @param float $importe
     * @return PagoAsignacion
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PagoAsignacion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return PagoAsignacion
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago
     */
    public function getPago() {
        return $this->pago;
    }

    /**
     * Set gasto
     *
     * @param \AppBundle\Entity\Gasto $gasto
     * @return PagoAsignacion
     */
    public function setGasto(\AppBundle\Entity\Gasto $gasto = null) {
        $this->gasto = $gasto;

        return $this;
    }

    /**
     * Get gasto
     *
     * @return \AppBundle\Entity\Gasto
     */
    public function getGasto() {
        return $this->gasto;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return PagoAsignacion
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return PagoAsignacion
     */
    public function setNotaCreditoDebito(\AppBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null) {
        $this->notaCreditoDebito = $notaCreditoDebito;

        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \AppBundle\Entity\NotaCreditoDebito
     */
    public function getNotaCreditoDebito() {
        return $this->notaCreditoDebito;
    }

    /**
     * Set cobranza
     *
     * @param \AppBundle\Entity\Cobranza $cobranza
     * @return PagoAsignacion
     */
    public function setCobranza(\AppBundle\Entity\Cobranza $cobranza = null) {
        $this->cobranza = $cobranza;

        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \AppBundle\Entity\Cobranza 
     */
    public function getCobranza() {
        return $this->cobranza;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Pago
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set consignacion
     *
     * @param \AppBundle\Entity\Consignacion $consignacion
     * @return PagoAsignacion
     */
    public function setConsignacion(\AppBundle\Entity\Consignacion $consignacion = null) {
        $this->consignacion = $consignacion;

        return $this;
    }

    /**
     * Get consignacion
     *
     * @return \AppBundle\Entity\Consignacion 
     */
    public function getConsignacion() {
        return $this->consignacion;
    }

}
