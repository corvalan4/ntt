<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TipoRetencionRepository")
 * @ORM\Table(name="tiporetencion")
 */
class TipoRetencion {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $porcentaje;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="Retencion", mappedBy="tiporetencion")
     */
    protected $retenciones;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->retenciones = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion

     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoRetencion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return TipoRetencion
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return TipoRetencion
     */
    public function setPorcentaje($porcentaje) {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje() {
        return $this->porcentaje;
    }


    /**
     * Add retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     * @return TipoRetencion
     */
    public function addRetencione(\AppBundle\Entity\Retencion $retenciones)
    {
        $this->retenciones[] = $retenciones;
    
        return $this;
    }

    /**
     * Remove retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     */
    public function removeRetencione(\AppBundle\Entity\Retencion $retenciones)
    {
        $this->retenciones->removeElement($retenciones);
    }

    /**
     * Get retenciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRetenciones()
    {
        return $this->retenciones;
    }
}
