<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ListaPrecioRepository")
 * @ORM\Table(name="listaprecio")
 */
class ListaPrecio{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="listaprecio")
     */
    protected $precios;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $iva = 0;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
        $this->precios = new ArrayCollection();
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getDescripcion();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ListaPrecio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add precios
     *
     * @param \AppBundle\Entity\Precio $precios
     * @return ListaPrecio
     */
    public function addPrecio(\AppBundle\Entity\Precio $precios)
    {
        $this->precios[] = $precios;

        return $this;
    }

    /**
     * Remove precios
     *
     * @param \AppBundle\Entity\Precio $precios
     */
    public function removePrecio(\AppBundle\Entity\Precio $precios)
    {
        $this->precios->removeElement($precios);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios()
    {
        return $this->precios;
    }

    /**
     * Set iva
     *
     * @param boolean $iva
     * @return ListaPrecio
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return boolean 
     */
    public function getIva()
    {
        return $this->iva;
    }
}
