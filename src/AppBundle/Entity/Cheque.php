<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ChequeRepository")
 * @ORM\Table(name="cheque")
 */
class Cheque {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaemision;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechacobro;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nrocheque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firmantes;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $conciliacion;

    /**
     * @ORM\ManyToOne(targetEntity="CuentaBancaria", inversedBy="chequesOrigen")
     * @ORM\JoinColumn(name="origen_id", referencedColumnName="id", nullable=true)
     */
    protected $cuentaorigen;

    /**
     * @ORM\ManyToOne(targetEntity="CuentaBancaria", inversedBy="chequesDestino")
     * @ORM\JoinColumn(name="destino_id", referencedColumnName="id", nullable=true)
     */
    protected $cuentadestino;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="cheques")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Cobranza", inversedBy="cheques")
     * @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id", nullable=true)
     */
    protected $cobranza;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="cheques")
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
     */
    protected $pago;

    /**
     * @ORM\ManyToOne(targetEntity="Gasto", inversedBy="cheques")
     * @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
     */
    protected $gasto;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Banco", inversedBy="cheques")
     * @ORM\JoinColumn(name="banco_id", referencedColumnName="id", nullable=true)
     */
    protected $banco;

    /**
     * @ORM\ManyToOne(targetEntity="TipoCheque", inversedBy="cheques")
     * @ORM\JoinColumn(name="tipocheque_id", referencedColumnName="id", nullable=true)
     */
    protected $tipocheque;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="MovimientoBancario", mappedBy="cuentabancaria", cascade={"persist"})
     */
    protected $movimientos;

    /**
     * @ORM\OneToMany(targetEntity="MovimientoBancario", mappedBy="cheque", cascade={"persist"})
     */
    protected $movimientosBancarios;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->movimientosBancarios = new ArrayCollection();
        $this->movimientos = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'CHEQUE '.$this->getId();
    }

    public function getEstadoActual() {
        if ($this->estado == 'U') {
            return 'USADO';
        }
        if ($this->estado == 'Z') {
            return 'RECHAZADO';
        }
        if ($this->estado == 'A') {
            return 'ACTIVO';
        }
        if ($this->estado == 'E') {
            return 'ELIMINADO';
        }
        if ($this->estado == 'C') {
            return 'CONCILIADO';
        }
        if ($this->estado == 'D') {
            return 'DEPOSITADO';
        }
         if ($this->estado == 'R') {
            return 'RETIRADO';
        }
        return $this->estado;
    }

    public function getColor() {
        if ($this->estado == 'C') {
            return 'blue';
        }
        if ($this->estado == 'D') {
            return 'green';
        }
        if ($this->estado == 'U') {
            return 'yellow';
        }
        if ($this->estado == 'Z') {
            return 'red';
        }
        if ($this->estado == 'E') {
            return 'grey';
        }
        if ($this->estado == 'R') {
            return 'brown';
        }
        return 'black';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fechaemision
     *
     * @param \DateTime $fechaemision
     * @return Cheque
     */
    public function setFechaemision($fechaemision) {
        $this->fechaemision = $fechaemision;

        return $this;
    }

    /**
     * Get fechaemision
     *
     * @return \DateTime 
     */
    public function getFechaemision() {
        return $this->fechaemision;
    }

    /**
     * Set fechacobro
     *
     * @param \DateTime $fechacobro
     * @return Cheque
     */
    public function setFechacobro($fechacobro) {
        $this->fechacobro = $fechacobro;

        return $this;
    }

    /**
     * Get fechacobro
     *
     * @return \DateTime 
     */
    public function getFechacobro() {
        return $this->fechacobro;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Cheque
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set nrocheque
     *
     * @param string $nrocheque
     * @return Cheque
     */
    public function setNrocheque($nrocheque) {
        $this->nrocheque = $nrocheque;

        return $this;
    }

    /**
     * Get nrocheque
     *
     * @return string 
     */
    public function getNrocheque() {
        return $this->nrocheque;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     * @return Cheque
     */
    public function setCuit($cuit) {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit() {
        return $this->cuit;
    }

    /**
     * Set firmantes
     *
     * @param string $firmantes
     * @return Cheque
     */
    public function setFirmantes($firmantes) {
        $this->firmantes = $firmantes;

        return $this;
    }

    /**
     * Get firmantes
     *
     * @return string 
     */
    public function getFirmantes() {
        return $this->firmantes;
    }

    /**
     * Set conciliacion
     *
     * @param string $conciliacion
     * @return Cheque
     */
    public function setConciliacion($conciliacion) {
        $this->conciliacion = $conciliacion;

        return $this;
    }

    /**
     * Get conciliacion
     *
     * @return string 
     */
    public function getConciliacion() {
        return $this->conciliacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cheque
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set cuentaorigen
     *
     * @param \AppBundle\Entity\CuentaBancaria $cuentaorigen
     * @return Cheque
     */
    public function setCuentaorigen(\AppBundle\Entity\CuentaBancaria $cuentaorigen = null) {
        $this->cuentaorigen = $cuentaorigen;

        return $this;
    }

    /**
     * Get cuentaorigen
     *
     * @return \AppBundle\Entity\CuentaBancaria 
     */
    public function getCuentaorigen() {
        return $this->cuentaorigen;
    }

    /**
     * Set cuentadestino
     *
     * @param \AppBundle\Entity\CuentaBancaria $cuentadestino
     * @return Cheque
     */
    public function setCuentadestino(\AppBundle\Entity\CuentaBancaria $cuentadestino = null) {
        $this->cuentadestino = $cuentadestino;

        return $this;
    }

    /**
     * Get cuentadestino
     *
     * @return \AppBundle\Entity\CuentaBancaria 
     */
    public function getCuentadestino() {
        return $this->cuentadestino;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Cheque
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set cobranza
     *
     * @param \AppBundle\Entity\Cobranza $cobranza
     * @return Cheque
     */
    public function setCobranza(\AppBundle\Entity\Cobranza $cobranza = null) {
        $this->cobranza = $cobranza;

        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \AppBundle\Entity\Cobranza 
     */
    public function getCobranza() {
        return $this->cobranza;
    }

    /**
     * Set pago
     *
     * @param \AppBundle\Entity\Pago $pago
     * @return Cheque
     */
    public function setPago(\AppBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \AppBundle\Entity\Pago 
     */
    public function getPago() {
        return $this->pago;
    }

    /**
     * Set banco
     *
     * @param \AppBundle\Entity\Banco $banco
     * @return Cheque
     */
    public function setBanco(\AppBundle\Entity\Banco $banco = null) {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \AppBundle\Entity\Banco 
     */
    public function getBanco() {
        return $this->banco;
    }

    /**
     * Set tipocheque
     *
     * @param \AppBundle\Entity\TipoCheque $tipocheque
     * @return Cheque
     */
    public function setTipocheque(\AppBundle\Entity\TipoCheque $tipocheque = null) {
        $this->tipocheque = $tipocheque;

        return $this;
    }

    /**
     * Get tipocheque
     *
     * @return \AppBundle\Entity\TipoCheque 
     */
    public function getTipocheque() {
        return $this->tipocheque;
    }

    /**
     * Add movimientos
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientos
     * @return Cheque
     */
    public function addMovimiento(\AppBundle\Entity\MovimientoBancario $movimientos) {
        $this->movimientos[] = $movimientos;

        return $this;
    }

    /**
     * Remove movimientos
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientos
     */
    public function removeMovimiento(\AppBundle\Entity\MovimientoBancario $movimientos) {
        $this->movimientos->removeElement($movimientos);
    }

    /**
     * Get movimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientos() {
        return $this->movimientos;
    }

    /**
     * Add movimientosBancarios
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientosBancarios
     * @return Cheque
     */
    public function addMovimientosBancario(\AppBundle\Entity\MovimientoBancario $movimientosBancarios) {
        $this->movimientosBancarios[] = $movimientosBancarios;

        return $this;
    }

    /**
     * Remove movimientosBancarios
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientosBancarios
     */
    public function removeMovimientosBancario(\AppBundle\Entity\MovimientoBancario $movimientosBancarios) {
        $this->movimientosBancarios->removeElement($movimientosBancarios);
    }

    /**
     * Get movimientosBancarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientosBancarios() {
        return $this->movimientosBancarios;
    }


    /**
     * Set gasto
     *
     * @param \AppBundle\Entity\Gasto $gasto
     * @return Cheque
     */
    public function setGasto(\AppBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;
    
        return $this;
    }

    /**
     * Get gasto
     *
     * @return \AppBundle\Entity\Gasto 
     */
    public function getGasto()
    {
        return $this->gasto;
    }
}
