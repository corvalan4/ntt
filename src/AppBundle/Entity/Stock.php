<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StockRepository")
 * @ORM\Table(name="stock")
 */
class Stock {
    
    const CAMARGO = 'CAMARGO';
    const SENILLOSA = 'SENILLOSA';
    const LEONISMO = 'LEONISMO ARGENTINO';
    const MAZZOLA = 'MAZZOLA';
    
    /*const CONFORMIDAD_RIVADAVIA_5 = 'RIVADAVIA 5';
    const CONFORMIDAD_RIVADAVIA_6 = 'RIVADAVIA 6';
    const CONFORMIDAD_AZCUENAGA = 'AZCUENAGA';
    const DEPOSIT_ERROR_5 = 'NO CONFORMES - 5';
    const DEPOSIT_ERROR_6 = 'NO CONFORMES - 6';
    const DEPOSIT_ERROR_A = 'NO CONFORMES - A';*/

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $vencimiento;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $lote;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $despacho;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $deposito = Stock::CAMARGO;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $serie;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $procedencia;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $origen;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $etiqueta = 'Cumple';

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $carga = 'Cumple';

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $higiene = 'Cumple';

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    protected $estado = 'Aprobado';

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="stock")
     */
    protected $productosFactura;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="stock")
     */
    protected $stocksConsignacion;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="stocks")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="stocks")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadnegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Cotizacion", inversedBy="stocks")
     * @ORM\JoinColumn(name="cotizacion_id", referencedColumnName="id")
     */
    protected $cotizacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $stock = 1;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $costo;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="realizo_id", referencedColumnName="id")
     */
    protected $realizo;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="controlo_id", referencedColumnName="id")
     */
    protected $controlo;

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->stocksConsignacion = new ArrayCollection();
        $this->productosFactura = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getUnidadnegocio() . " - Cantidad: " . $this->getStock();
    }
    /*
     * get Depositos
     */
    public function getDepositos(){
        return [
            Stock::CAMARGO => Stock::CAMARGO,
            Stock::SENILLOSA => Stock::SENILLOSA,
            Stock::LEONISMO => Stock::LEONISMO,
            Stock::MAZZOLA => Stock::MAZZOLA
        ];
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Stock
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Stock
     */
    public function setStock($stock) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock() {
        return $this->stock;
    }

    /**
     * Set costo
     *
     * @param float $costo
     * @return Stock
     */
    public function setCosto($costo) {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto() {
        return $this->costo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Stock
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosFactura
     *
     * @param \AppBundle\Entity\ProductoFactura $productosFactura
     * @return Stock
     */
    public function addProductosFactura(\AppBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \AppBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\AppBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     * @return Stock
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set cotizacion
     *
     * @param \AppBundle\Entity\Cotizacion $cotizacion
     * @return Stock
     */
    public function setCotizacion(\AppBundle\Entity\Cotizacion $cotizacion = null) {
        $this->cotizacion = $cotizacion;

        return $this;
    }

    /**
     * Get cotizacion
     *
     * @return \AppBundle\Entity\Cotizacion
     */
    public function getCotizacion() {
        return $this->cotizacion;
    }

    /**
     * Set UnidadNegocio
     *
     * @param \AppBundle\Entity\unidadnegocio $unidadnegocio
     * @return Stock
     */
    public function setUnidadnegocio(\AppBundle\Entity\UnidadNegocio $unidadnegocio = null) {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get UnidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadnegocio() {
        return $this->unidadnegocio;
    }


    /**
     * Set vencimiento
     *
     * @param \DateTime $vencimiento
     * @return Stock
     */
    public function setVencimiento($vencimiento)
    {
        $this->vencimiento = $vencimiento;

        return $this;
    }

    /**
     * Get vencimiento
     *
     * @return \DateTime 
     */
    public function getVencimiento()
    {
        return $this->vencimiento;
    }

    /**
     * Set serie
     *
     * @param string $serie
     * @return Stock
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return string 
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set procedencia
     *
     * @param string $procedencia
     * @return Stock
     */
    public function setProcedencia($procedencia)
    {
        $this->procedencia = $procedencia;

        return $this;
    }

    /**
     * Get procedencia
     *
     * @return string 
     */
    public function getProcedencia()
    {
        return $this->procedencia;
    }

    /**
     * Set origen
     *
     * @param string $origen
     * @return Stock
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen
     *
     * @return string 
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set etiqueta
     *
     * @param string $etiqueta
     * @return Stock
     */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;

        return $this;
    }

    /**
     * Get etiqueta
     *
     * @return string 
     */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * Set carga
     *
     * @param string $carga
     * @return Stock
     */
    public function setCarga($carga)
    {
        $this->carga = $carga;

        return $this;
    }

    /**
     * Get carga
     *
     * @return string 
     */
    public function getCarga()
    {
        return $this->carga;
    }

    /**
     * Set higiene
     *
     * @param string $higiene
     * @return Stock
     */
    public function setHigiene($higiene)
    {
        $this->higiene = $higiene;

        return $this;
    }

    /**
     * Get higiene
     *
     * @return string 
     */
    public function getHigiene()
    {
        return $this->higiene;
    }

    /**
     * Set realizo
     *
     * @param \AppBundle\Entity\Usuario $realizo
     * @return Stock
     */
    public function setRealizo(\AppBundle\Entity\Usuario $realizo = null)
    {
        $this->realizo = $realizo;

        return $this;
    }

    /**
     * Get realizo
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getRealizo()
    {
        return $this->realizo;
    }

    /**
     * Set controlo
     *
     * @param \AppBundle\Entity\Usuario $controlo
     * @return Stock
     */
    public function setControlo(\AppBundle\Entity\Usuario $controlo = null)
    {
        $this->controlo = $controlo;

        return $this;
    }

    /**
     * Get controlo
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getControlo()
    {
        return $this->controlo;
    }

    /**
     * Set lote
     *
     * @param string $lote
     * @return Stock
     */
    public function setLote($lote)
    {
        $this->lote = $lote;

        return $this;
    }

    /**
     * Get lote
     *
     * @return string 
     */
    public function getLote()
    {
        return $this->lote;
    }

    /**
     * Set despacho
     *
     * @param string $despacho
     * @return Stock
     */
    public function setDespacho($despacho)
    {
        $this->despacho = $despacho;

        return $this;
    }

    /**
     * Get despacho
     *
     * @return string 
     */
    public function getDespacho()
    {
        return $this->despacho;
    }

    /**
     * Set deposito
     *
     * @param string $deposito
     * @return Stock
     */
    public function setDeposito($deposito)
    {
        $this->deposito = $deposito;

        return $this;
    }

    /**
     * Get deposito
     *
     * @return string 
     */
    public function getDeposito()
    {
        return $this->deposito;
    }

    /**
     * Add stocksConsignacion
     *
     * @param \AppBundle\Entity\ProductoFactura $stocksConsignacion
     * @return Stock
     */
    public function addStocksConsignacion(\AppBundle\Entity\ProductoFactura $stocksConsignacion)
    {
        $this->stocksConsignacion[] = $stocksConsignacion;

        return $this;
    }

    /**
     * Remove stocksConsignacion
     *
     * @param \AppBundle\Entity\ProductoFactura $stocksConsignacion
     */
    public function removeStocksConsignacion(\AppBundle\Entity\ProductoFactura $stocksConsignacion)
    {
        $this->stocksConsignacion->removeElement($stocksConsignacion);
    }

    /**
     * Get stocksConsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStocksConsignacion()
    {
        return $this->stocksConsignacion;
    }
}
