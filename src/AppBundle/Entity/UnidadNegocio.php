<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UnidadNegocioRepository")
 * @ORM\Table(name="unidadnegocio")
 */
class UnidadNegocio {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $responsable;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $celular;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $punto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $iibb;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="unidadnegocio")
     */
    protected $stocks;

    /**
     * @ORM\OneToMany(targetEntity="Usuario", mappedBy="unidadNegocio")
     */
    protected $usuarios;

    /**
     * @ORM\OneToMany(targetEntity="MovimientoBancario", mappedBy="unidadNegocio")
     */
    protected $movimientoscc;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="unidadNegocio")
     */
    protected $cheques;

    /**
     * @ORM\OneToMany(targetEntity="Cobranza", mappedBy="unidadnegocio")
     */
    protected $cobranzas;

    /**
     * @ORM\OneToMany(targetEntity="Pago", mappedBy="unidadnegocio")
     */
    protected $pagos;

    /**
     * @ORM\OneToMany(targetEntity="Gasto", mappedBy="unidadnegocio")
     */
    protected $gastos;

    /**
     * @ORM\OneToMany(targetEntity="Consignacion", mappedBy="unidadNegocio")
     */
    protected $consignaciones;

    /**
     * @ORM\OneToMany(targetEntity="Presupuesto", mappedBy="unidadNegocio")
     */
    protected $presupuestos;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaprecio_id", referencedColumnName="id", nullable=true)
     */
    protected $listaprecio;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->stocks = new ArrayCollection();
        $this->usuarios = new ArrayCollection();
        $this->movimientoscc = new ArrayCollection();
        $this->cheques = new ArrayCollection();
        $this->cobranzas = new ArrayCollection();
        $this->pagos = new ArrayCollection();
        $this->gastos = new ArrayCollection();
        $this->consignaciones = new ArrayCollection();
        $this->presupuestos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return UnidadNegocio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return UnidadNegocio
     */
    public function setResponsable($responsable) {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable() {
        return $this->responsable;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return UnidadNegocio
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return UnidadNegocio
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return UnidadNegocio
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular() {
        return $this->celular;
    }

    public function setPunto($punto) {
        $this->punto = $punto;

        return $this;
    }

    public function getPunto() {
        return $this->punto;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return UnidadNegocio
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return UnidadNegocio
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add stocks
     *
     * @param \AppBundle\Entity\Stock $stocks
     * @return UnidadNegocio
     */
    public function addStock(\AppBundle\Entity\Stock $stocks) {
        $this->stocks[] = $stocks;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \AppBundle\Entity\Stock $stocks
     */
    public function removeStock(\AppBundle\Entity\Stock $stocks) {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocks() {
        return $this->stocks;
    }

    /**
     * Add usuarios
     *
     * @param \AppBundle\Entity\Usuario $usuarios
     * @return UnidadNegocio
     */
    public function addUsuario(\AppBundle\Entity\Usuario $usuarios) {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \AppBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\AppBundle\Entity\Usuario $usuarios) {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios() {
        return $this->usuarios;
    }

    public function setCuit($cuit) {
        $this->cuit = $cuit;

        return $this;
    }

    public function getCuit() {
        return $this->cuit;
    }

    public function setIibb($iibb) {
        $this->iibb = $iibb;

        return $this;
    }

    /**
     * Get iibb
     *
     * @return string 
     */
    public function getIibb() {
        return $this->iibb;
    }

    /**
     * Add movimientoscc
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientoscc
     * @return UnidadNegocio
     */
    public function addMovimientoscc(\AppBundle\Entity\MovimientoBancario $movimientoscc) {
        $this->movimientoscc[] = $movimientoscc;

        return $this;
    }

    /**
     * Remove movimientoscc
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientoscc
     */
    public function removeMovimientoscc(\AppBundle\Entity\MovimientoBancario $movimientoscc) {
        $this->movimientoscc->removeElement($movimientoscc);
    }

    /**
     * Get movimientoscc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientoscc() {
        return $this->movimientoscc;
    }

    /**
     * Add cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     * @return UnidadNegocio
     */
    public function addCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques[] = $cheques;

        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques() {
        return $this->cheques;
    }

    /**
     * Add cobranzas
     *
     * @param \AppBundle\Entity\Cobranza $cobranzas
     * @return UnidadNegocio
     */
    public function addCobranza(\AppBundle\Entity\Cobranza $cobranzas) {
        $this->cobranzas[] = $cobranzas;

        return $this;
    }

    /**
     * Remove cobranzas
     *
     * @param \AppBundle\Entity\Cobranza $cobranzas
     */
    public function removeCobranza(\AppBundle\Entity\Cobranza $cobranzas) {
        $this->cobranzas->removeElement($cobranzas);
    }

    /**
     * Get cobranzas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzas() {
        return $this->cobranzas;
    }

    /**
     * Add pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     * @return UnidadNegocio
     */
    public function addPago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     */
    public function removePago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }

    /**
     * Add gastos
     *
     * @param \AppBundle\Entity\Gasto $gastos
     * @return UnidadNegocio
     */
    public function addGasto(\AppBundle\Entity\Gasto $gastos) {
        $this->gastos[] = $gastos;

        return $this;
    }

    /**
     * Remove gastos
     *
     * @param \AppBundle\Entity\Gasto $gastos
     */
    public function removeGasto(\AppBundle\Entity\Gasto $gastos) {
        $this->gastos->removeElement($gastos);
    }

    /**
     * Get gastos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGastos() {
        return $this->gastos;
    }

    /**
     * Set listaprecio
     *
     * @param \AppBundle\Entity\ListaPrecio $listaprecio
     * @return UnidadNegocio
     */
    public function setListaprecio(\AppBundle\Entity\ListaPrecio $listaprecio = null) {
        $this->listaprecio = $listaprecio;

        return $this;
    }

    /**
     * Get listaprecio
     *
     * @return \AppBundle\Entity\ListaPrecio 
     */
    public function getListaprecio() {
        return $this->listaprecio;
    }

    /**
     * Add consignaciones
     *
     * @param \AppBundle\Entity\Consignacion $consignaciones
     * @return UnidadNegocio
     */
    public function addConsignacione(\AppBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones[] = $consignaciones;

        return $this;
    }

    /**
     * Remove consignaciones
     *
     * @param \AppBundle\Entity\Consignacion $consignaciones
     */
    public function removeConsignacione(\AppBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones->removeElement($consignaciones);
    }

    /**
     * Get consignaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsignaciones() {
        return $this->consignaciones;
    }


    /**
     * Add presupuestos
     *
     * @param \AppBundle\Entity\Presupuesto $presupuestos
     * @return UnidadNegocio
     */
    public function addPresupuesto(\AppBundle\Entity\Presupuesto $presupuestos)
    {
        $this->presupuestos[] = $presupuestos;

        return $this;
    }

    /**
     * Remove presupuestos
     *
     * @param \AppBundle\Entity\Presupuesto $presupuestos
     */
    public function removePresupuesto(\AppBundle\Entity\Presupuesto $presupuestos)
    {
        $this->presupuestos->removeElement($presupuestos);
    }

    /**
     * Get presupuestos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresupuestos()
    {
        return $this->presupuestos;
    }
}
