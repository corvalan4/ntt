<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CuentaBancariaRepository")
 * @ORM\Table(name="cuentabancaria")
 */
class CuentaBancaria {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nrocuenta;

    /**
     * @ORM\ManyToOne(targetEntity="Banco", inversedBy="cuentasBancarias")
     * @ORM\JoinColumn(name="banco_id", referencedColumnName="id", nullable=true)
     */
    protected $banco;

    /**
     * @ORM\OneToMany(targetEntity="MovimientoBancario", mappedBy="cuentabancaria", cascade={"persist"})
     */
    private $movimientos;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="cuentaorigen", cascade={"persist"})
     */
    private $chequesOrigen;
    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="cuentadestino", cascade={"persist"})
     */
    private $chequesDestino;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->movimientos = new ArrayCollection();
        $this->chequesOrigen = new ArrayCollection();
        $this->chequesDestino = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion

     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Banco
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set nrocuenta
     *
     * @param string $nrocuenta
     * @return CuentaBancaria
     */
    public function setNrocuenta($nrocuenta) {
        $this->nrocuenta = $nrocuenta;

        return $this;
    }

    /**
     * Get nrocuenta
     *
     * @return string 
     */
    public function getNrocuenta() {
        return $this->nrocuenta;
    }

    /**
     * Set banco
     *
     * @param \AppBundle\Entity\banco $banco
     * @return CuentaBancaria
     */
    public function setBanco(\AppBundle\Entity\banco $banco = null) {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \AppBundle\Entity\banco
     */
    public function getBanco() {
        return $this->banco;
    }

    /**
     * Set movimiento
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimiento
     * @return CuentaBancaria
     */
    public function setMovimiento(\AppBundle\Entity\MovimientoBancario $movimiento = null) {
        $this->movimiento = $movimiento;

        return $this;
    }

    /**
     * Get movimiento
     *
     * @return \AppBundle\Entity\MovimientoBancario
     */
    public function getMovimiento() {
        return $this->movimiento;
    }

    /**
     * Add movimientos
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientos
     * @return CuentaBancaria
     */
    public function addMovimiento(\AppBundle\Entity\MovimientoBancario $movimientos) {
        $this->movimientos[] = $movimientos;

        return $this;
    }

    /**
     * Remove movimientos
     *
     * @param \AppBundle\Entity\MovimientoBancario $movimientos
     */
    public function removeMovimiento(\AppBundle\Entity\MovimientoBancario $movimientos) {
        $this->movimientos->removeElement($movimientos);
    }

    /**
     * Get movimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientos() {
        return $this->movimientos;
    }


    /**
     * Add chequesOrigen
     *
     * @param \AppBundle\Entity\Cheque $chequesOrigen
     * @return CuentaBancaria
     */
    public function addChequesOrigen(\AppBundle\Entity\Cheque $chequesOrigen)
    {
        $this->chequesOrigen[] = $chequesOrigen;
    
        return $this;
    }

    /**
     * Remove chequesOrigen
     *
     * @param \AppBundle\Entity\Cheque $chequesOrigen
     */
    public function removeChequesOrigen(\AppBundle\Entity\Cheque $chequesOrigen)
    {
        $this->chequesOrigen->removeElement($chequesOrigen);
    }

    /**
     * Get chequesOrigen
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChequesOrigen()
    {
        return $this->chequesOrigen;
    }

    /**
     * Add chequesDestino
     *
     * @param \AppBundle\Entity\Cheque $chequesDestino
     * @return CuentaBancaria
     */
    public function addChequesDestino(\AppBundle\Entity\Cheque $chequesDestino)
    {
        $this->chequesDestino[] = $chequesDestino;
    
        return $this;
    }

    /**
     * Remove chequesDestino
     *
     * @param \AppBundle\Entity\Cheque $chequesDestino
     */
    public function removeChequesDestino(\AppBundle\Entity\Cheque $chequesDestino)
    {
        $this->chequesDestino->removeElement($chequesDestino);
    }

    /**
     * Get chequesDestino
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChequesDestino()
    {
        return $this->chequesDestino;
    }
}
