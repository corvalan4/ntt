<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AjusteRepository")
 * @ORM\Table(name="ajuste")
 */
class Ajuste {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCaja", mappedBy="ajuste")
     */
    protected $movimientoCaja;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
         return "AJUSTE " . str_pad($this->id, 5, "0", STR_PAD_LEFT);        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Ajuste
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Ajuste
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Ajuste
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set movimientoCaja
     *
     * @param \AppBundle\Entity\MovimientoCaja $movimientoCaja
     * @return Ajuste
     */
    public function setMovimientoCaja(\AppBundle\Entity\MovimientoCaja $movimientoCaja = null) {
        $this->movimientoCaja = $movimientoCaja;

        return $this;
    }

    /**
     * Get movimientoCaja
     *
     * @return \AppBundle\Entity\MovimientoCaja 
     */
    public function getMovimientoCaja() {
        return $this->movimientoCaja;
    }


    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Ajuste
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return 'ajuste';
    }
}
