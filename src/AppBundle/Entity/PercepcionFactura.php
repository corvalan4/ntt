<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\TipoCosto;
use AppBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PercepcionFacturaRepository")
 * @ORM\Table(name="percepcionfactura")
 */
class PercepcionFactura {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="percepcionfacturas")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    protected $factura;

    /**
     * @ORM\ManyToOne(targetEntity="TipoPercepcion", inversedBy="percepcionfacturas")
     * @ORM\JoinColumn(name="tipopercepcion_id", referencedColumnName="id")
     */
    protected $tipopercepcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return PercepcionFactura
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PercepcionFactura
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     * @return PercepcionFactura
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura 
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set tipopercepcion
     *
     * @param \AppBundle\Entity\TipoPercepcion $tipopercepcion
     * @return PercepcionFactura
     */
    public function setTipopercepcion(\AppBundle\Entity\TipoPercepcion $tipopercepcion = null) {
        $this->tipopercepcion = $tipopercepcion;

        return $this;
    }

    /**
     * Get tipopercepcion
     *
     * @return \AppBundle\Entity\TipoPercepcion 
     */
    public function getTipopercepcion() {
        return $this->tipopercepcion;
    }

}
