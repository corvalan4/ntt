<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MovimientoCajaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MovimientoCajaRepository extends EntityRepository {

    public function findSaldoFinal($id) {
        $query = $this->createQueryBuilder('e')
                ->select('SUM(e.importe) as total')
                ->where('e.caja =:id')
                ->setParameter(':id', $id);

        return $query->getQuery()->getSingleScalarResult();
    }

}
