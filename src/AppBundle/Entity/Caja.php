<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CajaRepository")
 * @ORM\Table(name="caja")
 */
class Caja {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $saldoInicial=0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $saldoFinal=0;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="MovimientoCaja", mappedBy="caja")
     */
    protected $movimientosCaja;

   
    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->movimientosCaja = new ArrayCollection();
         $this->fecha= new \DateTime();
      
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    public function getEstadoActual() {
        if ($this->estado == 'A') {
            return 'Activa';
        }
        if ($this->estado == 'E') {
            return 'Eliminada';
        }
        if ($this->estado == 'C') {
            return 'Cerrada';
        }
        return $this->estado;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Caja
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set saldoInicial
     *
     * @param float $saldoInicial
     * @return Caja
     */
    public function setSaldoInicial($saldoInicial)
    {
        $this->saldoInicial = $saldoInicial;
    
        return $this;
    }

    /**
     * Get saldoInicial
     *
     * @return float 
     */
    public function getSaldoInicial()
    {
        return $this->saldoInicial;
    }

    /**
     * Set saldoFinal
     *
     * @param float $saldoFinal
     * @return Caja
     */
    public function setSaldoFinal($saldoFinal)
    {
        $this->saldoFinal = $saldoFinal;
    
        return $this;
    }

    /**
     * Get saldoFinal
     *
     * @return float 
     */
    public function getSaldoFinal()
    {
        return $this->saldoFinal;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Caja
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Caja
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }

    /**
     * Add movimientosCaja
     *
     * @param \AppBundle\Entity\MovimientoCaja $movimientosCaja
     * @return Caja
     */
    public function addMovimientosCaja(\AppBundle\Entity\MovimientoCaja $movimientosCaja)
    {
        $this->movimientosCaja[] = $movimientosCaja;
    
        return $this;
    }

    /**
     * Remove movimientosCaja
     *
     * @param \AppBundle\Entity\MovimientoCaja $movimientosCaja
     */
    public function removeMovimientosCaja(\AppBundle\Entity\MovimientoCaja $movimientosCaja)
    {
        $this->movimientosCaja->removeElement($movimientosCaja);
    }

    /**
     * Get movimientosCaja
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientosCaja()
    {
        return $this->movimientosCaja;
    }
}
