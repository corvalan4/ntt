<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ConsignacionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ConsignacionRepository extends EntityRepository {

    public function getAllActivas($unidad = NULL) {
        if (isset($unidad)) {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM AppBundle\Entity\Consignacion e WHERE e.unidadNegocio = :unidad ORDER BY e.id DESC')->setParameter(':unidad', $unidad);
            return $query->getResult();
        } else {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM AppBundle\Entity\Consignacion e ORDER BY e.id DESC');
            return $query->getResult();
        }
    }

    public function findByFilter($filter) {
        $query = $this->createQueryBuilder('e');

        if (!empty($filter['fecha_desde'])) {
            $query->andWhere(':fechaDesde <= e.fecha');
            $query->setParameter(':fechaDesde', $filter['fecha_desde']);
        }

        if (!empty($filter['fecha_hasta'])) {
            $to = new \DateTime($filter['fecha_hasta'] . ' +1 days');
            $query->andWhere('e.fecha < :fechaHasta');
            $query->setParameter(':fechaHasta', $to);
        }

        if (!empty($filter['cliente'])) {
            $query->join('e.clienteProveedor', 'cp')
                  ->andWhere('cp.razonSocial LIKE :cliente OR cp.cuit LIKE :cliente OR cp.denominacion LIKE :cliente')
            ;
            $query->setParameter(':cliente', '%'.$filter['cliente'].'%');
        }

        if (!empty($filter['numero'])) {
            $query->andWhere("e.numero = :idnumero")
            ->setParameter(':idnumero', $filter['numero']);

        }

        if (!empty($filter['unidad'])) {
            $query->andWhere('e.unidadNegocio = :unidadNegocio');
            $query->setParameter(':unidadNegocio', $filter['unidad']);
        }

        if (!empty($filter['estado'])) {
            $query->andWhere('e.estado = :estado');
            $query->setParameter(':estado', $filter['estado']);
        }

        $query->orderBy('e.id', 'DESC');

        return $query->getQuery()->getResult();
    }
   
}
