<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PresupuestoRepository")
 * @ORM\Table(name="presupuesto")
 */
class Presupuesto {

    const PRESUPUESTO_PENDIENTE = 'PENDIENTE';
    const PRESUPUESTO_ACTIVO = 'ACTIVO';
    const PRESUPUESTO_FACTURARLO = 'FACTURARLO';
    const PRESUPUESTO_ANULADO = 'ANULADO';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaVencimiento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $validez;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $paciente;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $plazoEntrega;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $plazo;

    /**
     * @ORM\OneToMany(targetEntity="ProductoPresupuesto", mappedBy="presupuesto", cascade={"persist"})
     */
    protected $productosPresupuesto;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="presupuestos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="presupuestos")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="vendedor_id", referencedColumnName="id",nullable=true)
     */
    protected $vendedor;

    /**
     * @ORM\ManyToOne(targetEntity="FormaPago")
     * @ORM\JoinColumn(name="formapago_id", referencedColumnName="id")
     */
    protected $formaPago;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $adjunto;

    /**
     * @ORM\ManyToOne(targetEntity="RemitoOficial", inversedBy="presupuestos")
     * @ORM\JoinColumn(name="remitooficial_id", referencedColumnName="id")
     */
    protected $remitoOficial;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $estado = Presupuesto::PRESUPUESTO_ACTIVO;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosPresupuesto = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $nro = $this->id;
        return "#" . str_pad($nro, 5, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Presupuesto
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return Presupuesto
     */
    public function setFechaVencimiento($fechaVencimiento) {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento() {
        return $this->fechaVencimiento;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Presupuesto
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Presupuesto
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Presupuesto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosPresupuesto
     *
     * @param \AppBundle\Entity\ProductoPresupuesto $productosPresupuesto
     * @return Presupuesto
     */
    public function addProductosPresupuesto(\AppBundle\Entity\ProductoPresupuesto $productosPresupuesto) {
        $this->productosPresupuesto[] = $productosPresupuesto;

        return $this;
    }

    /**
     * Remove productosPresupuesto
     *
     * @param \AppBundle\Entity\ProductoPresupuesto $productosPresupuesto
     */
    public function removeProductosPresupuesto(\AppBundle\Entity\ProductoPresupuesto $productosPresupuesto) {
        $this->productosPresupuesto->removeElement($productosPresupuesto);
    }

    /**
     * Get productosPresupuesto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosPresupuesto() {
        return $this->productosPresupuesto;
    }

    /**
     * Set unidadNegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Presupuesto
     */
    public function setUnidadNegocio(\AppBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Presupuesto
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Presupuesto
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Set sucursal
     *
     * @param \AppBundle\Entity\Sucursal $sucursal
     * @return Presupuesto
     */
    public function setSucursal(\AppBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \AppBundle\Entity\Sucursal 
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * Set vendedor
     *
     * @param \AppBundle\Entity\Usuario $vendedor
     * @return Presupuesto
     */
    public function setVendedor(\AppBundle\Entity\Usuario $vendedor = null) {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getVendedor() {
        return $this->vendedor;
    }

    /**
     * Set formaPago
     *
     * @param \AppBundle\Entity\FormaPago $formaPago
     * @return Presupuesto
     */
    public function setFormaPago(\AppBundle\Entity\FormaPago $formaPago = null) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return \AppBundle\Entity\FormaPago 
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set plazoEntrega
     *
     * @param \DateTime $plazoEntrega
     * @return Presupuesto
     */
    public function setPlazoEntrega($plazoEntrega) {
        $this->plazoEntrega = $plazoEntrega;

        return $this;
    }

    /**
     * Get plazoEntrega
     *
     * @return \DateTime 
     */
    public function getPlazoEntrega() {
        return $this->plazoEntrega;
    }

    /**
     * Set adjunto
     *
     * @param string $adjunto
     * @return Presupuesto
     */
    public function setAdjunto($adjunto) {
        $this->adjunto = $adjunto;

        return $this;
    }

    /**
     * Get adjunto
     *
     * @return string 
     */
    public function getAdjunto() {
        return $this->adjunto;
    }

    /**
     * Set validez
     *
     * @param string $validez
     * @return Presupuesto
     */
    public function setValidez($validez) {
        $this->validez = $validez;

        return $this;
    }

    /**
     * Get validez
     *
     * @return string 
     */
    public function getValidez() {
        return $this->validez;
    }

    /**
     * Set paciente
     *
     * @param string $paciente
     * @return Presupuesto
     */
    public function setPaciente($paciente) {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return string 
     */
    public function getPaciente() {
        return $this->paciente;
    }

    /**
     * Set remitoOficial
     *
     * @param \AppBundle\Entity\RemitoOficial $remitoOficial
     * @return Presupuesto
     */
    public function setRemitoOficial(\AppBundle\Entity\RemitoOficial $remitoOficial = null) {
        $this->remitoOficial = $remitoOficial;

        return $this;
    }

    /**
     * Get remitoOficial
     *
     * @return \AppBundle\Entity\RemitoOficial 
     */
    public function getRemitoOficial() {
        return $this->remitoOficial;
    }

    /**
     * Set plazo
     *
     * @param string $plazo
     * @return Presupuesto
     */
    public function setPlazo($plazo) {
        $this->plazo = $plazo;

        return $this;
    }

    /**
     * Get plazo
     *
     * @return string 
     */
    public function getPlazo() {
        return $this->plazo;
    }

}
