<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CobranzaRepository")
 * @ORM\Table(name="cobranza")
 */
class Cobranza {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="cobranzas")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadnegocio;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="cobranzas")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCaja", mappedBy="cobranza")
     */
        protected $movimientoCaja;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="cobranza", cascade={"persist"})
     */
    protected $cobranzasfactura;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="cobranza", cascade={"persist"})
     */
    protected $cheques;

    /**
     * @ORM\OneToMany(targetEntity="MedioDocumento", mappedBy="cobranza", cascade={"persist"})
     */
    protected $medios;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="pago", cascade={"persist"})
     */
    protected $pagosAsignacion;

    /**
     * @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="cobranza", cascade={"persist"})
     */
    protected $pagosAsignacionCobranza;

    /**
     * @ORM\OneToMany(targetEntity="Retencion", mappedBy="cobranza", cascade={"persist"})
     */
    protected $retenciones;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fechaRegistracion   = new \DateTime();
        $this->cobranzasfactura = new ArrayCollection();
        $this->pagosAsignacion = new ArrayCollection();
        $this->pagosAsignacionCobranza = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
        $this->medios = new ArrayCollection();
        $this->cheques = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return "COBRO " . str_pad($this->id, 5, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cobranza
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return Cobranza
     */
    public function setFechaRegistracion($fechaRegistracion) {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion() {
        return $this->fechaRegistracion;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Cobranza
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Cobranza
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cobranza
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set unidadnegocio
     *
     * @param \AppBundle\Entity\UnidadNegocio $unidadnegocio
     * @return Cobranza
     */
    public function setUnidadnegocio(\AppBundle\Entity\UnidadNegocio $unidadnegocio = null) {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get unidadnegocio
     *
     * @return \AppBundle\Entity\UnidadNegocio
     */
    public function getUnidadnegocio() {
        return $this->unidadnegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \AppBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Cobranza
     */
    public function setClienteProveedor(\AppBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \AppBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return Cobranza
     */
    public function addCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura[] = $cobranzasfactura;

        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \AppBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\AppBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzasfactura() {
        return $this->cobranzasfactura;
    }

    /**
     * Add mediosdocumento
     *
     * @param \AppBundle\Entity\MedioDocumento $mediosdocumento
     * @return Cobranza
     */
    public function addMedios(\AppBundle\Entity\MedioDocumento $mediosdocumento) {
        $this->medios[] = $mediosdocumento;

        return $this;
    }

    /**
     * Remove mediosdocumento
     *
     * @param \AppBundle\Entity\MedioDocumento $mediosdocumento
     */
    public function removeMedios(\AppBundle\Entity\MedioDocumento $mediosdocumento) {
        $this->medios->removeElement($mediosdocumento);
    }

    /**
     * Get mediosdocumento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedios() {
        return $this->medios;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return Cobranza
     */
    public function setNrocomprobante($nrocomprobante) {
        $this->nrocomprobante = $nrocomprobante;

        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string
     */
    public function getNrocomprobante() {
        return $this->nrocomprobante;
    }

    /**
     * Add retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     * @return Cobranza
     */
    public function addRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones[] = $retenciones;

        return $this;
    }

    /**
     * Remove retenciones
     *
     * @param \AppBundle\Entity\Retencion $retenciones
     */
    public function removeRetencione(\AppBundle\Entity\Retencion $retenciones) {
        $this->retenciones->removeElement($retenciones);
    }

    /**
     * Get retenciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRetenciones() {
        return $this->retenciones;
    }

    /**
     * Add medios
     *
     * @param \AppBundle\Entity\MedioDocumento $medios
     * @return Cobranza
     */
    public function addMedio(\AppBundle\Entity\MedioDocumento $medios) {
        $this->medios[] = $medios;

        return $this;
    }

    /**
     * Remove medios
     *
     * @param \AppBundle\Entity\MedioDocumento $medios
     */
    public function removeMedio(\AppBundle\Entity\MedioDocumento $medios) {
        $this->medios->removeElement($medios);
    }

    /**
     * Add pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return Cobranza
     */
    public function addPagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion[] = $pagosAsignacion;

        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\AppBundle\Entity\PagoAsignacion $pagosAsignacion) {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion() {
        return $this->pagosAsignacion;
    }

    /**
     * Add cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     * @return Cobranza
     */
    public function addCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques[] = $cheques;

        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \AppBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\AppBundle\Entity\Cheque $cheques) {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCheques() {
        return $this->cheques;
    }

    /**
     * Add pagosAsignacionCobranza
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacionCobranza
     * @return Cobranza
     */
    public function addPagosAsignacionCobranza(\AppBundle\Entity\PagoAsignacion $pagosAsignacionCobranza) {
        $this->pagosAsignacionCobranza[] = $pagosAsignacionCobranza;

        return $this;
    }

    /**
     * Remove pagosAsignacionCobranza
     *
     * @param \AppBundle\Entity\PagoAsignacion $pagosAsignacionCobranza
     */
    public function removePagosAsignacionCobranza(\AppBundle\Entity\PagoAsignacion $pagosAsignacionCobranza) {
        $this->pagosAsignacionCobranza->removeElement($pagosAsignacionCobranza);
    }

    /**
     * Get pagosAsignacionCobranza
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacionCobranza() {
        return $this->pagosAsignacionCobranza;
    }

    /**
     * Set movimientoCaja
     *
     * @param \AppBundle\Entity\MovimientoCaja $movimientoCaja
     * @return Cobranza
     */
    public function setMovimientoCaja(\AppBundle\Entity\MovimientoCaja $movimientoCaja = null) {
        $this->movimientoCaja = $movimientoCaja;

        return $this;
    }

    /**
     * Get movimientoCaja
     *
     * @return \AppBundle\Entity\MovimientoCaja 
     */
    public function getMovimientoCaja() {
        return $this->movimientoCaja;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return 'cobranza';
    }

}
