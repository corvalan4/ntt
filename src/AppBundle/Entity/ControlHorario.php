<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ControlHorarioRepository")
 * @ORM\Table(name="conotrolhorario")
 */
class ControlHorario {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="controlesHorario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=true)
     */
    protected $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuariocargaingreso_id", referencedColumnName="id", nullable=true)
     */
    protected $usuarioCargaIngreso;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuariocargaegreso_id", referencedColumnName="id", nullable=true)
     */
    protected $usuarioCargaEgreso;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaIngreso;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaEgreso;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
      
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return ControlHorario
     */
    public function setFechaIngreso($fechaIngreso) {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso() {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaEgreso
     *
     * @param \DateTime $fechaEgreso
     * @return ControlHorario
     */
    public function setFechaEgreso($fechaEgreso) {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    /**
     * Get fechaEgreso
     *
     * @return \DateTime 
     */
    public function getFechaEgreso() {
        return $this->fechaEgreso;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ControlHorario
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return ControlHorario
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario() {
        return $this->usuario;
    }


    /**
     * Set usuarioCargaIngreso
     *
     * @param \AppBundle\Entity\Usuario $usuarioCargaIngreso
     * @return ControlHorario
     */
    public function setUsuarioCargaIngreso(\AppBundle\Entity\Usuario $usuarioCargaIngreso = null)
    {
        $this->usuarioCargaIngreso = $usuarioCargaIngreso;

        return $this;
    }

    /**
     * Get usuarioCargaIngreso
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuarioCargaIngreso()
    {
        return $this->usuarioCargaIngreso;
    }

    /**
     * Set usuarioCargaEgreso
     *
     * @param \AppBundle\Entity\Usuario $usuarioCargaEgreso
     * @return ControlHorario
     */
    public function setUsuarioCargaEgreso(\AppBundle\Entity\Usuario $usuarioCargaEgreso = null)
    {
        $this->usuarioCargaEgreso = $usuarioCargaEgreso;

        return $this;
    }

    /**
     * Get usuarioCargaEgreso
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuarioCargaEgreso()
    {
        return $this->usuarioCargaEgreso;
    }
}
