<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * DolarRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DolarRepository extends EntityRepository {
    /*
     * Filtro de búsqueda 
     */

    public function filtro($fechaDesde, $fechaHasta) {
        $query = $this->createQueryBuilder('e')->where('e.estado=:estado');
        $query->setParameter(':estado', 'A');
        if (isset($fechaDesde) && $fechaDesde != '') {
            $query->andWhere(':fechaDesde <= e.fecha');
            $query->setParameter(':fechaDesde', $fechaDesde);
        }
        if (isset($fechaHasta) && $fechaHasta != '') {
            $query->andWhere('e.fecha <=  :fechaHasta');
            $query->setParameter(':fechaHasta', $fechaHasta);
        }


        $query->orderBy('e.id', 'DESC');
        return $query->getQuery()->getResult();
    }

}
