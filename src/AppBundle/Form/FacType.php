<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class FacType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
           
            ->add('idsucursal', 'text', array(
                'mapped' => false,
                'label' => 'ID - Sucursal'
            ))
            ->add('condicionpago', 'text', array(
                'label' => 'Condición de Pago',
                'required' => false,
                'attr' => array('class' => 'form-control')
            ))
            ->add('observacion', 'textarea', array(
                'label' => 'Descripción',
                'required' => false,
                'attr' => array('class' => 'form-control', 'style' => 'height: 150px;')
            ))
            ->add('bonificacion', null, array(
                    'required' => false,
                    'label' => 'Bonificación'
                )
            )
            ->add('descuento', null, array('required' => false))
           
            ->add('tipofactura', 'choice', array(
                'label' => ' Tipo de Factura',
                'attr' => array('class' => 'form-control'),
                'choices' => array(
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'NC' => 'NC',
                    'ND' => 'ND',
                    'N/A' => 'N/A'
                )))
            ->add('jurisdiccion', 'choice', array(
                'required' => false,
                'label' => 'Jurisdicción',
                'attr' => array('class' => 'form-control'),
                'choices' => array(
                    ''=>'Seleccionar una jurisdicción',
                    'caba' => 'CABA',
                    'gran-bsas' => 'Gran Buenos Aires',
                    'otros' => 'Otros'
                )))
            ->add('fecha', 'date', array(
                'label' => 'Fecha Factura',
                'attr' => array('value' => date('Y-m-d')),
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('unidadNegocio', 'entity', array(
                'class' => 'AppBundle:UnidadNegocio',
                'label' => 'Unidad Negocio',
                'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) {
                    return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');
                }
            ))           
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Factura'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_factura';
    }

}
