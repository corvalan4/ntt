<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PedidoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $usuario = $builder->getData()->getUsuario();

        $builder
            ->add('observacion', 'textarea', array(
                'label'=>'Observaciones',
                'required'=>false,
                'attr'=>array('class'=>'form-control', 'attr'=>'height: 200px;')
            ))
            ->add('fecha', 'text', array(
				'mapped'=>false,
				'label'=>'Fecha',
				'attr'=>array('class'=>'datetimepicker', 'value'=>date('Y-m-d'))
			))
            ->add('estado')
            ->add('unidadOrigen', 'entity', array (
                'class' => 'AppBundle:UnidadNegocio',
                'label' => 'Unidad Origen',
                'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository)
                    {
                         return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion', 'ASC');
                    }
                    ));
            if($usuario and $usuario->getUnidadNegocio()){
                $builder->add('unidadDestino', 'entity', array (
                    'class' => 'AppBundle:UnidadNegocio',
                    'label' => 'Unidad Destino',
                    'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) use ($usuario)
                    {
                        return $repository
                            ->createQueryBuilder('u')->where('u.estado = ?1 AND u.id = :id_unidad')
                            ->setParameter(1, 'A')
                            ->setParameter(':id_unidad', $usuario->getUnidadNegocio()->getId())
                            ->orderBy('u.descripcion', 'ASC');
                    }
                ));
            }else{
                $builder->add('unidadDestino', 'entity', array (
                    'class' => 'AppBundle:UnidadNegocio',
                    'label' => 'Unidad Destino',
                    'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository)
                    {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion', 'ASC');
                    }
                ));
            }

            $builder->add('usuario')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pedido'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_pedido';
    }
}
