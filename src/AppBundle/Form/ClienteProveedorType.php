<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClienteProveedorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('razonSocial', 'text', array('label' => $options['razonSocialLabel'].' *'))
            ->add('cuit', 'text', array('label'=>'CUIT *', 'required'=>true))
            ->add('idanterior','text', array('label'=>'ID de Referencia', 'required'=>false))
            ->add('dni','text', array('required'=>false))
            ->add('codigo', 'text', array('label'=>'Código', 'required'=>false))
//            ->add('listaprecio', 'entity', array (
//                'class' => 'AppBundle:ListaPrecio',
//                'label' => "Lista de Precio",
//                'required'=>false,
//                'attr'=>['class'=>'select2'],
//                'empty_value'=>'Lista Base',
//                'query_builder' => function (\AppBundle\Entity\ListaPrecioRepository $repository)
//                {
//                    return $repository->createQueryBuilder('u')
//                        ->where('u.estado = ?1')->setParameter(1, 'A')
//                        ->orderBy('u.descripcion');
//                }
//            ))
//
            ->add('usuario', 'entity', array (
                'class' => 'AppBundle:Usuario',
                'label' => "Usuario / Vendedor asignado",
                'required'=>false,
                'attr'=>['class'=>'select2'],
                'empty_value'=>'Selecccionar Usuario / Vendedor',
                'query_builder' => function (\AppBundle\Entity\UsuarioRepository $repository)
                {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = ?1')->setParameter(1, 'A');
                }
            ))
            ->add('denominacion','text', array('label'=>'Denominación','required'=>false))
    		->add('condicioniva', 'entity', array (
    			'class' => 'AppBundle:CondicionIva',
    			'label' => $options['condicioniva'],
                'attr'=>['class'=>'select2'],
    			'query_builder' => function (\AppBundle\Entity\CondicionIvaRepository $repository)
    				 {
    					 return $repository->createQueryBuilder('u')
                                    ->where('u.estado = ?1')->setParameter(1, 'A')
                                    ->orderBy('u.descripcion');
    				 }
    				))
            ->add('provincia')
            ->add('localidad')
            ->add('domiciliocomercial', 'text', array(
                                'label' => $options['direccionLabel'],
                                'required'=>true
                                ))
            ->add('codigopostal', 'text', array(
                                'label' => 'Codigo Postal',
                                'required'=>true
                                ))
			->add('clienteProveedor', 'choice', array (
				'label' => ' Cliente o Proveedor',
				'attr'=> array('class'=>'form-control select2'),
				'choices' => array(
                    1 => 'Cliente',
					2 => 'Proveedor'
		   		)))
			->add('saldo', 'text', array('label'=>'Saldo Inicial', 'required'=>false))
            ->add('telefono', 'text', array('label' => $options['telefonoLabel'], 'required'=>false))
            ->add('celular', 'text', array('required'=>false))
            ->add('mail', 'email', array('required'=>false))
            ->add('informacion_adicional', 'textarea', array('required'=>false, 'attr'=>['style'=>'height: 150px']))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ClienteProveedor',
			'razonSocialLabel' =>  'Razón Social',
			'condicioniva' =>  'Condición IVA',
			'direccionLabel' =>  'Domicilio Comercial',
			'telefonoLabel' =>  'Teléfono',
			'numremito' =>  'Número de Remito'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_clienteproveedor';
    }
}
