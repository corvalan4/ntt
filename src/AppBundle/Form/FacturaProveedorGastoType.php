<?php

namespace AppBundle\Form;

use AppBundle\Form\FacturaProveedorType;
use AppBundle\Entity\ClienteProveedor;
use Symfony\Component\Form\FormBuilderInterface;

class FacturaProveedorGastoType extends FacturaProveedorType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);

        $builder
                ->remove('subtotal');
        $builder
                ->add('tipoGasto', 'entity', array(
                    'class' => 'AppBundle:TipoGasto',
                    'label' => 'Tipo de gasto',                    
                    'query_builder' => function (\AppBundle\Entity\TipoGastoRepository $repository) {
                        return $repository->createQueryBuilder('u')
                                ->andWhere('u.estado = :estado')
                                ->setParameter(':estado', 'A')
                                ->orderBy('u.nombre', 'asc');
                    }
                ))
        ;
    }

}
