<?php

namespace AppBundle\Form;

use AppBundle\Entity\Stock;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StockType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        //$producto = $builder->getData()->getProducto();
        $stock = $builder->getData();
        $depositos_disponibles = $stock->getDepositos();
        $builder
                ->add('lote', null, ['label' => 'Lote *']);
        $builder
                ->add('stock', 'number', ['label' => 'Cantidad *', 'attr' => ['step' => '0.001']]);
        $builder
                ->add('costo', null, ['required' => false, 'label' => 'Costo $'])
                ->add('fecha', 'date', array(
                    'label' => 'Fecha Creación',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('deposito', 'choice', array(
                    'label' => 'Depósito',
                    'attr' => array('class' => 'form-control'),
                    'choices' => $depositos_disponibles/* array(
                          Stock::CONFORMIDAD_RIVADAVIA_5 => Stock::CONFORMIDAD_RIVADAVIA_5,
                          Stock::CONFORMIDAD_RIVADAVIA_6 => Stock::CONFORMIDAD_RIVADAVIA_6,
                          Stock::CONFORMIDAD_AZCUENAGA => Stock::CONFORMIDAD_AZCUENAGA,
                          Stock::DEPOSIT_ERROR_5 => Stock::DEPOSIT_ERROR_5,
                          ) */))
                ->add('procedencia', null, ['required' => false])
                ->add('origen', null, ['required' => false])
                ->add('producto')
        ;
        $builder
                ->add('unidadnegocio', 'entity', array(
                    'class' => 'AppBundle:UnidadNegocio',
                    'empty_value' => false,
                    'label' => 'Unidad Negocio',
                    'attr' => ['class' => 'select2'],
                    'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');
                    }
                ))
        ;
        $builder
                ->add('realizo', 'entity', array(
                    'class' => 'AppBundle:Usuario',
                    'required' => false,
                    'empty_value' => 'Seleccionar usuario',
                    'label' => '¿Qué Usuario Realizó?',
                    'attr' => ['class' => 'select2'],
                    'query_builder' => function (\AppBundle\Entity\UsuarioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.apellidonombre');
                    }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Stock'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_stock';
    }

}
