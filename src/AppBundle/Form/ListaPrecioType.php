<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ListaPrecioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', 'text', [
                    'label' => 'Descripción',
                    'attr' => array('class' => 'form-control'),
                ])
                ->add('iva', 'choice', array(
                    'label' => 'Aplica IVA?',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        '0' => 'NO',
                        '1' => 'SI',
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ListaPrecio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_listaprecio';
    }

}
