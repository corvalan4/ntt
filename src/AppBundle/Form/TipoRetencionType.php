<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TipoRetencionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion')
            ->add('importe', 'integer', array('attr'=>array('min'=>'0', 'step'=>'2')))
            ->add('porcentaje', 'integer', array('attr'=>array('min'=>'0', 'max'=>'100', 'step'=>'0.01')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TipoRetencion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_tiporetencion';
    }
}
