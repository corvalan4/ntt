<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentoType extends AbstractType
{
	private $gasto;
	
	public function __construct($tipo){
    	
		if($tipo == 'G'){
			$this->gasto = '1';
		}else{
			$this->gasto = '2';
		}
	}    

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder
				->add('fecha')
				->add('moneda', 'choice', array (
					'attr'=> array('class'=>'form-control'),
					'choices' => array(
						1 => 'ARG',
						2 => 'USD'
					)))
				->add('tipoGasto') 
                ->add('importe', 'text', array ('label' => 'Importe', 'attr'=> array('class'=>'form-control','pattern'=>'[0-9]+([\.,][0-9]+)?','step'=>'0.01','title'=>'Se espera un numero de la forma 000000.00 o 000000,00')))
				->add('tipoDocumento')
				->add('descripcion')
				->add('nrofactura')
				->add('nroremito')
                ->add('observacion', 'textarea', array ('label' => 'Observaciones', 'attr'=> array('class'=>'form-control', 'style'=>'height:200px')))
				->add('nrocomprobante');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Documento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_documento';
    }
}
