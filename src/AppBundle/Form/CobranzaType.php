<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CobranzaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $clienteProveedor = $builder->getData()->getClienteProveedor();

        $builder
                ->add('fecha', 'date', array(
                    'label' => 'Fecha',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('fechaRegistracion')
                ->add('importe')
                ->add('descripcion', 'textarea', array(
                    'required' => false,
                    'attr' => array('style' => 'height: 200px;',
                        'class' => 'form-control')))
                ->add('nrocomprobante')
                ->add('unidadnegocio');
        if ($clienteProveedor) {
            $builder
                    ->add('clienteProveedor', 'entity', array(
                        'class' => 'AppBundle:ClienteProveedor',
                        'label' => 'Cliente',
                        'query_builder' => function (\AppBundle\Entity\ClienteProveedorRepository $repository) use ($clienteProveedor) {
                            return $repository->createQueryBuilder('c')
                                    ->where('c.id = ?1')
                                    ->setParameter(1, $clienteProveedor->getId());
                        }
            ));
        } else {
            $builder->add('clienteProveedor', 'text', array(
                'mapped' => false,
                'label' => 'Cliente'
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Cobranza'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_cobranza';
    }

}
