<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $stock = $builder->getData()->getStock();
        $maxAmount = $stock ? $stock->getStock() : 1000;
        $builder
            ->add('cliente', 'entity', array(
                'class' => 'AppBundle:ClienteProveedor',
                'label' => 'Cliente / Banco',
                'required' => true,
                'empty_value' => 'Seleccionar Cliente',
                'attr'=>['class'=>'select2'],
                'query_builder' => function (\AppBundle\Entity\ClienteProveedorRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = :ACTIVO')
                        ->andWhere('u.clienteProveedor = 1')
                        ->orderBy('u.razonSocial', 'asc')
                        ->setParameter(':ACTIVO', 'A');
                }
            ))
            ->add('usuario', null, ['required' => true, 'attr'=>['class'=>'select2', 'min'=>1]])
            ->add('cantidad', null, ['attr'=>['max'=>$maxAmount]])
            ->add('descripcion', 'textarea', ['required'=>false, 'attr'=>['style'=>'height: 200px']])
            ->add('stock')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reserva'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_reserva';
    }
}
