<?php

namespace AppBundle\Form;

use AppBundle\Entity\Factura;
use AppBundle\Entity\ClienteProveedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotaCreditoDebitoProveedorType extends AbstractType {

    private $proveedor;

    function __construct($proveedor) {
        $this->proveedor = $proveedor;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        //$clienteproveedor = $builder->getData()->getClienteProveedor();

        $builder
                ->add('clienteProveedor', 'entity', array(
                    'class' => 'AppBundle:ClienteProveedor',
                    'label' => 'Proveedor',
                    'query_builder' => function (\AppBundle\Entity\ClienteProveedorRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.id = :idClienteproveedor')
                                ->setParameter(':idClienteproveedor', $this->proveedor->getId());
                    }
                        )
                )
                ->add('tiponota', 'hidden', array())
                ->add('nrocomprobante', null, array(
                    'label' => 'Nro. Comprobante',
                    'required' => false
                ))
                ->add('facturas', 'entity', array(
                    'class' => 'AppBundle:Factura',
                    'label' => 'Facturas',
                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (\AppBundle\Entity\FacturaRepository $repository) {
                        return $repository->createQueryBuilder('u')
                                ->where('u.clienteProveedor = :clienteproveedor')
                                ->orderBy('u.id', 'DESC')
                                ->setParameter(':clienteproveedor', $this->proveedor->getId())
                                ->setMaxResults(30);
                    },
                    'choice_label' => function ($factura) {
                        return strip_tags($factura);
                    }
                ))
                ->add('descripcion', 'textarea', array(
                    'label' => 'Descripción',
                    'required' => false,
                    'attr' => array('class' => 'form-control',
                        'rows' => '10'
                    )
                ))
                ->add('fecha', 'date', array(
                    'label' => 'Fecha Factura',
                    'attr' => array('value' => date('Y-m-d')),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('importe', null, array(
                    'label' => 'Importe',
                    'required' => true,
                    'attr' => array('class' => 'form-control')
                ))
                ->add('unidadNegocio', 'entity', array(
                    'class' => 'AppBundle:UnidadNegocio',
                    'label' => 'Unidad Negocio',
                    'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');
                    }
                        )
                )
                ->add('codigonota', 'choice', [
                    'label' => 'Tipo de Nota',
                    'choices' =>
                    ['A' => 'A',
                        'B' => 'B']
                ])


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\NotaCreditoDebito'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_notacreditodebito';
    }

}
