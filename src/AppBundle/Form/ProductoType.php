<?php

namespace AppBundle\Form;

use AppBundle\Entity\Diseno;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $categoria = $builder->getData()->getCategoria() ? $builder->getData()->getCategoria() : null;

        $builder
            ->add('nombre', 'text', ['label' => 'Nombre *'])
            ->add('descripcion', 'text', ['label' => 'Descripción *'])
            ->add('codigo', 'text', array('label' => 'Código *'))
            ->add('gtin', 'hidden', array('label' => 'GTIN *', 'required'=>false))
            ->add('codigoOriginal', 'hidden', array('label' => 'Código Original', 'required' => false))
            ->add('pm', 'hidden', array('label' => 'PM', 'required' => false))
            ->add('precio', 'text', ['label' => 'Precio Base *', 'required' => true])
            ->add('categoria', 'entity', array(
                'class' => 'AppBundle:Categoria',
                'label' => 'Categoría',
                'mapped' => true,
                'required' => false,
                'empty_value' => 'Seleccionar categoría',
                'data' => $categoria,
                'query_builder' => function (\AppBundle\Entity\CategoriaRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = :ACTIVO')
                        ->orderBy('u.descripcion', 'asc')
                        ->setParameter(':ACTIVO', 'A');
                }
            ))
            ->add('proveedor', 'hidden', array(
                'label' => 'Proveedor',
                'required' => false,
            ))
            ->add('subcategoria',null, array(
                    'label' => 'SubCategoría')
            )
            ->add('marca', 'hidden', ['required'=>false])
            ->add('modelo', 'hidden', ['required'=>false])
            ->add('medida', 'hidden', ['required'=>false])
            ->add('tipoiva', 'choice', array(
                'label' => 'Tipo Iva',
                'attr' => array('class' => 'form-control'),
                'choices' => array(
                    '21' => '21',
                    '10.5' => '10.5',
                    '0' => '0',
                )))
            ->add('facturable', 'hidden', ['required' => false])
            ->add('trazable', 'hidden', ['required' => false])
            ->add('stock_inicial', 'integer', ['required' => false, 'mapped'=>false])
            ->add('detalle', 'textarea', ['attr' => ['style' => 'height:150px;'], 'required' => false])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_producto';
    }

}
