<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AjusteType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                 ->add('fecha', 'date', array(
                    'label' => 'Fecha Factura',
                    'attr' => array('value' => date('Y-m-d')),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                 ->add('importe')  
                ->add('descripcion', 'textarea', array(
                    'label' => 'Descripción',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'attr' => 'height: 200px;')
                ))
                             
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Ajuste'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_ajuste';
    }

}
