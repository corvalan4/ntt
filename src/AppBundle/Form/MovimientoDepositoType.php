<?php

namespace AppBundle\Form;

use AppBundle\Entity\Stock;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MovimientoDepositoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $stock = $builder->getData()->getStock();
        $depositos_disponibles = [
            Stock::CAMARGO => Stock::CAMARGO,
            Stock::SENILLOSA => Stock::SENILLOSA,
            Stock::LEONISMO => Stock::LEONISMO,
            Stock::MAZZOLA => Stock::MAZZOLA
        ];

        if ($stock) {
            $builder
                    ->add('stock', 'entity', array(
                        'class' => 'AppBundle:Stock',
                        'label' => 'Stock',
                        'query_builder' => function (\AppBundle\Entity\StockRepository $repository) use ($stock) {
                            return $repository->createQueryBuilder('u')->where('u.id = :stock')->setParameter(':stock', $stock->getId());
                        }
                            )
            );
            unset($depositos_disponibles[$stock->getDeposito()]);
        } else {
            $builder
                    ->add('stock');
        }

        $builder
                ->add('deposito_desde', 'text', ['read_only' => true])
                ->add('deposito_hasta', 'choice', array(
                    'label' => 'Depósito Hasta',
                    'attr' => array('class' => 'form-control'),
                    'choices' => $depositos_disponibles))
                ->add('motivo', null, ['required' => true])
                ->add('descripcion', 'textarea',
                        [
                            'label' => 'Descripción del pasaje',
                            'attr' => ['style' => 'height: 200px']
                        ]
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\MovimientoDeposito'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_movimientodeposito';
    }

}
