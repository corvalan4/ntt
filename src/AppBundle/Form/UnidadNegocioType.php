<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnidadNegocioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion', 'text', array('label' => $options['descripcionLabel']))
            ->add('responsable')
            ->add('listaprecio', 'entity', array (
                'class' => 'AppBundle:ListaPrecio',
                'label' => "Lista de Precio",
                'required'=>false,
                'empty_value'=>'Lista Base',
                'query_builder' => function (\AppBundle\Entity\ListaPrecioRepository $repository)
                {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = ?1')->setParameter(1, 'A')
                        ->orderBy('u.descripcion');
                }
            ))
            ->add('cuit', 'text', array('label' => 'CUIT'))
            ->add('iibb', null, ['required'=>false])
            ->add('direccion', 'text', array('label' => $options['direccionLabel']))
            ->add('telefono', 'text', array('label' => $options['telefonoLabel']))
            ->add('punto', 'text', array('label' => "Nro. Punto de Venta"))
            ->add('celular', null, ['required'=>false])
            ->add('mail', null, ['required'=>false])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
             'data_class' => 'AppBundle\Entity\UnidadNegocio',
			'descripcionLabel' =>  'Descripción',
			'direccionLabel' =>  'Dirección',
			'telefonoLabel' =>  'Teléfono'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_unidadnegocio';
    }
}
