<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConsignacionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cliente', 'text', array(
                'mapped' => false,
                'label' => 'Cliente'
            ))
            ->add('idsucursal', 'text', array(
                'mapped' => false,
                'label' => 'ID - Sucursal'
            ))
            ->add('observacion', 'textarea', array(
                'label' => 'Descripción',
                'required' => false,
                'attr' => array('class' => 'form-control', 'attr' => 'height: 200px;')
            ))
            ->add('numero', 'text', array(
                'label' => 'Número de Consignacion',
                'required'=>true
            ))
            ->add('fecha', 'date', array(
                'label' => 'Fecha Consignacion',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('fechaVencimiento', 'date', array(
                'label' => 'Fecha Vencimiento',
                'required'=>false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('unidadNegocio', 'entity', array(
                'class' => 'AppBundle:UnidadNegocio',
                'label' => 'Unidad Negocio',
                'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) {
                    return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');
                }
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Consignacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_consignacion';
    }
}
