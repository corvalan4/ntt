<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellidonombre', null, ['label'=>'Apellido y Nombre'])
            ->add('email', null, ['label'=>'Email'])
            ->add('username', null, ['label'=>'Nombre de Usuario (Sin espacios ni guiones)'])
            ->add('sector', null, ['label'=>'Sector en la empresa'])
            ->add('enabled', 'choice', [
                'label'=>'Habilitado para el Log In?',
                'choices'=>[false=>'NO', true=>'SI']
            ])
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
                'required'=>false
            ))
            ->add('unidadNegocio', null, ['attr'=>['class'=>'select2']])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_usuario';
    }
}
