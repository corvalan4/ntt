<?php

namespace AppBundle\Form;

use AppBundle\Form\FacType;
use AppBundle\Entity\ClienteProveedor;
use Symfony\Component\Form\FormBuilderInterface;

class FacturaProveedorType extends FacType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);

        $builder
                ->add('proveedor', 'text', array(
                    'mapped' => false,
                    'label' => 'Proveedor'
                ))
               
                ->add('percepcionib', null, array('required' => false, 'label' => 'Percepción IIBB'))
                ->add('percepcioniva', null, array('required' => false, 'label' => 'Percepción IVA'))
                ->add('cae', 'text', array('label' => 'CAE', 'required' => true, 'attr' => array('minlength' => '14', 'maxlength' => '14')))
                ->add('fechavtocae', 'date', array(
                    'label' => 'Fecha Vto. CAE',
                    'attr' => array('value' => date('Y-m-d')),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('mes', 'integer', array(
                    'label' => 'Mes',
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('min' => 1, 'max' => 12)))
                ->add('anio', 'integer', array(
                    'label' => 'Año',
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('min' => 2017, 'max' => date('Y'))))
                ->add('subtotal', 'text', array(
                    'mapped' => false,
                    'label' => 'Subtotal',
                    'attr' => array('readonly' => 'readonly')
                ))
               
        ;
    }

}
