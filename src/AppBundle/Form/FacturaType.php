<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturaType extends FacType {

    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder
            ->add('cliente', 'text', array(
                'mapped' => false,
                'label' => 'Cliente'
            ))        
            ->add('listaprecio', 'entity', array(
                'class' => 'AppBundle:ListaPrecio',
                'label' => 'Lista de Precios',
                'mapped' => false,
                'required' => false,
                'empty_value' => 'Lista de Precio Base',
                'query_builder' => function (\AppBundle\Entity\ListaPrecioRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = :ACTIVO')
                        ->orderBy('u.descripcion', 'asc')
                        ->setParameter(':ACTIVO', 'A');
                }
            ))
        ;
    }

}
