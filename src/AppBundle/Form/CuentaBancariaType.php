<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CuentaBancariaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', 'text', array('label' => 'Descripción'))
                ->add('nrocuenta', 'text', array('label' => 'Nro. Cuenta'))
                ->add('banco', 'entity', array(
                    'class' => 'AppBundle:Banco',
                    'label' => 'Banco',
                    'query_builder' => function (\AppBundle\Entity\BancoRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');
                    }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CuentaBancaria'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_cuentabancaria';
    }

}
