<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PagoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clienteProveedor = $builder->getData()->getClienteProveedor();
        $builder
            ->add('importe')
            ->add('descripcion', 'textarea', array('required'=>false, 'attr'=>array('style'=>'height: 200px;', 'class'=>'form-control')))
            ->add('descuento')
            ->add('bonificacion')
            ->add('estado')
            ->add('puntoventa')
            ->add('unidadnegocio')
            ->add('fecha', 'date', array(
                'label'=>'Fecha',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
        ;
        if($clienteProveedor){
            $builder
                ->add('clienteProveedor', 'entity', array (
                    'class' => 'AppBundle:ClienteProveedor',
                    'label' => 'Cliente',
                    'attr'=>['class'=>'select2'],
                    'query_builder' => function (\AppBundle\Entity\ClienteProveedorRepository $repository) use ($clienteProveedor)
                    {
                        return $repository->createQueryBuilder('c')
                            ->where('c.id = ?1')
                            ->setParameter(1, $clienteProveedor->getId());
                    }
                ));
        }else{
            $builder
                ->add('clienteProveedor', 'entity', array (
                    'class' => 'AppBundle:ClienteProveedor',
                    'attr'=>['class'=>'select2'],
                    'label' => 'Cliente',
                    'query_builder' => function (\AppBundle\Entity\ClienteProveedorRepository $repository)
                    {
                        return $repository->createQueryBuilder('c')
                            ->where('c.clienteProveedor = 2')->orderBy("c.razonSocial", "ASC");
                    }
                ));
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pago'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_pago';
    }
}
