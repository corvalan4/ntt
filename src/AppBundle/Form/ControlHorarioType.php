<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ControlHorarioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fechaIngreso', 'datetime',
                        ['widget' => 'single_text',
                            'html5' => false,
                            'format' => 'dd-MM-yyyy H:mm',
                            'label' => 'Fecha de Ingreso',
                            'required' => false,
                            'attr' => ['class' => 'datetimepickerHoras', 
                                'autocomplete' => 'off',
                                'placeholder' => 'Ingrese una Fecha de Ingreso'
                                ]])
                ->add('fechaEgreso', 'datetime',
                        ['widget' => 'single_text',
                            'html5' => false,
                            'format' => 'dd-MM-yyyy H:mm',
                            'label' => 'Fecha de Egreso',
                            'required' => false,
                            'attr' => ['class' => 'datetimepickerHoras',
                                'autocomplete' => 'off',
                                'placeholder' => 'Ingrese una Fecha de Egreso'
                                ]])
                ->add('usuario', 'entity', array(
                    'class' => 'AppBundle:Usuario',
                    'label' => 'Usuario',
                    'attr' => ['class'=>'select2'],
                    'query_builder' => function (\AppBundle\Entity\UsuarioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.apellidonombre');
                    }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ControlHorario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_controlhorario';
    }

}
