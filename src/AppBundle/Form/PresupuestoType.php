<?php

namespace AppBundle\Form;

use AppBundle\Entity\FormaPago;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PresupuestoType extends AbstractType {

    private $update;

    function __construct($update = false) {
        $this->update = $update;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cliente', 'text', array(
                    'mapped' => false,
                    'label' => 'Cliente'
                ))
                ->add('idsucursal', 'text', array(
                    'mapped' => false,
                    'label' => 'ID - Sucursal'
                ))
                ->add('observacion', 'textarea', array(
                    'label' => 'Descripción',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'attr' => 'height: 200px;')
                ))
                ->add('fecha', 'date', array(
                    'label' => 'Fecha Presupuesto',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('validez', 'text', array(
                    'label' => 'Validez',
                    'required' => false,
                ))
                ->add('paciente', 'text', array(
                    'label' => 'Paciente',
                    'required' => false,
                ))
                ->add('fechaVencimiento', 'date', array(
                    'label' => 'Fecha Vencimiento',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('plazoEntrega', 'date', array(
                    'label' => 'Plazo Entrega',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('plazo', 'text', array(
                    'label' => 'plazo',
                    'required' => false,
                ))
                ->add('formaPago', 'entity', array(
                    'class' => 'AppBundle:FormaPago',
                    'label' => 'Forma de Pago',
                    'query_builder' => function (\AppBundle\Entity\FormaPagoRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', FormaPago::ACTIVO);
                    }
                ))
                ->add('unidadNegocio', 'entity', array(
                    'class' => 'AppBundle:UnidadNegocio',
                    'label' => 'Unidad Negocio',
                    'query_builder' => function (\AppBundle\Entity\UnidadNegocioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');
                    }
        ));

        if (!$this->update) {
            $builder
                    ->add('adjunto', 'file', array(
                        'label' => 'Archivo adjunto',
                        'required' => false,
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Presupuesto'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app_bundle_presupuesto';
    }

}
