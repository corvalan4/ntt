<?php

namespace AppBundle\Services;

use Psr\Container\ContainerInterface;
use AppBundle\Repository\BalanceRepository;
use function React\Promise\all;
use Symfony\Component\DependencyInjection\Container;

class StockService {

    private $stockRepository;

    public function __construct(Container $container) {
        $this->reservaRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Reserva');
        $this->stockRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Stock');
    }

    /**
     * Obtengo Stock por Codigo Lote Vencimiento
     */
    public function sumByCodigoLoteVencimiento($codigo, $lote, $vencimiento, $filtro, $print = true) {
        $stock = $this->stockRepository->getSumByCodigoLoteVencimiento($codigo, $lote, $vencimiento, $filtro);

        if ($print === true) {
            return $stock;
        } else {
            return $stock;
        }
    }

    /**
     * Obtengo Stock por Codigo Lote Vencimiento
     */
    public function reservaPorStock($stock_id) {
        $amount = $this->reservaRepository->findAmountByStock($stock_id);
        return (int)$amount;
    }

    /**
     * Obtengo Stocks por Codigo Lote Vencimiento
     */
    public function stocksByCodigoLoteVencimiento($codigo, $lote, $vencimiento, $filtro) {
        $stock = $this->stockRepository->getStocksByCodigoLoteVencimiento($codigo, $lote, $vencimiento, $filtro);

        return $stock;
    }

}
