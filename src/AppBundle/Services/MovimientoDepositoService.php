<?php

namespace AppBundle\Services;

use Psr\Container\ContainerInterface;
use AppBundle\Repository\MovimientoDepositoRepository;
use Symfony\Component\DependencyInjection\Container;

class MovimientoDepositoService {

    private $movimientoDepositoRepository;

    public function __construct(Container $container) {
        $this->movimientoDepositoRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:MovimientoDeposito');
    }

    /**
     * Obtengo Motivo de Devolucion
     */
   /* public function getMotivoDevolucion($stockId, $print = null) {
        $movimientoDeposito = $this->movimientoDepositoRepository->getDevolucionByStock($stockId);
        if (!$movimientoDeposito) {
            $response = null;
        } else {
            $response = $movimientoDeposito->getDescripcion();
        }
        if ($print === true) {
            echo $response;
        } else {
            return $response;
        }
    }
*/
}
