<?php

namespace AppBundle\Services;

use Psr\Container\ContainerInterface;
use AppBundle\Repository\BalanceRepository;
use Symfony\Component\DependencyInjection\Container;

class BalanceService {

    private $balanceRepository;

    public function __construct(Container $container) {
        $this->balanceRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Balance');
    }

    /**
     * Obtengo Stock por Producto Lote Despacho
     */
    public function stockByProductoLoteDespacho($productoId, $lote, $despacho, $print = true) {
        $stock = $this->balanceRepository->getStockByProductoLoteDespacho($productoId, $lote, $despacho);
        if ($print === true) {
            echo $stock;
        } else {
            return $stock;
        }
    }

    /**
     * Obtengo Rango de Nro. Serie para balance Producto Lote Despacho
     */
    public function rangoNroSeriePorProductoLoteDespacho($productoId, $lote, $despacho, $print = true) {
        $maxNroSerie = $this->balanceRepository->getMaxNroSerieByProductoLoteDespacho($productoId, $lote, $despacho);
        $minNroSerie = $this->balanceRepository->getMinNroSerieByProductoLoteDespacho($productoId, $lote, $despacho);
        $rangoSerie = 'Desde ' . $minNroSerie . ' al ' . $maxNroSerie;
        if ($print === true) {
            echo $rangoSerie;
        } else {
            return $rangoSerie;
        }
    }
    /*
     * Verificación si el stock tuvo movimientos
     */
    public function verifyMovimientos($id) {
        if ($this->balanceRepository->stockFacturaTieneMovimientos($id)) {
            return false;            
        }
        return true;
    }

}
