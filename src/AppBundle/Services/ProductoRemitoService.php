<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductoRemito;
use Psr\Container\ContainerInterface;
use AppBundle\Repository\ProductoRemitoRepository;
use Symfony\Component\DependencyInjection\Container;

class ProductoRemitoService {

    private $productoRemitoRepository;

    public function __construct(Container $container) {
        $this->productoRemitoRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:ProductoRemito');
    }

   public function getProductoRemitoByStock($stockId){
        $productoRemito = $this->productoRemitoRepository->findOneBy(['stock' => $stockId]);

        return ($productoRemito and $productoRemito->getEstado() == ProductoRemito::PRODUCTO_REMITO_CANCEL) ? null : $productoRemito;
   }

}
