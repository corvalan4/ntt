<?php

namespace AppBundle\Services;

use AppBundle\Entity\Balance;
use AppBundle\Entity\Caja;
use AppBundle\Entity\NotaCreditoDebito;
use AppBundle\Entity\Stock;
use AppBundle\Entity\UnidadNegocio;
use AppBundle\Entity\Usuario;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
// AFIP
use AppBundle\Afip\Exceptionhandler;
use AppBundle\Afip\WSAA;
use AppBundle\Afip\WSFEV1;
// Se agrega para el manejo de archivos
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
//Se agrega para exportación a excel con spreadSheet
use AppBundle\Form\Type\ExcelFormatType;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SessionManager {

    /**
     *
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var Session
     */
    public $session;
    public $cbu;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->session = $container->get('session');
        $this->cbu = "2850329330000000119662";
    }

    /**
     * Agrega un mensaje de tipo msgType a la siguiente response.
     * msgType validos: msgOk, msgInfo, msgWarn, msgError.
     * @param string $msgType
     * @param string $msg
     */
    public function addFlash($msgType, $msg) {
        $this->session->getFlashBag()->add($msgType, $msg);
    }

    /**
     * Setea un parametro en la sesion
     * @param string $attr
     * @param string $value
     * @return mixed
     */
    public function setSession($attr, $value) {
        $this->session->set($attr, $value);
    }

    /**
     * Devuelve un valor de la sesion
     * @param string $attr
     * @return mixed
     */
    public function getSession($attr) {
        return $this->session->get($attr);
    }

    public function facturar($entity) {
        $cuitEmisor = str_replace("-", "", $entity->getUnidadNegocio()->getCuit());

        $produccion = $this->container->getParameter('produccion');
        $wsaa = new WSAA($produccion, 'wsfe');

        if ($wsaa->get_expiration() < (new \DateTime('NOW'))) {
            $wsaa->generar_TA();
        }

        $wsfe = new WSFEV1($produccion);
        $wsfe->openTA();
        $ptovta = $entity->getPtovta();

        $tipofactura = $entity->getTipofactura();
        $tipofacturastr = $entity->getTipoFacturaStr() . " " . $tipofactura;
        //        $tipocbte = ($entity->getTipofactura() == 'A') ? 01 : 06; // OLD
        $tipocbte = (int) $this->getTipoComprobante($tipofacturastr);

        $nc = '';
        $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta, $cuitEmisor);

        $regfac['concepto'] = 1;                          # 1: productos, 2: servicios, 3: ambos
        $regfac['tipodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 99 : 80; // Aca falta el 96 si es nroDoc.
        $regfac['numerodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 0 : $entity->getClienteProveedor()->getDni(); # 0 para Consumidor Final (<$10000)
        $regfac['cuit'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 0 : str_replace("-", "", $entity->getClienteProveedor()->getCuit()); # 0 para Consumidor Final (<$10000)
        $regfac['capitalafinanciar'] = 0;

        // Trato de IVA
        //$baseimp = 0;
        $regfac['detalleiva'] = [];

        if (count($entity->getIvas()) > 0) {
            $importeIva = 0;
            foreach ($entity->getIvas() as $iva) {
                if ($iva->getTipoIva()->getPorcentaje() == 0) {
                    //$importeiva['21'] = $iva->getValor();
                    //$baseimp = (100 * $importeiva['21'] / 21);

                    $regfac['detalleiva'][] = [
                        'Id' => 3,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                            //'BaseImp' => $baseimp,                   
                            //'BaseImp' => $datosfactura['baseimp'],                   
                            //'Importe' => round($datosfactura['importeiva'], 2)
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                }


                if ($iva->getTipoIva()->getPorcentaje() == 21) {
                    //$importeiva['21'] = $iva->getValor();
                    //$baseimp = (100 * $importeiva['21'] / 21);

                    $regfac['detalleiva'][] = [
                        'Id' => 5,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                            //'BaseImp' => $baseimp,                   
                            //'BaseImp' => $datosfactura['baseimp'],                   
                            //'Importe' => round($datosfactura['importeiva'], 2)
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                }
                if ($iva->getTipoIva()->getPorcentaje() == 10.5) {

                    $regfac['detalleiva'][] = [
                        'Id' => 4,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                            //'BaseImp' => $baseimp,                   
                            //'BaseImp' => $datosfactura['baseimp'],                   
                            //'Importe' => round($datosfactura['importeiva'], 2)
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                    //$importeiva['10-5'] = $iva->getValor();
                    //$baseimp = (100 * $importeiva['10-5'] / 10.5);
                }
            }
        }

        if ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {

            $regfac['coniva'] = "SI";
            //$regfac['importetotal'] = $entity->getImporte();	       # total del comprobante

            $regfac['importetotal'] = $entity->getTotal();        # total del comprobante

            if (count($entity->getIvas()) > 0) {
                //$regfac['importeiva'] = (!empty($importeiva['21']) ? $importeiva['21'] : 0);                    # subtotal neto sujeto a IVA
                $regfac['importeiva'] = $importeIva;
            } else {
                $regfac['importeiva'] = 0;
            }

//            $regfac['baseimp'] = round($baseimp, 2);

            $regfac['importeneto'] = $entity->getImporte();
            //$regfac['importeneto'] = ($entity->getImporte() - round($importeiva['21'] ,2));            
        } else {
            $regfac['importetotal'] = $entity->getImporte(); # total del comprobante
            $regfac['importeiva'] = 0;
            $regfac['importeneto'] = 0;   # subtotal neto sujeto a IVA
            $regfac['coniva'] = "NO";
            $regfac['capitalafinanciar'] = $entity->getImporte();
        }

        $regfac['fechaemision'] = $entity->getFecha()->format('Ymd');
        $regfac['fechadesde'] = '';
        $regfac['fechahasta'] = '';

        $regfac['fecha_'] = '';
        $regfac['imp_trib'] = 1.0;
        $regfac['imp_op_ex'] = 0.0;
        $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
//        $regfac['fecha_venc_pago'] = $tipofacturastr == 'FACTURA CREDITO' ? date('Ymd') : '';
        $regfac['fecha_venc_pago'] = $entity->getTipoFacturaStr() == 'FACTURA DE CREDITO' ? date('Ymd') : '';

        if ($entity->getTipoFacturaStr() == 'FACTURA DE CREDITO') {
            $regfac['opcionales'] = ['Id' => '2101', 'Valor' => $this->cbu];
        }

        // Armo con la factura los parametros de entrada para el pedido
        $params = $wsfe->armadoFacturaUnica(
                $tipofactura,
                $ptovta, // el punto de venta
                $nc,
                $regfac, // los datos a facturar
                $cuitEmisor,
                $tipocbte
        );

        //Solicito el CAE
        try {

            $cae = $wsfe->solicitarCAE($params, $cuitEmisor);

            if (!empty($cae->FECAESolicitarResult->Errors)) {
                $this->addFlash('msgError', 'Error al dar de alta factura: ' . json_encode($cae->FECAESolicitarResult->Errors));
                $entity->setObservacion($entity->getObservacion() . " - ERROR: " . json_encode($cae->FECAESolicitarResult->Errors));
                $this->em->flush();
                return false;
            }

            if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'A' or $cae->FECAESolicitarResult->FeCabResp->Resultado == 'P') {
                $entity->setNrofactura($regfac['nrofactura']);
                $entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
                $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
                $this->em->flush();
                return true;
            } else {
                if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'R') {
                    $observaciones = "";
                    if (isset($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones)) {
                        $observaciones = json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones);
                        $entity->setObservacion($entity->getObservacion() . " - ERROR: $observaciones");
                        $this->em->flush();
                    }
                    if (isset($cae->FECAESolicitarResult->Errors->Err)) {
                        $observaciones = json_encode($cae->FECAESolicitarResult->Errors->Err);
                        $entity->setObservacion($entity->getObservacion() . " - ERROR: $observaciones");
                        $this->em->flush();
                    }
                    $this->addFlash('msgError', 'Error al dar de alta factura: ' . json_encode($observaciones));
                }
                return false;
            }
        } catch (\Exception $exception) {
            $this->addFlash('msgError', $exception->getMessage());
            return false;
        }
    }

    public function facturarNota(NotaCreditoDebito $entity) {
        $cuitEmisor = str_replace("-", "", $entity->getUnidadNegocio()->getCuit());

        $produccion = $this->container->getParameter('produccion');
        $wsaa = new WSAA($produccion, 'wsfe');

        if ($wsaa->get_expiration() < (new \DateTime('NOW'))) {
            $wsaa->generar_TA();
        }

        $wsfe = new WSFEV1($produccion);
        $wsfe->openTA();
        $ptovta = $entity->getPtovta();
        $tipofactura = $entity->getCodigonota();

        $tipoFacturaAsociada = '';

        if (sizeof($entity->getFacturas()) > 0) {
            $comprobantes = [];
            foreach ($entity->getFacturas() as $factura) {
                $tipoFacturaAsociada = $factura->getTipoFacturaStr() . " " . $factura->getTipofactura();
                $comprobantes[] = [
                    'Tipo' => (int) $this->getTipocomprobante($tipoFacturaAsociada),
                    'PtoVta' => $factura->getPtovta(),
                    'Nro' => (int) $factura->getNrofactura(),
                    'Cuit' => $factura->getClienteProveedor()->getCuit(),
                    'CbteFch' => $factura->getFecha()->format('Ymd'),
                ];
            }
            $factura = $entity->getFacturas()[0];
            $tipoFacturaAsociada = $factura->getTipoFacturaStr();

            $regfac['CbtesAsoc'] = $comprobantes;
        }

        $tiponota = "NOTA DE " . $entity->getTiponota();
        $tiponota .= (!empty($tipoFacturaAsociada) and $tipoFacturaAsociada == 'FACTURA DE CREDITO') ? " " . $tipoFacturaAsociada : '';
        $tiponota .= " " . $tipofactura;
        $tipocbte = (int) $this->getTipoComprobante($tiponota);
        $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta, $cuitEmisor);

        $regfac['concepto'] = 1;                 # 1: productos, 2: servicios, 3: ambos
        $regfac['tipodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 99 : 80; // Aca falta el 96 si es nroDoc.
        $regfac['numerodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 0 : $entity->getClienteProveedor()->getDni(); # 0 para Consumidor Final (<$10000)
        $regfac['cuit'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final' and $entity->getImporte() < 10000) ? 0 : str_replace("-", "", $entity->getClienteProveedor()->getCuit()); # 0 para Consumidor Final (<$10000)
        $regfac['capitalafinanciar'] = 0;


        $regfac['detalleiva'] = [];

        if (count($entity->getIvas()) > 0) {
            $importeIva = 0;
            foreach ($entity->getIvas() as $iva) {
                if ($iva->getTipoIva()->getPorcentaje() == 0) {
                    $regfac['detalleiva'][] = [
                        'Id' => 3,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                }
                if ($iva->getTipoIva()->getPorcentaje() == 21) {
                    $regfac['detalleiva'][] = [
                        'Id' => 5,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                }
                if ($iva->getTipoIva()->getPorcentaje() == 10.5) {

                    $regfac['detalleiva'][] = [
                        'Id' => 4,
                        'BaseImp' => $iva->getImporteFactura(),
                        'Importe' => $iva->getValor()
                    ];
                    $importeIva = $importeIva + $iva->getValor();
                }
            }
        }





        /* if (count($entity->getIvas()) > 0) {
          foreach ($entity->getIvas() as $iva) {
          if ($iva->getTipoIva()->getPorcentaje() == 21) {
          $importeiva['21'] = $iva->getValor();
          $baseimp = (100 * $importeiva['21'] / 21);
          }
          if ($iva->getTipoIva()->getPorcentaje() == 10.5) {
          $importeiva['10-5'] = $iva->getValor();
          $baseimp = (100 * $importeiva['10-5'] / 10.5);
          }
          }
          } */

        if ($entity->getClienteProveedor()->getCondicioniva() == 'Responsable Inscripto') {
                        
            $regfac['coniva'] = "SI";
            $regfac['importetotal'] = $entity->getTotal();        # total del comprobante
            
            if (count($entity->getIvas()) > 0) {            
                $regfac['importeiva'] = $importeIva;
            } else {
                $regfac['importeiva'] = 0;
            }

            $regfac['importeneto'] = $entity->getImporte();

            
            
            
            /*$regfac['coniva'] = "SI";
            $regfac['importetotal'] = $entity->getImporte();                                 # total del comprobante
            $regfac['importeiva'] = (!empty($importeiva['21']) ? $importeiva['21'] : 0);     # subtotal neto sujeto a IVA
            $regfac['baseimp'] = round($baseimp, 2);
            $regfac['importeneto'] = ($entity->getImporte() - round($importeiva['21'], 2));
             * 
             */
        } else {
            $regfac['importetotal'] = $entity->getImporte(); # total del comprobante
            $regfac['importeiva'] = 0;
            $regfac['importeneto'] = 0;   # subtotal neto sujeto a IVA
            $regfac['coniva'] = "NO";
            $regfac['capitalafinanciar'] = $entity->getImporte();
        }

        $regfac['fechaemision'] = $entity->getFecha()->format('Ymd');
        $regfac['fechadesde'] = '';
        $regfac['fechahasta'] = '';

        $regfac['fecha_'] = '';
        $regfac['imp_trib'] = 1.0;
        $regfac['imp_op_ex'] = 0.0;
        $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
        $regfac['fecha_venc_pago'] = '';

        $regfac['PeriodoAsoc'] = ['FchDesde' => $entity->getFecha()->format('Ymd'), 'FchHasta' => $entity->getFecha()->format('Ymd')];
        $params = $wsfe->armadoFacturaUnica($tipofactura, $ptovta, $entity->getTiponota(), $regfac, $cuitEmisor, $tipocbte);
        $cae = $wsfe->solicitarCAE($params, $cuitEmisor);

        if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'A' or $cae->FECAESolicitarResult->FeCabResp->Resultado == 'P') {
            $entity->setNrocomprobante($regfac['nrofactura']);
            $entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
            $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
            $this->em->flush();
            return true;
        } else {
            if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'R') {
                $observaciones = "";
                if (isset($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones)) {
                    $observaciones = json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones);
                }
                if (isset($cae->FECAESolicitarResult->Errors->Err)) {
                    $observaciones = json_encode($cae->FECAESolicitarResult->Errors->Err);
                }
                $entity->setDescripcion($entity->getDescripcion() . " - ERROR: $observaciones");
                $this->em->flush();
                $this->addFlash('msgError', 'Error al dar de alta factura: ' . $observaciones);
            }

            return false;
        }
    }

    public function obtenerTextoCodigoBarras($v_cuit, $v_tipo_comprobante, $v_punto_venta, $v_cae, $v_vencimiento_cae) {
        $base_calculo = $v_cuit .
                str_pad($v_tipo_comprobante, 3, "0", STR_PAD_LEFT) .
                str_pad($v_punto_venta, 5, "0", STR_PAD_LEFT) .
                $v_cae .
                $v_vencimiento_cae;

        return $base_calculo . $this->digitoVerificador($base_calculo);
    }

    public function digitoVerificador(string $codigo) {
        $aux_1 = 0;
        $aux_2 = 0;
        $largo = (int) strlen(trim($codigo));
        for ($i = 0; $i < $largo + 1; $i = $i + 2) {
            $aux_1 = $aux_1 + (int) substr($codigo, $i, 1);
        }
        for ($i = 1; $i < $largo + 1; $i = $i + 2) {
            $aux_2 = $aux_2 + (int) substr($codigo, $i, 1);
        }
        $aux_3 = $aux_1 * 3;
        $aux_4 = $aux_3 + $aux_2;
        $largo_final = strlen(trim($aux_4));
        $digito = 10 - substr($aux_4, $largo_final - 1, 1);

        if ($digito === 10) {
            $digito = 0;
        }

        return (string) $digito;
    }

    public function getSlug($text) {
        return strtolower(str_replace(" ", "-", $this->eliminaAcentos($text)));
    }

    public function eliminaAcentos($text) {
        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text = strtolower($text);
        $patron = array(
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',
        );
        $text = preg_replace(array_keys($patron), array_values($patron), $text);
        $text = str_replace(".", "", $text);
        return $text;
    }

    public function uploadFile($file, $path, $size = ['width' => 100, 'height' => 100]) {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
        try {
            $file->move(
                    $path,
                    $newFilename
            );
            $resize = new Resize();
            $resize->max_width($size['width']);
            $resize->max_height($size['height']);
            $resize->image_path($path . "/" . $newFilename);
            $resize->image_resize();
        } catch (FileException $e) {
            return '';
//            die("Error al subir archivo: ".$e->getMessage());
        }

        return $path . "/" . $newFilename;
    }

    /*
     * Tabla de alicuotas IVA
     */

    public function getCodigoAlicuota($iva) {
        $codigo = '';
        switch ($iva) {
            case 0:
                $codigo = "0003";
                break;
            case 10.5:
                $codigo = "0004";
                break;
            case 21:
                $codigo = "0005";
                break;
            case 27:
                $codigo = "0006";
                break;
            case 5:
                $codigo = "0008";
                break;
            case 2.5:
                $codigo = "0009";
                break;
            default;
                $codigo = "NOOK";
        }
        return $codigo;
    }

    /*
     * Obtengo el código del tipo de comprobante
     */

    public function getTipoComprobante($tipo) {
        $codigo = '';
        switch ($tipo) {
            case 'FACTURA A':
                $codigo = "001";
                break;
            case 'NOTA DE DEBITO A':
                $codigo = "002";
                break;
            case 'NOTA DE CREDITO A':
                $codigo = "003";
                break;
            case 'RECIBOS A':
                $codigo = "004";
                break;
            case 'NOTA DE VENTA AL CONTADO A':
                $codigo = "005";
                break;
            case 'FACTURA B':
                $codigo = "006";
                break;
            case 'NOTA DE DEBITO B':
                $codigo = "007";
                break;
            case 'NOTA DE CREDITO B':
                $codigo = "008";
                break;
            case 'RECIBOS B':
                $codigo = "009";
                break;
            case 'NOTA DE VENTA AL CONTADO B':
                $codigo = "010";
                break;
            case 'FACTURA C':
                $codigo = "011";
                break;
            case 'NOTA DE DEBITO C':
                $codigo = "012";
                break;
            case 'NOTA DE CREDITO C':
                $codigo = "013";
                break;
            case 'FACTURA DE CREDITO A':
                $codigo = 201;
                break;
            case 'NOTA DE DEBITO FACTURA DE CREDITO A':
                $codigo = 202;
                break;
            case 'NOTA DE CREDITO FACTURA DE CREDITO A':
                $codigo = 203;
                break;
            case 'FACTURA DE CREDITO B':
                $codigo = 206;
                break;
            case 'NOTA DE DEBITO FACTURA DE CREDITO B':
                $codigo = 207;
                break;
            case 'NOTA DE CREDITO FACTURA DE CREDITO B':
                $codigo = 208;
                break;
            default;
                $codigo = "NOK";
        }
        return $codigo;
    }

    public function getTipoFactura($tipofactura) {
        switch ($tipofactura) {
            case 'FACTURA A':
                return 1;
                break;
            case 'NOTA DEBITO A':
                return 2;
                break;
            case 'NOTA CREDITO A':
                return 3;
                break;
            case 'FACTURA B':
                return 6;
                break;
            case 'NOTA DEBITO B':
                return 7;
                break;
            case 'NOTA CREDITO B':
                return 8;
                break;
            case 'FACTURA C':
                return 11;
                break;
            case 'NOTA DEBITO C':
                return 12;
                break;
            case 'NOTA CREDITO C':
                return 13;
                break;
            case 'FACTURA CREDITO A':
                return 201;
                break;
            case 'NOTA DEBITO CREDITO A':
                return 202;
                break;
            case 'NOTA CREDITO CREDITO A':
                return 203;
                break;
            case 'FACTURA CREDITO B':
                return 206;
                break;
            case 'NOTA DEBITO CREDITO B':
                return 207;
                break;
            case 'NOTA CREDITO CREDITO B':
                return 208;
                break;
        }
    }

    /*
     *  Obtengo el código del tipo de documento
     */

    public function getTipoDocumento($tipo) {
        $codigo = '';
        switch ($tipo) {
            case 'C I CAPITAL FEDERAL':
                $codigo = "00";
                break;
            case 'C I BUENOS AIRES':
                $codigo = "01";
                break;
            case 'C I CATAMARCA':
                $codigo = "02";
                break;
            case 'C I CORDOBA':
                $codigo = "03";
                break;
            case 'C I CORRIENTES':
                $codigo = "04";
                break;
            case 'C I ENTRE RIOS':
                $codigo = "05";
                break;
            case 'C I JUJUY':
                $codigo = "06";
                break;
            case 'C I MENDOZA':
                $codigo = "07";
                break;
            case 'C I LA RIOJA':
                $codigo = "08";
                break;
            case 'C I SALTA':
                $codigo = "09";
                break;
            case 'C I SAN JUAN':
                $codigo = "10";
                break;
            case 'C I SAN LUIS':
                $codigo = "11";
                break;
            case 'C I SANTA FE':
                $codigo = "12";
                break;
            case 'C I SGO DEL ESTERO':
                $codigo = "13";
                break;
            case 'C I TUCUMAN':
                $codigo = "14";
                break;
            case 'C I CHACO':
                $codigo = "16";
                break;
            case 'C I CHUBUT':
                $codigo = "17";
                break;
            case 'C I FORMOSA':
                $codigo = "18";
                break;
            case 'C I MISIONES':
                $codigo = "19";
                break;
            case 'C I NEUQUEN':
                $codigo = "20";
                break;
            case 'C I LA PAMPA':
                $codigo = "21";
                break;
            case 'C I RIO NEGRO':
                $codigo = "22";
                break;
            case 'C I SANTA CRUZ':
                $codigo = "23";
                break;
            case 'C I TIERRA DEL FUEGO':
                $codigo = "24";
                break;
            case 'CUIT':
                $codigo = "80";
                break;
            case 'CUIL':
                $codigo = "86";
                break;
            case 'CDI':
                $codigo = "87";
                break;
            case 'LIBRETA DE ENROLAMIENTO':
                $codigo = "89";
                break;
            case 'LIBRETA CIVICA':
                $codigo = "90";
                break;
            case 'C I EXTRANJERA':
                $codigo = "91";
                break;
            case 'EN TRAMITE':
                $codigo = "92";
                break;
            case 'ACTA DE NACIMIENTO':
                $codigo = "93";
                break;
            case 'PASAPORTE':
                $codigo = "94";
                break;
            case 'C I BS AS R N P':
                $codigo = "95";
                break;
            case 'DNI':
                $codigo = "96";
                break;
            case 'SIN IDENTIFICAR / VENTA GLOBAL DIARIA':
                $codigo = "99";
                break;
            default;
                $codigo = "NO";
        }
        return $codigo;
    }

    /*
     * Para los CITI
     */

    public function trim_all($str, $what = NULL, $with = ' ') {
        if ($what === NULL) {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space

            $what = "\\x00-\\x20";    //all white-spaces and control chars
        }

        return trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
    }

    public function crearCaja(UnidadNegocio $unidadNegocio) {
        $caja = new Caja();
        $caja->setUnidadNegocio($unidadNegocio);
        $this->em->persist($caja);
        $this->em->flush();
    }

    public function newBalance(Stock $stock, Usuario $user, $price, $amount, $type = Balance::TYPE_POSITIVE, $additional = []) {
        $balance = new Balance();
        $balance->setStock($stock);
        $balance->setUser($user);
        $balance->setPrice($price);
        $balance->setAmount($amount);
        $balance->setType($type);

        if ($stock->getProducto()->getTrazable()) {
            $balance->setVerifarmaStatus(Balance::VERIFARMA_ALTA);
        }

        $balance->setDescription("Creación de Balance");
        if (!empty($additional['description']))
            $balance->setDescription($additional['description']);

        $balance->setAmountBalance($amount);

        return $balance;
    }

    public function registerBalance(Stock $stock, Usuario $user, $price, $amount, $type = Balance::TYPE_NEGATIVE, $additional = []) {

        $lastBalance = $this->em->getRepository('AppBundle:Balance')->findOneBy(['stock' => $stock->getId()], ['id' => 'DESC']);
        if (!empty($additional['consignacion'])) {
            $duplicatedBalance = $this->em
                    ->getRepository('AppBundle:Balance')
                    ->findOneBy(['stock' => $stock->getId(), 'type' => $type, 'amount' => $amount, 'consignacion' => $additional['consignacion']->getId()]);
            if ($duplicatedBalance) {
                return $duplicatedBalance;
            }
        }

        $amountBalance = 0;

        if ($type == Balance::TYPE_NEGATIVE) {
            $amountBalance = $lastBalance->getAmountBalance() - $amount;
        } else {
            $amountBalance = $lastBalance->getAmountBalance() + $amount;
        }
        $verifarmaStatus = '';

        if ($stock->getProducto()->getTrazable()) {
            if (!empty($additional['consignacion']) and $type == Balance::TYPE_NEGATIVE) {
                $verifarmaStatus = Balance::VERIFARMA_DESPACHAR;
            }

            if (!empty($additional['remitooficial']) and $type == Balance::TYPE_NEGATIVE) {
                $verifarmaStatus = Balance::VERIFARMA_DESPACHAR;
            }

            if (empty($additional['consignacion']) and!empty($additional['factura']) and $type == Balance::TYPE_NEGATIVE) {
                $verifarmaStatus = Balance::VERIFARMA_DESPACHAR;
            }

            if ((!empty($additional['consignacion']) or!empty($additional['notacredito'])) and $type == Balance::TYPE_POSITIVE) {
                $verifarmaStatus = Balance::VERIFARMA_DEVOLUCION;
            }

            if (!empty($additional['consignacion']) and $type == Balance::TYPE_NEGATIVE and $amount == 0) {
                $verifarmaStatus = Balance::VERIFARMA_IMPLANTACION;
            }
        }
        // TODO: CHEQEUAR DUPLICIDAD.

        $balance = new Balance();
        $balance->setStock($stock);
        $balance->setUser($user);
        $balance->setPrice($price);
        $balance->setAmount($amount);
        $balance->setType($type);
        $balance->setVerifarmaStatus($verifarmaStatus);
        $balance->setAmountBalance($amountBalance);
        if (!empty($additional['description']))
            $balance->setDescription($additional['description']);
        if (!empty($additional['factura']))
            $balance->setFactura($additional['factura']);
        if (!empty($additional['consignacion']))
            $balance->setConsignacion($additional['consignacion']);
        if (!empty($additional['remitooficial']))
            $balance->setRemitooficial($additional['remitooficial']);
        if (!empty($additional['notacredito']))
            $balance->setNotacredito($additional['notacredito']);

        return $balance;
    }

    /*
     * Exportación a excel
     */

    public function exportarExcel($spreadSheet, $titulo) {

        $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        $writer = new Xlsx($spreadSheet);
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $titulo . '.xlsx"');
        $response->setPrivate();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        $response->setCallback(function() use ($writer) {
            $writer->save('php://output');
        });
        return $response;
    }

    public function csvVerifarmaAlta($entities, $titulo) {
        $verifarmaStatus = "#VF_ALTALOTE";
        $archivo_csv = fopen('./verifarma/' . $titulo . '.csv', 'w');
        if ($archivo_csv) {
            fputs($archivo_csv, $verifarmaStatus . PHP_EOL);
            $line = "";
            foreach ($entities as $entity) {
                $line .= str_pad($entity->getStock()->getProducto()->getGtin(), 14, "0", STR_PAD_LEFT);
                $line .= ',' . $entity->getStock()->getSerie();
                $line .= ',' . $entity->getStock()->getLote();
                $line .= ',' . ($entity->getStock()->getVencimiento() ? $entity->getStock()->getVencimiento()->format('d/m/Y') : '');
                $line .= ',1';
                fputs($archivo_csv, $line . PHP_EOL);
                $line = "";
            }
            fclose($archivo_csv);
        } else {
            return false;
        }

        return true;
    }

    public function csvVerifarmaDespacho($entities, $titulo) {
        $verifarmaStatus = "#VF_CREAR_Y_DESPACHAR";
        $archivo_csv = fopen('./verifarma/' . $titulo . '.csv', 'w');
        if ($archivo_csv) {
            fputs($archivo_csv, $verifarmaStatus . PHP_EOL);
            $line = "";
            foreach ($entities as $entity) {
                $documento = $entity->getConsignacion() ? $entity->getConsignacion() : $entity->getRemitoOficial();
                $factura = $entity->getFactura() ? $entity->getFactura() : null;

                $line .= str_pad($entity->getStock()->getProducto()->getGtin(), 14, "0", STR_PAD_LEFT);
                $line .= ',' . $entity->getStock()->getSerie();
                $line .= ',' . $entity->getStock()->getLote();
                $line .= ',' . ($entity->getStock()->getVencimiento() ? $entity->getStock()->getVencimiento()->format('d/m/Y') : '');
                $line .= ',1';
                $line .= ',' . ($documento ? $documento->getFecha()->format('d/m/Y') : '');
                $line .= ',0001R' . str_pad(($documento ? $documento->getNumero() : ($factura ? $factura->getNumero() : '')), 8, "0", STR_PAD_LEFT);
                $line .= ',' . ($factura ? $factura->getFecha()->format('d/m/Y') : ''); // Fecha de Factura
                $line .= ',' . ($factura ? $factura->getNrofactura() : ''); // Numero de Factura
                $line .= ',' . strtoupper(($documento ? $documento : $factura)->getClienteProveedor()->getRazonSocial());
                $line .= ',' . str_replace("-", "", ($documento ? $documento : $factura)->getClienteProveedor()->getCuit());
                $line .= ',' . ($documento ? $documento : $factura)->getClienteProveedor()->getGln();
                $line .= ',' . ($documento ? $documento : $factura)->getClienteProveedor()->getTelefono();
                $line .= ',' . ($documento ? $documento : $factura)->getClienteProveedor()->getDomiciliocomercial();
                $line .= ',' . ($documento ? $documento : $factura)->getClienteProveedor()->getId();

                fputs($archivo_csv, $line . PHP_EOL);
                $line = "";
            }
            fclose($archivo_csv);
        } else {
            return false;
        }

        return true;
    }

    public function csvVerifarmaDevolucion($entities, $titulo) {
        $verifarmaStatus = "#VF_DEVOLUCION_IN";
        $archivo_csv = fopen('./verifarma/' . $titulo . '.csv', 'w');
        if ($archivo_csv) {
            fputs($archivo_csv, $verifarmaStatus . PHP_EOL);
            $line = "";
            foreach ($entities as $entity) {
                $documento = $entity->getConsignacion() ? $entity->getConsignacion() : ($entity->getNotacredito() ? $entity->getNotacredito() : $entity->getRemitoOficial());
                $line .= str_pad($entity->getStock()->getProducto()->getGtin(), 14, "0", STR_PAD_LEFT);
                $line .= ',' . $entity->getStock()->getSerie();
                $line .= ',' . $entity->getStock()->getLote();
                $line .= ',' . ($entity->getStock()->getVencimiento() ? $entity->getStock()->getVencimiento()->format('d/m/Y') : '');
                $line .= ',1';
                $line .= ',' . $documento->getFecha()->format('d/m/Y');
                $line .= ',0001R' . str_pad($documento->getNumero(), 8, "0", STR_PAD_LEFT);
                $line .= ',' . ($entity->getFactura() ? $entity->getFactura()->getFecha()->format('d/m/Y') : ''); // Fecha de Factura
                $line .= ',' . ($entity->getFactura() ? $entity->getFactura()->getNrofactura() : ''); // Numero de Factura
                $line .= ',' . strtoupper($documento->getClienteProveedor()->getRazonSocial());
                $line .= ',' . str_replace("-", "", $documento->getClienteProveedor()->getCuit());
                $line .= ',' . $documento->getClienteProveedor()->getGln();
                $line .= ',' . $documento->getClienteProveedor()->getTelefono();
                $line .= ',' . str_replace(",", "", $documento->getClienteProveedor()->getDomiciliocomercial());
                $line .= ',' . $documento->getClienteProveedor()->getId();



                fputs($archivo_csv, $line . PHP_EOL);
                $line = "";
            }
            fclose($archivo_csv);
        } else {
            return false;
        }

        return true;
    }

}
