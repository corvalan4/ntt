<?php

namespace AppBundle\Services;

use Psr\Container\ContainerInterface;
use AppBundle\Repository\BalanceRepository;
use AppBundle\Entity\ProductoConsignacion;
use Symfony\Component\DependencyInjection\Container;

class ProductoConsignacionService {

    private $productoConsignacionRepository;

    public function __construct(Container $container) {
        $this->productoConsignacionRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:ProductoConsignacion');
    }

    /**
     * Obtengo Stock por Producto Lote Despacho
     */
    public function sumConsignadosByProveedorCodigoModeloLote($proveedorId, $codigo, $modelo, $lote, $filtros, $print = true) {
        $sumConsignados = $this->productoConsignacionRepository->getSumConsignadosByProveedorCodigoModeloLote($proveedorId, $codigo, $modelo, $lote, $filtros);
        if ($print === true) {
            echo $sumConsignados;
        } else {
            return $sumConsignados;
        }
    }

    /**
     * Obtengo Productos Consignados por  Proveedotr, Codigo, Modelo
     */
    public function productosConsignacionesByProveedorModeloCodigo($proveedorId, $codigo, $modelo, $filtros) {
        $productosConsignados = $this->productoConsignacionRepository->getProductosConsignacionesByProveedorModeloCodigo($proveedorId, $codigo, $modelo, $filtros);

        return $productosConsignados;
    }

    /**
     * Obtengo Consignacion By Stock Id
     */

    public function getProductoConsignacionByStock($stockId, $print = null) {

        $productoConsignacion = $this->productoConsignacionRepository->findOneBy(
            [
                'stock' => $stockId,
                'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK
            ]
        );
        return $productoConsignacion;
    }

    /**
     * Obtengo Consignacion By Stock Id
     */

    public function getProductoConsignacionByStockInClient($stockId, $print = null) {

        $productoConsignacion = $this->productoConsignacionRepository->findOneBy(
            ['stock' => $stockId,
                'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_IN_CLIENT
            ]
        );
        return $productoConsignacion;
    }

}
