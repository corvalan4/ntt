<?php

namespace AppBundle\Services;

use Psr\Container\ContainerInterface;
//use AppBundle\Repository\PresupuRepository;
use AppBundle\Entity\Presupuesto;
use AppBundle\Entity\ProductoRemito;
use AppBundle\Entity\ProductoPresupuesto;
use AppBundle\Entity\RemitoOficial;
use Symfony\Component\DependencyInjection\Container;

class PresupuestoService {

    //private $productoConsignacionRepository;
    private $container;

    public function __construct(Container $container) {

        $this->container = $container;

        //$this->productoConsignacionRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Presupuesto');        
    }

    /*
     * Remitir Presupuesto
     */

    public function remitirPresupuesto(Presupuesto $presupuesto,$nroRemito,$observacion) {
        try {
            $remitoOficial = new RemitoOficial();
            $remitoOficial->setNumero($nroRemito);
            $remitoOficial->setClienteProveedor($presupuesto->getClienteProveedor());
            $remitoOficial->setObservacion($observacion);
            $remitoOficial->setUnidadNegocio($presupuesto->getUnidadNegocio());
            $remitoOficial->setSucursal($presupuesto->getSucursal());  
            $importe = 0;
            foreach ($presupuesto->getProductosPresupuesto() as $productoPresupuesto) {
                if ($productoPresupuesto->getEstado() == ProductoPresupuesto::PRODUCTO_PRESUPUESTO_ACEPTADO) {
                    $productoRemito = new ProductoRemito();
                    $productoRemito->setDescripcion($productoPresupuesto->getDescripcion());
                    $productoRemito->setPrecio($productoPresupuesto->getPrecio());
                    $productoRemito->setCantidad($productoPresupuesto->getCantidad());
                    $productoRemito->setProducto($productoPresupuesto->getProducto());
                    $productoRemito->setRemitooficial($remitoOficial);
                    $productoRemito->setRenglon($productoPresupuesto->getRenglon());
                    $importe = $importe + $productoPresupuesto->getSubtotal();
                    $this->container->get('doctrine.orm.entity_manager')->persist($productoRemito);
                }
            }
            $remitoOficial->setImporte($importe);
            $this->container->get('doctrine.orm.entity_manager')->persist($remitoOficial);
            $this->container->get('doctrine.orm.entity_manager')->flush();
            return $remitoOficial;
        } catch (\Exception $e) {
            return false;
        }
    }
}
