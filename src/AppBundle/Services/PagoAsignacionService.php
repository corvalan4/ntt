<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Entity\PagoAsignacion;

class PagoAsignacionService {

    private $pagoAsignacionRepository;
    private $clienteProveedorRepository;

    public function __construct(Container $container) {
        $this->pagoAsignacionRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:PagoAsignacion');
        $this->clienteProveedorRepository = $container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:ClienteProveedor');
    }

    /*
     * Get Parcial Balance
     */

    public function getParcialBalance($id) {
        $movimiento = $this->pagoAsignacionRepository->find($id);

        $saldoInicial = $movimiento->getClienteProveedor()->getSaldo();
        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;

        $sumasNotaCredito = $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::NOTA_CREDITO);
        $sumasNotaDebito = $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::NOTA_DEBITO);

        if ($movimiento->getClienteProveedor()->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

            $sumasReciboCliente = $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::RECIBO_CLIENTE);
            $sumasFactura = $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::FACTURA);

            $total = $saldoInicial - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
        } else {
            $sumasOrdenPago = $sumasFacturaProveedor + $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::ORDEN_PAGO);
            $sumasGastos = $sumasFacturaProveedor + $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::GASTO);
            $sumasFacturaProveedor = $this->pagoAsignacionRepository->getSumas($movimiento, PagoAsignacion::FACTURA_PROVEEDOR);

            $total = $saldoInicial - $sumasFacturaProveedor + $sumasOrdenPago + $sumasGastos + $sumasNotaCredito - $sumasNotaDebito;
        }

        return round($total,2);
    }

    /*
     * Get Balance
     */

    public function getBalance($clienteProveedorId) {
        $clienteProveedor = $this->clienteProveedorRepository->find($clienteProveedorId);

        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;
        $sumasNotaCredito = 0;
        $sumasNotaDebito = 0;       
        $saldoInicial = $clienteProveedor->getSaldo();
        $ultimoMovimiento = $this->pagoAsignacionRepository->getLastMovimiento($clienteProveedor);
        $total=0;

        if ($ultimoMovimiento) {

            $sumasNotaCredito = floatval($this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::NOTA_CREDITO));
            $sumasNotaDebito = floatval($this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::NOTA_DEBITO));

            if ($clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

                $sumasReciboCliente = $this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::RECIBO_CLIENTE);
                $sumasFactura = $this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::FACTURA);

                $total = $saldoInicial - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
            } else {
                $sumasOrdenPago = $sumasFacturaProveedor + $this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::ORDEN_PAGO);
                $sumasGastos = $sumasFacturaProveedor + $this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::GASTO);
                $sumasFacturaProveedor = $this->pagoAsignacionRepository->getSumas($ultimoMovimiento, PagoAsignacion::FACTURA_PROVEEDOR);
                $total = $saldoInicial - $sumasFacturaProveedor + $sumasOrdenPago + $sumasGastos + $sumasNotaCredito - $sumasNotaDebito;
            }
        }

        return round($total,2);
    }

}
