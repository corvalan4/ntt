<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Entity\Factura;

use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Stock;
use AppBundle\Entity\Sucursal;
use AppBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Consignacion;
use AppBundle\Entity\ProductoConsignacion;
use AppBundle\Form\ConsignacionType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Consignacion controller.
 *
 */
class ConsignacionController extends Controller
{
    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Consignacion entities.
     *
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $unidad = !$this->isGranted('ROLE_SUPER_ADMIN') ? $this->getUser()->getUnidadNegocio()->getId() : '';
        $fecha_desde = $request->get('fechaDesde') ?? '';
        $fecha_hasta = $request->get('fechaHasta') ?? '';
        $estado = $request->get('estado') ?? '';
        $cliente = $request->get('cliente') ?? '';
        $numero = $request->get('numero') ?? '';
        $filter = ['cliente'=>$cliente, 'unidad' => $unidad, 'estado' => $estado, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'numero' => $numero];

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy([], ['descripcion'=>'asc']);

        $entities = $em->getRepository('AppBundle:Consignacion')->findByFilter($filter);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Consignacion:index.html.twig', array(
            'entities' => $paginador,
            'unidades' => $unidades,
        ));
    }
    /**
     * Creates a new Consignacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Consignacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $desde_reserva = $request->get("desde_reserva");

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

        $factura_id = $request->request->get("factura");
        if (!empty($factura_id)) {
            $factura = $em->getRepository(Factura::class)->find($factura_id);
            $entity->setFactura($factura);
        }

        $hayProductos = false;

        if ($form->isValid()) {
            for ($i = 1; $i <= 300; $i++) {
                $trazable = $request->get("trazable_elemento$i");
                $idproducto = $request->get("idprod_elemento$i");
                $idstock = $request->get("idstock_elemento$i");
                $cantidad = (float) $request->get("cantidad_elemento$i");

                if (!empty($idstock)) {
                    if($desde_reserva){
                        $reserva = $em->getRepository('AppBundle:Reserva')->findOneBy(['stock'=>$idstock, 'cliente'=>$entity->getClienteProveedor()->getId(), 'estado'=>'A']);
                        if($reserva){
                            $reserva->setEstado('F');
                        }
                    }
                    $elementostock = $em->getRepository('AppBundle:Stock')->find($idstock);
                    $productoconsignacion = new ProductoConsignacion();
                    $productoconsignacion->setProducto($elementostock->getProducto());
                    $productoconsignacion->setStock($elementostock);
                    $productoconsignacion->setConsignacion($entity);

                    $additional = ["description" => "Alta de Consignacion Cliente", "consignacion" => $entity];
                    $cantidad = $elementostock->getProducto()->getTrazable() ? $elementostock->getStock() : $cantidad;
                    $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                    $em->persist($balance);

                    $productoconsignacion->setCantidad($cantidad);
                    $elementostock->setStock($elementostock->getStock() - $cantidad);

                    $em->persist($productoconsignacion);
                    $hayProductos = true;
                } else {
                    if (!empty($idproducto)) {
                        $elementostocks = $em->getRepository('AppBundle:Stock')->findBy(array('producto' => $idproducto, 'unidadnegocio' => $entity->getUnidadNegocio()->getId()), array('fecha' => 'ASC'));

                        foreach ($elementostocks as $elementostock) {
                            if ($cantidad > 0 and $cantidad <= $elementostock->getStock()) {
                                $stock = $elementostock->getStock() - $cantidad;

                                $additional = ["description" => "Alta de Consignacion Cliente", "consignacion" => $entity];
                                $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                                $em->persist($balance);

                                $productoconsignacion = new ProductoConsignacion();
                                $productoconsignacion->setStock($elementostock);
                                $productoconsignacion->setProducto($elementostock->getProducto());
                                $productoconsignacion->setConsignacion($entity);
                                $productoconsignacion->setCantidad($cantidad);
                                $elementostock->setStock($stock);
                                $em->persist($productoconsignacion);

                                $cantidad = 0;
                            } else {
                                if ($cantidad > 0 and $elementostock->getStock() > 0) {
                                    $productoconsignacion = new ProductoConsignacion();
                                    $productoconsignacion->setStock($elementostock);
                                    $productoconsignacion->setProducto($elementostock->getProducto());
                                    $productoconsignacion->setConsignacion($entity);
                                    $cantidad = $cantidad - $elementostock->getStock();

                                    $additional = ["description" => "Alta de Consignacion Cliente", "consignacion" => $entity];
                                    $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $elementostock->getStock(), Balance::TYPE_NEGATIVE, $additional);
                                    $em->persist($balance);

                                    $productoconsignacion->setCantidad($elementostock->getStock());
                                    $elementostock->setStock(0);
                                    $em->persist($productoconsignacion);
                                }
                            }
                        }
                        $hayProductos = true;
                    }
                }
            }

            if (!$hayProductos) {
                $this->sessionManager->addFlash('msgError', 'Debe agregar productos.');
                return $this->render('AppBundle:Consignacion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'stocks' => [],
                    'stock' => null,
                    'reservas' => 0,
                ));
            }


            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setConsignacion($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte(0);

            $em->persist($pagoAsignacion);
            $em->persist($entity);

            $em->flush();

            $balances = $em->getRepository('AppBundle:Balance')->findBy(array('consignacion' => $entity->getId()));
            foreach ($balances as $balance) {
                if($balance->getStock()->getProducto()->getTrazable()){
                    $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
                    $this->sessionManager->csvVerifarmaDespacho([$balance], "DES" . $namesheet);
                }
            }

            return $this->redirect($this->generateUrl('consignacion'));

        }
    }

    /**
     * Creates a form to create a Consignacion entity.
     *
     * @param Consignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consignacion $entity)
    {
        $form = $this->createForm(new ConsignacionType(), $entity, array(
            'action' => $this->generateUrl('consignacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Confirmar Consignacion', 'attr' => array('class' => 'btn btn-success  btn-xs', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new Consignacion entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Consignacion();
        $stock = null;
        $reservas = 0;
        if(!empty($request->get('stock_id'))){
            $stock = $em->getRepository('AppBundle:Stock')->find($request->get('stock_id'));
            if($stock){
                $stockService = $this->container->get('stockservice');
                $reservas = (int)$stockService->reservaPorStock($stock->getId());
                $entity->setUnidadNegocio($stock->getUnidadnegocio());
            }
        }

        $stocks = [];
        $cliente = null;
        if(!empty($request->get('reservas'))){
            foreach ($request->get('reservas') as $reserva_id){
                $reserva = $em->getRepository('AppBundle:Reserva')->find($reserva_id);
                if($reserva){
                    if(!$cliente) $cliente = $reserva->getCliente();
                    if($cliente != $reserva->getCliente()){
                        $this->addFlash("msgError", "No podes consignar reservas de clientes diferentes.");
                        return $this->redirectToRoute('reserva');
                    }
                    $stocks[] = ['stock' => $reserva->getStock(), 'cantidad'=>$reserva->getCantidad()];
                    $entity->setClienteProveedor($cliente);
                }
            }

            if($stock){
                $stockService = $this->container->get('stockservice');
                $reservas = (int)$stockService->reservaPorStock($stock->getId());
                $entity->setUnidadNegocio($stock->getUnidadnegocio());
            }
        }

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Consignacion:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'stock' => $stock,
            'stocks' => $stocks,
            'reservas' => $reservas,
        ));

    }

    /**
     * Finds and displays a Consignacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);
        $usuarios = $em->getRepository('AppBundle:Usuario')->findBy([], ['apellidonombre'=>'ASC']);        

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Consignacion:show.html.twig', array(
            'entity'      => $entity,
            'usuarios'    => $usuarios,            
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a Consignacion entity.
     *
     */
    public function devolucionAction($productoConsignacionId)
    {
        $em = $this->getDoctrine()->getManager();
        $productoConsignacion = $em->getRepository('AppBundle:ProductoConsignacion')->find($productoConsignacionId);

        // Verifico la existencia del stock en la consignacion.
        if(!$productoConsignacion){
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('consignacion');
        }

        $entity = $productoConsignacion->getConsignacion();
        $stock = $productoConsignacion->getStock();

        // Cambio el estado del stock en la cosignacion.
        $productoConsignacion->setEstado(ProductoConsignacion::PRODUCTO_CONSIGNACION_ERROR);

        // La cantidad ahora es el total de la consignacion, pero podrían ser menos
        // y aca tendrian que existir documentos intermedios.
        $cantidad = $productoConsignacion->getCantidad();

        // Devuelvo stock
        $additional = ["description" => "Devolución de Stock en Consignacion Cliente", "consignacion" => $entity];

        $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, $cantidad, Balance::TYPE_POSITIVE, $additional);
        $em->persist($balance);

        // Verifico el total de los productos en la consignacion para cambiar el estado global.
        if(sizeof($entity->getProductosConsignacion()) == 1){
            // Significa que es el unico producto en consignacion.
            $entity->setEstado(Consignacion::CONSIGNACION_ERROR);
        }else{
            $estadoConsignacion = Consignacion::CONSIGNACION_ERROR;
            foreach ($entity->getProductosConsignacion() as $item) {
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_IN_CLIENT){
                    $estadoConsignacion = Consignacion::CONSIGNACION_PARCIAL;
                }
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_OK and $estadoConsignacion != Consignacion::CONSIGNACION_PARCIAL){
                    $estadoConsignacion = Consignacion::CONSIGNACION_MIXTA;
                }
            }
            $entity->setEstado($estadoConsignacion);
        }

        $em->flush();
        $stock->setStock($balance->getAmountBalance());
        $em->flush();

        if($stock->getProducto()->getTrazable()){
            $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
            $this->sessionManager->csvVerifarmaDevolucion([$balance], 'DEV'.$namesheet);
        }

        $this->addFlash('msgOk', 'Devolución realizada.');

        return $this->redirectToRoute('consignacion_show', ['id'=>$entity->getId()]);
    }

    public function implantacionAction(Request $request)
    {
        $productoconsignacion_id = $request->get('productoconsignacion_id');
        $sucursal_id = $request->get('sucursal_id');
        
        $vendedor_id = $request->get('vendedor');

        $paciente = $request->get('paciente');
        $observaciones = $request->get('observaciones');
        $fechauso = $request->get('fechauso') ? new \DateTime($request->get('fechauso')) : new \DateTime('NOW');

        $em = $this->getDoctrine()->getManager();

        $productoConsignacion = $em->getRepository('AppBundle:ProductoConsignacion')->find($productoconsignacion_id);

        
        $usuario = $vendedor_id ? $em->getRepository(Usuario::class)->find($vendedor_id) : null;
        $sucursal = $em->getRepository(Sucursal::class)->find($sucursal_id);

        // Verifico la existencia del stock en la consignacion.
        if(!$productoConsignacion or !$sucursal){
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('consignacion');
        }

        $entity = $productoConsignacion->getConsignacion();
        $stock = $productoConsignacion->getStock();
        $productoConsignacion->setCliente($sucursal->getClienteproveedor());
        
        $productoConsignacion->setVendedor($usuario);
        $productoConsignacion->setFechauso($fechauso);
        $productoConsignacion->setObservaciones($observaciones);
        $productoConsignacion->setPaciente($paciente);
        $productoConsignacion->setEstado(ProductoConsignacion::PRODUCTO_CONSIGNACION_OK);

        $cantidad = $productoConsignacion->getCantidad();

        $additional = ["description" => "Uso de Stock en Consignacion Cliente", "consignacion" => $entity];
        $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, 0, Balance::TYPE_NEGATIVE, $additional);
        $em->persist($balance);

        // Verifico el total de los productos en la consignacion para cambiar el estado global.
        if(sizeof($entity->getProductosConsignacion()) == 1){
            // Significa que es el unico producto en consignacion.
            $entity->setEstado(Consignacion::CONSIGNACION_OK);
        }else{
            $estadoConsignacion = Consignacion::CONSIGNACION_OK;
            foreach ($entity->getProductosConsignacion() as $item) {
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_IN_CLIENT){
                    $estadoConsignacion = Consignacion::CONSIGNACION_PARCIAL;
                }
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_ERROR and $estadoConsignacion != Consignacion::CONSIGNACION_PARCIAL){
                    $estadoConsignacion = Consignacion::CONSIGNACION_MIXTA;
                }
            }
            $entity->setEstado($estadoConsignacion);
        }

        $em->flush();

        $this->addFlash('msgOk', 'Uso confirmado.');

        return $this->redirectToRoute('consignacion_show', ['id'=>$entity->getId()]);
    }

    public function anularUsoAction(Request $request)
    {
        $productoconsignacion_id = $request->get('productoconsignacion_id');

        $em = $this->getDoctrine()->getManager();
        $productoConsignacion = $em->getRepository('AppBundle:ProductoConsignacion')->find($productoconsignacion_id);

        // Verifico la existencia del stock en la consignacion.
        if(!$productoConsignacion){
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('consignacion');
        }

        $productoConsignacion->setCliente(null);
        
        $productoConsignacion->setVendedor(null);
        $productoConsignacion->setFechauso(null);
        $productoConsignacion->setObservaciones('');
        $productoConsignacion->setPaciente('');
        $productoConsignacion->setEstado(ProductoConsignacion::PRODUCTO_CONSIGNACION_IN_CLIENT);

        $entity = $productoConsignacion->getConsignacion();
        $stock = $productoConsignacion->getStock();

        $additional = ["description" => "Anulacion de uso de Stock en Consignacion Cliente", "consignacion" => $entity];
        $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, 0, Balance::TYPE_POSITIVE, $additional);
        $em->persist($balance);

        // Verifico el total de los productos en la consignacion para cambiar el estado global.
        if(sizeof($entity->getProductosConsignacion()) == 1){
            // Significa que es el unico producto en consignacion.
            $entity->setEstado(Consignacion::CONSIGNACION_IN_PROCESS);
        }else{
            $estadoConsignacion = Consignacion::CONSIGNACION_IN_PROCESS;
            foreach ($entity->getProductosConsignacion() as $item) {
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_OK){
                    $estadoConsignacion = Consignacion::CONSIGNACION_PARCIAL;
                }
                if($item->getEstado() == ProductoConsignacion::PRODUCTO_CONSIGNACION_ERROR and $estadoConsignacion != Consignacion::CONSIGNACION_PARCIAL){
                    $estadoConsignacion = Consignacion::CONSIGNACION_MIXTA;
                }
            }
            $entity->setEstado($estadoConsignacion);
        }

        $em->flush();

        $this->addFlash('msgWarn', 'Uso ANULADO.');

        return $this->redirectToRoute('consignacion_show', ['id'=>$entity->getId()]);
    }

    public function implantacionEditAction(Request $request)
    {
        $productoconsignacion_id = $request->get('productoconsignacion_id');
        $cliente_id = $request->get('cliente_id');
        
        $vendedor_id = $request->get('vendedor');

        $paciente = $request->get('paciente');
        $observaciones = $request->get('observaciones');
        $fechauso = $request->get('fechauso') ? new \DateTime($request->get('fechauso')) : new \DateTime('NOW');

        $em = $this->getDoctrine()->getManager();

        $productoConsignacion = $em->getRepository('AppBundle:ProductoConsignacion')->find($productoconsignacion_id);
        
        $usuario = $vendedor_id ? $em->getRepository(Usuario::class)->find($vendedor_id) : null;
        $cliente = $em->getRepository(ClienteProveedor::class)->find($cliente_id);

        // Verifico la existencia del stock en la consignacion.
        if(!$productoConsignacion or !$cliente){
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('consignacion');
        }

        $entity = $productoConsignacion->getConsignacion();

        $productoConsignacion->setCliente($cliente);
        
        $productoConsignacion->setVendedor($usuario);
        $productoConsignacion->setFechauso($fechauso);
        $productoConsignacion->setObservaciones($observaciones);
        $productoConsignacion->setPaciente($paciente);

        $em->flush();

        $this->addFlash('msgOk', 'Edición de Uso Confirmado.');

        return $this->redirectToRoute('consignacion_show', ['id'=>$entity->getId()]);
    }

    /**
     * Finds and displays a Consignacion entity.
     *
     */
    public function printAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

        return $this->render('AppBundle:Consignacion:print.html.twig', array(
            'entity'      => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing Consignacion entity.
     *
     */
    public function editAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

        return $this->render('AppBundle:Consignacion:edit.html.twig', array(
            'entity' => $entity,
            'algo' => 'editar',
        ));

    }

    public function editNumberAction(Request $request, $id)
    {
        if(!empty($request->get('consignacion_numero'))){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

            $factura_id = $request->request->get("factura");
            if (!empty($factura_id)) {
                $factura = $em->getRepository(Factura::class)->find($factura_id);
                $entity->setFactura($factura);
            }else{
                $entity->setFactura(null);
            }

            $entity->setNumero($request->get('consignacion_numero'));
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consignacion_show', array('id'=>$id)));

    }

    public function facturarAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

        return $this->render('AppBundle:Consignacion:edit.html.twig', array(
            'entity' => $entity,
            'algo' => 'facturar',
        ));


    }

    public function cerrarAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);
        $entity->setEstado('I');
        $cant = 0;
        $stock = 0;


        foreach($entity->getProductosConsignacion() as $prodCon){
            $stock = $prodCon->getProducto()->getStock();
            $cant = $stock + $prodCon->getCantidad();
            $prodCon->getProducto()->setStock($cant);
        }

        $em->flush();
        $this->addFlash('msgOk','La consignacion '.$entity->getId().' fue cerrada.');
        return $this->redirect($this->generateUrl('consignacion'));
    }

    /**
     * Creates a form to edit a Consignacion entity.
     *
     * @param Consignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Consignacion $entity)
    {
        $form = $this->createForm(new ConsignacionType(), $entity, array(
            'action' => $this->generateUrl('consignacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Consignacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

        $entity->setDescuento($request->get('descuento'));
        if($request->get('fechavencimiento')!=''){$entity->setFechaVencimiento( new \DateTime($request->get('fechavencimiento')));}
        $entity->setBonificacion($request->get('bonificacion'));
        $entity->setImporte($request->get('importeFinal'));

        $em->flush();

        $contador = $request->get('contador');

        $cantVieja = count($entity->getProductosConsignacion());

        for($x = 0; $x <= $contador; $x++){
            if($x<$cantVieja){
                $codigo = $request->get('codigo'.$x);
                $cantidad = $request->get('cantidad'.$x);
                foreach($entity->getProductosConsignacion() as $pCv){
                    if($pCv->getProducto()->getCodigo() == $codigo){
                        if($pCv->getCantidad()>$cantidad){
                            $resta = $pCv->getCantidad() - $cantidad;
                            $pCv->setCantidad($cantidad);
                            $stk = $pCv->getProducto()->getStock() + $resta;
                            $pCv->getProducto()->setStock($stk);
                            $pCv->setPrecio($request->get('precio'.$x));
                            $em->flush();
                        }else{
                            if($pCv->getCantidad()<$cantidad){
                                $resta = $cantidad - $pCv->getCantidad();
                                $pCv->setCantidad($cantidad);
                                $stk = $pCv->getProducto()->getStock() - $resta;
                                if($stk<=0){$stk=0;}
                                $pCv->getProducto()->setStock($stk);
                                $pCv->setPrecio($request->get('precio'.$x));
                                $em->flush();
                            }else{
                                $pCv->setPrecio($request->get('precio'.$x));
                            }
                        }
                        break;
                    }
                }
            }else{
                $codigo = $request->get('codigo'.$x);
                $desc = $request->get('descripcion'.$x);
                if(!is_null($codigo) and $codigo!='' and $desc != '' and !is_null($desc) ){
                    $productoConsignacion = new ProductoConsignacion();

                    if($codigo!='000'){
                        $producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio'=>$entity->getListaPrecio()->getUnidadNegocio()));
                    }else{
                        $producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo));
                    }

                    if(!is_null($producto) and $codigo!='000'){
                        $stock = $producto->getStock() - $request->get('cantidad'.$x);
                        if($stock<0){$stock=0;}
                        $producto->setStock($stock);
                    }

                    if(!is_null($producto)){
                        $productoConsignacion->setProducto($producto);
                        $productoConsignacion->setConsignacion($entity);
                        $productoConsignacion->setPrecio($request->get('precio'.$x));
                        $productoConsignacion->setCantidad($request->get('cantidad'.$x));
                        $entity->addProductosConsignacion($productoConsignacion);
                    }
                }
            }
        }

        if(count($entity->getProductosConsignacion())<1){
            $em->remove($entity);
            $em->flush();
            $this->sessionManager->addFlash('msgError','La consignacion debe tener al menos un producto.');
            $this->sessionManager->setSession('listaprecio', $listaPrecio->getId());
            return $this->redirect($this->generateUrl('consignacion_new'));
        }
        $em->flush();

        $this->sessionManager->setSession('listaprecio', '');
        $this->sessionManager->addFlash('msgOk','Consignacion registrada.');

        return $this->redirect($this->generateUrl('consignacion_show', array('id'=>$entity->getId())));
    }
    /**
     * Deletes a Consignacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Consignacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Consignacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consignacion'));
    }

    /**
     * Creates a form to delete a Consignacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consignacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
