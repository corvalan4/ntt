<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\IngresosBrutos;
use AppBundle\Form\IngresosBrutosType;

/**
 * IngresosBrutos controller.
 *
 */
class IngresosBrutosController extends Controller
{

    /**
     * Lists all IngresosBrutos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:IngresosBrutos')->findAll();

        return $this->render('AppBundle:IngresosBrutos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new IngresosBrutos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new IngresosBrutos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ingresosbrutos_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:IngresosBrutos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a IngresosBrutos entity.
     *
     * @param IngresosBrutos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(IngresosBrutos $entity)
    {
        $form = $this->createForm(new IngresosBrutosType(), $entity, array(
            'action' => $this->generateUrl('ingresosbrutos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new IngresosBrutos entity.
     *
     */
    public function newAction()
    {
        $entity = new IngresosBrutos();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:IngresosBrutos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a IngresosBrutos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:IngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IngresosBrutos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:IngresosBrutos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing IngresosBrutos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:IngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IngresosBrutos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:IngresosBrutos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a IngresosBrutos entity.
    *
    * @param IngresosBrutos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(IngresosBrutos $entity)
    {
        $form = $this->createForm(new IngresosBrutosType(), $entity, array(
            'action' => $this->generateUrl('ingresosbrutos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing IngresosBrutos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:IngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IngresosBrutos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ingresosbrutos_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:IngresosBrutos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a IngresosBrutos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:IngresosBrutos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find IngresosBrutos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ingresosbrutos'));
    }

    /**
     * Creates a form to delete a IngresosBrutos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ingresosbrutos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
