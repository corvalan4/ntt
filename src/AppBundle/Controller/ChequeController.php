<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Cheque;
use AppBundle\Entity\MovimientoBancario;
use AppBundle\Form\ChequeType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Cheque controller.
 *
 */
class ChequeController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Cheque entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $idCheque = $request->get('idCheque');
        $estado = $request->get('estado');
        $banco = $request->get('banco');
        $nro = $request->get('nro');
        $tipoCheque = $request->get('tipoCheque');
        $fechaEmisionDesde = $request->get('fechaEmisionDesde');
        $fechaEmisionHasta = $request->get('fechaEmisionHasta');
        $fechaCobroDesde = $request->get('fechaCobroDesde');
        $fechaCobroHasta = $request->get('fechaCobroHasta');

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $this->sessionManager->getUnidad()->getId();
        } else {
            $unidadNegocio = $request->get('unidadNegocio');
        }

        $this->sessionManager->setSession('bIdCheque', $idCheque);
        $this->sessionManager->setSession('bEstado', $estado);
        $this->sessionManager->setSession('bBanco', $banco);
        $this->sessionManager->setSession('bNro', $nro);
        $this->sessionManager->setSession('bTipoCheque', $tipoCheque);
        $this->sessionManager->setSession('bFechaEmisionDesde', $fechaEmisionDesde);
        $this->sessionManager->setSession('bFechaEmisionHasta', $fechaEmisionHasta);
        $this->sessionManager->setSession('bFechaCobroDesde', $fechaCobroDesde);
        $this->sessionManager->setSession('bFechaCobroHasta', $fechaCobroHasta);
        $this->sessionManager->setSession('bUnidadNegocio', $unidadNegocio);

        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado' => 'A'));
        $entities = $em->getRepository('AppBundle:Cheque')->filtroBusqueda($idCheque, $estado, $banco, $nro, $tipoCheque, $fechaEmisionDesde, $fechaEmisionHasta, $fechaCobroDesde, $fechaCobroHasta, $unidadNegocio);
        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Cheque:index.html.twig', array(
                    'entities' => $paginador,
                    'cuentas' => $cuentas,
                    'origen' => 'cheque'
        ));
    }

    /**
     * Creates a new Cheque entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
        $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque'));
        $cheque = new Cheque();

        $cheque->setImporte(floatval(str_replace(',', '.', $request->get('importe'))));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setUnidadNegocio($unidad);
        $cheque->setCuentaorigen($cuenta);
        $em->persist($cheque);
        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Cheque registrado correctamente.');
        $origen = $request->get('origen');

        if ($origen == 'cheque') {
            return $this->redirect($this->generateUrl('cheque'));
        }
        if ($origen == 'pago') {
            return $this->redirect($this->generateUrl('pago_new'));
        }
        if ($origen == 'gasto') {
            return $this->redirect($this->generateUrl('gasto_new'));
        }
    }

    /**
     * Creates a form to create a Cheque entity.
     *
     * @param Cheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cheque $entity) {
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Cheque entity.
     *
     */
    public function newAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->sessionManager->getUnidad()->getId());
        } else {
            $unidadId = $request->get('unidadNegocio');
            if (empty($unidadId)) {
                return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
            }
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($unidadId);
        }
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $origen = $request->get('origen');
        return $this->render('AppBundle:Cheque:new.html.twig', array(
                    'tipocheques' => $tipocheques,
                    'cuentas' => $cuentas,
                    'bancos' => $bancos,
                    'unidadnegocio' => $unidad,
                    'origen' => $origen
        ));
    }

    /**
     * Finds and displays a Cheque entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Cheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Cheque:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cheque entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Cheque')->find($id);
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));

        return $this->render('AppBundle:Cheque:edit.html.twig', array(
                    'tipocheques' => $tipocheques,
                    'entity' => $entity,
                    'bancos' => $bancos,
                    'cuentas' => $cuentas,
        ));
    }

    public function depositarAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
        if ($request->get('idcuenta') != 0) {
            $cuentabancaria = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('idcuenta'));

            $cheque->setEstado('D');
            $cheque->setCuentadestino($cuentabancaria);
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cuentabancaria);
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setEstado('D');
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $em->persist($movimiento);
            if ($cheque->getCuentaorigen()) {
                $movimiento = new MovimientoBancario();
                $movimiento->setCheque($cheque);
                $movimiento->setCuentabancaria($cheque->getCuentaorigen());
                $movimiento->setTipoMovimiento('CH');
                $movimiento->setValor($cheque->getImporte());
                $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
                $movimiento->setEstado('D');
                $em->persist($movimiento);
            }

            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Cheque Depositado correctamente.');
        } else {
            $this->sessionManager->addFlash('msgWarn', 'Debe seleccionar una Cuenta Bancaria.');
        }
        return $this->redirect($this->generateUrl('cheque'));
    }

    public function conciliarAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
        $cheque->setConciliacion('SI');
        $cheque->setEstado('C');
        $em->persist($cheque);

        if ($cheque->getCuentaorigen()) {
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cheque->getCuentaorigen());
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $movimiento->setEstado('C');
            $em->persist($movimiento);
        }

        if ($cheque->getCuentadestino()) {
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cheque->getCuentadestino());
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $movimiento->setEstado('C');
            $em->persist($movimiento);
        }

        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Cheque Conciliado correctamente.');
        return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Creates a form to edit a Cheque entity.
     *
     * @param Cheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cheque $entity) {
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Cheque entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('AppBundle:Cheque')->find($request->get('idcheque'));

        $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque'));

        $cheque->setImporte($request->get('importe'));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setCuentaorigen($cuenta);

        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Cheque editado correctamente.');
        return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Deletes a Cheque entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Cheque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cheque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Creates a form to delete a Cheque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cheque_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function cambiarestadoAction(Request $request) {
        $estado = $request->get('estado');
        $id = $request->get('id');
        if (isset($estado)) {
            $em = $this->getDoctrine()->getManager();
            $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
            $cheque->setEstado($estado);
            $em->persist($cheque);
            
            if ($cheque->getCuentaorigen()) {
                $movimiento = new MovimientoBancario();
                $movimiento->setCheque($cheque);
                $movimiento->setCuentabancaria($cheque->getCuentaorigen());
                $movimiento->setTipoMovimiento('CH');
                $movimiento->setValor($cheque->getImporte());
                $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
                $movimiento->setEstado($estado);
                $em->persist($movimiento);
            }
            if ($cheque->getCuentadestino()) {
                $movimiento = new MovimientoBancario();
                $movimiento->setCheque($cheque);
                $movimiento->setCuentabancaria($cheque->getCuentadestino());
                $movimiento->setTipoMovimiento('CH');
                $movimiento->setValor($cheque->getImporte());
                $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
                $movimiento->setEstado('Z');
                $em->persist($movimiento);
            }
            $em->flush();
            $this->sessionManager->addFlash('msgOk', 'Acción realizada.');
        }
        return $this->redirect($this->generateUrl('cheque'));
    }

}
