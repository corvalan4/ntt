<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Entity\Consignacion;
use AppBundle\Entity\ProductoConsignacion;
use AppBundle\Entity\ProductoFactura;
use AppBundle\Entity\Sucursal;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Stock;
use AppBundle\Form\StockType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Stock controller.
 *
 */
class StockController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @DI\Inject("stockservice")
     */
    public $stockService;

    /**
     * @DI\Inject("productoremitoservice")
     */
    public $productoRemitoService;

    /**
     * @DI\Inject("productoconsignacionservice")
     */
    public $productoConsignacionService;

    /**
     * Lists all Stock entities.
     *
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();
        if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
            $entities = $em->getRepository('AppBundle:Stock')->findBy(array('producto' => $id, 'unidadnegocio' => $this->getUser()->getUnidadnegocio()->getId()));
        } else {
            $entities = $em->getRepository('AppBundle:Stock')->findBy(array('producto' => $id));
        }


        $producto = $em->getRepository('AppBundle:Producto')->find($id);

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        return $this->render('AppBundle:Stock:index.html.twig', array(
                    'entities' => $entities,
                    'producto' => $producto,
                    'unidades' => $unidades,
        ));
    }

    /**
     * Creates a new Stock entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Stock();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            if ($entity->getProducto() and $entity->getProducto()->getTrazable()) {
                $entity->setStock(1);
            }
            $balance = $this->sessionManager->newBalance($entity, $this->getUser(), $entity->getCosto(), $entity->getStock());
            $em->persist($balance);

            $em->persist($entity);
            $em->flush();

            if ($entity->getProducto()->getTrazable()) {
                $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
                $this->sessionManager->csvVerifarmaAlta([$balance], "A" . $namesheet);
            }

            $this->addFlash('msgOk', 'Alta realizada.');


            return $this->redirect($this->generateUrl('stock', array('id' => $entity->getProducto()->getId())));
        }

        return $this->render('AppBundle:Stock:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Stock entity.
     *
     * @param Stock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Stock $entity) {
        $form = $this->createForm(new StockType(), $entity, array(
            'action' => $this->generateUrl('stock_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Stock entity.
     *
     */
    public function newAction($id) {
        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository('AppBundle:Producto')->find($id);
        $entity = new Stock();
        $entity->setRealizo($this->getUser());
        $entity->setProducto($producto);
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Stock:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Stock entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Stock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Stock:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Stock entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Stock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Stock:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Stock entity.
     *
     * @param Stock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Stock $entity) {
        $form = $this->createForm(new StockType(), $entity, array(
            'action' => $this->generateUrl('stock_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Stock entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Stock')->find($id);
        $lastBalance = $em->getRepository('AppBundle:Balance')->findOneBy(['stock' => $entity->getId()], ['id' => 'DESC']);
        $lastStock = $entity->getStock();
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($lastStock > $entity->getStock()) {
                $balance = new Balance();
                $balance->setStock($entity);
                $balance->setUser($this->getUser());
                $balance->setPrice($entity->getCosto());
                $balance->setAmount(($lastStock - $entity->getStock()));
                $balance->setType(Balance::TYPE_NEGATIVE);
                $balance->setAmountBalance($lastBalance->getAmountBalance() - ($lastStock - $entity->getStock()));
                $balance->setDescription("Ajuste de Stock");
                $em->persist($balance);
            }
            if ($lastStock < $entity->getStock()) {
                $balance = new Balance();
                $balance->setStock($entity);
                $balance->setUser($this->getUser());
                $balance->setPrice($entity->getCosto());
                $balance->setAmount(($entity->getStock() - $lastStock));
                $balance->setType(Balance::TYPE_POSITIVE);
                $balance->setAmountBalance($lastBalance->getAmountBalance() + ($entity->getStock() - $lastStock));
                $balance->setDescription("Ajuste de Stock");
                $em->persist($balance);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('stock', array('id' => $entity->getProducto()->getId())));
        }

        return $this->render('AppBundle:Stock:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /*
     * Predictiva Compras
     */

    public function predictivaAction(Request $request) {

        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();

        $string = '<ul>';

        if (strlen($valor) > 1) {
            /* if (!empty($request->get('idunidad'))) {
              $lista = $request->get('lista');
              $entities = $em->getRepository('AppBundle:Stock')->predictiva($request->get('valor'), $request->get('idunidad'), $lista);

              foreach ($entities as $entity) {

              $string .= '<li class=suggest-element idproducto=' . $entity->getProducto()->getId() .
              ' idstock=' . ($entity->getProducto()->getTrazable() ? $entity->getId() : '') .
              ' trazable=' . $entity->getProducto()->getTrazable() .
              ' stock=' . $entity->getProducto()->getStock();
              if ($lista == 0 or empty($lista)) {
              $string .= ' precio=' . $entity->getProducto()->getPrecio();
              } else {
              foreach ($entity->getProducto()->getPrecios() as $precio) {
              if ($precio->getListaprecio()->getId() == $lista) {
              $string .= ' precio=' . $precio->getValor();
              }
              }
              }

              $string .= '><a href="#" id=predictivaelemento' . $entity->getId() .
              ' data=' . $entity->getProducto()->getId() . ' > COD: ' .
              $entity->getProducto()->getCodigo() . ' - ' . $entity->getProducto()->getNombre() . ' - ' . $entity->getSerie()
              ;
              }
              } else { */

            $entities = $em->getRepository('AppBundle:Producto')->findByFilter(array("codigoLike" => $valor, "descripcion" => $valor), 'A');

            foreach ($entities as $entity) {
                $string = $string .
                        '<li class=suggest-element id=' . $entity->getId() .
                        ' precio=' . $entity->getPrecio() .
                        '><a href="#" id=predictivaelemento' . $entity->getId() .
                        ' data=' . $entity->getId() . ' >' . $entity->getCodigo() .
                        ' - ' . $entity->getNombre();
            }
            //}
        }
        $string = $string . '</ul>';
        return new Response($string);
    }

    public function predictivaSelect2Action(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $trazable = $request->get("trazable") == 'true' ? 1 : 0;
        $response = ['items' => []];

        if (strlen($request->get('search')) < 3) {
            return new JsonResponse([]);
        }
        if ($trazable) {
            $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad')];
            $entities = $em->getRepository('AppBundle:Stock')->findByFilter($filter, $trazable);
            $stockService = $this->container->get('stockservice');
            foreach ($entities as $entity) {
                $reservas = (int) $stockService->reservaPorStock($entity->getId());
                if (($entity->getStock() - $reservas) > 0) {
                    $response['items'][] = [
                        'id' => $entity->getId(),
                        'trazable' => $entity->getProducto()->getTrazable(),
                        'max_stock' => $entity->getStock() - $reservas,
                        'tipo_iva' => $entity->getProducto()->getTipoiva(),
                        'price' => $entity->getProducto()->getPrecio(),
                        'id_prod' => $entity->getProducto()->getId(),
                        'id_stock' => $entity->getId(),
                        //'name' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                        //'full_name' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                        //'text' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                        'name' => $entity->getProducto()->getNombre(),
                        'full_name' => $entity->getProducto()->getNombre(),
                        'text' => $entity->getProducto()->getNombre()
                    ];
                }
            }
        } else {
//            TODO: Old, quizas borrar a futuro pero chequear que no sirva mas.
//            $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad')];
//            $entities = $em->getRepository('AppBundle:Producto')->findByFilterWithStock($filter);
//            foreach ($entities as $entity) {
//                $response['items'][] = [
//                    'id' => $entity->getId(),
//                    'id_prod' => $entity->getId(),
//                    'name' => $entity->getNombre() . ' - GTIN ' . $entity->getGtin(),
//                    'full_name' => $entity->getNombre() . ' - GTIN ' . $entity->getGtin(),
//                    'text' => $entity->getNombre() . ' - GTIN ' . $entity->getGtin(),
//                ];
//            }

            $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad')];
            $entities = $em->getRepository('AppBundle:Stock')->findByFilter($filter, '');
            $stockService = $this->container->get('stockservice');
            foreach ($entities as $entity) {
                $reservas = (int) $stockService->reservaPorStock($entity->getId());
                if (($entity->getStock() - $reservas) > 0) {
                    $response['items'][] = [
                        'id' => $entity->getId(),
                        'trazable' => $entity->getProducto()->getTrazable(),
                        'max_stock' => $entity->getStock() - $reservas,
                        'price' => $entity->getProducto()->getPrecio(),
                        'tipo_iva' => $entity->getProducto()->getTipoiva(),
                        'id_prod' => $entity->getProducto()->getId(),
                        'id_stock' => $entity->getId(),
                        //'name' => $entity->getProducto()->getNombre() . (!empty($entity->getSerie()) ? ' - N° ' . $entity->getSerie() : '') . ' - Lote: ' . $entity->getLote(),
                        //'full_name' => $entity->getProducto()->getNombre() . (!empty($entity->getSerie()) ? ' - N° ' . $entity->getSerie() : '') . ' - Lote: ' . $entity->getLote(),
                        //'text' => $entity->getProducto()->getNombre() . (!empty($entity->getSerie()) ? ' - N° ' . $entity->getSerie() : '') . ' - Lote: ' . $entity->getLote(),
                        'name' => $entity->getProducto()->getNombre(),
                        'full_name' => $entity->getProducto()->getNombre(),
                        'text' => $entity->getProducto()->getNombre(),
                    ];
                }
            }
        }

        $response['total_count'] = sizeof($entities);

        return new JsonResponse($response);
    }

    public function predictivaSelect2ProductoFacturaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $facturas = $request->get("facturas") ? $request->get("facturas") : null;
        $response = ['items' => []];

        if (strlen($request->get('search')) < 3 or!$facturas) {
            return new JsonResponse([]);
        }

        $filter = ['search' => $request->get('search'), 'facturas' => $facturas];
        $productosFactura = $em->getRepository(ProductoFactura::class)->findByFilter($filter);

        foreach ($productosFactura as $productoFactura) {
            $entity = $productoFactura->getStock();
            //$serie = $entity and $entity->getProducto()->getTrazable() ? ' - N° ' . $entity->getSerie() : '';
            // $name = $productoFactura->getDescripcion() ? $productoFactura->getDescripcion() : $entity->getProducto()->getNombre() . $serie . ' - Lote: ' . $entity->getLote();
            $name = $productoFactura->getDescripcion() ? $productoFactura->getDescripcion() : $entity->getProducto()->getNombre(); // . $serie . ' - Lote: ' . $entity->getLote();

            $response['items'][] = [
                'id' => $productoFactura->getId(),
                'trazable' => $entity ? $entity->getProducto()->getTrazable() : false,
                'max_stock' => $productoFactura->getCantidad(),
                'tipo_iva' => $entity->getProducto()->getTipoiva(),
                'price' => $productoFactura->getPrecio(),
                'id_prod' => $entity ? $entity->getProducto()->getId() : '',
                'id_productofactura' => $productoFactura->getId(),
                'name' => $name,
                'text' => $name,
            ];
        }

        $response['total_count'] = sizeof($productosFactura);

        return new JsonResponse($response);
    }

    public function predictivaRemitoSelect2Action(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $response = ['items' => []];

        if (strlen($request->get('search')) < 3 or empty($request->get('idsucursal'))) {
            return new JsonResponse($response);
        }

        $sucursal = $em->getRepository(Sucursal::class)->find($request->get('idsucursal'));
        if (!$sucursal) {
            return new JsonResponse($response);
        }

        $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad'), 'cliente_id' => $sucursal->getClienteproveedor()->getId()];
        $productosconsignacion = $em->getRepository(ProductoConsignacion::class)->findByFilter($filter);

        foreach ($productosconsignacion as $productoconsignacion) {
            $entity = $productoconsignacion->getStock();
            $response['items'][] = [
                'id' => $entity->getId(),
                'consignacion_id' => $productoconsignacion->getConsignacion()->getId(),
                'trazable' => $entity->getProducto()->getTrazable(),
                'max_stock' => $productoconsignacion->getCantidad(),
                'vendedor_id' => $productoconsignacion->getVendedor() ? $productoconsignacion->getVendedor()->getId() : 0,
                'id_prod' => $entity->getProducto()->getId(),
                'id_stock' => $entity->getId(),
                //'name' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                'name' => $entity->getProducto()->getNombre(),
                //'full_name' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                'full_name' => $entity->getProducto()->getNombre(),
                //'text' => $entity->getProducto()->getNombre() . ' - N° ' . $entity->getSerie() . ' - Lote: ' . $entity->getLote(),
                'text' => $entity->getProducto()->getNombre(),
            ];
        }

        $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad')];
        $entities = $em->getRepository('AppBundle:Stock')->findByFilter($filter);
        $stockService = $this->container->get('stockservice');
        foreach ($entities as $entity) {
            $reservas = (int) $stockService->reservaPorStock($entity->getId());
            if (($entity->getStock() - $reservas) > 0) {
                $name = '*DEPÓSITO*: ' . $entity->getProducto()->getNombre() . ($entity->getSerie() ? ' - N° ' . $entity->getSerie() : '') . ' - Lote: ' . $entity->getLote();
                $response['items'][] = [
                    'id' => $entity->getId(),
                    'trazable' => $entity->getProducto()->getTrazable(),
                    'consignacion_id' => 0,
                    'vendedor_id' => 0,
                    'max_stock' => $entity->getStock() - $reservas,
                    'id_prod' => $entity->getProducto()->getId(),
                    'id_stock' => $entity->getId(),
                    'name' => $name,
                    'full_name' => $name,
                    'text' => $name,
                ];
            }
        }

        $response['total_count'] = sizeof($response);

        return new JsonResponse($response);
    }

    /**
     * Deletes a Stock entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Stock')->find($id);
        $balances = $em->getRepository('AppBundle:Balance')->findBy(['stock' => $entity->getId()]);

        foreach ($balances as $balance) {
            $em->remove($balance);
        }

        $producto_id = $entity->getProducto()->getId();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('stock', ['id' => $producto_id]));
    }

    /**
     * Creates a form to delete a Stock entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('stock_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Lists all Stock entities.
     *
     */
    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $codigo = $request->get('codigo');
        $nombre = $request->get('nombre');
        $categoria = $request->get('categoria');
        $subcategoria = $request->get('subcategoria');

        if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
            $unidadNegocio = $this->getUser()->getUnidadnegocio()->getId();
        } else {
            $unidadNegocio = $request->get('unidadNegocio');
        }

        $entities = $em->getRepository('AppBundle:Producto')->filtroBusquedaInformeStock($codigo, $nombre, $categoria, $subcategoria, $unidadNegocio);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(20);
        $paginador->setCurrentPage($page);
        return $this->render('AppBundle:Stock:informe.html.twig', array(
                    'entities' => $paginador,
        ));
    }

    public function disenoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $unidadNegocio = $request->get('unidadNegocio');

        $stockDiseno = $em->getRepository('AppBundle:Stock')->sumStockDiseno($id, $unidadNegocio);
        if (!isset($stockDiseno)) {
            $stockDiseno = 0;
        }
        return new Response($stockDiseno);
    }

    public function importarAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $unidad = $request->get('unidad_id');
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        if (!empty($unidad) and!empty($request->files->get('archivo'))) {
            $em = $this->getDoctrine()->getManager();
            $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($unidad);
            $reader = new Xlsx();
            $reader->setReadDataOnly(TRUE);
            $archivoTemp = $request->files->get('archivo')->getPathName();
            $spreadsheet = $reader->load($archivoTemp);
            $worksheet = $spreadsheet->getSheet(0);

            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            $errorArray = [];

            for ($row = 2; $row <= $highestRow; $row++) {
                $cod_producto = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $fecha_ingreso = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $despacho = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $deposito = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $lote = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $fecha_vencimiento = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                $cantidad = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $procedencia = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $origen = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                $serie = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                $etiqueta = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                $carga = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                $higiene = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                $estado = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                $usu_realizo = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                $usu_controlo = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                $costo = $worksheet->getCellByColumnAndRow(17, $row)->getValue();

                $producto = $em->getRepository('AppBundle:Producto')->findOneBy(['codigo' => $cod_producto]);
                if (empty($producto)) {
                    $errorArray[] = ['Fila' => $row, 'Codigo' => $cod_producto, 'Mensaje' => 'No se encontró entre los productos'];
                    // Manejar Error
                    continue;
                }

                $stock = $em->getRepository('AppBundle:Stock')->findOneBy(['producto' => $producto->getId(), 'serie' => $serie]);

                if (!empty($stock)) {
                    $errorArray[] = ['Fila' => $row, 'Codigo' => $cod_producto, 'Mensaje' => "El stock con n° de serie $serie para el codigo de producto $cod_producto ya existe."];
                    // Manejar Error
                    continue;
                }

                if ($fecha_vencimiento and is_numeric($fecha_vencimiento)) {
                    $fecha_vencimiento = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($fecha_vencimiento);
                }

                if ($fecha_vencimiento and is_string($fecha_vencimiento)) {
                    $fecha_vencimiento = new \DateTime($fecha_vencimiento);
                }

                if ($fecha_ingreso and is_numeric($fecha_ingreso)) {
                    $fecha_ingreso = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($fecha_ingreso);
                }

                if ($fecha_ingreso and is_string($fecha_ingreso)) {
                    $fecha_ingreso = new \DateTime($fecha_ingreso);
                }

                $stock = new Stock();
                $stock->setProducto($producto);
                $stock->setSerie($serie);
                $stock->setDeposito(strtoupper($deposito));
                $stock->setUnidadnegocio($unidadNegocio);
                $stock->setFecha($fecha_ingreso ? $fecha_ingreso : new \DateTime('NOW'));
                $stock->setVencimiento($fecha_vencimiento ? $fecha_vencimiento : null);
                $stock->setCosto($costo);

                $user_realizo = $em->getRepository('AppBundle:Usuario')->findOneBy(['username' => $usu_realizo]);
                $stock->setRealizo($user_realizo ? $user_realizo : $this->getUser());

                $user_controlo = $em->getRepository('AppBundle:Usuario')->findOneBy(['username' => $usu_controlo]);
                $stock->setControlo($user_controlo ? $user_controlo : null);

                $stock->setLote($lote);
                $stock->setDespacho($despacho);
                $stock->setEstado($estado);
                $stock->setHigiene(!empty($higiene) ? ucfirst($higiene) : 'Cumple');
                $stock->setCarga(!empty($etiqueta) ? ucfirst($carga) : 'Cumple');
                $stock->setEtiqueta(!empty($etiqueta) ? ucfirst($etiqueta) : 'Cumple');
                $stock->setOrigen($origen);
                $stock->setProcedencia($procedencia);
                $stock->setStock($cantidad ? $cantidad : 1);

                $em->persist($stock);
                $adicional = ['description' => 'Importación Inicial'];
                $balance = $this->sessionManager->newBalance($stock, $this->getUser(), $stock->getCosto(), $cantidad, Balance::TYPE_POSITIVE, $adicional);
                $em->persist($balance);
            }

            $em->flush();
            if (sizeof($errorArray) > 0) {
                $this->addFlash("msgWarn", "Errores en la migración: " . json_encode($errorArray));
            }
            $this->addFlash("msgOk", "Importación Finalizada");
            return $this->redirectToRoute("stock_importar");
        }

        return $this->render('AppBundle:Stock:importar.html.twig', array(
                    'unidades' => $unidades,
        ));
    }

    public function importarConsignacionesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $unidad = $request->get('unidad_id');
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        if (!empty($unidad) and!empty($request->files->get('archivo'))) {
            $em = $this->getDoctrine()->getManager();
            $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($unidad);
            $reader = new Xlsx();
            $reader->setReadDataOnly(TRUE);
            $archivoTemp = $request->files->get('archivo')->getPathName();
            $spreadsheet = $reader->load($archivoTemp);
            $worksheet = $spreadsheet->getSheet(1);

            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $errorArray = [];
            for ($row = 2; $row <= $highestRow; $row++) {
                $nuevaConsignacion = false;

                $tazable = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $cod_producto = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $lote = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $serie = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $banco_cuit = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $banco_nombre = $worksheet->getCellByColumnAndRow(6, $row)->getValue();

                $consignacion_fecha = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $consignacion_numero = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $consignacion_descripcion = $worksheet->getCellByColumnAndRow(9, $row)->getValue();

                $consignacion_cantidad = $worksheet->getCellByColumnAndRow(10, $row)->getValue();

                $clienteProveedor = $em->getRepository(ClienteProveedor::class)->findOneBy(['cuit' => $banco_cuit]);
                if (!$clienteProveedor) {
                    $errorArray[] = ['Fila' => $row, 'CUIT' => $banco_cuit, 'Mensaje' => 'No se encontró Cliente con el CUIT indicado.'];
                    continue;
                }

                $consignacion = $em->getRepository(Consignacion::class)->findOneBy(['numero' => $consignacion_numero]);
                if (empty($consignacion)) {
                    $nuevaConsignacion = true;

                    $consignacion = new Consignacion();

                    if ($consignacion_fecha and is_numeric($consignacion_fecha)) {
                        $consignacion_fecha = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($consignacion_fecha);
                    }

                    if ($consignacion_fecha and is_string($consignacion_fecha)) {
                        $consignacion_fecha = new \DateTime($consignacion_fecha);
                    }

                    $consignacion->setFecha($consignacion_fecha ? $consignacion_fecha : new \DateTime('NOW'));
                    $consignacion->setClienteProveedor($clienteProveedor);
                    $consignacion->setNumero($consignacion_numero);
                    $consignacion->setObservacion($consignacion_descripcion);
                    $consignacion->setSucursal($clienteProveedor->getSucursales()[0]);
                    $consignacion->setUnidadNegocio($unidadNegocio);
                    $consignacion->setEstado(Consignacion::CONSIGNACION_IN_PROCESS);

                    $em->persist($consignacion);
                }

                $producto = $em->getRepository('AppBundle:Producto')->findOneBy(['codigo' => $cod_producto]);
                if (empty($producto)) {
                    $errorArray[] = ['Fila' => $row, 'Codigo' => $cod_producto, 'Mensaje' => 'No se encontró producto con el código indicado.'];
                    continue;
                }

                $stock = null;

                if (!empty($serie)) {
                    $stock = $em->getRepository(Stock::class)->findOneBy(['serie' => $serie, 'producto' => $producto->getId(), 'lote' => $lote]);
                    if (!$stock) {
                        $errorArray[] = ['Fila' => $row, 'Serie' => $serie, 'Mensaje' => 'No se encontró STOCK para el numero de serie indicado.'];
                        continue;
                    }
                } else {
                    $stock = $em->getRepository(Stock::class)->findOneBy(['producto' => $producto->getId(), 'lote' => $lote]);
                    if (!$stock) {
                        $errorArray[] = ['Fila' => $row, 'Lote' => $lote, 'CodProducto' => $cod_producto, 'Mensaje' => 'No se encontró STOCK para el lote y codigo indicados.'];
                        continue;
                    }
                }

                if (!$nuevaConsignacion) {
                    $productoConsignacion = $em->getRepository(ProductoConsignacion::class)->findOneBy(['stock' => $stock->getId(), 'consignacion' => $consignacion->getId()]);
                    if ($productoConsignacion) {
                        $errorArray[] = ['Fila' => $row, 'Consignacion' => $consignacion_numero, 'CodProducto' => $cod_producto, 'Mensaje' => 'Este producto ya se encuentra en esta consignacion.'];
                        continue;
                    }
                }

                $stock->setStock($stock->getStock() - $consignacion_cantidad);
                $productoConsignacion = new ProductoConsignacion();
                $productoConsignacion->setProducto($producto);
                $productoConsignacion->setStock($stock);
                $productoConsignacion->setCantidad($consignacion_cantidad);
                $productoConsignacion->setConsignacion($consignacion);

                $em->persist($productoConsignacion);

                $additional = ["description" => "Alta de Consignacion Cliente MIGRACION", "consignacion" => $consignacion];
                $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, $consignacion_cantidad, Balance::TYPE_NEGATIVE, $additional);
                $em->persist($balance);

                $em->flush();
            }

            if (sizeof($errorArray) > 0) {
                $this->addFlash("msgWarn", "Errores en la migración: " . json_encode($errorArray));
            }
            $this->addFlash("msgOk", "Importación Finalizada");
            return $this->redirectToRoute("stock_importar_consignaciones");
        }

        return $this->render('AppBundle:Stock:importarConsignaciones.html.twig', array(
                    'unidades' => $unidades,
        ));
    }

    public function importarProductoAction($id_producto, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $producto = $em->getRepository('AppBundle:Producto')->find($id_producto);
        if (!$producto) {
            return $this->redirectToRoute('producto');
        }

        $unidad = $request->get('unidad_id');
        $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($unidad);

        $fecha_ingreso = new \DateTime('NOW');
        $despacho = $request->get('despacho');
        $deposito = $request->get('deposito');
        $lote = $request->get('lote');
        $fecha_vencimiento = $request->get('fecha_vencimiento');
        $cantidad = $request->get('cantidad');
        $procedencia = $request->get('procedencia');
        $origen = $request->get('origen');
        $etiqueta = $request->get('etiqueta');
        $carga = $request->get('carga');
        $higiene = $request->get('higiene');
        $costo = $request->get('costo');

        // Para la conformación del código
        $codigo = $request->get('codigo');
        $nrodesde = (int) $request->get('nrodesde');
        $nrohasta = (int) $request->get('nrohasta');
        if ($nrohasta < $nrodesde) {
            return $this->redirect($this->generateUrl('stock', array('id' => $producto->getId())));
        }

        if (empty($fecha_vencimiento)) {
            $this->addFlash('msgError', 'Debe agregar fecha de vencimiento');
            return $this->redirect($this->generateUrl('stock', array('id' => $producto->getId())));
        }

        $vencimiento = new \DateTime($fecha_vencimiento);
        $hoy = new \DateTime("NOW");

        if ($vencimiento < $hoy) {
            $this->addFlash('msgError', 'La fecha de vencimiento no puede ser menor a hoy.');
            return $this->redirect($this->generateUrl('stock', array('id' => $producto->getId())));
        }

        $arrBalances = [];
        for ($i = $nrodesde; $i <= $nrohasta; $i++) {
            $stock = new Stock();
            $serie = $codigo . str_pad($i, 4, "0", STR_PAD_LEFT);

            $stockExist = $em->getRepository('AppBundle:Stock')->findBy(['producto' => $producto->getId(), 'serie' => $serie]);

            if ($stockExist) {
                $this->addFlash('msgError', 'Ya existe el número de serie ' . $serie . ' para este producto');
                continue;
            }


            $stock->setProducto($producto);
            $stock->setSerie($serie);
            $stock->setUnidadnegocio($unidadNegocio);
            $stock->setFecha($fecha_ingreso);
            $stock->setVencimiento($vencimiento);
            $stock->setCosto($costo);
            $stock->setDeposito($deposito);
            $stock->setRealizo($this->getUser());
//        $user_controlo = $em->getRepository('AppBundle:Usuario')->findOneBy(['username'=>$usu_controlo]);
//        $stock->setControlo($user_controlo);
            $stock->setLote($lote);
            $stock->setDespacho($despacho);
            $stock->setHigiene(ucfirst($higiene));
            $stock->setCarga(ucfirst($carga));
            $stock->setEtiqueta(ucfirst($etiqueta));
            $stock->setOrigen($origen);
            $stock->setProcedencia($procedencia);
            $stock->setStock($cantidad ? $cantidad : 1);
            $em->persist($stock);
            $balance = $this->sessionManager->newBalance($stock, $this->getUser(), $stock->getCosto(), $cantidad);
            $em->persist($balance);
            $em->flush();

            $arrBalances[] = $balance;
        }

        if ($producto->getTrazable() and sizeof($arrBalances) > 0) {
            $status = '#VF_ALTALOTE';
            $letter = substr($status, 4, 1);
            $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);

            $this->sessionManager->csvVerifarmaAlta($arrBalances, $letter . $namesheet);
        }

        $this->addFlash("msgOk", "Importación Finalizada");
        return $this->redirectToRoute("stock", ['id' => $producto->getId()]);
    }

    /*
     * Informe Depósitos
     */

    public function informeDepositosAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();

        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Stock')->filtroBusquedaInformeDepositos($filtros);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcelInformeDepositos($entities, $filtros);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe Depósitos');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Stock:informeDepositos.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Exportación a excel de Informe de Depósitos
     */

    private function exportarExcelInformeDepositos($entities, $filtros) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Código');
        $sheet->setCellValue('B1', 'N° Lote');
        $sheet->setCellValue('C1', 'Vencimiento');
        $sheet->setCellValue('D1', 'Unidades Totales');
        $sheet->setCellValue('E1', 'Nro. Interno (Serie)');
        $sheet->setCellValue('F1', 'Proveedor');
        $sheet->setCellValue('G1', 'Depósito');
        $sheet->setCellValue('H1', 'Modelo');
        $sheet->setCellValue('I1', 'Unidades');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('J1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            $stocks = $this->stockService->stocksByCodigoLoteVencimiento($entity->getProducto()->getCodigo(), $entity->getLote(), $entity->getVencimiento()->format("d-m-Y"), $filtros);
            foreach ($stocks as $stock) {
                $unidadesTotales = $this->stockService->sumByCodigoLoteVencimiento($entity->getProducto()->getCodigo(), $entity->getLote(), $entity->getVencimiento()->format("d-m-Y"), $filtros, false);
                if ($unidadesTotales > 0) {
                    $sheet->setCellValue('A' . $fila, $entity->getProducto()->getCodigo());
                    $sheet->setCellValue('B' . $fila, $entity->getLote());
                    $sheet->setCellValue('C' . $fila, $entity->getVencimiento() ? $entity->getVencimiento()->format("d/m/Y") : '');
                    $sheet->setCellValue('D' . $fila, $unidadesTotales);
                    $sheet->setCellValue('E' . $fila, $stock->getSerie());
                    $sheet->setCellValue('F' . $fila, $stock->getProducto()->getProveedor());
                    $sheet->setCellValue('G' . $fila, $stock->getDeposito());
                    $sheet->setCellValue('H' . $fila, $stock->getProducto()->getModelo());
                    $sheet->setCellValue('I' . $fila, $stock->getStock());

                    if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                        $sheet->setCellValue('J' . $fila, $stock->getUnidadNegocio());
                    }
                    $fila++;
                }
            }
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Informe Pendientes de Facturación
     */

    public function informePendientesFacturacionAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();

        if (!empty($request->get('fechaDesde'))) {
            $filtros['fechaDesde'] = new \DateTime($request->get('fechaDesde'));
        }
        if (!empty($request->get('fechaHasta'))) {
            $filtros['fechaHasta'] = (new \DateTime($request->get('fechaHasta') . ' +1 day'));
        }

        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $entities = array_merge(
                $em->getRepository('AppBundle:Stock')->filtroBusquedaInformePendientesFacturacion($filtros),
                $em->getRepository('AppBundle:Stock')->filtroBusquedaInformePendientesFacturacionDesdeRemito($filtros)
        );

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcelInformePendientesFacturacion($entities, $filtros);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe Pendientes Facturación');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Stock:informePendientesFacturacion.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Exportación a excel de Informe de Depósitos
     */

    private function exportarExcelinformePendientesFacturacion($entities, $filtros) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Código');
        $sheet->setCellValue('B1', 'N° Lote');
        $sheet->setCellValue('C1', 'Nro. Interno (Serie)');
        $sheet->setCellValue('D1', 'Fecha de Consignación');
        $sheet->setCellValue('E1', 'Fecha de Remito');
        $sheet->setCellValue('F1', 'Fecha de Uso');
        $sheet->setCellValue('G1', 'Remito');
        $sheet->setCellValue('H1', 'Consignación');
        $sheet->setCellValue('I1', 'Vendedor');
        $sheet->setCellValue('J1', 'Paciente');
        $sheet->setCellValue('K1', 'Cliente');
        $sheet->setCellValue('L1', 'Banco');
        $sheet->setCellValue('N1', 'Observaciones uso');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('O1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            if ($entity['fromRemito']) {
                $producto = $this->productoRemitoService->getProductoRemitoByStock($entity[0]->getId());
            } else {
                $producto = $this->productoConsignacionService->getProductoConsignacionByStock($entity[0]->getId());
            }

            $sheet->setCellValue('A' . $fila, $entity[0]->getProducto()->getCodigo());
            $sheet->setCellValue('B' . $fila, $entity[0]->getLote());
            $sheet->setCellValue('C' . $fila, $entity[0]->getSerie());
            if ($entity['fromRemito']) {
                $sheet->setCellValue('E' . $fila, $producto->getRemitooficial()->getFecha() ? $producto->getRemitooficial()->getFecha()->format("d/m/Y") : '');
                $sheet->setCellValue('G' . $fila, $producto->getRemitooficial());
                $sheet->setCellValue('L' . $fila, $producto->getRemitooficial()->getClienteProveedor());
            } else {
                $sheet->setCellValue('D' . $fila, $producto->getConsignacion()->getFecha() ? $producto->getConsignacion()->getFecha()->format("d/m/Y") : '');
                $sheet->setCellValue('F' . $fila, $producto->getFechauso() ? $producto->getFechauso()->format("d/m/Y") : '');
                $sheet->setCellValue('H' . $fila, $producto->getConsignacion());
                $sheet->setCellValue('J' . $fila, $producto->getPaciente());
                $sheet->setCellValue('N' . $fila, $producto->getObservaciones());
                $sheet->setCellValue('L' . $fila, $producto->getConsignacion()->getClienteProveedor());
            }
            $sheet->setCellValue('K' . $fila, $producto->getCliente());
            $sheet->setCellValue('I' . $fila, $producto->getVendedor());



            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('O' . $fila, $entity[0]->getUnidadNegocio());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:N1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);

        return $spreadsheet;
    }

}
