<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\Consignacion;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Entity\ItemNota;
use AppBundle\Entity\Iva;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\ProductoConsignacion;
use AppBundle\Entity\ProductoFactura;
use AppBundle\Entity\ProductoRemito;
use AppBundle\Entity\RemitoOficial;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Symfony\Bridge\Propel1\Tests\Fixtures\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\NotaCreditoDebito;
use AppBundle\Form\NotaCreditoDebitoType;
use AppBundle\Form\NotaCreditoDebitoProveedorType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * NotaCreditoDebito controller.
 *
 */
class NotaCreditoDebitoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all NotaCreditoDebito entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:NotaCreditoDebito')->findAll();

        return $this->render('AppBundle:NotaCreditoDebito:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new NotaCreditoDebito entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $idClienteProveedor = $request->request->get('app_bundle_notacreditodebito')['clienteProveedor'];
        $clienteProveedor = $em->getRepository(ClienteProveedor::class)->find($idClienteProveedor);

        $entity = new NotaCreditoDebito();
        $form = $this->createCreateForm($entity, $clienteProveedor);
        $form->handleRequest($request);

        $hayProductos = false;
        $total = 0;
        $iva21 = 0;
        $iva105 = 0;
        $montoIva0 = 0;

        if ($form->isValid()) {
            for ($i = 1; $i <= 30; $i++) {
                $elementonombre = $request->get("elemento$i");
                if (!empty($elementonombre)) {
                    $tipoIva = $request->get("tipo_iva_$i");
                    if (!isset($tipoIva)) { //Precio final
                        $tipoIva = 21;
                    }
                    $productofactura_id = $request->get("productofactura_$i");
                    $cantidad = $request->get("cantidad$i");
                    $precio = $request->get("precio$i");
                    $subtotal = $cantidad * $precio;
                    $total += $subtotal;

                    $item = new ItemNota();

                    if (!empty($productofactura_id)) {
                        $productofactura = $em->getRepository(ProductoFactura::class)->find($productofactura_id);
                        if ($productofactura) {
                            $item->setProductofactura($productofactura);
                            if ($productofactura->getStock()) {
                                //$balanceConsignacion = $em->getRepository(Balance::class)->getLastBalanceWithConsignacion($productofactura->getStock());
                                $productoRemito = $em->getRepository(ProductoRemito::class)->getProductoRemitoByStockAndFactura($productofactura->getStock(), $productofactura->getFactura());

                                //$tipoIva = $productofactura->getStock()->getProducto()->getTipoiva();

                                if ($productoRemito) {
                                    $productoRemito->getRemitooficial()->setEstado(RemitoOficial::IN_PROCESS);
                                    $productoRemito->getRemitooficial()->setFactura(null);
                                    $productoRemito->setEstado(ProductoRemito::PRODUCTO_REMITO_IN_CLIENT);
                                }

                                $additional = ["description" => "Alta de Nota de Credito", "notacredito" => $entity];
                                $cantidadBalance = $productoRemito ? 0 : $cantidad;
                                $balance = $this->sessionManager->registerBalance($productofactura->getStock(), $this->getUser(), $precio, $cantidadBalance, Balance::TYPE_POSITIVE, $additional);
                                $em->persist($balance);
                                // habria que chequear trazabilidad.

                                $productofactura->getStock()->setStock($balance->getAmountBalance());
                            } else {
                                $remitos = $em->getRepository(RemitoOficial::class)->findBy(['factura' => $productofactura->getFactura()->getId()]);
                                foreach ($remitos as $remito) {
                                    $remito->setFactura(null);
                                    $remito->setEstado(RemitoOficial::IN_PROCESS);
                                    foreach ($remito->getProductosRemito() as $productoRemito) {
                                        $productoRemito->setEstado(ProductoRemito::PRODUCTO_REMITO_IN_CLIENT);
                                    }
                                }
                            }
                        }
                    }

                    $item->setTipoiva($tipoIva);

                    if ($tipoIva == 0) {
                        $montoIva0 += $subtotal;
                    } else {
                        $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                        if ($tipoIva == 21) {
                            $iva21 += $iva;
                        }
                        if ($tipoIva == 10.5) {
                            $iva105 += $iva;
                        }
                    }

                    $item->setPrecio($precio);
                    $item->setCantidad($cantidad);
                    $item->setDescripcion($elementonombre);
                    $item->setNotadebitocredito($entity);
                    $em->persist($item);

                    $hayProductos = true;

                    if ($entity->getClienteProveedor()->getCondicioniva() == 'Responsable Inscripto') {
                        $entity->setCodigonota("A");
                    }
                }
            }
            if (!$hayProductos) {
                $this->sessionManager->addFlash('msgError', 'Debe agregar items.');
                $entity->setTiponota($form['tiponota']->getData());
                $form = $this->createCreateForm($entity, $clienteProveedor);
                $tiposIva = $em->getRepository('AppBundle:TipoIva')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

                return $this->render('AppBundle:NotaCreditoDebito:new.html.twig', array(
                            'entity' => $entity,
                            'tiposIva'=> $tiposIva,
                            'form' => $form->createView(),
                ));
            }

            $entity->setPtovta($entity->getUnidadNegocio()->getPunto());
            $entity->setImporte($total);

            // Consultar


            if ($entity->getCodigonota() == 'A') {

                if ($montoIva0) {
                    $tipoIva0 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 0));
                    $alicuotaIva0 = new Iva();
                    $alicuotaIva0->setTipoIva($tipoIva0);
                    $alicuotaIva0->setNotaCreditoDebito($entity);
                    $alicuotaIva0->setValor(0);
                    $alicuotaIva0->setImporteFactura($montoIva0);
                    $em->persist($alicuotaIva0);
                    $entity->addIva($alicuotaIva0);
                }
                if ($iva21 > 0) {
                    $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
                    $alicuotaIva21 = new Iva();
                    $alicuotaIva21->setTipoIva($tipoIva21);
                    $alicuotaIva21->setNotaCreditoDebito($entity);
                    $alicuotaIva21->setValor(round($iva21, 2));
                    $alicuotaIva21->setImporteFactura(round(100 * $iva21 / 21, 2));
                    $em->persist($alicuotaIva21);
                    $entity->addIva($alicuotaIva21);
                }

                if ($iva105 > 0) {
                    $tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));
                    $alicuotaIva105 = new Iva();
                    $alicuotaIva105->setTipoIva($tipoIva105);
                    $alicuotaIva105->setNotaCreditoDebito($entity);
                    $alicuotaIva105->setValor(round($iva105, 2));
                    $alicuotaIva105->setImporteFactura(round(100 * $iva105 / 10.5, 2));
                    $em->persist($alicuotaIva105);
                    $entity->addIva($alicuotaIva105);
                }
            }

            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setNotaCreditoDebito($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte(round($entity->getTotal(), 2));

            $em->persist($pagoAsignacion);
            $em->persist($entity);
            $em->flush();

            $this->sessionManager->facturarNota($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('notacreditodebito_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:NotaCreditoDebito:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new NotaCreditoDebito entity.
     *
     */
    public function createProveedorAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $idClienteProveedor = $request->request->get('app_bundle_notacreditodebito')['clienteProveedor'];
        $clienteProveedor = $em->getRepository(ClienteProveedor::class)->find($idClienteProveedor);

        $entity = new NotaCreditoDebito();
        $form = $this->createCreateForm($entity, $clienteProveedor);
        $form->handleRequest($request);


        if ($form->isValid()) {
            if ($entity->getCodigonota() == 'A') {
                $tipoIva = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
                $iva = new Iva();

                if ($tipoIva) {
                    $iva->setTipoIva($tipoIva);
                }
                $iva->setNotaCreditoDebito($entity);
                $montoIva = $entity->getImporte() - ($entity->getImporte() / 1.21);
                $iva->setValor(round($montoIva, 2));
                $iva->setImporteFactura($entity->getImporte()); // Neto o total???
                $em->persist($iva);
                $entity->addIva($iva);
            }

            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setNotaCreditoDebito($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($entity->getImporte());

            $em->persist($pagoAsignacion);
            $em->persist($entity);

            $em->flush();
            return $this->redirect($this->generateUrl('notacreditodebito_proveedor_show', array('id' => $entity->getId())));
        }
        return $this->render('AppBundle:NotaCreditoDebito:newProveedor.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a NotaCreditoDebito entity.
     *
     * @param NotaCreditoDebito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NotaCreditoDebito $entity, $clienteProveedor) {
        if ($clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

            $form = $this->createForm(new NotaCreditoDebitoType(), $entity, array(
                'action' => $this->generateUrl('notacreditodebito_create'),
                'method' => 'POST',
            ));
        } else {
            $form = $this->createForm(new NotaCreditoDebitoProveedorType($clienteProveedor), $entity, array(
                'action' => $this->generateUrl('notacreditodebito_create_proveedor'),
                'method' => 'POST',
            ));
        }
        $form->add('submit', 'submit', array('label' => 'Crear Nota', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right')));

        return $form;
    }

    /**
     * Displays a form to create a new NotaCreditoDebito entity.
     *
     */
    public function newAction($id_clienteproveedor, $tipo) {

        if (empty($tipo) or empty($id_clienteproveedor)) {
            return $this->redirect($this->generateUrl("joyas_joyas_homepage"));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = new NotaCreditoDebito();
        $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id_clienteproveedor);
        $entity->setClienteProveedor($clienteProveedor);
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }

        $entity->setTiponota(strtoupper($tipo));
        $form = $this->createCreateForm($entity, $clienteProveedor);

        $tiposIva = $em->getRepository('AppBundle:TipoIva')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        if ($clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {
            return $this->render('AppBundle:NotaCreditoDebito:new.html.twig', array(
                        'entity' => $entity,
                        'tiposIva' => $tiposIva,
                        'form' => $form->createView(),
            ));
        } else {
            return $this->render('AppBundle:NotaCreditoDebito:newProveedor.html.twig', array(
                        'entity' => $entity,
                        'tiposIva' => $tiposIva,
                        'form' => $form->createView(),
            ));
        }
    }

    /**
     * Finds and displays a NotaCreditoDebito entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);
        switch ($entity->getCodigonota()) {
            case 'A':
                $nrocomprobante = strtoupper($entity->getTiponota()) == NotaCreditoDebito::TIPO_DEBITO ? '03' : '02';
                break;
            case 'B':
                $nrocomprobante = strtoupper($entity->getTiponota()) == NotaCreditoDebito::TIPO_CREDITO ? '08' : '07';
                break;
        }
        $codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
                str_replace("-", "", $entity->getUnidadNegocio()->getCuit()),
                $nrocomprobante,
                $entity->getPtovta(),
                $entity->getCae(),
                ($entity->getFechavtocae() ? $entity->getFechavtocae()->format('Ymd') : '')
        );
        $generator = new BarcodeGeneratorPNG();
        $barcode = base64_encode($generator->getBarcode($codigobarras, $generator::TYPE_INTERLEAVED_2_5));
        return $this->render('AppBundle:NotaCreditoDebito:show' . $entity->getCodigonota() . '.html.twig', array(
                    'entity' => $entity,
                    'tipofactura' => $entity->getCodigonota(),
                    'nrocomprobante' => $nrocomprobante,
                    'barcode' => $barcode,
                    'codigobarras' => $codigobarras
        ));
    }

    /**
     * Finds and displays a NotaCreditoDebito Proveedor entity.
     *
     */
    public function showProveedorAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);

        return $this->render('AppBundle:NotaCreditoDebito:showProveedor.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing NotaCreditoDebito entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:NotaCreditoDebito:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a NotaCreditoDebito entity.
     *
     * @param NotaCreditoDebito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(NotaCreditoDebito $entity) {
        $form = $this->createForm(new NotaCreditoDebitoType(), $entity, array(
            'action' => $this->generateUrl('notacreditodebito_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing NotaCreditoDebito entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('notacreditodebito_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:NotaCreditoDebito:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a NotaCreditoDebito entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notacreditodebito'));
    }

    /**
     * Creates a form to delete a NotaCreditoDebito entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('notacreditodebito_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function refacturarAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(NotaCreditoDebito::class)->find($id);
        $this->sessionManager->facturarNota($entity);

        return $this->redirect($this->generateUrl('pagoasignacion', ['id' => $entity->getClienteProveedor()->getId()]));
    }

}
