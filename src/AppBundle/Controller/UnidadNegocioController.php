<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UnidadNegocio;
use AppBundle\Entity\Caja;
use AppBundle\Form\UnidadNegocioType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * UnidadNegocio controller.
 *
 */
class UnidadNegocioController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all UnidadNegocio entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

        return $this->render('AppBundle:UnidadNegocio:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new UnidadNegocio entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new UnidadNegocio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->sessionManager->crearCaja($entity);

            return $this->redirect($this->generateUrl('unidadnegocio'));
        }

        return $this->render('AppBundle:UnidadNegocio:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a UnidadNegocio entity.
     *
     * @param UnidadNegocio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UnidadNegocio $entity) {
        $form = $this->createForm(new UnidadNegocioType(), $entity, array(
            'action' => $this->generateUrl('unidadnegocio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary btn-xs', 'onclick' => 'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new UnidadNegocio entity.
     *
     */
    public function newAction() {
        $entity = new UnidadNegocio();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:UnidadNegocio:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UnidadNegocio entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:UnidadNegocio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnidadNegocio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:UnidadNegocio:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing UnidadNegocio entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:UnidadNegocio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnidadNegocio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:UnidadNegocio:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a UnidadNegocio entity.
     *
     * @param UnidadNegocio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(UnidadNegocio $entity) {
        $form = $this->createForm(new UnidadNegocioType(), $entity, array(
            'action' => $this->generateUrl('unidadnegocio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing UnidadNegocio entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:UnidadNegocio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnidadNegocio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('unidadnegocio'));
        }

        return $this->render('AppBundle:UnidadNegocio:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a UnidadNegocio entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:UnidadNegocio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UnidadNegocio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('unidadnegocio'));
    }

    /**
     * Creates a form to delete a UnidadNegocio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unidadnegocio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }

    /*
     * Render Select 
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $unidadesNegocio = $em->getRepository('AppBundle:UnidadNegocio')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:UnidadNegocio:select.html.twig', array(
            'unidadesNegocio' => $unidadesNegocio
        ));
    }

    public function descripcionAction(Request $request) {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($id);
        if (isset($unidadNegocio)){
            $descripcion=$unidadNegocio->getDescripcion();
        }
        return new Response($descripcion);
    }
}
