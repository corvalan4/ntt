<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PagoFactura;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Pago;
use AppBundle\Entity\Caja;
use AppBundle\Entity\MedioDocumento;
use AppBundle\Entity\MovimientoCaja;
use AppBundle\Entity\MovimientoBancario;
use AppBundle\Form\PagoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Pago controller.
 *
 */
class PagoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Pago entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $desde = $request->get('desde');
        if (empty($desde)) {
            $desde = new \DateTime('NOW -30 days');
        } else {
            $desde = new \DateTime($request->get('desde'));
        }
        $hasta = $request->get('hasta');
        if (empty($hasta)) {
            $hasta = new \DateTime('NOW +1 days');
        } else {
            $hasta = new \DateTime($request->get('hasta'));
        }
        $descripcion = $request->get('descripcion');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Pago')->filtro($desde, $hasta, $descripcion, $unidadNegocio);
                
        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Pago:index.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /**
     * Creates a new Pago entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Pago();
        $form = $this->createCreateForm($entity);
        $form
                ->add('facturas', 'entity', array(
                    'class' => 'AppBundle:Factura',
                    'label' => 'Facturas',
                    'mapped' => false,
                    'multiple' => true
        ));

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $facturasAsociadas = $form['facturas']->getData();

//        $proveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($request->get('clienteProveedor'));
        $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
//        $entity->setClienteProveedor($proveedor);

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
            $entity->setUnidadnegocio($unidad);
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $request->get('unidad'), 'estado' => 'A'));
        } else {
            $entity->setUnidadnegocio($this->getUser()->getUnidadNegocio());
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $this->getUser()->getUnidadNegocio(), 'estado' => 'A'));
        }

        $importemedios = 0;
        foreach ($medios as $medio) {
            $importemedio = $request->get('importemedio' . $medio->getId());
            $numerocupon = $request->get('numerocupon' . $medio->getId());
            $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
            $cuotas = $request->get('cuotas' . $medio->getId());
            if (!empty($importemedio)) {
                $mediodocumento = new MedioDocumento();
                $mediodocumento->setMedio($medio);
                $mediodocumento->setPago($entity);
                $mediodocumento->setImporte($importemedio);
                $mediodocumento->setNumerolote((int) $numerotransaccion);
                $mediodocumento->setNumerocupon((int) $numerocupon);
                $mediodocumento->setCuotas((int) $cuotas);

                $em->persist($mediodocumento);
                $importemedios += $importemedio;
                if ($medio->getDescripcion() == 'Efectivo') {
                    $medioEfectivo = $mediodocumento;
                }
            }
        }

        $importeRetenciones = 0;

        $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        for ($i = 1; $i <= count($tiporetenciones); $i++) {
            if ($request->get('retencionimporte' . $i) != '' and $request->get('retencionimporte' . $i) > 0) {
                $retencion = new Retencion();
                $tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->findOneBy(array('descripcion' => $request->get('retencion' . $i)));
                $retencion->setImporte($request->get('retencionimporte' . $i));
                $retencion->setTiporetencion($tiporetencion);
                $retencion->setPago($entity);
                $em->persist($retencion);
                $importeRetenciones += $request->get('retencionimporte' . $i);
            }
        }
        $importe = $importeRetenciones + $importemedios;
        $entity->setImporte($importe);

        $chequesId = $request->get('selectcheques');

        if ($form->isValid()) {
            $em->persist($entity);
            if (!isset($caja)) {
                $caja = $this->crearCaja($entity->getUnidadNegocio());
            }

            //Se agrega el movimiento de Caja
            if (isset($medioEfectivo)) {
                //Siempre debe existir una caja activa
                $movimientoCaja = new MovimientoCaja();
                $movimientoCaja->setImporte($medioEfectivo->getImporte() * (-1));
                $movimientoCaja->setCaja($caja);
                $movimientoCaja->setPago($entity);
                $em->persist($movimientoCaja);
            }
            if (is_array($chequesId)) {
                foreach ($chequesId as $chequeId) {
                    $cheque = $em->getRepository('AppBundle:Cheque')->find($chequeId);
                    $cheque->setEstado('U');
                    $cheque->setPago($entity);
                    $em->persist($cheque);
                    $em->flush();
                    $importe = $importe + $cheque->getImporte();
                    $cuentaOrigen = $cheque->getCuentaorigen();
                    if (isset($cuentaOrigen)) {
                        $movimiento = new MovimientoBancario();
                        $movimiento->setCheque($cheque);
                        $movimiento->setCuentabancaria($cheque->getCuentaorigen());
                        $movimiento->setTipoMovimiento('CH');
                        $movimiento->setValor($cheque->getImporte());
                        $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
                        $movimiento->setEstado('U');
                        $em->persist($movimiento);
                        $em->flush();
                    }
                }
            }
            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setPago($entity);
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setImporte($importe);
            $em->persist($pagoAsignacion);
            $em->flush();
            $entity->setNrocomprobante($entity->getId());

            $entity->setImporte($importe);
            $em->persist($entity);
            $em->flush();

            if (count($facturasAsociadas) > 0) {
                foreach ($facturasAsociadas as $facturasAsociada) {
                    $pagofactura = new PagoFactura();
                    $pagofactura->setPago($entity);
                    $pagofactura->setImporte($importe);
                    $pagofactura->setFactura($facturasAsociada);

                    $em->persist($pagofactura);
                }
                $em->flush();
            }

            return $this->redirect($this->generateUrl('pago_show', array('id' => $entity->getId())));
        }

        $cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('clienteProveedor' => 2));
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
        $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $cheques = $em->getRepository('AppBundle:Cheque')->findBy(array('estado' => 'A'), array('fechacobro' => 'ASC'));

        return $this->render('AppBundle:Pago:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'medios' => $medios,
                    'tiporetenciones' => $tiporetenciones,
                    'cheques' => $cheques,
                    'form' => $form->createView(),
                    'clientesProveedores' => $cliProvs)
        );
    }

    /**
     * Creates a form to create a Pago entity.
     *
     * @param Pago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pago $entity) {
        $form = $this->createForm(new PagoType(), $entity, array(
            'action' => $this->generateUrl('pago_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Pago entity.
     *
     */
    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Pago();
        $entity->setFecha(new \DateTime("NOW"));

        $cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('clienteProveedor' => 2));
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
        $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $cheques = $em->getRepository('AppBundle:Cheque')->findBy(array('estado' => 'A'), array('fechacobro' => 'ASC'));

        $idfacturas = $request->get('idfacturas');
        $totalFacturas = 0;
        $arrFacturas = array();
        if (!empty($idfacturas)) {
            foreach ($idfacturas as $idfactura) {
                $facturas = $em->getRepository('AppBundle:Factura')->find($idfactura);
                if ($facturas) {
                    $arrFacturas[] = $facturas;
                    $entity->setClienteProveedor($facturas->getClienteProveedor());
                    $totalFacturas += $facturas->getImporte();
                }
            }
            $entity->setClienteProveedor($facturas->getClienteProveedor());
            $entity->setFecha(new \DateTime("NOW"));
        }
        $form = $this->createCreateForm($entity);
        if (count($arrFacturas) > 0) {
            $form
                    ->add('facturas', 'entity', array(
                        'class' => 'AppBundle:Factura',
                        'label' => 'Facturas',
                        'mapped' => false,
                        'data' => $arrFacturas,
                        'multiple' => true,
                        'query_builder' => function (\AppBundle\Entity\FacturaRepository $repository) use ($idfacturas) {
                            return $repository->createQueryBuilder('c')
                                    ->where('c.id IN (?1)')
                                    ->setParameter(1, $idfacturas);
                        },
                        'choice_label' => function ($factura) {
                            return strip_tags($factura);
                        }
            ));
        } else {
            $form
                    ->add('facturas', 'choice', array(
                        'choices' => array(),
                        'mapped' => false,
                        'required' => false)
            );
        }


        return $this->render('AppBundle:Pago:new.html.twig', array(
                    'entity' => $entity,
                    'medios' => $medios,
                    'unidades' => $unidades,
                    'tiporetenciones' => $tiporetenciones,
                    'cheques' => $cheques,
                    'form' => $form->createView(),
                    'facturas' => $arrFacturas,
                    'totalFacturas' => $totalFacturas,
                    'clientesProveedores' => $cliProvs,
                    'origen' => 'pago')
        );
    }

    /**
     * Finds and displays a Pago entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Pago:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pago entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Pago:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Pago entity.
     *
     * @param Pago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pago $entity) {
        $form = $this->createForm(new PagoType(), $entity, array(
            'action' => $this->generateUrl('pago_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Edits an existing Pago entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pago_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Pago:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Pago entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Pago')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pago entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pago'));
    }

    /**
     * Creates a form to delete a Pago entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pago_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $medio = $request->get('medio');
        $proveedor = $request->get('proveedor');
        $medioEntity = null;
        if (isset($medio)) {
            $medioEntity = $em->getRepository('AppBundle:Medio')->find($medio);
        }

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entidades = $em->getRepository('AppBundle:Pago')->filtroBusquedaInformePagos($fechaDesde, $fechaHasta, $proveedor, $medio, $unidadNegocio);
        $entities = [];
        if ($medio == 'cheque') {
            foreach ($entidades as $entidad) {
                if (count($entidad->getCheques()) > 0) {
                    $entities[] = $entidad;
                }
            }
            $total = $em->getRepository('AppBundle:Cheque')->totalInformePagos($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);
        } else if ($medio != '') {
            $total = $em->getRepository('AppBundle:MedioDocumento')->totalInformePagos($fechaDesde, $fechaHasta, $medio, $proveedor, $unidadNegocio);
            $entities = $entidades;
        } else {
            $total = $em->getRepository('AppBundle:Pago')->totalInformePagos($fechaDesde, $fechaHasta, $medio, $proveedor, $unidadNegocio);
            $entities = $entidades;
        }
        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities, $total, $medio);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Pago');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Pago:informe.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total,
                    'medioEntity' => $medioEntity
        ));
    }

    public function getUltimoPagoAction(Request $request) {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $pago = $em->getRepository('AppBundle:Pago')->getUltimoPago($id);

        $respuesta['fecha'] = '-';
        $respuesta['importe'] = '-';
        $respuesta['unidadNegocio'] = '-';
        if (isset($id)) {
            $saldo = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
            $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
            $respuesta['saldo'] = round($clienteProveedor->getSaldo() + $saldo['cobranzas'] + $saldo['notacredito'] - $saldo['notadebito'] - $saldo['facturas'] + $saldo['facturasproveedor'] - $saldo['gastos'] - $saldo['pagos'], 2);
        }

        if (isset($pago) && !empty($pago)) {
            $respuesta['fecha'] = date_format($pago[0][0]->getFecha(), 'Y-m-d');
            $respuesta['importe'] = $pago[0][0]->getImporte();
            $respuesta['unidadNegocio'] = $pago[0][0]->getUnidadNegocio()->getDescripcion();
        }

        return new JsonResponse($respuesta);
    }

    private function crearCaja($unidadNegocio) {
        $em = $this->getDoctrine()->getManager();
        $caja = new Caja();
        $caja->setUnidadNegocio($unidadNegocio);
        $em->persist($caja);
        $em->flush();
        return $caja;
    }

    /*
     * Generación de contenido archivo excel Informe de Ingreso -Egreso
     */

    private function excelInforme($entities, $total, $medioSelect) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Proveedor');
        $sheet->setCellValue('D1', 'Medios');
        $sheet->setCellValue('E1', 'Cheques');
        $sheet->setCellValue('F1', 'Unidad Negocio');
        $sheet->setCellValue('G1', 'Importe total del Pago');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity);
            $sheet->setCellValue('C' . $fila, $entity->getClienteProveedor());

            if ($medioSelect != 'cheque' && sizeof($entity->getMedios()) > 0) {
                foreach ($entity->getMedios() as $medio) {
                    $sheet->setCellValue('D' . $fila, $medio->getMedio()->getDescripcion() . ' - ' . $medio->getImporte());
                }
            }
            $ch = '';
            if (($medioSelect == 'cheque' || $medioSelect == '') && $entity->getCheques() !== NULL) {
                foreach ($entity->getCheques() as $cheque) {
                    if ($cheque->getEstado() != 'E') {
                        if ($ch == '') {
                            $ch = $ch . $cheque . ' - ' . $cheque->getImporte();
                        } else {
                            $ch = $ch . ' / ' . $cheque . ' - ' . $cheque->getImporte();
                        }
                    }
                }
                $sheet->setCellValue('E' . $fila, $ch);
            }
            $sheet->setCellValue('F' . $fila, $entity->getUnidadNegocio());
            $sheet->setCellValue('G' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('F' . $fila, 'Total');
        $sheet->setCellValue('G' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('F' . $fila . ':G' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);


        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }

}
