<?php

namespace AppBundle\Controller;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Reserva;
use AppBundle\Form\ReservaType;

/**
 * Reserva controller.
 *
 */
class ReservaController extends Controller
{

    /**
     * Lists all Reserva entities.
     *
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $estado = $request->get('estado');
        $filter = array(
            'desde' => $request->get('desde'),
            'hasta' => $request->get('hasta'),
            'cliente' => $request->get('cliente'),
            'estado' => (isset($estado) ? $request->get('estado') : 'A'),
        );

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Reserva')->findByFilter($filter);

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcel($entities);
            return $this->container->get('session.manager')->exportarExcel($spreadSheet, 'Reservas-'.date('Ymd'));
        }

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Reserva:index.html.twig', array(
            'entities' => $paginador,
        ));
    }

    private function exportarExcel($entities) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Producto');
        $sheet->setCellValue('B1', 'Cantidad');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Usuario');
        $sheet->setCellValue('E1', 'Estado');
        $sheet->setCellValue('F1', 'Fecha');

        $fila = 2;
        foreach ($entities as $entity) {
            $producto_nombre = $entity->getStock()->getProducto()->getTrazable()
                ?
                $entity->getStock()->getProducto()->getNombre() . ' - N° Serie: ' . $entity->getStock()->getSerie() . ' - LOTE: ' . $entity->getStock()->getLote()
                :
                $entity->getStock()->getProducto() . " - LOTE: " . $entity->getStock()->getLote();
            ;
            $sheet->setCellValue('A' . $fila, $producto_nombre);
            $sheet->setCellValue('B' . $fila, $entity->getCantidad());
            $sheet->setCellValue('C' . $fila, $entity->getCliente()->getRazonSocial());
            $sheet->setCellValue('D' . $fila, $entity->getUsuario());
            $sheet->setCellValue('E' . $fila, $entity->getEstado() == 'A' ? 'ACTIVA' : 'FINALIZADA');
            $sheet->setCellValue('F' . $fila, $entity->getCreated() ? $entity->getCreated()->format("d/m/Y") : '');

            $fila++;
        }

        return $spreadsheet;
    }

    /**
     * Creates a new Reserva entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reserva();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reserva_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Reserva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Reserva entity.
     *
     * @param Reserva $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Reserva $entity)
    {
        $form = $this->createForm(new ReservaType(), $entity, array(
            'action' => $this->generateUrl('reserva_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Reserva entity.
     *
     */
    public function newAction(Request $request)
    {
        $stock_id = $request->get('stock_id');
        if(!$stock_id) return $this->redirectToRoute('reserva');
        $stock = $this->getDoctrine()->getManager()->getRepository('AppBundle:Stock')->find($stock_id);

        $entity = new Reserva();
        $entity->setStock($stock);
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Reserva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reserva entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Reserva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reserva entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Reserva:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reserva entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Reserva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reserva entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Reserva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Reserva entity.
    *
    * @param Reserva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reserva $entity)
    {
        $form = $this->createForm(new ReservaType(), $entity, array(
            'action' => $this->generateUrl('reserva_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Reserva entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Reserva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reserva entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('reserva_show', array('id' => $id)));
        }

        return $this->render('AppBundle:Reserva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reserva entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Reserva')->find($id);
        $entity->setEstado('F');
        $em->flush();

        return $this->redirect($this->generateUrl('reserva'));
    }

    /**
     * Creates a form to delete a Reserva entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reserva_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
