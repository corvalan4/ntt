<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ps\PdfBundle\Annotation\Pdf;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Factura;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Iva;
use AppBundle\Entity\Stock;
use AppBundle\Form\FacturaType;
use AppBundle\Form\FacturaProveedorType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Factura controller.
 *
 */
class FacturaProveedorController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @var BalanceService
     * @DI\Inject("balanceservice")
     */
    public $balanceService;

    /**
     * Lists all FacturaProveedor entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $proveedor = $request->get('proveedor');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Factura')->filtroBusquedaCompras($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:FacturaProveedor:index.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /**
     * Creates a new FacturaProveedorentity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $entity->setTipo("FP");
        $mes = $form->get("mes")->getData();
        $anio = $form->get("anio")->getData();
        $entity->setMesanio($mes . "/" . $anio);

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        } else {
            $this->sessionManager->addFlash("msgError", "Se debe seleccionar un Proveedor.");
            return $this->render('AppBundle:FacturaProveedor:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        }

        $importetotal = (float) $request->get("importetotal");
        $entity->setImporte($importetotal);

        $total21 = 0;
        //$total105 = 0;

        if ($form->isValid()) {
            $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            //$tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            //$importeiva105 = $request->get("importeiva105");
            if (!empty($importeiva21) and $importeiva21 > 0) {
                $iva = new Iva();
                if ($tipoIva21) {
                    $iva->setTipoIva($tipoIva21);
                }
                $iva->setFactura($entity);
                $iva->setValor(round($importeiva21, 2));
                $iva->setImporteFactura(round($total21, 2));
                $em->persist($iva);
            }
            /* if (!empty($importeiva105) and $importeiva105 > 0) {
              $iva = new Iva();
              if ($importeiva105) {
              $iva->setTipoIva($tipoIva105);
              }
              $iva->setFactura($entity);
              $iva->setValor(round($importeiva105, 2));
              $iva->setImporteFactura(round($total105, 2));
              $em->persist($iva);
              $entity->addIva($iva);
              } */
            $puntoVta = $request->get("puntoventa");
            if (!$puntoVta) {
                $puntoVta = 0;
            }

            $entity->setPtovta($puntoVta);
            $entity->setNrofactura($request->get("nrofactura"));

            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setFactura($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($entity->getImporte());
            $em->persist($pagoAsignacion);

            //Alta de stocks de productos
            $lote = $request->get("lote");
            $origen = $request->get("origen");
            $procedencia = $request->get("procedencia");
            $deposito = $request->get("deposito");

            for ($i = 1; $i <= 30; $i++) {
                $idProducto = $request->get("idelemento" . $i);
                $producto = $em->getRepository('AppBundle:Producto')->find($idProducto);
                if ($idProducto != '') {
                    $cantidad = $request->get("cantidad" . $i);
                    $precio = $request->get("precioReal" . $i);
                    $stock = new Stock();
                    $stock->setLote($lote);
                    $stock->setOrigen($origen);
                    $stock->setProcedencia($procedencia);
                    $stock->setDeposito($deposito);
                    $stock->setProducto($producto);
                    $stock->setUnidadnegocio($entity->getUnidadNegocio());
                    $stock->setRealizo($this->getUser());
                    $stock->setCosto($precio);
                    $stock->setStock($cantidad);
                    $em->persist($stock);

                    $balance = $this->sessionManager->newBalance($stock, $this->getUser(), $precio, $cantidad);
                    $balance->setFactura($entity);
                    $em->persist($balance);
                }
            }

            $em->persist($entity);

            $em->flush();
            $this->sessionManager->addFlash("msgOk", "Factura Proveedor creada exitosamente.");

            /* if ($form->get('submitAndFinish')->isClicked()) {
              return $this->redirect($this->generateUrl('facturaproveedor_edit', ['id' => $entity->getId()]));
              } */

            return $this->redirect($this->generateUrl('facturaproveedor'));
        }

        $this->sessionManager->addFlash("msgError", "Error al dar de alta la factura.");
        return $this->render('AppBundle:FacturaProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function refacturarAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        $this->sessionManager->facturar($entity);

        return $this->redirect($this->generateUrl('facturaproveedor'));
    }

    /**
     * Creates a form to create a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Factura $entity) {

        $form = $this->createForm(new FacturaProveedorType(), $entity, array(
            'action' => $this->generateUrl('facturaproveedor_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));
        // ->add('submitAndFinish', 'submit', array('label' => 'Guardar y Completar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right; margin-right: 1em;')));

        return $form;
    }

    /**
     * Displays a form to create a new Factura entity.
     *
     */
    public function newAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }

        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:FacturaProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function remitofacturaAction($id) {


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {
            return $this->render('AppBundle:FacturaProveedor:remitofacturaA.html.twig', array(
                        'entity' => $entity,
            ));
        } else {
            return $this->render('AppBundle:FacturaProveedor:remitofacturaB.html.twig', array(
                        'entity' => $entity,
            ));
        }
    }

    public function facturaremitoAction($id) {


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        return $this->render('AppBundle:FacturaProveedor:facturaremito.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function crearfacturaremitoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($request->get('id'));
        $entity->setNroremito($request->get('nroremito'));
        $contador = count($entity->getProductosFactura());
        for ($x = 1; $x <= $contador; $x++) {
            $id = $request->get('id' . $x);
            $this->sessionManager->setSession('id' . $id, $id);
            if (!is_null($id) and $id != '') {
                $productoFactura = $em->getRepository('AppBundle:ProductoFactura')->find($id);
                $productoFactura->setPertenece('RF');
            }
        }
        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Remito registrado.');

        return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId(), 'tipo' => 'R')));
    }

    public function showAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        return $this->render('AppBundle:FacturaProveedor:show.html.twig', array(
                    'entity' => $entity,
                    'nrocomprobante' => '01'
        ));
    }

    public function imprimirAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }
        return $this->render('AppBundle:FacturaProveedor:imprimir.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        if ($entity->getMesanio() == '/') {
            $entity->setMesanio($entity->getFecha()->format('m/Y'));
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        return $this->render('AppBundle:FacturaProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Factura $entity) {

        $form = $this->createForm(new FacturaProveedorType(), $entity, array(
            'action' => $this->generateUrl('facturaproveedor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Edits an existing Factura entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $importetotal = $request->get("importetotal");
        $entity->setImporte($importetotal);

        if ($editForm->isValid()) {
            /* $bandera = 0;
              for ($i = 1; $i <= 300; $i++) {
              $idelemento = $request->get("idelemento$i");
              if (!empty($idelemento)) {
              $bandera = 1;
              $diseno = $em->getRepository('AppBundle:Diseno')->find($idelemento);
              if ($diseno) {
              //Revisar todo esto
              $cantidad = $request->get("cantidad$i");
              $precio = $request->get("precioReal$i");

              $elementostock = new Stock();
              $elementostock->setFecha(new \DateTime('NOW'));
              $elementostock->setCosto($precio);
              $elementostock->setProducto($diseno->getProducto());
              $elementostock->setDiseno($diseno);
              $elementostock->setStock($cantidad);
              $elementostock->setUnidadnegocio($entity->getUnidadnegocio());

              $em->persist($elementostock);
              $balance = $this->sessionManager->newBalance($elementostock, $this->getUser(), $elementostock->getCosto(), $elementostock->getStock());
              $em->persist($balance);

              $productofactura = new ProductoFactura();
              $productofactura->setStock($elementostock);
              $productofactura->setFactura($entity);
              $productofactura->setCantidad($cantidad);
              $productofactura->setPrecio($precio);
              $elementostock->setStock($cantidad);
              $em->persist($productofactura);
              }
              }
              }
             */
            $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            $tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            $importeiva105 = $request->get("importeiva105");

            if (!empty($importeiva21) and $importeiva21 > 0) {
                $iva21 = $em->getRepository('AppBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva21, 'factura' => $entity));
                if ($iva21) {
                    $iva21->setValor(round($importeiva21, 2));
                } else {
                    $iva21 = new Iva();
                    if ($tipoIva21) {
                        $iva21->setTipoIva($tipoIva21);
                    }
                    $iva21->setFactura($entity);
                    $iva21->setValor(round($importeiva21, 2));
                    $em->persist($iva21);
                    $entity->addIva($iva21);
                }
            }
            if (!empty($importeiva105) and $importeiva105 > 0) {
                $iva105 = $em->getRepository('AppBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva105, 'factura' => $entity));
                if ($iva105) {
                    $iva105->setValor(round($importeiva21, 2));
                } else {
                    $iva105 = new Iva();
                    if ($importeiva105) {
                        $iva105->setTipoIva($tipoIva105);
                    }
                    $iva105->setFactura($entity);
                    $iva105->setValor(round($importeiva105, 2));
                    $em->persist($iva105);
                    $entity->addIva($iva105);
                }
            }

            $em->persist($entity);

            $em->flush();
            /* if ($bandera == 1) {
              $this->sessionManager->addFlash("msgOk", "Factura Completa.");
              } else { */
            $this->sessionManager->addFlash("msgOk", "Factura editada.");
            //}
            return $this->redirect($this->generateUrl('facturaproveedor'));
        }

        $this->sessionManager->addFlash("msgError", "Error al editar la factura.");

        return $this->render('AppBundle:FacturaProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     */
    public function deleteAction($id) {
        //Ver esto
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if ($this->balanceService->verifyMovimientos($id) === false) {
            $this->sessionManager->addFlash("msgError", "No es posible eliminar esta factura. Ya se han realizado acciones sobre el stock asociado.");
        }

        $balances = $em->getRepository('AppBundle:Balance')->findBy(['factura' => $id]);

        foreach ($balances as $balance) {
            $em->remove($balance);
            $em->remove($balance->getStock());
        }

        $iva = $em->getRepository('AppBundle:Iva')->findOneBy(['factura' => $id]);
        $movimiento = $em->getRepository('AppBundle:PagoAsignacion')->findOneBy(['factura' => $id]);
        $em->remove($iva);
        $em->remove($movimiento);
        $em->remove($entity);

        $em->flush();

        $this->sessionManager->addFlash("msgOk", "Factura de proveedor eliminada con éxito.");

        return $this->redirect($this->generateUrl('facturaproveedor'));
    }

    public function verclienteAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $string = '';
        $cliente = $em->getRepository('AppBundle:ClienteProveedor')->findOneBy(array('cuit' => $search));
        if (empty($cliente)) {
            $cliente = $em->getRepository('AppBundle:ClienteProveedor')->findOneBy(array('dni' => $search));
        }
        if (empty($cliente)) {
            $response = '0///0';
        } else {
            $response = $cliente->getId() . '///' . $cliente->getRazonSocial();
        }
        return new Response($response);
    }

    public function altaclienteAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $condicion = $em->getRepository('AppBundle:CondicionIva')->find(1);
        $tipo = $request->get('data');
        $nrodoc = $request->get('data1');
        $direc = $request->get('data2');
        $razon = $request->get('data3');
        $telefono = $request->get('data4');
        $email = $request->get('data5');

        $respuesta = '0';

        $cliente = new ClienteProveedor();
        $cliente->setRazonSocial($razon);
        if ($tipo == 'CUIT') {
            $cliente->setCuit(str_replace('-', '', $nrodoc));
        } else {
            $cliente->setDni($nrodoc);
        }
        $cliente->setCondicioniva($condicion);
        $cliente->setDomiciliocomercial($direc);
        $cliente->setTelefono($telefono);
        $cliente->setMail($email);

        $em->persist($cliente);
        $em->flush();

        $respuesta = $nrodoc;

        return new Response($respuesta);
    }

    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $proveedor = $request->get('proveedor');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Factura')->filtroBusquedaInformeCompra($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);
        $total = $em->getRepository('AppBundle:Factura')->totalInformeCompra($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);

        //Verifico si se presiono el botón exportar excel

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities, $total);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Compra');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:FacturaProveedor:informe.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total
        ));
    }

    public function informeCuentasPorPagarAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $proveedor = $request->get('proveedor');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $facturas = $em->getRepository('AppBundle:Factura')->getUltimaFactura('FP', $proveedor, $unidadNegocio);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelCuentasPorPagar($facturas);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Cuentas por Pagar - Proveedores');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($facturas));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:FacturaProveedor:informeCuentasPorPagar.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Muestra formulario para la generación del iva Compras
     */

    public function ivaComprasAction(Request $request) {
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio()->getId();
        }
        if (isset($fechaDesde) && isset($fechaHasta) && isset($unidadNegocio)) {
            return $this->ivaComprasExcel($fechaDesde, $fechaHasta, $unidadNegocio);
        }

        return $this->render('AppBundle:FacturaProveedor:ivaCompras.html.twig');
    }

    /*
     * Generación de archivo Excel con informe de IVA Ventas
     */

    public function ivaComprasExcel($fechaDesde, $fechaHasta, $unidadNegocio) {

        $em = $this->getDoctrine()->getManager();

        $ivas = $em->getRepository('AppBundle:Iva')->ivaCompras($fechaDesde, $fechaHasta, $unidadNegocio);

        if ($ivas) {
            // ask the service for a Excel5
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            $phpExcelObject->getProperties()->setCreator($this->container->getParameter('empresa_razonsocial'))
                    ->setLastModifiedBy($this->container->getParameter('empresa_razonsocial'))
                    ->setTitle("Informe de IVA Compras")
                    ->setSubject($this->container->getParameter('empresa_razonsocial'));

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Fecha')
                    ->setCellValue('B1', 'Punto de Venta')
                    ->setCellValue('C1', 'Nro. Factura')
                    ->setCellValue('D1', 'Razón Social')
                    ->setCellValue('E1', 'CUIT')
                    ->setCellValue('F1', 'Importe')
                    ->setCellValue('G1', 'IVA')
                    ->setCellValue('H1', 'Porcentaje de IVA');

            $count = 2;
            foreach ($ivas as $iva) {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $iva->getFactura()->getFecha()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $iva->getFactura()->getPtovta())
                        ->setCellValue('C' . $count, $iva->getFactura()->getNrofactura())
                        ->setCellValue('D' . $count, $iva->getFactura()->getClienteProveedor()->getRazonSocial())
                        ->setCellValue('E' . $count, $iva->getFactura()->getClienteProveedor()->getCuit())
                        ->setCellValue('F' . $count, $iva->getFactura()->getImporte())
                        ->setCellValue('G' . $count, $iva->getValor())
                        ->setCellValue('H' . $count, $iva->getTipoIva()->getPorcentaje());
                $count++;
            }
            $phpExcelObject->setActiveSheetIndex(0);
            $phpExcelObject->getActiveSheet()->setTitle('Hoja1');
            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(20);

            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=ivaVentas.xlsx');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }

        $this->sessionManager->addFlash('msgError', 'No existen registros para mostrar.');
        return $this->render('AppBundle:FacturaProveedor:ivaCompras.html.twig');
    }

    /*
     * Muestra formulario para la generación del Citi Compras
     */

    public function citiComprasAction(Request $request) {
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $tipoTxt = $request->get('tipoTxt');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio()->getId();
        }
        if (isset($fechaDesde) && isset($fechaHasta) && isset($unidadNegocio)) {
            return $this->citicomprasTxt($fechaDesde, $fechaHasta, $tipoTxt, $unidadNegocio);
        }

        return $this->render('AppBundle:FacturaProveedor:citiCompras.html.twig');
    }

    /*
     * Generación de archivo txt de Citi Compras
     */

    public function citicomprasTxt($fechaDesde, $fechaHasta, $tipoTxt, $unidadNegocio) {

        if (file_exists($tipoTxt . 'Compras.txt')) {
            unlink($tipoTxt . 'Compras.txt');
            clearstatcache();
        }
        $em = $this->getDoctrine()->getManager();
        $facturas = $em->getRepository('AppBundle:Factura')->citicompras($fechaDesde, $fechaHasta, $unidadNegocio);

        /*
         * Agrego las Facturas
         */
        foreach ($facturas as $entity) {
            $tipocmb = $this->sessionManager->getTipoComprobante('FACTURAS ' . $entity->getTipofactura());
            if ($entity->getClienteProveedor()->getCuit()) {
                $g = $this->sessionManager->getTipoDocumento('CUIT');
                $h = str_replace('-', '', $entity->getClienteProveedor()->getCuit());
            } else {
                $g = $this->sessionManager->getTipoDocumento('DNI');
                $h = str_replace(' ', '', $entity->getClienteProveedor()->getDni());
            }

            $proveedor = substr($this->sessionManager->trim_all($entity->getClienteProveedor()->getRazonSocial()), 0, 30);

            //Por la codificación de la ñ por ejemplo
            $largoA = strlen($proveedor); //Retorna cantidad de Bytes
            $largoB = mb_strlen($proveedor); //Retorna cantidad de caracteres 

            if ($largoA > $largoB) {
                $diferencia = $largoA - $largoB;
                $proveedor = str_pad($proveedor, 30 + $diferencia, " ");
            } else {
                $proveedor = str_pad($proveedor, 30, " ");
            }

            if ($tipoTxt == 'Comprobantes') {
                file_put_contents($tipoTxt . 'Compras.txt',
                        $entity->getFecha()->format('Ymd') . //Fecha de Comprobante 8 Numérico
                        str_pad($tipocmb, 3, "0", STR_PAD_LEFT) . //Tipo de Comprobante 3 Numérico
                        str_pad($entity->getPtovta(), 5, "0", STR_PAD_LEFT) . //Punto de Venta 5 Numérico
                        str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) . //Número de Comprobante 20 Numérico
                        str_pad('', 16) . // N° de despacho de importación 16 Alfanumérico  
                        str_pad($g, 2, "0", STR_PAD_LEFT) . //Código de documento del vendedor 2 Numérico
                        str_pad($h, 20, "0", STR_PAD_LEFT) . //Número de identificacion del vendedor 20 Alfanumérico
                        $proveedor . //Apellido y nombre del vendedor 30 Alfanumérico
                        str_pad($entity->getImporte() * 100, 15, "0", STR_PAD_LEFT) . //Importe total de la operación 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe total de conceptos que no integran el precio neto gravado 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe operaciones exentas 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de percepciones o pagos a cuenta del Impuesto al Valor Agregado 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de percecpiones o pagos a cuenta de otros impuestos nacionales 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de perecepciones de ingresos brutos 15 Numérico.
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de percepciones Impuestos Municipales 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe impuestos internos 15 Numérico.
                        str_pad('PES', 3, "0", STR_PAD_LEFT) . //Código de Moneda 3 Alfanumérico.
                        str_pad('0001', 10, "0", STR_PAD_RIGHT) . //Tipo de Cambio 10 Numérico.
                        str_pad(strval(count($entity->getIvas())), 1, "0", STR_PAD_LEFT) . //Cantidad de alicuotas del IVA 1 Numérico.
                        str_pad('0', 1, "0", STR_PAD_LEFT) . //Código de operación 1 Alfanumérico.
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Crédito Fiscal Computable 15 Numérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Otros Tributos 15 Numérico
                        str_pad('0', 11, "0", STR_PAD_LEFT) . //Cuit emisor/corredor 11 Numérico
                        str_pad('', 30) . //Denominación del emisor/corredor 30 Alfanumérico
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Iva Comisión 15 Numérico                                                
                        PHP_EOL
                        , FILE_APPEND);
            }
            if ($tipoTxt == 'Alicuotas') {
                foreach ($entity->getIvas() as $iva) {
                    $ivaCodigo = $this->sessionManager->getCodigoAlicuota($iva->getTipoIva()->getPorcentaje());
                    file_put_contents($tipoTxt . 'Compras.txt',
                            str_pad($tipocmb, 3, "0", STR_PAD_LEFT) . //Tipo de Comprobante 3 Numérico
                            str_pad($entity->getPtovta(), 5, "0", STR_PAD_LEFT) . //Punto de Venta 5 Numérico
                            str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) . //Nro de Comprobante 20 Numérico
                            str_pad($g, 2, "0", STR_PAD_LEFT) . // Código de documento del vendedor 2 Numérico     
                            str_pad($h, 20, "0", STR_PAD_LEFT) . //Número identificación del vendedor 20 Alfanúmerico
                            str_pad(str_replace('.', '', $iva->getImporteFactura() * 100), 15, "0", STR_PAD_LEFT) . //Importe neto gravado 15 Numérico
                            $ivaCodigo . //Alícuota del IVA 4 Numérico
                            str_pad(str_replace('.', '', $iva->getValor() * 100), 15, "0", STR_PAD_LEFT) . //Impuesto liquidado 15 Numérico                            
                            PHP_EOL
                            , FILE_APPEND);
                }
            }
        }
        if (file_exists($tipoTxt . 'Compras.txt')) {
            $response = new Response();
            $response->headers->set('Content-Description', 'File Transfer');
            $response->headers->set('Content-disposition', ' attachment;filename="' . $tipoTxt . 'Compras.txt"');
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Transfer-Encoding', 'binary');

            $response->headers->set('Content-Length', filesize($tipoTxt . 'Compras.txt'));

            $response->headers->set('Pragma', 'public');
            $response->headers->set('Expires', '0');
            $response->headers->set('Cache-Control', 'must-revalidate , post-check=0, pre-check=0');

            readfile($tipoTxt . 'Compras.txt');
            unlink($tipoTxt . 'Compras.txt');
            return $response;
        }
        $this->sessionManager->addFlash("msgError", "No existen registros para exportar.");
        return $this->redirectToRoute('facturaproveedor_citicompras');
    }

    /*
     *  Generación de contenido archivo excel Informe de Venta
     */

    private function excelInforme($entities, $total) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Proveedor');
        $sheet->setCellValue('D1', 'Unidad Negocio');
        $sheet->setCellValue('E1', 'Importe');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $nro = !empty($entity->getNrofactura()) ? $entity->getNrofactura() : $entity->getId();
            $facturaNro = "Factura" . $entity->getTipofactura() . " #" . str_pad($nro, 5, "0", STR_PAD_LEFT) . " - Pto." . $entity->getPtovta();
            $sheet->setCellValue('B' . $fila, $facturaNro);
            $sheet->setCellValue('C' . $fila, $entity->getClienteProveedor());
            $sheet->setCellValue('D' . $fila, $entity->getUnidadNegocio());
            $sheet->setCellValue('E' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('D' . $fila, 'Total');
        $sheet->setCellValue('E' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('D' . $fila . ':E' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     *  Generación de contenido archivo excel Informe de Cuentas por pagar
     */

    private function excelCuentasPorPagar($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Unidad Negocio');
        $sheet->setCellValue('B1', 'Razón Social');
        $sheet->setCellValue('C1', 'Fecha Última Compra ($)');
        $sheet->setCellValue('D1', 'Fecha Último Pago');
        $sheet->setCellValue('E1', 'Último Pago ($)');
        $sheet->setCellValue('F1', 'Saldo Actual ($)');

        $fila = 2;
        $total = 0;
        foreach ($entities as $entity) {
            $cobranza = $this->getUltimoPago($entity[0]->getClienteProveedor()->getId());
            $sheet->setCellValue('A' . $fila, $entity[0]->getUnidadNegocio());
            $sheet->setCellValue('B' . $fila, $entity[0]->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, date_format($entity[0]->getFecha(), "d-m-Y"));
            $sheet->setCellValue('D' . $fila, (new \DateTime($cobranza['fecha']))->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $cobranza['importe']);
            $sheet->setCellValue('F' . $fila, $cobranza['saldo']);
            $total = $total + $cobranza['saldo'];
            $fila++;
        }

        //Totales
        $sheet->setCellValue('E' . $fila, 'Total');
        $sheet->setCellValue('F' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Obtengo último pago
     */

    public function getUltimoPago($id) {

        $em = $this->getDoctrine()->getManager();
        $pago = $em->getRepository('AppBundle:Pago')->getUltimoPago($id);

        $respuesta['fecha'] = '';
        $respuesta['importe'] = 0;

        if (isset($id)) {
            $saldo = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
            $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
            $respuesta['saldo'] = round($clienteProveedor->getSaldo() + $saldo['cobranzas'] + $saldo['notacredito'] + $saldo['notadebito'] - $saldo['facturas'] + $saldo['facturasproveedor'] - $saldo['gastos'] - $saldo['pagos'], 2);
        }

        if (isset($pago) && !empty($pago)) {
            $respuesta['fecha'] = date_format($pago[0][0]->getFecha(), 'Y-m-d');
            $respuesta['importe'] = $pago[0][0]->getImporte();
        }

        return $respuesta;
    }

}

?>
