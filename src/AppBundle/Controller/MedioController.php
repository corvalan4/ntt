<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Medio;
use AppBundle\Form\MedioType;

/**
 * Medio controller.
 *
 */
class MedioController extends Controller
{

    /**
     * Lists all Medio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Medio')->findAll();

        return $this->render('AppBundle:Medio:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Medio entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Medio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('medio'));
        }

        return $this->render('AppBundle:Medio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Medio entity.
     *
     * @param Medio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Medio $entity)
    {
        $form = $this->createForm(new MedioType(), $entity, array(
            'action' => $this->generateUrl('medio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Medio entity.
     *
     */
    public function newAction()
    {
        $entity = new Medio();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Medio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Medio entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Medio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Medio:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Medio entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Medio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Medio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Medio entity.
     *
     * @param Medio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Medio $entity)
    {
        $form = $this->createForm(new MedioType(), $entity, array(
            'action' => $this->generateUrl('medio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Medio entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Medio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('medio'));
        }

        return $this->render('AppBundle:Medio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Medio entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Medio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Medio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('medio'));
    }

    /**
     * Creates a form to delete a Medio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('medio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
     /*
     * Render Select 
     */
    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $medios = $em->getRepository('AppBundle:Medio')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:Medio:select.html.twig', array(
                    'medios' => $medios
        ));
    }

}
