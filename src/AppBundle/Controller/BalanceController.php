<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Balance;
use AppBundle\Form\BalanceType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Balance controller.
 *
 */
class BalanceController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @var BalanceService
     * @DI\Inject("balanceservice")
     */
    public $balanceService;

    /**
     * @var MovimientoDepositoService
     * @DI\Inject("movimientodepositoservice")
     */
    public $movimientoDepositoService;

    /**
     * Lists all Balance entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Balance')->findAll();

        return $this->render('AppBundle:Balance:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function altaLoteMasivoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Balance::class)->findBy(['verifarma_status' => Balance::VERIFARMA_ALTA, 'type'=>Balance::TYPE_POSITIVE, 'amount'=>1], ['id'=>'DESC']);

        if(sizeof($entities)==0) return new JsonResponse(['success'=>false, 'message'=>'Sin registros.']);

        foreach ($entities as $entity){
            if(empty($entity->getStock()->getSerie())) continue;
            $status = Balance::VERIFARMA_ALTA;
            $namesheet = str_pad($entity->getId(), 7, "0", STR_PAD_LEFT);
            $this->sessionManager->csvVerifarmaAlta([$entity], "A".$namesheet);
        }

        return new JsonResponse(['success'=>true, 'message'=>'Alta Masiva Realizada.']);
    }

    /**
     * Creates a new Balance entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Balance();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('balance_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Balance:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Balance entity.
     *
     * @param Balance $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Balance $entity) {
        $form = $this->createForm(new BalanceType(), $entity, array(
            'action' => $this->generateUrl('balance_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Balance entity.
     *
     */
    public function newAction() {
        $entity = new Balance();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Balance:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Balance entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Balance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Balance entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Balance:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Balance entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Balance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Balance entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Balance:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Balance entity.
     *
     * @param Balance $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Balance $entity) {
        $form = $this->createForm(new BalanceType(), $entity, array(
            'action' => $this->generateUrl('balance_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Balance entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Balance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Balance entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('balance_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Balance:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Balance entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Balance')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Balance entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('balance'));
    }

    /**
     * Creates a form to delete a Balance entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('balance_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Lists all Balance entities.
     *
     */
    public function movimientosStockAction(Request $request, $id, $page = 1) {

        $unidadNegocio = $request->get('unidadNegocio');
        $categoria = $request->get('categoria');
        $subcategoria = $request->get('subcategoria');
        $codigo = $request->get('codigo');
        $nombre = $request->get('nombre');

        $em = $this->getDoctrine()->getManager();

        $diseno = $em->getRepository('AppBundle:Diseno')->find($id);
        if (!$diseno) {
            throw $this->createNotFoundException('Unable to find Diseno entity.');
        }
        $entities = $em->getRepository('AppBundle:Balance')->filtroBusqueda($id, $unidadNegocio, $categoria, $subcategoria, $codigo, $nombre);

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Balance:movimientosStock.html.twig', array(
                    'entities' => $paginador,
                    'diseno' => $diseno,
        ));
    }

    public function verifarmaInformeAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $balance = $em->getRepository('AppBundle:Balance')->find($id);

        $status = $request->get('status') ?? '#VF_ALTALOTE';
        $letter = substr($status, 4, 1);
        $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);

        $this->sessionManager->csvVerifarmaAlta([$balance], $letter . $namesheet);

        $this->addFlash('msgOk', 'Exportación realizada.');
        return $this->redirectToRoute('balance_verifarma', ['stock_id' => $balance->getStock()->getId()]);
    }

    public function verifarmaAction($stock_id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        if (!$stock_id)
            return $this->redirectToRoute('producto');
        $entities = $em->getRepository('AppBundle:Balance')->findBy(['stock' => $stock_id], ['id' => 'DESC']);
        $stock = $em->getRepository('AppBundle:Stock')->find($stock_id);

        return $this->render('AppBundle:Balance:verifarma.html.twig', array(
                    'entities' => $entities,
                    'stock' => $stock,
        ));
    }

    /*
     * Informe de Ventas
     */

    public function informeStockProductosAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }
        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta') . ' +1 day');
        }

        $despacho = $request->get('despacho');
        $producto = $request->get('producto');

        $unidadNegocio = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Balance')->filtroBusquedaInformeStockProductos($fechaDesde, $fechaHasta, $despacho, $producto, $unidadNegocio);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Stock de Productos');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Balance:informeStockProductos.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     *  Generación de contenido archivo excel Informe Stock de Productos
     */

    private function excelInforme($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha de creación');
        $sheet->setCellValue('B1', 'Producto');
        $sheet->setCellValue('C1', 'Cantidad');
        $sheet->setCellValue('D1', 'Lote');
        $sheet->setCellValue('E1', 'Despacho');
        $sheet->setCellValue('F1', 'Nro. Serie');
        $sheet->setCellValue('G1', 'Fecha Vencimiento');
        $sheet->setCellValue('H1', 'Depósito');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('I1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getCreated(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity->getStock()->getProducto());
            $sheet->setCellValue('C' . $fila, $entity->getAmount());
            $sheet->setCellValue('D' . $fila, $entity->getStock()->getLote());
            $sheet->setCellValue('E' . $fila, $entity->getStock()->getDespacho());
            $sheet->setCellValue('F' . $fila, $entity->getStock()->getSerie());
            $sheet->setCellValue('G' . $fila, date_format($entity->getStock()->getVencimiento(), "d/m/Y"));
            $sheet->setCellValue('H' . $fila, $entity->getStock()->getDeposito());
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('I' . $fila, $entity->getStock()->getUnidadnegocioF());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
        }


        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Informe de Importaciones
     */

    public function informeImportacionesAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();

        if (empty($request->get('fechaDesde'))) {
            $filtros['fechaDesde'] = new \DateTime('NOW -30 days');
        } else {
            $filtros['fechaDesde'] = new \DateTime($request->get('fechaDesde'));
        }
        if (empty($request->get('fechaHasta'))) {
            $filtros['fechaHasta'] = (new \DateTime('NOW +1 day'));
        } else {
            $filtros['fechaHasta'] = (new \DateTime($request->get('fechaHasta') . ' +1 day'));
        }
        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Balance')->filtroBusquedaInformeImportacion($filtros);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInformeImportacion($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Importaciones');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Balance:informeImportaciones.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     *  Generación de contenido archivo excel Informe Stock de Productos
     */

    private function excelInformeImportacion($entities) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Producto');
        $sheet->setCellValue('B1', 'Lote');
        $sheet->setCellValue('C1', 'Despacho');
        $sheet->setCellValue('D1', 'Cantidad');
        $sheet->setCellValue('E1', 'Código');
        $sheet->setCellValue('F1', 'GTIN');
        $sheet->setCellValue('G1', 'Rango Serie');
        $sheet->setCellValue('H1', 'Fecha de creación');
        $sheet->setCellValue('I1', 'Fecha Vencimiento');
        $sheet->setCellValue('J1', 'Proveedor');
        $sheet->setCellValue('K1', 'Modelo');
        $sheet->setCellValue('L1', 'Procedencia');
        $sheet->setCellValue('M1', 'Origen');
        $sheet->setCellValue('N1', 'Etiqueta');
        $sheet->setCellValue('O1', 'Carga');
        $sheet->setCellValue('P1', 'Higiene');
        $sheet->setCellValue('Q1', 'Estado');
        $sheet->setCellValue('R1', 'Realizó');
        $sheet->setCellValue('S1', 'Controló');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('T1', 'Unidad Negocio');
        }

        $fila = 2;

        foreach ($entities as $entity) {
            $stock = $this->balanceService->stockByProductoLoteDespacho($entity->getStock()->getProducto()->getId(), $entity->getStock()->getLote(), $entity->getStock()->getDespacho(), false);
            $ranogSerie = $this->balanceService->rangoNroSeriePorProductoLoteDespacho($entity->getStock()->getProducto()->getId(), $entity->getStock()->getLote(), $entity->getStock()->getDespacho(), false);
            $sheet->setCellValue('A' . $fila, $entity->getStock()->getProducto()->getNombre());
            $sheet->setCellValue('B' . $fila, $entity->getStock()->getLote());
            $sheet->setCellValue('C' . $fila, $entity->getStock()->getDespacho());

            $sheet->setCellValue('D' . $fila, $stock);
            $sheet->setCellValue('E' . $fila, $entity->getStock()->getProducto()->getCodigo());
            $sheet->setCellValue('F' . $fila, $entity->getStock()->getProducto()->getGtin());

            $sheet->setCellValue('G' . $fila, $ranogSerie);
            $sheet->setCellValue('H' . $fila, $entity->getCreated() ? $entity->getCreated()->format("d/m/Y") : '');
            $sheet->setCellValue('I' . $fila, $entity->getStock()->getVencimiento() ? $entity->getStock()->getVencimiento()->format("d/m/Y") : '');
            $sheet->setCellValue('J' . $fila, $entity->getStock()->getProducto()->getProveedor());
            $sheet->setCellValue('K' . $fila, $entity->getStock()->getProducto()->getModelo());
            $sheet->setCellValue('L' . $fila, $entity->getStock()->getProcedencia());
            $sheet->setCellValue('M' . $fila, $entity->getStock()->getOrigen());
            $sheet->setCellValue('N' . $fila, $entity->getStock()->getEtiqueta());
            $sheet->setCellValue('O' . $fila, $entity->getStock()->getCarga());
            $sheet->setCellValue('P' . $fila, $entity->getStock()->getHigiene());
            $sheet->setCellValue('Q' . $fila, $entity->getStock()->getEstado());
            $sheet->setCellValue('R' . $fila, $entity->getStock()->getRealizo());
            $sheet->setCellValue('S' . $fila, $entity->getStock()->getControlo());
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('T' . $fila, $entity->getStock()->getUnidadnegocio());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:S1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:T1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);
        $sheet->getColumnDimension('R')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Informe de Importaciones
     */

  /*  public function informeDevolucionesAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();

        if (empty($request->get('fechaDesde'))) {
            $filtros['fechaDesde'] = new \DateTime('NOW -30 days');
        } else {
            $filtros['fechaDesde'] = new \DateTime($request->get('fechaDesde'));
        }
        if (empty($request->get('fechaHasta'))) {
            $filtros['fechaHasta'] = (new \DateTime('NOW +1 day'));
        } else {
            $filtros['fechaHasta'] = (new \DateTime($request->get('fechaHasta') . ' +1 day'));
        }
        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Balance')->filtroBusquedaInformeDevoluciones($filtros);

        
         //Verifico si se presiono el botón exportar excel
        

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInformeDevoluciones($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Devoluciones');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Balance:informeDevoluciones.html.twig', array(
                    'entities' => $paginador
        ));
    }
   * */
   

    /*
     *  Generación de contenido archivo excel Informe Stock de Productos
     */

   /* private function excelInformeDevoluciones($entities) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha Devolución');
        $sheet->setCellValue('B1', 'Producto');
        $sheet->setCellValue('C1', 'GTIN');
        $sheet->setCellValue('D1', 'Cliente (Banco)');
        $sheet->setCellValue('E1', 'Motivo');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('F1', 'Unidad Negocio');
        }

        $fila = 2;

        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity->getCreated() ? $entity->getCreated()->format("d/m/Y") : '');
            $sheet->setCellValue('B' . $fila, $entity->getStock()->getProducto()->getCodigo());
            $sheet->setCellValue('C' . $fila, $entity->getStock()->getProducto()->getGtin());
            $sheet->setCellValue('D' . $fila, $entity->getConsignacion()->getClienteProveedor());
            $sheet->setCellValue('E' . $fila, $this->movimientoDepositoService->getMotivoDevolucion($entity->getStock()->getId(), false));
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('F' . $fila, $entity->getStock()->getUnidadnegocio());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }*/

}
