<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProductoPresupuesto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * ProductoPresupuesto controller.
 *
 */
class ProductoPresupuestoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Delete Producto Presupuesto
     *
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ProductoPresupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductoPresupuesto entity.');
        }
        $cuenta = count($em->getRepository('AppBundle:ProductoPresupuesto')->findBy(['presupuesto' => $entity->getPresupuesto()]));
        if ($cuenta > 1) {
            $presupuesto = $entity->getPresupuesto();
            $presupuesto->setImporte($presupuesto->getImporte() - $entity->getSubtotal());
            $em->persist($presupuesto);
            $em->remove($entity);
            $em->flush();
            return new Response('Ok');
        } else {
            return new Response('Error');
        }
    }

}
