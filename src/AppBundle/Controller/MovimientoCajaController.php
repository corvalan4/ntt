<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\MovimientoCaja;
use AppBundle\Form\MovimientoCajaType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * MovimientoCaja controller.
 *
 */
class MovimientoCajaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all MovimientoCaja entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:MovimientoCaja')->findAll();

        return $this->render('AppBundle:MovimientoCaja:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new MovimientoCaja entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new MovimientoCaja();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocaja_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:MovimientoCaja:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MovimientoCaja entity.
     *
     * @param MovimientoCaja $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MovimientoCaja $entity) {
        $form = $this->createForm(new MovimientoCajaType(), $entity, array(
            'action' => $this->generateUrl('movimientocaja_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MovimientoCaja entity.
     *
     */
    public function newAction() {
        $entity = new MovimientoCaja();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:MovimientoCaja:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MovimientoCaja entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
  
        $caja= $em->getRepository('AppBundle:Caja')->find($id);
        if (!$caja) {
            $this->sessionManager->addFlash('msgWarn', 'No existe la caja seleccionada.');
            return $this->redirect($this->generateUrl('caja'));
        }

        $entities = $em->getRepository('AppBundle:MovimientoCaja')->findBy(array('caja' => $id), array('id' => 'ASC'));
        if (!$entities) {
            $this->sessionManager->addFlash('msgWarn', 'No existen movimientos para esta caja.');
            return $this->redirect($this->generateUrl('caja'));
        }

        return $this->render('AppBundle:MovimientoCaja:show.html.twig', array(
                    'entities' => $entities,
                    'caja'=>$caja
        ));
    }

    /**
     * Displays a form to edit an existing MovimientoCaja entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoCaja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCaja entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:MovimientoCaja:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a MovimientoCaja entity.
     *
     * @param MovimientoCaja $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MovimientoCaja $entity) {
        $form = $this->createForm(new MovimientoCajaType(), $entity, array(
            'action' => $this->generateUrl('movimientocaja_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing MovimientoCaja entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoCaja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCaja entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocaja_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:MovimientoCaja:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a MovimientoCaja entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:MovimientoCaja')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MovimientoCaja entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('movimientocaja'));
    }

    /**
     * Creates a form to delete a MovimientoCaja entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('movimientocaja_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
