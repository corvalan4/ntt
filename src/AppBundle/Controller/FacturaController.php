<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\Consignacion;
use AppBundle\Entity\ProductoConsignacion;
use AppBundle\Entity\ProductoPedido;
use AppBundle\Entity\ProductoRemito;
use AppBundle\Entity\RemitoOficial;
use AppBundle\Entity\Stock;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ps\PdfBundle\Annotation\Pdf;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Factura;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\ProductoFactura;
use AppBundle\Entity\Iva;
use AppBundle\Entity\TipoIva;
use AppBundle\Entity\Producto;
use AppBundle\Form\FacturaType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Factura controller.
 *
 */
class FacturaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Factura entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();
        $unidad = !$this->isGranted('ROLE_SUPER_ADMIN') ? $this->getUser()->getUnidadNegocio()->getId() : '';
        $fecha_desde = $request->get('fechaDesde') ?? '';
        $fecha_hasta = $request->get('fechaHasta') ?? '';
        $cliente = $request->get('cliente') ?? '';
        $filter = [
            'cliente' => $cliente,
            'unidad' => $unidad,
            'tipo' => ["F", "FC"],
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'numerofactura' => $request->get('numerofactura') ?? '',
            'numeroremito' => $request->get('numeroremito') ?? '',
            'tipofactura' => $request->get('tipofactura') ?? '',
        ];

        $entities = $em->getRepository('AppBundle:Factura')->findByFilter($filter);


        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcel($entities);
            return $this->container->get('session.manager')->exportarExcel($spreadSheet, 'Facturas-' . date('Ymd'));
        }

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

        return $this->render('AppBundle:Factura:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
        ));
    }

    private function exportarExcel($entities) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Factura');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Importe');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('B' . $fila, strip_tags($entity));
            $sheet->setCellValue('C' . $fila, $entity->getClienteproveedor()->getRazonSocial());
            $sheet->setCellValue('D' . $fila, $entity->getImporte());

            $fila++;
        }

        return $spreadsheet;
    }

    /**
     * Creates a new Factura entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

//        $es_factura_credito = $form->get("es_factura_credito")->getData();

        $hayProductos = false;
        $total = 0;
        $iva21 = 0;
        $iva105 = 0;
        $montoIva0 = 0;

        if ($form->isValid()) {
            for ($i = 1; $i <= 30; $i++) {
                $idstock = $request->get("idstock_elemento$i");
                $idelemento = $request->get("idprod_elemento$i");
                $id_productoremito = $request->get("id_productoremito$i");
                $cantidad = (float) $request->get("cantidad_$i");
                $precio = (float) $request->get("precio_$i");
                $subtotal = $cantidad * $precio;
                $total += $subtotal;

                $descripcion = $request->get("descripcion_elemento$i");

                if ($request->get("from_remito")) {
                    $esProducto = false;
                    if (isset($id_productoremito) && $id_productoremito != '') {

                        $productoremito = $em->getRepository(ProductoRemito::class)->find($id_productoremito);
                        $tipoIva = $productoremito->getProducto()->getTipoiva();
                        if ($tipoIva == 0) {
                            $montoIva0 += $subtotal;
                        } else {
                            $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                            if ($tipoIva == 21) {
                                $iva21 += $iva;
                            }
                            if ($tipoIva == 10.5) {
                                $iva105 += $iva;
                            }
                        }

                        $productofactura = new ProductoFactura();
                        if ($productoremito->getStock()) {
                            $productofactura->setStock($productoremito->getStock());
                        }
                        $productofactura->setFactura($entity);
                        $productofactura->setPrecio($precio);
//                    $productofactura->setDescripcion($descripcion);
                        $productofactura->setRenglon($productoremito->getRenglon());
                        $productofactura->setDescripcion($productoremito->getDescripcion());
                        $productofactura->setCantidad($productoremito->getCantidad());
                        $em->persist($productofactura);
                        $productoremito->getRemitoOficial()->setEstado(RemitoOficial::INVOICED);
                        $productoremito->getRemitoOficial()->setFactura($entity);
                        $productoremito->setEstado(ProductoRemito::PRODUCTO_REMITO_INVOICED);
                        $hayProductos = true;
                    } else {
                        if (isset($idstock) && $idstock != '') {
                            $elementostock = $em->getRepository(Stock::class)->find($idstock);
                            $productofactura = new ProductoFactura();
                            $tipoIva = $productofactura->getProducto()->getTipoiva();
                            if ($tipoIva == 0) {
                                $montoIva0 += $subtotal;
                            } else {
                                $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                                if ($tipoIva == 21) {
                                    $iva21 += $iva;
                                }
                                if ($tipoIva == 10.5) {
                                    $iva105 += $iva;
                                }
                            }
                            //                    $productofactura->setDescripcion($descripcion);
                            $productofactura->setStock($elementostock);
                            $productofactura->setFactura($entity);
                            $productofactura->setPrecio($precio);
                            $productofactura->setRenglon($i);

                            $productofactura->setCantidad($cantidad);
                            $em->persist($productofactura);
                            $productoremito->getRemitoOficial()->setEstado(RemitoOficial::INVOICED);
                            $productoremito->getRemitoOficial()->setFactura($entity);
                            $productoremito->setEstado(ProductoRemito::PRODUCTO_REMITO_INVOICED);
                            $additional = ["description" => "Alta de Factura Cliente desde Remito. Producto agregado en factura.", "factura" => $entity];
                            $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), $precio, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                            $em->persist($balance);

                            $productofactura->setCantidad($cantidad);

                            if ($elementostock->getStock() >= $cantidad) {
                                $elementostock->setStock($elementostock->getStock() - $cantidad);
                            } else {
                                $elementostock->setStock(0);
                            }

                            $hayProductos = true;
                        }
                    }
                } else {

                    if (!empty($idstock)) {

                        $elementostock = $em->getRepository(Stock::class)->find($idstock);
                        $productofactura = new ProductoFactura();

                        $tipoIva = $elementostock->getProducto()->getTipoiva();

                        if ($tipoIva == 0) {
                            $montoIva0 += $subtotal;
                        } else {
                            $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                            if ($tipoIva == 21) {
                                $iva21 += $iva;
                            }
                            if ($tipoIva == 10.5) {
                                $iva105 += $iva;
                            }
                        }

                        $productofactura->setDescripcion($descripcion);
                        $productofactura->setStock($elementostock);
                        $productofactura->setFactura($entity);
                        $productofactura->setPrecio($precio);
                        $productofactura->setDescripcion($descripcion);

                        $additional = ["description" => "Alta de Factura Cliente", "factura" => $entity];
                        if ($elementostock->getProducto()->getTrazable()) {
                            $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), $precio, $elementostock->getStock(), Balance::TYPE_NEGATIVE, $additional);
                            $em->persist($balance);

                            $productofactura->setCantidad($elementostock->getStock());
                            $elementostock->setStock(0);
                            $em->persist($productofactura);
                        } else {
                            $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), $precio, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                            $em->persist($balance);

                            $productofactura->setCantidad($cantidad);

                            if ($elementostock->getStock() >= $cantidad) {
                                $elementostock->setStock($elementostock->getStock() - $cantidad);
                            } else {
                                $elementostock->setStock(0);
                            }
                            $em->persist($productofactura);
                        }
                        $hayProductos = true;
                    } else {
                        if (!empty($idelemento)) {
                            $elementostocks = $em->getRepository(Stock::class)->findBy(array('producto' => $idelemento, 'unidadnegocio' => $entity->getUnidadNegocio()->getId()), array('fecha' => 'ASC'));

                            foreach ($elementostocks as $elementostock) {
                                if ($cantidad > 0 and $cantidad <= $elementostock->getStock()) {
                                    $stock = $elementostock->getStock() - $cantidad;

                                    $additional = ["description" => "Alta de Factura Cliente", "factura" => $entity];
                                    $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), $precio, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                                    $em->persist($balance);

                                    $productofactura = new ProductoFactura();
                                    $tipoIva = $productofactura->getProducto()->getTipoiva();

                                    if ($tipoIva == 0) {
                                        $montoIva0 += $subtotal;
                                    } else {
                                        $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                                        if ($tipoIva == 21) {
                                            $iva21 += $iva;
                                        }
                                        if ($tipoIva == 10.5) {
                                            $iva105 += $iva;
                                        }
                                    }
                                    $productofactura->setDescripcion($descripcion);
                                    $productofactura->setStock($elementostock);
                                    $productofactura->setFactura($entity);
                                    $productofactura->setCantidad($cantidad);
                                    $productofactura->setPrecio($precio);

                                    $elementostock->setStock($stock);
                                    $em->persist($productofactura);

                                    $cantidad = 0;
                                } else {
                                    if ($cantidad > 0 and $elementostock->getStock() > 0) {
                                        $productofactura = new ProductoFactura();

                                        $tipoIva = $productofactura->getProducto()->getTipoiva();
                                        if ($tipoIva == 0) {
                                            $montoIva0 += $subtotal;
                                        } else {
                                            $iva = $subtotal * (1 + $tipoIva / 100) - $subtotal;
                                            if ($tipoIva == 21) {
                                                $iva21 += $iva;
                                            }
                                            if ($tipoIva == 10.5) {
                                                $iva105 += $iva;
                                            }
                                        }

                                        $productofactura->setDescripcion($descripcion);
                                        $productofactura->setStock($elementostock);
                                        $productofactura->setFactura($entity);
                                        $productofactura->setCantidad($cantidad);
                                        $productofactura->setPrecio($precio);

                                        $cantidad = $cantidad - $elementostock->getStock();

                                        $additional = ["description" => "Alta de Factura Cliente", "factura" => $entity];
                                        $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), $precio, $elementostock->getStock(), Balance::TYPE_NEGATIVE, $additional);
                                        $em->persist($balance);

                                        $productofactura->setCantidad($elementostock->getStock());
                                        $elementostock->setStock(0);
                                        $em->persist($productofactura);
                                    }
                                }
                            }
                            $hayProductos = true;
                        }
                    }
                }
            }

            $productosremito = [];
            if (!empty($request->get('remitosoficiales'))) {
                $response = $this->getStockFromRemitos($request->get('remitosoficiales'), $entity);
                $productosremito = $response['productosremito'];
            }

            if (!$hayProductos) {
                $this->sessionManager->addFlash('msgError', 'Debe agregar productos.');
                return $this->render('AppBundle:Factura:new.html.twig', array(
                            'productosremito' => $productosremito,
                            'entity' => $entity,
                            'form' => $form->createView(),
                ));
            }

            $entity->setPtovta($entity->getUnidadNegocio()->getPunto());
            $entity->setImporte($total);
            $limiteFacturaCredito = $this->getParameter('limite_factura_credito');

//            if ($total >= $limiteFacturaCredito and $es_factura_credito) {
//                $entity->setTipo('FC');
//            }       

            if ($entity->getTipofactura() == 'A') {

                if ($montoIva0) {
                    $tipoIva0 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 0));
                    $alicuotaIva0 = new Iva();
                    $alicuotaIva0->setTipoIva($tipoIva0);
                    $alicuotaIva0->setFactura($entity);
                    $alicuotaIva0->setValor(0);
                    $alicuotaIva0->setImporteFactura($montoIva0);
                    $em->persist($alicuotaIva0);
                    $entity->addIva($alicuotaIva0);
                }
                if ($iva21 > 0) {
                    $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
                    $alicuotaIva21 = new Iva();
                    $alicuotaIva21->setTipoIva($tipoIva21);
                    $alicuotaIva21->setFactura($entity);
                    $alicuotaIva21->setValor(round($iva21,2));
                    $alicuotaIva21->setImporteFactura(100 * $iva21 / 21);
                    $em->persist($alicuotaIva21);
                    $entity->addIva($alicuotaIva21);
                }

                if ($iva105 > 0) {
                    $tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));
                    $alicuotaIva105 = new Iva();
                    $alicuotaIva105->setTipoIva($tipoIva105);
                    $alicuotaIva105->setFactura($entity);
                    $alicuotaIva105->setValor(round($iva105,2));
                    $alicuotaIva105->setImporteFactura(100 * $iva105 / 10.5);
                    $em->persist($alicuotaIva105);
                    $entity->addIva($alicuotaIva105);
                }
            }


            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setFactura($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte(round($entity->getTotal(),2));
            $em->persist($pagoAsignacion);
            $em->persist($entity);

            $em->flush();

            $this->sessionManager->facturar($entity);
            $balances = $em->getRepository('AppBundle:Balance')->findBy(array('factura' => $entity->getId()));

            foreach ($balances as $balance) {
                if ($balance->getStock()->getProducto()->getTrazable()) {
                    $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
                    $this->sessionManager->csvVerifarmaDespacho([$balance], "DES" . $namesheet);
                }
            }

            $cae = $entity->getCae();
            if (!empty($cae)) {
                return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId())));
            } else {
                return $this->redirect($this->generateUrl('factura'));
            }
        }
    }

    public function refacturarAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $tipo = $request->get('tipo') ?? 'F';

        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        $entity->setTipo($tipo);
        $em->flush();

        $this->sessionManager->facturar($entity);

        return $this->redirect($this->generateUrl('factura'));
    }

    public function crearremitofacturaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($request->get('id'));
        $tipofactura = $request->get('tipofactura');

        $entity->setTipofactura($tipofactura);
        $entity->setFecha(new \DateTime());
        $entity->setImporte($request->get('resultadoFinal'));
        $entity->getMovimientocc()->setTipoDocumento('FC');

        $idsucursal = $request->get('sucursal');
        if (isset($idsucursal)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($idsucursal);
            if (!is_null($sucursal)) {
                $entity->setSucursal($sucursal);
            }
        }

        foreach ($entity->getProductosFactura() as $prodFac) {
            $prodFac->setPertenece('F');
        }

        $contador = $request->get('contador');
        for ($x = 0; $x <= $contador; $x++) {
            $predictiva = $request->get('predictiva' . $x);

            if (!is_null($predictiva) and $predictiva != '') {
                $subdiv = explode(" ( ", $predictiva);
                if (count($subdiv) > 1) {
                    $codigo = explode(" )", $subdiv[1]);
                    $productoFactura = new ProductoFactura();
                    $producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo[0], 'unidadNegocio' => $entity->getListaPrecio()->getUnidadNegocio()));

                    if (!is_null($producto)) {
                        $stock = $producto->getStock() - $request->get('cantidad' . $x);
                        if ($stock < 0) {
                            $stock = 0;
                        }
                        $producto->setStock($stock);
                    }

                    if (!is_null($producto)) {
                        $productoFactura->setProducto($producto);
                        $productoFactura->setFactura($entity);
                        $productoFactura->setDescuento($request->get('bonificacion' . $x));
                        $productoFactura->setPrecio($request->get('precio' . $x));
                        $productoFactura->setPertenece('F');
                        $productoFactura->setCantidad($request->get('cantidad' . $x));
                        $entity->addProductosFactura($productoFactura);
                    }
                }
            }
        }

        $wsaa = new WSAA('./');

        // Si la fecha de expiracion es menor a hoy, obtengo un nuevo Ticket de Acceso.
        if ($wsaa->get_expiration() < date("Y-m-d h:m:i")) {
            if ($wsaa->generar_TA()) {
                
            } else {
                echo 'msgError', 'error al obtener el TA.';
            }
        } else {
            
        };

        $wsfe = new WSFEV1('./');

        // Carga el archivo TA.xml
        $wsfe->openTA();

        // devuelve el cae
        $ptovta = 3;

        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {
            $tipocbte = 01;
            $tipofactura = 'A';
        } else {
            $tipocbte = 06;
            $tipofactura = 'B';
        }

        /*
          - 01, 02, 03, 04, 05,34,39,60, 63 para los clase A
          - 06, 07, 08, 09, 10, 35, 40,64, 61 para los clase B.
          - 11, 12, 13, 15 para los clase C.
          - 51, 52, 53, 54 para los clase M.
          - 49 para los Bienes Usados
          Consultar m�todo FEParamGetTiposCbte.
         */

        $nc = '';
        // Ultimo comprobante autorizado, a este le sumo uno para procesar el siguiente.
        $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta);

        //Armo array con valores hardcodeados de la factura.
        $regfac['concepto'] = 1;              # 1: productos, 2: servicios, 3: ambos

        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Consumidor Final') {
            $regfac['tipodocumento'] = 99;                  # 80: CUIT, 96: DNI, 99: Consumidor Final
        } else {
            $regfac['tipodocumento'] = 80;                  # 80: CUIT, 96: DNI, 99: Consumidor Final
        }
        $regfac['numerodocumento'] = $entity->getMovimientocc()->getClienteProveedor()->getDni();     # 0 para Consumidor Final (<$1000)
        $regfac['cuit'] = str_replace("-", "", $entity->getMovimientocc()->getClienteProveedor()->getCuit());
        $regfac['capitalafinanciar'] = 0;           # subtotal de conceptos no gravados
        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {
            $regfac['importetotal'] = $entity->getImporte(); # total del comprobante
            $regfac['importeiva'] = ($entity->getImporte() - ($entity->getImporte() / 1.21));   # subtotal neto sujeto a IVA
            $regfac['importeneto'] = ($entity->getImporte() / 1.21);
        } else {
            $regfac['importetotal'] = $entity->getImporte(); # total del comprobante
            $regfac['importeiva'] = 0;
            $regfac['importeneto'] = 0;   # subtotal neto sujeto a IVA
            $regfac['capitalafinanciar'] = $entity->getImporte();
        }

        $regfac['imp_trib'] = 1.0;
        $regfac['imp_op_ex'] = 0.0;
        $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
        $regfac['fecha_venc_pago'] = date('Ymd');

        // Armo con la factura los parametros de entrada para el pedido
        $params = $wsfe->armadoFacturaUnica(
                $tipofactura,
                $ptovta, // el punto de venta
                $nc,
                $regfac     // los datos a facturar
        );

        //Solicito el CAE
        $cae = $wsfe->solicitarCAE($params);
        $this->sessionManager->setSession('CAE', json_encode($cae));
        if (!empty($cae->FECAESolicitarResult->Errors)) {
            $this->sessionManager->addFlash('msgError', 'Error al dar de alta factura: ' . json_encode($cae->FECAESolicitarResult->Errors));
            $entity->setObservacion($entity->getObservacion() . " - ERROR: " . json_encode($cae->FECAESolicitarResult->Errors));
            $em->flush();
            return $this->redirect($this->generateUrl('factura'));
        }

        if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'A' or $cae->FECAESolicitarResult->FeCabResp->Resultado == 'P') {
            $em->flush();
            $entity->setNrofactura($regfac['nrofactura']);
            $entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
            $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
            $em->flush();

            $cae = $entity->getCae();
            if (!empty($cae)) {
                return $this->redirect($this->generateUrl('cobranza_new', array('idfactura' => $entity->getId())));
            } else {
                return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId(), 'tipo' => 'F')));
            }
        } else {
            if ($cae->FECAESolicitarResult->FeCabResp->Resultado == 'R') {
                $this->sessionManager->addFlash('msgError', 'Error al dar de alta factura: ' . json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones));
            }
            return $this->redirect($this->generateUrl('factura'));
        }

        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Venta registrada.');

        $cae = $entity->getCae();
        if (!empty($cae)) {
            return $this->redirect($this->generateUrl('cobranza_new', array('idfactura' => $entity->getId())));
        } else {
            return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId(), 'tipo' => 'F')));
        }
    }

    /**
     * Creates a form to create a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Factura $entity) {
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('factura_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Confirmar Factura', 'attr' => array('class' => 'btn btn-success  btn-xs', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new Factura entity.
     *
     */
    public function newAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }
        $entity->setTipofactura('B');

        $productosremito = [];
        if (!empty($request->get('remitosoficiales'))) {
            $response = $this->getStockFromRemitos($request->get('remitosoficiales'), $entity);
            if (!$response['success'])
                return $response['redirect'];
            $entity = $response['entity'];
            $productosremito = $response['productosremito'];
        }

        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Factura:new.html.twig', array(
                    'entity' => $entity,
                    'productosremito' => $productosremito,
                    'idremitos' => $request->get('remitosoficiales') ? $request->get('remitosoficiales') : [],
                    'form' => $form->createView(),
        ));
    }

    public function getStockFromRemitos($idremitos = [], $entity) {
        $em = $this->getDoctrine()->getManager();
        $cliente = null;
        $productosremito = [];
        foreach ($idremitos as $remitooficial_id) {
            $remitooficial = $em->getRepository('AppBundle:RemitoOficial')->find($remitooficial_id);
            if ($remitooficial) {
                if (!$cliente)
                    $cliente = $remitooficial->getClienteProveedor();
                if ($cliente != $remitooficial->getClienteProveedor()) {
                    $this->addFlash("msgError", "No se puede facturar a múltiples clientes.");
                    return ['success' => false, 'redirect' => $this->redirectToRoute('remitooficial')];
                }
                $productosremito = array_merge($productosremito, $remitooficial->getProductosRemito()->getValues());
                $entity->setClienteProveedor($cliente);
            }
        }

        return ['success' => true, 'productosremito' => $productosremito, 'entity' => $entity];
    }

    public function remitofacturaAction($id) {


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {
            return $this->render('AppBundle:Factura:remitofacturaA.html.twig', array(
                        'entity' => $entity,
            ));
        } else {
            return $this->render('AppBundle:Factura:remitofacturaB.html.twig', array(
                        'entity' => $entity,
            ));
        }
    }

    public function facturaremitoAction($id) {


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        return $this->render('AppBundle:Factura:facturaremito.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function crearfacturaremitoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($request->get('id'));
        $entity->setNroremito($request->get('nroremito'));
        $contador = count($entity->getProductosFactura());
        for ($x = 1; $x <= $contador; $x++) {
            $id = $request->get('id' . $x);
            $this->sessionManager->setSession('id' . $id, $id);
            if (!is_null($id) and $id != '') {
                $productoFactura = $em->getRepository('AppBundle:ProductoFactura')->find($id);
                $productoFactura->setPertenece('RF');
            }
        }
        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Remito registrado.');

        return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId(), 'tipo' => 'R')));
    }

    public function showAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        $remitosoficiales = $em->getRepository('AppBundle:RemitoOficial')->findBy(['factura' => $id]);
        $productos = [];

        $vista = "AppBundle:Factura:showB.html.twig";

        $cantidadFactura = $em->getRepository(ProductoFactura::class)->cantidadProductosEnFactura($entity->getId()) ?? 0;
        $cantidadConsignacion = $em->getRepository(ProductoConsignacion::class)->cantidadProductosEnConsignacion($entity->getId()) ?? 0;
        $consignaciones = $em->getRepository(Consignacion::class)->findBy(['factura' => $entity->getId()]);
        foreach ($entity->getProductosFactura() as $productofactura) {
            $productoConsignacion = null;
            if ($productofactura->getStock()) {
                $productoConsignacion = $em->getRepository(ProductoConsignacion::class)->findOneBy(['stock' => $productofactura->getStock()->getId(), 'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK]);
//                    $productoConsignacion = $em->getRepository(ProductoConsignacion::class)->getUsedProductByStock($productofactura->getStock()->getId());
            }
            $productos[] = ['productoFactura' => $productofactura, 'productoConsignacion' => $productoConsignacion];
        }

        $tipofactura = $entity->getTipofactura();
        $tipofacturastr = $entity->getTipoFacturaStr() . " " . $tipofactura;
        $nrocomprobante = (int) $this->sessionManager->getTipoComprobante($tipofacturastr);

        if ($entity->getTipofactura() == 'A') {
            $vista = "AppBundle:Factura:showA.html.twig";
        }

        $codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
                str_replace("-", "", $entity->getUnidadNegocio()->getCuit()),
                $nrocomprobante,
                $entity->getPtovta(),
                $entity->getCae(),
                ($entity->getFechavtocae() ? $entity->getFechavtocae()->format('Ymd') : '')
        );

        $generator = new BarcodeGeneratorPNG();
        $barcode = base64_encode($generator->getBarcode($codigobarras, $generator::TYPE_INTERLEAVED_2_5));

        return $this->render($vista, array(
                    'entity' => $entity,
                    'tipofactura' => $entity->getTipofactura(),
                    'nrocomprobante' => $nrocomprobante,
                    'barcode' => $barcode,
                    'remitosoficiales' => $remitosoficiales,
                    'codigobarras' => $codigobarras,
                    'consignaciones' => $consignaciones,
                    'cantidadConsignacion' => $cantidadConsignacion,
                    'cantidadFactura' => $cantidadFactura,
                    'productos' => $productos,
        ));
    }

    public function imprimirAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }


        return $this->render('AppBundle:Factura:imprimir.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Factura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Factura $entity) {
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('factura_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Factura entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('factura_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Factura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        $pagoAsignacion = $em->getRepository('AppBundle:PagoAsignacion')->findOneBy(['factura' => $id]);
        $pagoAsignacion->setEstado("E");
        $entity->setEstado("E");

        $remitos = $em->getRepository(RemitoOficial::class)->findBy(['factura' => $id]);

        if (sizeof($remitos) > 0) {
            foreach ($remitos as $remitoOficial) {
                $remitoOficial->setFactura(null);
                $remitoOficial->setEstado(RemitoOficial::IN_PROCESS);
                foreach ($remitoOficial->getProductosRemito() as $productoRemito) {
                    $productoRemito->setEstado(ProductoRemito::PRODUCTO_REMITO_IN_CLIENT);
                }
            }
        } else {
            foreach ($entity->getProductosFactura() as $productofactura) {
                $stock = $productofactura->getStock()->getStock() + $productofactura->getCantidad();
                $productofactura->getStock()->setStock($stock);
                $additional = ["description" => "Anulación de Factura Cliente", "factura" => $entity];
                $balance = $this->sessionManager->registerBalance($productofactura->getStock(), $this->getUser(), $productofactura->getPrecio(), $productofactura->getCantidad(), Balance::TYPE_POSITIVE, $additional);
                $em->persist($balance);
            }
        }

        $em->flush();

        $this->sessionManager->addFlash("msgOk", "Factura eliminada con éxito.");

        return $this->redirect($this->generateUrl('factura'));
    }

    public function borraremitoAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        $entity->setEstado('B');
        $entity->getMovimientocc()->setEstado('B');

        $em->flush();

        return $this->redirect($this->generateUrl('factura'));
    }

    /**
     * Creates a form to delete a Factura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('factura_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function verclienteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $string = '';
        $cliente = $em->getRepository('AppBundle:ClienteProveedor')->findOneBy(array('cuit' => $search));
        if (empty($cliente)) {
            $cliente = $em->getRepository('AppBundle:ClienteProveedor')->findOneBy(array('dni' => $search));
        }
        if (empty($cliente)) {
            $response = '0///0';
        } else {
            $response = $cliente->getId() . '///' . $cliente->getRazonSocial();
        }
        return new Response($response);
    }

    public function altaclienteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $condicion = $em->getRepository('AppBundle:CondicionIva')->find(1);
        $tipo = $request->get('data');
        $nrodoc = $request->get('data1');
        $direc = $request->get('data2');
        $razon = $request->get('data3');
        $telefono = $request->get('data4');
        $email = $request->get('data5');

        $respuesta = '0';

        $cliente = new ClienteProveedor();
        $cliente->setRazonSocial($razon);
        if ($tipo == 'CUIT') {
            $cliente->setCuit(str_replace('-', '', $nrodoc));
        } else {
            $cliente->setDni($nrodoc);
        }
        $cliente->setCondicioniva($condicion);
        $cliente->setDomiciliocomercial($direc);
        $cliente->setTelefono($telefono);
        $cliente->setMail($email);

        $em->persist($cliente);
        $em->flush();

        $respuesta = $nrodoc;

        return new Response($respuesta);
    }

    /*
     * Muestra formulario para la generación del Citi Ventas
     */

    public function citiVentasAction(Request $request) {
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $tipoTxt = $request->get('tipoTxt');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio()->getId();
        }
        if (isset($fechaDesde) && isset($fechaHasta) && isset($unidadNegocio)) {
            $respuesta = $this->citiventasTxt($fechaDesde, $fechaHasta, $tipoTxt, $unidadNegocio);
            if ($respuesta === false) {
                $this->sessionManager->addFlash('msgError', 'No existen registros para el período seleccionado.');
            } else {
                return $respuesta;
            }
        }

        return $this->render('AppBundle:Factura:citiVentas.html.twig');
    }

    /*
     * Generación de archivo txt de Citi Ventas
     */

    public function citiventasTxt($fechaDesde, $fechaHasta, $tipoTxt, $unidadNegocio) {

        if (file_exists($tipoTxt . '.txt')) {
            unlink($tipoTxt . '.txt');
            clearstatcache();
        }

        $em = $this->getDoctrine()->getManager();
        $facturas = $em->getRepository('AppBundle:Factura')->citiventas($fechaDesde, $fechaHasta, $unidadNegocio);
        if (sizeof($facturas) == 0) {
            return false;
        }
        $cuenta = 1;
        foreach ($facturas as $entity) {

            $tipocmb = $this->sessionManager->getTipoComprobante('FACTURAS ' . $entity->getTipofactura());
            if ($entity->getClienteProveedor()->getCuit()) {
                $g = $this->sessionManager->getTipoDocumento('CUIT');
                $numeroIdentificacionComprador = str_replace('-', '', $entity->getClienteProveedor()->getCuit());
            } else {
                $g = $this->sessionManager->getTipoDocumento('DNI');
                $numeroIdentificacionComprador = str_replace(' ', '', $entity->getClienteProveedor()->getDni());
            }

            $cliente = substr($this->sessionManager->trim_all($entity->getClienteProveedor()->getRazonSocial()), 0, 30);

            //Por la codificación de la ñ por ejemplo
            $largoA = strlen($cliente); //Retorna cantidad de Bytes
            $largoB = mb_strlen($cliente); //Retorna cantidad de caracteres 

            if ($largoA > $largoB) {
                $diferencia = $largoA - $largoB;
                $cliente = str_pad($cliente, 30 + $diferencia, " ");
            } else {
                $cliente = str_pad($cliente, 30, " ");
            }
            if ($tipoTxt == 'Comprobantes') {
                file_put_contents($tipoTxt . '.txt',
                        $entity->getFecha()->format('Ymd') . //Fecha de comprobante
                        str_pad($tipocmb, 3, "0", STR_PAD_LEFT) . //Tipo de comprobante
                        str_pad($entity->getPtovta(), 5, "0", STR_PAD_LEFT) . //Punto de Venta
                        str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) . //Número de comprobante
                        str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) . //Número de comprobante hasta
                        str_pad($g, 2, "0", STR_PAD_LEFT) . //Código de documento del comprador
                        str_pad($numeroIdentificacionComprador, 20, "0", STR_PAD_LEFT) . //Número de identificación del comprador
                        $cliente . //Apellido y nombre del comprador
                        str_pad($entity->getImporte() * 100, 15, "0", STR_PAD_LEFT) . //Importe total de la operación
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe total de conceptos que no integran el precio neto gravado
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Percepción a no categorizados
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe operaciones exentas
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de percepciones o pagos a cuenta de impuestos nacionales
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe de percepciones de ingresos brutos
                        str_pad('0', 15, "0", STR_PAD_LEFT) . // Importe de percepciones impuestos municipales
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Importe impuestos internos
                        str_pad('PES', 3, "0", STR_PAD_LEFT) . //Código de moneda
                        str_pad('0001', 10, "0", STR_PAD_RIGHT) . //Tipo de cambio
                        str_pad('1', 1, "0", STR_PAD_LEFT) . //Cantidad de alícuotas de IVA
                        str_pad('0', 1, "0", STR_PAD_LEFT) . //Código de operación
                        str_pad('0', 15, "0", STR_PAD_LEFT) . //Otros tributos
                        $entity->getFecha()->format('Ymd') //Fecha de vencimiento de pago
                        , FILE_APPEND);
            }
            /* var_dump(str_pad($tipocmb, 3, "0", STR_PAD_LEFT) .
              str_pad($entity->getPtovta(), 5, "0", STR_PAD_LEFT) .
              str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) .
              str_pad(round($entity->getImporte() / 1.21, 2) * 100, 15, "0", STR_PAD_LEFT) .
              $iva .
              str_pad(round($entity->getImporte() - ($entity->getImporte() / 1.21), 2) * 100, 15, "0", STR_PAD_LEFT));
              exit(); */
            if ($tipoTxt == 'Alicuotas') {
                foreach ($entity->getIvas() as $iva) {
                    $ivaCodigo = $this->sessionManager->getCodigoAlicuota($iva->getTipoIva()->getPorcentaje());
                    file_put_contents($tipoTxt . '.txt',
                            str_pad($tipocmb, 3, "0", STR_PAD_LEFT) . // Tipo de Comprobante 3 Numérico
                            str_pad($entity->getPtovta(), 5, "0", STR_PAD_LEFT) . // Punto de Venta 5 Numérico
                            str_pad($entity->getNrofactura(), 20, "0", STR_PAD_LEFT) . //Número de comprobante 20 Numérico
                            str_pad(str_replace('.', '', $iva->getImporteFactura() * 100), 15, "0", STR_PAD_LEFT) . //Importe neto gravado 15 Numérico
                            $ivaCodigo . //Alícuota de IVA 4 Numérico
                            str_pad(str_replace('.', '', $iva->getValor() * 100), 15, "0", STR_PAD_LEFT) . //Impuesto liquidado 15 Numérico
                            PHP_EOL
                            , FILE_APPEND);
                }
            }
        }

        /*
         * Agrego las Notas de Crédito
         */
        $notasCredito = $em->getRepository('AppBundle:NotaCreditoDebito')->citiventas($fechaDesde, $fechaHasta, $unidadNegocio);
        foreach ($notasCredito as $nc) {
            $tipocmb = $this->sessionManager->getTipoComprobante('NOTAS DE CREDITO ' . $nc->getCodigonota());
            if ($nc->getClienteProveedor()->getCuit()) {
                $g = '80';
                $h = str_replace('-', '', $nc->getClienteProveedor()->getCuit());
            } else {
                $g = '96';
                $h = str_replace(' ', '', $nc->getClienteProveedor()->getDni());
            }

            $cliente = substr($this->sessionManager->trim_all($nc->getClienteProveedor()->getRazonSocial()), 0, 30);

            //Por la codificación de la ñ por ejemplo
            $largoA = strlen($cliente); //Retorna cantidad de Bytes
            $largoB = mb_strlen($cliente); //Retorna cantidad de caracteres

            if ($largoA > $largoB) {
                $diferencia = $largoA - $largoB;
                $cliente = str_pad($cliente, 30 + $diferencia, " ");
            } else {
                $cliente = str_pad($cliente, 30, " ");
            }
            if ($tipoTxt == 'Comprobantes') {
                file_put_contents($tipoTxt . '.txt',
                        $nc->getFecha()->format('Ymd') .
                        str_pad($tipocmb, 3, "0", STR_PAD_LEFT) .
                        str_pad($nc->getPtovta(), 5, "0", STR_PAD_LEFT) .
                        str_pad($nc->getNrocomprobante(), 20, "0", STR_PAD_LEFT) .
                        str_pad($nc->getNrocomprobante(), 20, "0", STR_PAD_LEFT) .
                        str_pad($g, 2, "0", STR_PAD_LEFT) .
                        str_pad($h, 20, "0", STR_PAD_LEFT) .
                        $cliente .
                        str_pad($nc->getImporte() * 100, 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        str_pad('PES', 3, "0", STR_PAD_LEFT) .
                        str_pad('0001', 10, "0", STR_PAD_RIGHT) .
                        str_pad(strval(count($nc->getIvas())), 1, "0", STR_PAD_LEFT) .
                        str_pad('0', 1, "0", STR_PAD_LEFT) .
                        str_pad('0', 15, "0", STR_PAD_LEFT) .
                        $nc->getFecha()->format('Ymd') .
                        PHP_EOL
                        , FILE_APPEND);
            }

            if ($tipoTxt == 'Alicuotas') {
                foreach ($nc->getIvas() as $iva) {
                    $ivaCodigo = $this->sessionManager->getCodigoAlicuota($iva->getTipoIva()->getPorcentaje());
                    file_put_contents($tipoTxt . '.txt',
                            str_pad($tipocmb, 3, "0", STR_PAD_LEFT) . // Tipo de Comprobante 3 Numérico
                            str_pad($nc->getPtovta(), 5, "0", STR_PAD_LEFT) . // Punto de Venta 5 Numérico
                            str_pad($nc->getNrocomprobante(), 20, "0", STR_PAD_LEFT) . //Número de comprobante 20 Numérico
                            str_pad(str_replace('.', '', $iva->getImporteFactura() * 100), 15, "0", STR_PAD_LEFT) . //Importe neto gravado 15 Numérico
                            $ivaCodigo . //Alícuota de IVA 4 Numérico
                            str_pad(str_replace('.', '', $iva->getValor() * 100), 15, "0", STR_PAD_LEFT) . //Impuesto liquidado 15 Numérico
                            PHP_EOL
                            , FILE_APPEND);
                }
            }
        }

        $response = new Response();
        $response->headers->set('Content-Description', 'File Transfer');
        $response->headers->set('Content-disposition', ' attachment;filename="' . $tipoTxt . '.txt"');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Length', filesize($tipoTxt . '.txt'));
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Expires', '0');
        $response->headers->set('Cache-Control', 'must-revalidate , post-check=0, pre-check=0');

        readfile($tipoTxt . '.txt');

        return $response;
    }

    /*
     * Muestra formulario para la generación del iva Ventas
     */

    public function ivaVentasAction(Request $request) {
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio()->getId();
        }
        if (isset($fechaDesde) && isset($fechaHasta) && isset($unidadNegocio)) {
            return $this->ivaVentasExcel($fechaDesde, $fechaHasta, $unidadNegocio);
        }

        return $this->render('AppBundle:Factura:ivaVentas.html.twig');
    }

    /*
     * Generación de archivo Excel con informe de IVA Ventas
     */

    public function ivaVentasExcel($fechaDesde, $fechaHasta, $unidadNegocio) {

        $em = $this->getDoctrine()->getManager();

        $ivas = $em->getRepository('AppBundle:Iva')->ivaVentas($fechaDesde, $fechaHasta, $unidadNegocio);

        if ($ivas) {
            // ask the service for a Excel5
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            $phpExcelObject->getProperties()->setCreator($this->container->getParameter('empresa_razonsocial'))
                    ->setLastModifiedBy($this->container->getParameter('empresa_razonsocial'))
                    ->setTitle("Informe de IVA Ventas")
                    ->setSubject($this->container->getParameter('empresa_razonsocial'));

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Fecha')
                    ->setCellValue('B1', 'Punto de Venta')
                    ->setCellValue('C1', 'Nro. Factura')
                    ->setCellValue('D1', 'Razón Social')
                    ->setCellValue('E1', 'CUIT')
                    ->setCellValue('F1', 'Importe')
                    ->setCellValue('G1', 'IVA')
                    ->setCellValue('H1', 'Porcentaje de IVA');

            $count = 2;
            foreach ($ivas as $iva) {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $iva->getFactura()->getFecha()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $iva->getFactura()->getPtovta())
                        ->setCellValue('C' . $count, $iva->getFactura()->getNrofactura())
                        ->setCellValue('D' . $count, $iva->getFactura()->getClienteProveedor()->getRazonSocial())
                        ->setCellValue('E' . $count, $iva->getFactura()->getClienteProveedor()->getCuit())
                        ->setCellValue('F' . $count, $iva->getFactura()->getImporte())
                        ->setCellValue('G' . $count, $iva->getValor())
                        ->setCellValue('H' . $count, $iva->getTipoIva()->getPorcentaje());
                $count++;
            }
            $phpExcelObject->setActiveSheetIndex(0);
            $phpExcelObject->getActiveSheet()->setTitle('Hoja1');
            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(20);

            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=ivaVentas.xlsx');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }

        $this->sessionManager->addFlash('msgError', 'No existen registros para mostrar.');
        return $this->render('AppBundle:Factura:ivaVentas.html.twig');
    }

    /*
     * Informe de Ventas
     */

    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }
        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $cliente = $request->get('cliente');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Factura')->filtroBusquedaInformeVenta($fechaDesde, $fechaHasta, $cliente, $unidadNegocio);
        $total = $em->getRepository('AppBundle:Factura')->totalInformeVenta($fechaDesde, $fechaHasta, $cliente, $unidadNegocio);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities, $total);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Venta');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Factura:informe.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total
        ));
    }

    public function informeCuentasPorCobrarAction(Request $request, $page = 1) {

        $em = $this->getDoctrine()->getManager();
        $cliente = $request->get('cliente');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $facturas = $em->getRepository('AppBundle:Factura')->getUltimaFactura('F', $cliente, $unidadNegocio);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelCuentasPorCobrar($facturas);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Cuentas por Cobrar');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($facturas));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Factura:informeCuentasPorCobrar.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     *  Generación de contenido archivo excel Informe de Venta
     */

    private function excelInforme($entities, $total) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Unidad Negocio');
        $sheet->setCellValue('E1', 'Importe');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity);
            $sheet->setCellValue('C' . $fila, $entity->getClienteProveedor());
            $sheet->setCellValue('D' . $fila, $entity->getUnidadNegocio());
            $sheet->setCellValue('E' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('D' . $fila, 'Total');
        $sheet->setCellValue('E' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('D' . $fila . ':E' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     *  Generación de contenido archivo excel Informe de Cuentas por cobrar
     */

    private function excelCuentasPorCobrar($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Unidad Negocio');
        $sheet->setCellValue('B1', 'Razón Social');
        $sheet->setCellValue('C1', 'Fecha Última Venta ($)');
        $sheet->setCellValue('D1', 'Fecha Última Cobranza');
        $sheet->setCellValue('E1', 'Última Cobranza ($)');
        $sheet->setCellValue('F1', 'Saldo Actual ($)');

        $fila = 2;
        $total = 0;
        foreach ($entities as $entity) {
            $cobranza = $this->getUltimaCobranza($entity[0]->getClienteProveedor()->getId());
            $sheet->setCellValue('A' . $fila, $entity[0]->getUnidadNegocio());
            $sheet->setCellValue('B' . $fila, $entity[0]->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, date_format($entity[0]->getFecha(), "d-m-Y"));
            $sheet->setCellValue('D' . $fila, (new \DateTime($cobranza['fecha']))->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $cobranza['importe']);
            $sheet->setCellValue('F' . $fila, $cobranza['saldo']);
            $total = $total + $cobranza['saldo'];
            $fila++;
        }

        //Totales
        $sheet->setCellValue('E' . $fila, 'Total');
        $sheet->setCellValue('F' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Obtengo última cobranza
     */

    private function getUltimaCobranza($id) {
        $em = $this->getDoctrine()->getManager();
        $cobranza = $em->getRepository('AppBundle:Cobranza')->getUltimaCobranza($id);

        $respuesta['fecha'] = '';
        $respuesta['importe'] = 0;
        $respuesta['saldo'] = 0;

        if (isset($id)) {
            $saldo = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
            $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
            $respuesta['saldo'] = round($clienteProveedor->getSaldo() + $saldo['cobranzas'] + $saldo['notacredito'] - $saldo['notadebito'] - $saldo['facturas'] + $saldo['facturasproveedor'] - $saldo['gastos'] - $saldo['pagos'], 2);
        }

        if (isset($cobranza) && !empty($cobranza)) {
            $respuesta['fecha'] = date_format($cobranza[0][0]->getFecha(), 'Y-m-d');
            $respuesta['importe'] = $cobranza[0][0]->getImporte();
        }

        return $respuesta;
    }

    public function predictivaSelect2Action(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $response = ['items' => []];

        if (strlen($request->get('search')) < 1) {
            return new JsonResponse($response);
        }
        $filter = [
            'tipo' => ["F"],
            'numerofactura' => $request->get('search'),
        ];

        $entities = $em->getRepository(Factura::class)->findByFilter($filter);

        foreach ($entities as $entity) {
            $cantidadFactura = $em->getRepository(ProductoFactura::class)->cantidadProductosEnFactura($entity->getId()) ?? 0;
            $cantidadConsignacion = $em->getRepository(ProductoConsignacion::class)->cantidadProductosEnConsignacion($entity->getId()) ?? 0;

            $response['items'][] = [
                'id' => $entity->getId(),
                'name' => strip_tags($entity) . ' - Cliente: ' . $entity->getClienteProveedor()->getRazonSocial() . " - Facturados (Consignados): $cantidadFactura ($cantidadConsignacion)",
                'full_name' => strip_tags($entity) . ' - Cliente: ' . $entity->getClienteProveedor()->getRazonSocial() . " - Facturados (Consignados): $cantidadFactura ($cantidadConsignacion)",
                'text' => strip_tags($entity) . ' - Cliente: ' . $entity->getClienteProveedor()->getRazonSocial() . " - Facturados (Consignados): $cantidadFactura ($cantidadConsignacion)",
            ];
        }

        $response['total_count'] = sizeof($entities);

        return new JsonResponse($response);
    }

}

?>
