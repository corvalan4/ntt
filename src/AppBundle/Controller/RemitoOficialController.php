<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Presupuesto;
use AppBundle\Entity\ProductoConsignacion;
use AppBundle\Entity\Stock;
use AppBundle\Entity\Sucursal;
use AppBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\RemitoOficial;
use AppBundle\Entity\ProductoRemito;
use AppBundle\Form\RemitoOficialType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * RemitoOficial controller.
 *
 */
class RemitoOficialController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all RemitoOficial entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $em = $this->getDoctrine()->getManager();

        $unidad = !$this->isGranted('ROLE_SUPER_ADMIN') ? $this->getUser()->getUnidadNegocio()->getId() : '';
        $fecha_desde = $request->get('fechaDesde') ?? '';
        $fecha_hasta = $request->get('fechaHasta') ?? '';
        $cliente = $request->get('cliente') ?? '';
        $estado = $request->get('estado') ?? '';
        $numero = $request->get('numero') ?? '';
        $filter = ['cliente' => $cliente, 'unidad' => $unidad, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'numero' => $numero, 'estado' => $estado];

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy([], ['descripcion' => 'asc']);

        $entities = $em->getRepository('AppBundle:RemitoOficial')->findByFilter($filter);

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcel($entities);
            return $this->container->get('session.manager')->exportarExcel($spreadSheet, 'Facturas-' . date('Ymd'));
        }

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:RemitoOficial:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
        ));
    }

    private function exportarExcel($entities) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Remito Oficial');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Estado');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('B' . $fila, "#" . strip_tags($entity));
            $sheet->setCellValue('C' . $fila, $entity->getClienteproveedor()->getRazonSocial());
            $sheet->setCellValue('D' . $fila, $entity->getEstado());

            $fila++;
        }

        return $spreadsheet;
    }

    /**
     * Creates a new RemitoOficial entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new RemitoOficial();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $desde_reserva = $request->get("desde_reserva");
        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

        $hayProductos = false;

        if ($form->isValid()) {
            for ($i = 1; $i <= 50; $i++) {
                $trazable = $request->get("trazable_elemento$i");
                $idproducto = $request->get("idprod_elemento$i");
                $idstock = $request->get("idstock_elemento$i");
                $consignacion_id = $request->get("consignacion_id_elemento$i");
                $vendedor_id = $request->get("vendedor_elemento$i");                
                $cantidad = (float) $request->get("cantidad_elemento$i");               
                $descripcion = $request->get("descripcion_elemento$i");
                $renglon = $request->get("renglon_elemento$i");

                if (!empty($idstock)) {
                    $elementostock = $em->getRepository('AppBundle:Stock')->find($idstock);

                    if ($desde_reserva) {
                        $reserva = $em->getRepository('AppBundle:Reserva')->findOneBy(['stock' => $idstock, 'cliente' => $entity->getClienteProveedor()->getId(), 'estado' => 'A']);
                        if ($reserva) {
                            $reserva->setEstado('F');
                        }
                    }

                    $vendedor = $vendedor_id ? $em->getRepository(Usuario::class)->find($vendedor_id) : null;

                    $productoRemito = new ProductoRemito();
                    $productoRemito->setDescripcion($descripcion);
                    $productoRemito->setProducto($elementostock->getProducto());
                    $productoRemito->setStock($elementostock);
                    $productoRemito->setRemitooficial($entity);
                    $productoRemito->setCantidad($cantidad);
                    $productoRemito->setCliente($sucursal->getClienteproveedor());
                    $productoRemito->setPrecio($elementostock->getProducto()->getPrecio());
                    $productoRemito->setVendedor($vendedor);
                    $productoRemito->setRenglon($renglon);

                    if (!empty($consignacion_id) and $consignacion_id != 0) {
                        $productoConsignacion = $em->getRepository(ProductoConsignacion::class)->findOneBy(['stock' => $elementostock->getId(), 'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK, 'consignacion' => $consignacion_id]);
                        $productoRemito->setProductoconsignacion($productoConsignacion);
                    } else {
                        $productoConsignaciones = $em->getRepository(ProductoConsignacion::class)->findBy(['stock' => $elementostock->getId(), 'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK, 'consignacion' => $consignacion_id]);
                        if (sizeof($productoConsignaciones) == 0) {
                            $additional = ["description" => "Alta de Remito Oficial", "remitooficial" => $entity];
                            $cantidad = $elementostock->getProducto()->getTrazable() ? $elementostock->getStock() : $cantidad;
                            $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $cantidad, Balance::TYPE_NEGATIVE, $additional);
                            $em->persist($balance);
                            $elementostock->setStock($balance->getAmountBalance());
                        }
                    }

                    $em->persist($productoRemito);
                    $hayProductos = true;
                }
            }

            if (!$hayProductos) {
                $this->sessionManager->addFlash('msgError', 'Debe agregar productos.');
                $vendedores = $em->getRepository('AppBundle:Usuario')->findBy([], ['apellidonombre' => 'ASC']);

                return $this->render('AppBundle:RemitoOficial:new.html.twig', array(
                            'entity' => $entity,
                            'form' => $form->createView(),
                            'stocks' => [],
                            'stock' => null,
                            'reservas' => 0,
                            'vendedores' => $vendedores
                ));
            }

            $em->persist($entity);
            $em->flush();

            $balances = $em->getRepository('AppBundle:Balance')->findBy(array('remitooficial' => $entity->getId()));
            foreach ($balances as $balance) {
                if ($balance->getStock()->getProducto()->getTrazable()) {
                    $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
                    $this->sessionManager->csvVerifarmaDespacho([$balance], "DES" . $namesheet);
                }
            }

            return $this->redirect($this->generateUrl('remitooficial'));
        }
    }

    /**
     * Creates a form to create a RemitoOficial entity.
     *
     * @param RemitoOficial $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RemitoOficial $entity) {
        $form = $this->createForm(new RemitoOficialType(), $entity, array(
            'action' => $this->generateUrl('remitooficial_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Confirmar Remito Oficial', 'attr' => array('class' => 'btn btn-success  btn-xs', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new RemitoOficial entity.
     *
     */
    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new RemitoOficial();
        $stock = null;
        $reservas = 0;
        if (!empty($request->get('stock_id'))) {
            $stock = $em->getRepository('AppBundle:Stock')->find($request->get('stock_id'));
            if ($stock) {
                $stockService = $this->container->get('stockservice');
                $reservas = (int) $stockService->reservaPorStock($stock->getId());
                $entity->setUnidadNegocio($stock->getUnidadnegocio());
            }
        }

        $stocks = [];
        $cliente = null;
        if (!empty($request->get('reservas'))) {
            foreach ($request->get('reservas') as $reserva_id) {
                $reserva = $em->getRepository('AppBundle:Reserva')->find($reserva_id);
                if ($reserva) {
                    if (!$cliente)
                        $cliente = $reserva->getCliente();
                    if ($cliente != $reserva->getCliente()) {
                        $this->addFlash("msgError", "No podes consignar reservas de clientes diferentes.");
                        return $this->redirectToRoute('reserva');
                    }
                    $stocks[] = ['stock' => $reserva->getStock(), 'cantidad' => $reserva->getCantidad()];
                    $entity->setClienteProveedor($cliente);
                }
            }

            if ($stock) {
                $stockService = $this->container->get('stockservice');
                $reservas = (int) $stockService->reservaPorStock($stock->getId());
                $entity->setUnidadNegocio($stock->getUnidadnegocio());
            }
        }

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }
        $form = $this->createCreateForm($entity);

        $vendedores = $em->getRepository('AppBundle:Usuario')->findBy([], ['apellidonombre' => 'ASC']);

        return $this->render('AppBundle:RemitoOficial:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'stock' => $stock,
                    'stocks' => $stocks,
                    'reservas' => $reservas,
                    'vendedores' => $vendedores
        ));
    }

    /**
     * Finds and displays a RemitoOficial entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RemitoOficial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:RemitoOficial:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a RemitoOficial entity.
     *
     */
    public function devolucionAction($productoRemitoId) {
        $em = $this->getDoctrine()->getManager();
        $productoRemito = $em->getRepository('AppBundle:ProductoRemito')->find($productoRemitoId);

        // Verifico la existencia del stock en la remitooficial.
        if (!$productoRemito) {
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('remitooficial');
        }

        $entity = $productoRemito->getRemitoOficial();
        $stock = $productoRemito->getStock();

        // Cambio el estado del stock en la cosignacion.
        $productoRemito->setEstado(ProductoRemito::PRODUCTO_RemitoOficial_ERROR);

        // La cantidad ahora es el total de la remitooficial, pero podrían ser menos
        // y aca tendrian que existir documentos intermedios.
        $cantidad = $productoRemito->getCantidad();

        // Devuelvo stock
        $additional = ["description" => "Devolución de Stock en RemitoOficial Cliente", "remitooficial" => $entity];

        $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, $cantidad, Balance::TYPE_POSITIVE, $additional);
        $em->persist($balance);
        $em->flush();
        $stock->setStock($balance->getAmountBalance());
        $em->flush();

        if ($stock->getProducto()->getTrazable()) {
            $namesheet = str_pad($balance->getId(), 8, "0", STR_PAD_LEFT);
            $this->sessionManager->csvVerifarmaDevolucion([$balance], 'DEV' . $namesheet);
        }

        $this->addFlash('msgOk', 'Devolución realizada.');

        return $this->redirectToRoute('remitooficial_show', ['id' => $entity->getId()]);
    }

    public function implantacionAction(Request $request) {
        $productoRemito_id = $request->get('productoRemito_id');
        $sucursal_id = $request->get('sucursal_id');
        $em = $this->getDoctrine()->getManager();
        $productoRemito = $em->getRepository('AppBundle:ProductoRemito')->find($productoRemito_id);
        $sucursal = $em->getRepository(Sucursal::class)->find($sucursal_id);

        // Verifico la existencia del stock en la remitooficial.
        if (!$productoRemito or!$sucursal) {
            $this->addFlash('msgWarn', 'Error de datos.');
            return $this->redirectToRoute('remitooficial');
        }

        $entity = $productoRemito->getRemitoOficial();
        $stock = $productoRemito->getStock();
        $productoRemito->setCliente($sucursal->getClienteproveedor());
        // Cambio el estado del stock en la cosignacion.
        $productoRemito->setEstado(ProductoRemito::PRODUCTO_RemitoOficial_OK);

        // La cantidad ahora es el total de la remitooficial, pero podrían ser menos
        // y aca tendrian que existir documentos intermedios.
        $cantidad = $productoRemito->getCantidad();

        // Devuelvo stock
        $additional = ["description" => "Uso de Stock en Remito Oficial Cliente", "remitooficial" => $entity];

        $balance = $this->sessionManager->registerBalance($stock, $this->getUser(), 0, 0, Balance::TYPE_NEGATIVE, $additional);
        $em->persist($balance);

        // Verifico el total de los productos en la remitooficial para cambiar el estado global.
        if (sizeof($entity->getProductosRemitoOficial()) == 1) {
            // Significa que es el unico producto en remitooficial.
            $entity->setEstado(RemitoOficial::REMITO_OFICIAL_OK);
        } else {
            $estadoRemitoOficial = RemitoOficial::REMITO_OFICIAL_OK;
            foreach ($entity->getProductosRemitoOficial() as $item) {
                
            }
            $entity->setEstado($estadoRemitoOficial);
        }

        $em->flush();

        $this->addFlash('msgOk', 'Uso confirmado.');

        return $this->redirectToRoute('remitooficial_show', ['id' => $entity->getId()]);
    }

    /**
     * Finds and displays a RemitoOficial entity.
     *
     */
    public function printAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        $productos = [];

        foreach ($entity->getProductosRemito() as $productoremito) {
            if ($productoremito->getStock()) {
                $productoconsignacion = $em->getRepository('AppBundle:ProductoConsignacion')->findOneBy(['stock' => $productoremito->getStock()->getId(), 'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK]);
            } else {
                $productoconsignacion = null;
            }
            $item = ['productoremito' => $productoremito, 'productoconsignacion' => $productoconsignacion];
            $productos[] = $item;
        }

        return $this->render('AppBundle:RemitoOficial:print.html.twig', array(
                    'entity' => $entity,
                    'productos' => $productos,
        ));
    }

    /**
     * Displays a form to edit an existing RemitoOficial entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        return $this->render('AppBundle:RemitoOficial:edit.html.twig', array(
                    'entity' => $entity,
                    'algo' => 'editar',
        ));
    }

    public function editNumberAction(Request $request, $id) {
        if (!empty($request->get('remitooficial_numero'))) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);
            $entity->setNumero($request->get('remitooficial_numero'));
            $em->flush();
        }

        return $this->redirect($this->generateUrl('remitooficial_show', array('id' => $id)));
    }

    public function facturarAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        return $this->render('AppBundle:RemitoOficial:edit.html.twig', array(
                    'entity' => $entity,
                    'algo' => 'facturar',
        ));
    }

    public function cerrarAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);
        $entity->setEstado('I');
        $cant = 0;
        $stock = 0;


        foreach ($entity->getProductosRemitoOficial() as $prodCon) {
            $stock = $prodCon->getProducto()->getStock();
            $cant = $stock + $prodCon->getCantidad();
            $prodCon->getProducto()->setStock($cant);
        }

        $em->flush();
        $this->addFlash('msgOk', 'La remitooficial ' . $entity->getId() . ' fue cerrada.');
        return $this->redirect($this->generateUrl('remitooficial'));
    }

    /**
     * Creates a form to edit a RemitoOficial entity.
     *
     * @param RemitoOficial $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(RemitoOficial $entity) {
        $form = $this->createForm(new RemitoOficialType(), $entity, array(
            'action' => $this->generateUrl('remitooficial_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing RemitoOficial entity.
     *
     */
    public function updateAction(Request $request, $id) {
        return $this->redirect($this->generateUrl('remitooficial'));
    }

    /**
     * Deletes a RemitoOficial entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        $presupuesto = $em->getRepository(Presupuesto::class)->findOneBy(['remitoOficial' => $id]);
        if ($presupuesto) {
            $presupuesto->setEstado(Presupuesto::PRESUPUESTO_ACTIVO);
            $presupuesto->setRemitoOficial(null);
        }

        $devoluciones = [];

        foreach ($entity->getProductosRemito() as $prodRemito) {
            if ($prodRemito->getStock()) {
                $productoConsignaciones = $em->getRepository(ProductoConsignacion::class)->findBy(['stock' => $prodRemito->getStock()->getId(), 'estado' => ProductoConsignacion::PRODUCTO_CONSIGNACION_OK]);
                if ($prodRemito->getProductoconsignacion()) {
                    $prodRemito->setProductoconsignacion(null);
                } else {
                    if (sizeof($productoConsignaciones) == 0) {
                        $additional = ["description" => "Anulación de Remito Oficial", "remitooficial" => $entity];

                        $balance = $this->sessionManager->registerBalance($prodRemito->getStock(), $this->getUser(), 0, $prodRemito->getCantidad(), Balance::TYPE_POSITIVE, $additional);
                        $em->persist($balance);
                        $prodRemito->getStock()->setStock($prodRemito->getStock()->getStock() + $prodRemito->getCantidad());

                        if ($prodRemito->getStock()->getProducto()->getTrazable()) {
                            $devoluciones[] = $balance;
                        }
                    }
                }
            }
            $prodRemito->setEstado(ProductoRemito::PRODUCTO_REMITO_CANCEL);
        }

        $entity->setEstado(RemitoOficial::CANCEL);
        $em->flush();

        if (sizeof($devoluciones) > 0) {
            $namesheet = str_pad($devoluciones[(sizeof($devoluciones) - 1)]->getId(), 8, "0", STR_PAD_LEFT);
            $this->sessionManager->csvVerifarmaDevolucion($devoluciones, 'DEV' . $namesheet);
        }

        $this->addFlash('msgOk', 'El remito oficial se anuló de forma exitosa');
        return $this->redirect($this->generateUrl('remitooficial'));
    }

    /**
     * Creates a form to delete a RemitoOficial entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('remitooficial_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Cancela un remito.
     *
     */
    public function cancelarAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:RemitoOficial')->find($id);

        $entity->setEstado(RemitoOficial::NO_FACTURAR);
        $em->flush();

        $this->addFlash('msgOk', 'El remito oficial se canceló de forma exitosa');
        return $this->redirect($this->generateUrl('remitooficial'));
    }

}
