<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoGasto;
use AppBundle\Form\TipoGastoType;

use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * TipoGasto controller.
 *
 */
class TipoGastoController extends Controller
{
    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all TipoGasto entities.
     *
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoGasto')->findAll();

        return $this->render('AppBundle:TipoGasto:index.html.twig', array(
            'entities' => $entities,
        ));

    }
    /**
     * Creates a new TipoGasto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoGasto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Alta satisfactoria, puede continuar.');

            $entity = new TipoGasto();
            $form = $this->createCreateForm($entity);

            return $this->render('AppBundle:TipoGasto:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }

        return $this->render('AppBundle:TipoGasto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoGasto entity.
     *
     * @param TipoGasto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoGasto $entity)
    {
        $form = $this->createForm(new TipoGastoType(), $entity, array(
            'action' => $this->generateUrl('tipogasto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new TipoGasto entity.
     *
     */
    public function newAction()
    {


        $entity = new TipoGasto();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoGasto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

    }

    /**
     * Finds and displays a TipoGasto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoGasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoGasto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoGasto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoGasto entity.
     *
     */
    public function editAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoGasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoGasto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoGasto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a TipoGasto entity.
     *
     * @param TipoGasto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(TipoGasto $entity)
    {
        $form = $this->createForm(new TipoGastoType(), $entity, array(
            'action' => $this->generateUrl('tipogasto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing TipoGasto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoGasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoGasto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipogasto'));
        }

        return $this->render('AppBundle:TipoGasto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a TipoGasto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoGasto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoGasto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipogasto'));
    }

    /**
     * Creates a form to delete a TipoGasto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipogasto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr'=> array('class'=>'btn')))
            ->getForm()
            ;
    }
     /*
     * Render Select 
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $tiposGastos = $em->getRepository('AppBundle:TipoGasto')->findBy(array('estado' => $estado), array('nombre' => 'ASC'));
        return $this->render('AppBundle:TipoGasto:select.html.twig', array(
                    'tiposGastos' => $tiposGastos
        ));
    }
}
