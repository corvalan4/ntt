<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Dolar;
use AppBundle\Form\DolarType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Dolar controller.
 *
 */
class DolarController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Dolar entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $em = $this->getDoctrine()->getManager();
        $dolares = $em->getRepository('AppBundle:Dolar')->filtro($fechaDesde, $fechaHasta);

        $adapter = new ArrayAdapter($dolares);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Dolar:index.html.twig', array(
                    'dolares' => $paginador
        ));
    }

    /**
     * Creates a new Dolar entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Dolar();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dolar'));
        }

        return $this->render('AppBundle:Dolar:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Dolar entity.
     *
     * @param Dolar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Dolar $entity) {
        $form = $this->createForm(new DolarType(), $entity, array(
            'action' => $this->generateUrl('dolar_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary btn-xs', 'onclick' => 'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Dolar entity.
     *
     */
    public function newAction() {
        $entity = new Dolar();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Dolar:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    
    /**
     * Displays a form to edit an existing Dolar entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Dolar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dolar entity.');
        }

        $editForm = $this->createEditForm($entity);      

        return $this->render('AppBundle:Dolar:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a Dolar entity.
     *
     * @param Dolar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Dolar $entity) {
        $form = $this->createForm(new DolarType(), $entity, array(
            'action' => $this->generateUrl('dolar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Dolar entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Dolar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dolar entity.');
        }
       
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dolar'));
        }

        return $this->render('AppBundle:Dolar:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

 
    /**
     * Cambia el estado del Dolar
     *
     */
    public function cambiarEstadoAction(Request $request, $id) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();

        $dolar = $em->getRepository('AppBundle:Dolar')->find($id);
        if ($dolar) {
            $dolar->setEstado($estado);
            $em->flush();
            $this->addFlash("msgOk", "Se ha eliminado la cotización exitosamente.");
        } else {
            $this->addFlash("msgError", "No se ha podido concretar la eliminación de la cotizaición.");
        }

        return $this->redirect($this->generateUrl('dolar'));
    }

}
