<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Precio;
use AppBundle\Form\AduanaType;

/**
 * Aduana controller.
 *
 */
class AduanaController extends Controller
{

    /**
     * Lists all Aduana entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Aduana')->findAll();

        return $this->render('AppBundle:Aduana:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Aduana entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Precio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('aduana_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Aduana:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Aduana entity.
     *
     * @param Precio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Precio $entity)
    {
        $form = $this->createForm(new AduanaType(), $entity, array(
            'action' => $this->generateUrl('aduana_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Aduana entity.
     *
     */
    public function newAction()
    {
        $entity = new Precio();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Aduana:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Aduana entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Aduana')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aduana entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Aduana:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Aduana entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Aduana')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aduana entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Aduana:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Aduana entity.
     *
     * @param Precio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Precio $entity)
    {
        $form = $this->createForm(new AduanaType(), $entity, array(
            'action' => $this->generateUrl('aduana_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Aduana entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Aduana')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aduana entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('aduana_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Aduana:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Aduana entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Aduana')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Aduana entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('aduana'));
    }

    /**
     * Creates a form to delete a Aduana entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('aduana_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
