<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProductoConsignacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * ProductoConsignacion controller.
 *
 */
class ProductoConsignacionController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @var ProductoConsignacionService
     * @DI\Inject("productoconsignacionservice")
     */
    public $productoConsignacionService;

    /*
     * Informe Depósitos
     */

    public function informeConsignadosAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();
        $page = $request->get('page') ? $request->get('page') : 1;
        $filtros = $request->query->all();

        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:ProductoConsignacion')->filtroBusquedaInformeConsignados($filtros);        
        
        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcelInformeConsignados($entities, $filtros);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe Consignados');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:ProductoConsignacion:informeConsignados.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Exportación a excel de Informe de Depósitos
     */

    private function exportarExcelInformeConsignados($entities, $filtros) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Proveedor');
        $sheet->setCellValue('B1', 'Modelo');
        $sheet->setCellValue('C1', 'Código');
        $sheet->setCellValue('D1', 'N° Lote');
        $sheet->setCellValue('E1', 'Unidades Totales');
        $sheet->setCellValue('F1', 'Nro. Interno (Serie)');
        $sheet->setCellValue('G1', 'Fecha de Vencimiento');
        $sheet->setCellValue('H1', 'Remito Nro.');
        $sheet->setCellValue('I1', 'Fecha de Consignación');
        $sheet->setCellValue('J1', 'Vendedor');
        
        $sheet->setCellValue('L1', 'Cliente');
       
        $sheet->setCellValue('M1', 'Unidades');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('N1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            $productosConsignacion = $this->productoConsignacionService->productosConsignacionesByProveedorModeloCodigo($entity->getProducto()->getProveedor()->getId(), $entity->getProducto()->getCodigo(), $entity->getProducto()->getModelo(), $filtros);            
            foreach ($productosConsignacion as $productoConsignacion) {
                $sheet->setCellValue('A' . $fila, $entity->getProducto()->getProveedor());
                $sheet->setCellValue('B' . $fila, $entity->getProducto()->getModelo());
                $sheet->setCellValue('C' . $fila, $entity->getProducto()->getCodigo());
                $sheet->setCellValue('D' . $fila, $entity->getStock()->getLote());                
                $sheet->setCellValue('E' . $fila, $this->productoConsignacionService->sumConsignadosByProveedorCodigoModeloLote($entity->getProducto()->getProveedor()->getId(), $entity->getProducto()->getCodigo(), $entity->getProducto()->getModelo(), $entity->getStock()->getLote(), $filtros,false));
                $sheet->setCellValue('F' . $fila, $productoConsignacion->getStock()->getSerie());
                $sheet->setCellValue('G' . $fila, $productoConsignacion->getStock()->getVencimiento() ? $productoConsignacion->getStock()->getVencimiento()->format("d/m/Y") : '');
                $sheet->setCellValue('H' . $fila, $productoConsignacion->getConsignacion()->getId());
                $sheet->setCellValue('I' . $fila, $productoConsignacion->getStock()->getFecha() ? $productoConsignacion->getStock()->getFecha()->format("d/m/Y") : '');
                $sheet->setCellValue('J' . $fila, $productoConsignacion->getConsignacion()->getVendedor());

                $sheet->setCellValue('L' . $fila, $productoConsignacion->getConsignacion()->getClienteProveedor());
               
                $sheet->setCellValue('M' . $fila, $productoConsignacion->getCantidad());
                if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                    $sheet->setCellValue('N' . $fila, $productoConsignacion->getConsignacion()->getUnidadNegocio());
                }
                $fila++;
            }
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:N1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        //$sheet->getColumnDimension('O')->setAutoSize(true);

        return $spreadsheet;
    }

}
