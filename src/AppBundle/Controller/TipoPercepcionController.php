<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoPercepcion;
use AppBundle\Form\TipoPercepcionType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * TipoPercepcion controller.
 *
 */
class TipoPercepcionController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;
    
    /**
     * Lists all TipoPercepcion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoPercepcion')->findAll();

        return $this->render('AppBundle:TipoPercepcion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoPercepcion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoPercepcion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipopercepcion'));
        }

        return $this->render('AppBundle:TipoPercepcion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoPercepcion entity.
     *
     * @param TipoPercepcion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoPercepcion $entity)
    {
        $form = $this->createForm(new TipoPercepcionType(), $entity, array(
            'action' => $this->generateUrl('tipopercepcion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Displays a form to create a new TipoPercepcion entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoPercepcion();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoPercepcion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoPercepcion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoPercepcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoPercepcion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoPercepcion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoPercepcion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoPercepcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoPercepcion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoPercepcion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoPercepcion entity.
    *
    * @param TipoPercepcion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoPercepcion $entity)
    {
        $form = $this->createForm(new TipoPercepcionType(), $entity, array(
            'action' => $this->generateUrl('tipopercepcion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing TipoPercepcion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoPercepcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoPercepcion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipopercepcion'));
        }

        return $this->render('AppBundle:TipoPercepcion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoPercepcion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoPercepcion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoPercepcion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipopercepcion'));
    }

    /**
     * Creates a form to delete a TipoPercepcion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipopercepcion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
