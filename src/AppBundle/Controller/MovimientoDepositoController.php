<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\MovimientoDeposito;
use AppBundle\Form\MovimientoDepositoType;

/**
 * MovimientoDeposito controller.
 *
 */
class MovimientoDepositoController extends Controller
{

    /**
     * Lists all MovimientoDeposito entities.
     *
     */
    public function indexAction(Request $request)
    {
        $stock_id = $request->get('stock_id');
        if(!$stock_id) return $this->redirectToRoute('producto');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:MovimientoDeposito')->findBy(['stock'=>$stock_id], ['id'=>'desc']);
        $stock = $em->getRepository('AppBundle:Stock')->find($stock_id);

        return $this->render('AppBundle:MovimientoDeposito:index.html.twig', array(
            'entities' => $entities,
            'stock' => $stock,
        ));
    }
    /**
     * Creates a new MovimientoDeposito entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MovimientoDeposito();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->getStock()->setDeposito($entity->getDepositoHasta());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stock_show', array('id' => $entity->getStock()->getId())));
        }

        return $this->render('AppBundle:MovimientoDeposito:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MovimientoDeposito entity.
     *
     * @param MovimientoDeposito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MovimientoDeposito $entity)
    {
        $form = $this->createForm(new MovimientoDepositoType(), $entity, array(
            'action' => $this->generateUrl('movimientodeposito_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        return $form;
    }

    /**
     * Displays a form to create a new MovimientoDeposito entity.
     *
     */
    public function newAction(Request $request)
    {
        $stock_id = $request->get('stock_id');
        if(!$stock_id) return $this->redirectToRoute('producto');
        $stock = $this->getDoctrine()->getManager()->getRepository('AppBundle:Stock')->find($stock_id);

        $entity = new MovimientoDeposito();
        $entity->setStock($stock);
        $entity->setDepositoDesde($stock->getDeposito());
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:MovimientoDeposito:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MovimientoDeposito entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoDeposito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoDeposito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:MovimientoDeposito:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MovimientoDeposito entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoDeposito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoDeposito entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:MovimientoDeposito:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a MovimientoDeposito entity.
    *
    * @param MovimientoDeposito $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MovimientoDeposito $entity)
    {
        $form = $this->createForm(new MovimientoDepositoType(), $entity, array(
            'action' => $this->generateUrl('movimientodeposito_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        return $form;
    }
    /**
     * Edits an existing MovimientoDeposito entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoDeposito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoDeposito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('movimientodeposito_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:MovimientoDeposito:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a MovimientoDeposito entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:MovimientoDeposito')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MovimientoDeposito entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('movimientodeposito'));
    }

    /**
     * Creates a form to delete a MovimientoDeposito entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('movimientodeposito_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
