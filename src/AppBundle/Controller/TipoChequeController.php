<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoCheque;
use AppBundle\Form\TipoChequeType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * TipoCheque controller.
 *
 */
class TipoChequeController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;	

    /**
     * Lists all TipoCheque entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion' => 'ASC'));

        return $this->render('AppBundle:TipoCheque:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoCheque entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoCheque();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipocheque'));
        }

        return $this->render('AppBundle:TipoCheque:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoCheque entity.
     *
     * @param TipoCheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoCheque $entity)
    {
        $form = $this->createForm(new TipoChequeType(), $entity, array(
            'action' => $this->generateUrl('tipocheque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new TipoCheque entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoCheque();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoCheque:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoCheque entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoCheque:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoCheque entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoCheque:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoCheque entity.
    *
    * @param TipoCheque $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoCheque $entity)
    {
        $form = $this->createForm(new TipoChequeType(), $entity, array(
            'action' => $this->generateUrl('tipocheque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing TipoCheque entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipocheque'));
        }

        return $this->render('AppBundle:TipoCheque:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoCheque entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoCheque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoCheque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipocheque'));
    }

    /**
     * Creates a form to delete a TipoCheque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipocheque_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
     /*
     * Render Select 
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $tiposCheque = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:TipoCheque:select.html.twig', array(
                    'tiposCheque' => $tiposCheque
        ));
    }
}
