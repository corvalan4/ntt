<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoIva;
use AppBundle\Form\TipoIvaType;

/**
 * TipoIva controller.
 *
 */
class TipoIvaController extends Controller
{

    /**
     * Lists all TipoIva entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoIva')->findAll();

        return $this->render('AppBundle:TipoIva:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoIva entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoIva();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoiva_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:TipoIva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoIva entity.
     *
     * @param TipoIva $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoIva $entity)
    {
        $form = $this->createForm(new TipoIvaType(), $entity, array(
            'action' => $this->generateUrl('tipoiva_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoIva entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoIva();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoIva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoIva entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIva entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoIva:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoIva entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIva entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoIva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoIva entity.
    *
    * @param TipoIva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoIva $entity)
    {
        $form = $this->createForm(new TipoIvaType(), $entity, array(
            'action' => $this->generateUrl('tipoiva_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoIva entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIva entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoiva_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:TipoIva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoIva entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoIva')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoIva entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoiva'));
    }

    /**
     * Creates a form to delete a TipoIva entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoiva_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
