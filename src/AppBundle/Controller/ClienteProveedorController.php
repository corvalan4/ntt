<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Entity\Sucursal;
use AppBundle\Form\ClienteProveedorType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * ClienteProveedor controller.
 *
 */
class ClienteProveedorController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all ClienteProveedor entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        $busqueda = $request->get("busqueda");
        $tipo = $request->get("tipo");
        $banco = $request->get("banco");

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:ClienteProveedor')->findByFilter(array("busqueda" => $busqueda, "tipo" => $tipo, "banco"=>$banco));

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:ClienteProveedor:index.html.twig', array(
                    'entities' => $paginador,
        ));
    }

    /**
     * Creates a new ClienteProveedor entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new ClienteProveedor();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getClienteProveedor() != '1' and $entity->getClienteProveedor() != '2') {
                $this->sessionManager->addFlash('msgWarn', 'Por favor vuelva a seleccionar Cliente/Proveedor.');
                return $this->render('AppBundle:ClienteProveedor:new.html.twig', array(
                            'entity' => $entity,
                            'form' => $form->createView(),
                ));
            }

            $sucursal = new Sucursal();
            $sucursal->setContacto('Asignar CONTACTO');
            $sucursal->setDireccion($entity->getDomiciliocomercial());
            $sucursal->setMail($entity->getMail());
            $sucursal->setTelefono($entity->getTelefono());
            $sucursal->setLocalidad($entity->getLocalidad());
            $sucursal->setProvincia($entity->getProvincia());
            $sucursal->setCodigopostal($entity->getCodigopostal());
            $sucursal->setClienteProveedor($entity);
            $em->persist($entity);
            $em->persist($sucursal);
            $em->flush();

            return $this->redirect($this->generateUrl('clienteproveedor_show', array('id' => $entity->getId())));
        }
        return $this->render('AppBundle:ClienteProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ClienteProveedor entity.
     *
     * @param ClienteProveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ClienteProveedor $entity) {
        $form = $this->createForm(new ClienteProveedorType(), $entity, array(
            'action' => $this->generateUrl('clienteproveedor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary btn-xs', 'onclick' => 'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new ClienteProveedor entity.
     *
     */
    public function newAction() {
        $entity = new ClienteProveedor();
        $form = $this->createCreateForm($entity);
     

        return $this->render('AppBundle:ClienteProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ClienteProveedor entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:ClienteProveedor:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

  /*  public function ccAction(Request $request, $id) {

        $sinCompensar = $request->get("sinCompensar");
        $desde = $request->get("desde");
        $tipoMovimiento = $request->get("tipo_movimiento") ? $request->get("tipo_movimiento") : 'TODOS';
        $hasta = $request->get("hasta");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

        if (empty($hasta)) {
            $ultimo = $em->getRepository('AppBundle:PagoAsignacion')->findOneBy(array('clienteProveedor' => $id), array('fecha' => 'DESC'));
            $hasta = $ultimo ? $ultimo->getFecha() : new \DateTime("NOW");
        } else {
            $hasta = new \DateTime($hasta);
        }
        if (empty($desde)) {
            $desde = new \DateTime($hasta->format('Y-m-d') . " -90 days");
        } else {
            $desde = new \DateTime($desde);
        }

        $filter = array("desde" => $desde, "hasta" => $hasta, "tipomovimiento" => $tipoMovimiento);
        
        $movimientos = $em->getRepository('AppBundle:PagoAsignacion')->findByClienteProveedor($id, $filter);
        $saldos = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
       
        return $this->render('AppBundle:ClienteProveedor:cuentacorriente.html.twig', array(
                    'entity' => $entity,
                    'movimientos' => $movimientos,
                    'saldos' => $saldos,
        ));
    }*/

    public function imprimirAction($id) {
        $em = $this->getDoctrine()->getManager();

        $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Willy Jhons")
                ->setLastModifiedBy("Willy Jhons")
                ->setTitle("Informe")
                ->setSubject("Willy Jhons");

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Cuenta Corriente: "' . $clienteProveedor->getRazonSocial() . '"')
                ->setCellValue('D1', 'Pesos ($)')
                ->setCellValue('G1', 'Dolar(u$s)');

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Fecha')
                ->setCellValue('B2', 'Movimiento')
                ->setCellValue('C2', 'Debe')
                ->setCellValue('D2', 'Haber')
                ->setCellValue('E2', 'Saldo')
                ->setCellValue('F2', 'Debe')
                ->setCellValue('G2', 'Haber')
                ->setCellValue('H2', 'Saldo');

        $count = 3;
        $suma = 0;

        $pesosD = 0;
        $pesosH = 0;
        $dolaresD = 0;
        $dolaresH = 0;
        $tipo = '';

        if ($clienteProveedor->getSaldo() != '' and $clienteProveedor->getSaldo() > 0) {
            $tipo = 'SALDO INICIAL';
            if ($clienteProveedor->getMoneda() == '1') {
                $pesosD = $pesosD + $clienteProveedor->getSaldo();
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B' . $count, $tipo)
                        ->setCellValue('C' . $count, '$ ' . $clienteProveedor->getSaldo())
                        ->setCellValue('E' . $count, '$ ' . round($pesosD, 2));
            } else {
                $dolaresD = $dolaresD + $clienteProveedor->getSaldo();
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B' . $count, $tipo)
                        ->setCellValue('F' . $count, 'u$s ' . $clienteProveedor->getSaldo())
                        ->setCellValue('H' . $count, 'u$s ' . round($dolaresD, 2));
            }
            $count++;
        } else {
            $tipo = 'SALDO INICIAL';
            if ($clienteProveedor->getSaldo() != '' and $clienteProveedor->getSaldo() < 0) {
                if ($clienteProveedor->getMoneda == '1') {
                    $pesosH = $pesosH + ($clienteProveedor->getSaldo() * (-1));
                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('B' . $count, $tipo)
                            ->setCellValue('D' . $count, '$ ' . $clienteProveedor->getSaldo() * (-1))
                            ->setCellValue('E' . $count, '$ ' . round($pesosD, 2));
                } else {
                    $dolaresH = $dolaresH + ($clienteProveedor->getSaldo() * (-1));
                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('B' . $count, $tipo)
                            ->setCellValue('G' . $count, 'u$s ' . $clienteProveedor->getSaldo() * (-1))
                            ->setCellValue('H' . $count, 'u$s ' . round($dolaresH, 2));
                }
                $count++;
            }
        }
        foreach ($clienteProveedor->getMovimientoscc() as $entity) {
            if ($entity->getTipoDocumento() == 'C') {
                $tipo = 'NOTA DE CREDITO ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'D') {
                $tipo = 'NOTA DE DEBITO ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'FC' or $entity->getTipoDocumento() == 'F') {
                $tipo = 'FACTURA ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'R') {
                $tipo = 'RECIBO CLIENTE ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'RP') {
                $tipo = 'RECIBO PROV ' . $entity->getId();
            }

            if ($entity->getTipoDocumento() == 'FC') {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $entity->getFactura()->getFecha()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $tipo);
            } else {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $entity->getDocumento()->getFecha()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $tipo);
            }

            if ($entity->getEstado() == 'A' and $entity->getClienteProveedor()->getClienteProveedor() == '2') {
                if ($entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'C') {
                    if ($entity->getMonedaStr() == 'ARG') {
                        $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                    } else {
                        $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                    }
                }
                if ($entity->getTipoDocumento() == 'F' or $entity->getTipoDocumento() == 'D') {
                    if ($entity->getMonedaStr() == 'ARG') {
                        $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                    } else {
                        $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                    }
                }
            } else {
                if ($entity->getEstado() == 'A' and $entity->getClienteProveedor()->getClienteProveedor() == '1') {
                    if ($entity->getTipoDocumento() == 'D' or $entity->getTipoDocumento() == 'FC') {
                        $this->sessionManager->setSession('segundoIF cliente', 'SI');
                        if ($entity->getMonedaStr() == 'ARG') {
                            if ($entity->getTipoDocumento() == 'D') {
                                $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                                $operacion = $pesosD - $pesosH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                            } else {
                                $importeFC = $entity->getFactura()->getImporte();

                                $pesosD = $importeFC + $pesosD;
                                $operacion = $pesosD - $pesosH;
                                $this->sessionManager->setSession('importefactura' . $entity->getId(), $importeFC);
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('C' . $count, '$ ' . round($importeFC, 2))
                                        ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                            }
                        } else {
                            if ($entity->getTipoDocumento() == 'D') {
                                $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                            } else {
                                $importeFC = 0;
                                foreach ($entity->getFactura()->getProductosFactura() as $prodFact) {
                                    $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                                }

                                if ($entity->getFactura()->getDescuento() != '') {
                                    $desc = '0.' . (100 - $entity->getFactura()->getDescuento());
                                    $importeFC = $importeFC * $desc;
                                }

                                if ($entity->getFactura()->getBonificacion() != '') {
                                    $importeFC = $importeFC - $entity->getFactura()->getBonificacion();
                                }

                                $dolaresD = $importeFC + $dolaresD;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('F' . $count, 'U$S ' . round($importeFC, 2))
                                        ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                            }
                        }
                    }
                    if ($entity->getTipoDocumento() == 'R' or $entity->getTipoDocumento() == 'C') {
                        if ($entity->getMonedaStr() == 'ARG') {
                            $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                        } else {
                            $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                        }
                    }
                }
            }
            $count++;
        }


        $sumaPesos = $pesosD - $pesosH;
        $sumaDolares = $dolaresD - $dolaresH;

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('D' . $count, 'SALDO CAJA $ ' . round($sumaPesos, 2))
                ->setCellValue('G' . $count, 'SALDO CAJA U$S ' . round($sumaDolares, 2));

        $phpExcelObject->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');

        $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->getStyle('D' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getStyle('G' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=CuentaCorriente-' . $clienteProveedor->getRazonSocial() . '.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * Displays a form to edit an existing ClienteProveedor entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
        $cuit = $entity->getCuit();
        $entity->setCuit(str_replace('-', '', $cuit));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:ClienteProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a ClienteProveedor entity.
     *
     * @param ClienteProveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ClienteProveedor $entity) {
        $form = $this->createForm(new ClienteProveedorType(), $entity, array(
            'action' => $this->generateUrl('clienteproveedor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing ClienteProveedor entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $cuit = $entity->getCuit();
        $entity->setCuit(str_replace('-', '', $cuit));

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $em->flush();
        $this->sessionManager->addFlash('msgOk', 'Edicion realizada.');
        return $this->redirect($this->generateUrl('clienteproveedor'));
    }

    /**
     * Deletes a ClienteProveedor entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clienteproveedor'));
    }

    /**
     * Creates a form to delete a ClienteProveedor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('clienteproveedor_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function filtroAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->get('razonsocial') != '') {
            $entities = $em->getRepository('AppBundle:ClienteProveedor')->filtro($request->get('razonsocial'));

            return $this->render('AppBundle:ClienteProveedor:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            return $this->redirect($this->generateUrl('clienteproveedor'));
        }
    }

    public function predictivaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $string = '<ul>';
        if (strlen($request->get('valor')) > 0) {
            $entities = $em->getRepository('AppBundle:ClienteProveedor')->predictiva($request->get('valor'), $request->get('tipo'));
            foreach ($entities as $entity) {
                if ($request->get('tipo') == 2) {
                    $string = $string . "<li class=suggest-element id='" . $entity->getId() . "' condicion='" . $entity->getClienteProveedor()->getCondicionIva() . "'><a href='#' id='predictivacliente' data='" . $entity->getId() . "' >" . $entity->getClienteProveedor()->getRazonSocial()." - Suc. Id. ".$entity->getId();
                } else {
                    $lista = $entity->getClienteProveedor()->getListaprecio() ? $entity->getClienteProveedor()->getListaprecio()->getId() : '0';
                    $string = $string . "<li class=suggest-element id='" . $entity->getId() . "' lista='" . $lista . "' condicion='" . $entity->getClienteProveedor()->getCondicionIva() . "'><a href='#' id='predictivacliente' data='" . $entity->getId() . "' >" . $entity->getClienteProveedor()->getRazonSocial() . " - " . $entity->getDireccion();
                }
            }
            $string = $string . '</ul>';
        }
        return new Response($string);
    }

    /*
     * Render Select 
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $clienteProveedor = $request->get('clienteProveedor');
        $banco = $request->get('banco');        

        $em = $this->getDoctrine()->getManager();
        if ($clienteProveedor == 0) {
            $clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('estado' => $estado), array('razonSocial' => 'ASC'));
            $opcion = 'Cliente/Proveedor';
        } else {                        
            if ($clienteProveedor == 1) {
                if($banco){
                    $clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('estado' => $estado, 'clienteProveedor' => $clienteProveedor,'es_banco'=>true), array('razonSocial' => 'ASC'));
                    $opcion = 'Banco';
                }else {
                    $clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('estado' => $estado, 'clienteProveedor' => $clienteProveedor,'es_banco'=>false), array('razonSocial' => 'ASC'));
                    $opcion = 'Cliente';
                }                
            } else {
                $clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('estado' => $estado, 'clienteProveedor' => $clienteProveedor), array('razonSocial' => 'ASC'));    
                $opcion = 'Proveedor';
            }
        }

        return $this->render('AppBundle:ClienteProveedor:select.html.twig', array(
                    'clientesProveedores' => $clientesProveedores,
                    'opcion' => $opcion
        ));
    }

   

}
