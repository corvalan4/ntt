<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Caja;
use AppBundle\Form\CajaType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Caja controller.
 *
 */
class CajaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Caja entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $this->getUser()->getUnidadNegocio()->getId();
        } else {
            $unidadNegocio = $request->get('unidadNegocio');
        }

        $entities = $em->getRepository('AppBundle:Caja')->filtro($fechaDesde, $fechaHasta, $estado, $unidadNegocio);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Caja:index.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /**
     * Creates a new Caja entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $cajaActiva = $em->getRepository('AppBundle:Caja')->findBy(array('unidadNegocio' => $request->get('unidadNegocio'), 'estado' => 'A'));
        if (isset($cajaActiva)) {
            $this->sessionManager->addFlash('msgWarn', 'Se debe cerrar la caja activa antes de poder crear una nueva.');
            return $this->redirect($this->generateUrl('caja'));
        }

        $entity = new Caja();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->sessionManager->addFlash('msgOk', 'Se creado una caja exitosamente.');
            return $this->redirect($this->generateUrl('caja'));
        }
        $this->sessionManager->addFlash('msgError', 'Los datos ingresados no son correctos.');

        return $this->render('AppBundle:Caja:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Caja entity.
     *
     * @param Caja $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Caja $entity) {
        $form = $this->createForm(new CajaType(), $entity, array(
            'action' => $this->generateUrl('caja_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Crear Caja', 'attr' => array('class' => 'btn btn-success  btn-xs pull-right')));

        return $form;
    }

    /**
     * Displays a form to create a new Caja entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_ADMIN')) {
            $cajaActiva = $em->getRepository('AppBundle:Caja')->findBy(array('unidadNegocio' => $this->getUser()->getUnidadNegocio(), 'estado' => 'A'));
            if (isset($cajaActiva)) {
                $this->sessionManager->addFlash('msgWarn', 'Se debe cerrar la caja activa antes de poder crear una nueva.');
                return $this->redirect($this->generateUrl('caja'));
            }
        }

        $entity = new Caja();
        $entity->setFecha(new \DateTime("NOW"));
        $unidadNegocio = $this->getUser()->getUnidadNegocio();
        if (isset($unidadNegocio)) {
            $entity->setUnidadNegocio($this->getUser()->getUnidadNegocio());
        }

        $form = $this->createCreateForm($entity);
        return $this->render('AppBundle:Caja:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Caja entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Caja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Caja:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Caja entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Caja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Caja:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Caja entity.
     *
     * @param Caja $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Caja $entity) {
        $form = $this->createForm(new CajaType(), $entity, array(
            'action' => $this->generateUrl('caja_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar Caja', 'attr' => array('class' => 'btn btn-success  btn-xs pull-right')));

        return $form;
    }

    /**
     * Edits an existing Caja entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Caja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->sessionManager->addFlash('msgOk', 'Se ha editado correctametne la Caja.');
            return $this->redirect($this->generateUrl('caja'));
        }

        return $this->render('AppBundle:Caja:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Caja entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Caja')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Caja entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('caja'));
    }

    /**
     * Creates a form to delete a Caja entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('caja_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Cambia estado a una Caja
     *
     */
    public function cambiarEstadoAction(Request $request, $id, $estado) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Caja')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }
        if ($estado == 'C') {
            $entity->setSaldoFinal($this->cerrarCaja($entity));
        }
        if ($estado == 'E') {
            if (count($entity->getMovimientosCaja()) > 0) {
                $this->sessionManager->addFlash('msgError', 'No es posible eliminar la caja seleccionada. Ya posee moviemientos asignados.');
                return $this->redirect($this->generateUrl('caja'));
            }
            $this->crearCaja($entity);
            $this->sessionManager->addFlash('msgError', 'Se ha eliminado la caja y creado una nueva.');
        }
        $entity->setEstado($estado);
        $em->flush();
        $this->sessionManager->addFlash('msgOk', 'Se ha modificado el estado de la caja exitosamente.');
        return $this->redirect($this->generateUrl('caja'));
    }

    /*
     * Cerrar la caja
     */

    private function cerrarCaja($caja) {
        $em = $this->getDoctrine()->getManager();
        $saldoFinal = $caja->getSaldoInicial() + $em->getRepository('AppBundle:MovimientoCaja')->findSaldoFinal($caja->getId());

        $this->crearCaja($caja);

        return $saldoFinal;
    }

    private function crearCaja($caja) {
        $em = $this->getDoctrine()->getManager();
        $newCaja = new Caja();
        $newCaja->setUnidadNegocio($caja->getUnidadNegocio());
        $em->persist($newCaja);
        $em->flush();
    }

    /**
     * Lists all Caja entities.
     *
     */
    public function diariaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $request->get('unidadNegocioCaja'), 'estado' => 'A'));
            if(!$caja){
                $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidadNegocioCaja'));
                if(!$unidadNegocio) return $this->redirectToRoute("homepage");
                $this->sessionManager->crearCaja($unidadNegocio);
                $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $request->get('unidadNegocioCaja'), 'estado' => 'A'));
            }

            return $this->redirect($this->generateUrl('movimientocaja_show',
                array('id' => $caja->getId())
            ));
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $unidadNegocio, 'estado' => 'A'));
            return $this->redirect($this->generateUrl('movimientocaja_show',
                                    array('id' => $caja->getId())
            ));
        }
       
    }

}
