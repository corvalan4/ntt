<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Motivo;
use AppBundle\Form\MotivoType;

/**
 * Motivo controller.
 *
 */
class MotivoController extends Controller
{

    /**
     * Lists all Motivo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Motivo')->findAll();

        return $this->render('AppBundle:Motivo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Motivo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Motivo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('motivo'));
        }

        return $this->render('AppBundle:Motivo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Motivo entity.
     *
     * @param Motivo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Motivo $entity)
    {
        $form = $this->createForm(new MotivoType(), $entity, array(
            'action' => $this->generateUrl('motivo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=>['class'=>'btn btn-primary']));

        return $form;
    }

    /**
     * Displays a form to create a new Motivo entity.
     *
     */
    public function newAction()
    {
        $entity = new Motivo();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Motivo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Motivo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Motivo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Motivo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Motivo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Motivo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Motivo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Motivo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Motivo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Motivo entity.
    *
    * @param Motivo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Motivo $entity)
    {
        $form = $this->createForm(new MotivoType(), $entity, array(
            'action' => $this->generateUrl('motivo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=>['class'=>'btn btn-primary']));

        return $form;
    }
    /**
     * Edits an existing Motivo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Motivo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Motivo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('motivo'));
        }

        return $this->render('AppBundle:Motivo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Motivo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Motivo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Motivo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('motivo'));
    }

    /**
     * Creates a form to delete a Motivo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('motivo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
