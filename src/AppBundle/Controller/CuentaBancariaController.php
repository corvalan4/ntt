<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\MovimientoBancario;
use AppBundle\Entity\CuentaBancaria;
use AppBundle\Form\CuentaBancariaType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * CuentaBancaria controller.
 *
 */
class CuentaBancariaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all CuentaBancaria entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:CuentaBancaria')->findAll();

        return $this->render('AppBundle:CuentaBancaria:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new CuentaBancaria entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new CuentaBancaria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cuentabancaria'));
        }

        return $this->render('AppBundle:CuentaBancaria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CuentaBancaria entity.
     *
     * @param CuentaBancaria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CuentaBancaria $entity) {
        $form = $this->createForm(new CuentaBancariaType(), $entity, array(
            'action' => $this->generateUrl('cuentabancaria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Displays a form to create a new CuentaBancaria entity.
     *
     */
    public function newAction() {
        $entity = new CuentaBancaria();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:CuentaBancaria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CuentaBancaria entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:CuentaBancaria:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function ajusteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);

        $movimiento = new MovimientoBancario();
        $movimiento->setTipoMovimiento('AJ');
        $movimiento->setValor($request->get('valor'));
        $movimiento->setObservacion($request->get('observacion'));


        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
        } else {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
        }
#        $movimiento->setCuentabancaria($entity);
#        $movimiento->setUnidadNegocio($unidad);

        $em->persist($movimiento);

        $entity->setMovimiento($movimiento);
        $em->flush();

        return $this->redirect($this->generateUrl('cuentabancaria_movimientos', array('id' => $entity->getId())));
    }

    /**
     * Displays a form to edit an existing CuentaBancaria entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:CuentaBancaria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a CuentaBancaria entity.
     *
     * @param CuentaBancaria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CuentaBancaria $entity) {
        $form = $this->createForm(new CuentaBancariaType(), $entity, array(
            'action' => $this->generateUrl('cuentabancaria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing CuentaBancaria entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cuentabancaria'));
        }

        return $this->render('AppBundle:CuentaBancaria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CuentaBancaria entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cuentabancaria'));
    }

    /**
     * Creates a form to delete a CuentaBancaria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cuentabancaria_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function movimientosAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:CuentaBancaria')->find($id);
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(array('estado' => 'A'), array('descripcion' => 'DESC'));
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $movimientos = $em->getRepository('AppBundle:MovimientoBancario')->findBy(array('cuentabancaria'=>$entity), array('id' => 'DESC'));
        } else {
            $movimientos = $em->getRepository('AppBundle:MovimientoBancario')->findBy(array('unidadNegocio' => $this->getUser()->getUnidadNegocio(),'cuentabancaria'=>$entity), array('id' => 'DESC'));
        }

        return $this->render('AppBundle:CuentaBancaria:movimientos.html.twig', array(
                    'entity' => $entity,
                    'movimientos' => $movimientos,
                    'unidades' => $unidades
        ));
    }

}
