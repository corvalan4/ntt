<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\ControlHorario;
use AppBundle\Form\ControlHorarioType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * ControlHorario controller.
 *
 */
class ControlHorarioController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Gasto entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();
        $filtros = $request->query->all();


        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio()->getId();
        }

        $entities = $em->getRepository('AppBundle:ControlHorario')->filtro($filtros);

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelControlHorario($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Control Horario');
        }
        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:ControlHorario:index.html.twig', array(
                    'entities' => $paginador));
    }

    /**
     * Creates a new ControlHorario entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new ControlHorario();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $usuario = $this->getUser();
            if (!empty($entity->getFechaIngreso())) {
                $entity->setUsuarioCargaIngreso($usuario);
            }
            if (!empty($entity->getFechaEgreso())) {
                if ($this->verificarControlesHorarios($entity)) {
                    $entity->setUsuarioCargaEgreso($usuario);
                } else {
                    $this->sessionManager->addFlash('msgError', 'La fecha de ingreso no puede ser mayor a la fecha de Egreso.');
                    return $this->render('AppBundle:ControlHorario:new.html.twig', array(
                                'entity' => $entity,
                                'form' => $form->createView(),
                    ));
                }
            }
            $em->persist($entity);
            $em->flush();
            $this->sessionManager->addFlash('msgOk', 'Control Horario creado exitosamente.');
            return $this->redirect($this->generateUrl('controlhorario'));
        }
        $this->sessionManager->addFlash('msgError', 'No ha sido posible crear el Control Horario');

        return $this->render('AppBundle:ControlHorario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /*
     * Verificacion de Control Horario
     */

    private function verificarControlesHorarios($entity) {

        if ($entity->getFechaIngreso() > $entity->getFechaEgreso()) {
            return false;
        }

        if ($entity->getFechaIngreso()->format('u') > $entity->getFechaEgreso()->format('u')) {
            return false;
        }
        return true;
    }

    /**
     * Creates a form to create a ControlHorario entity.
     *
     * @param ControlHorario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ControlHorario $entity) {
        $form = $this->createForm(new ControlHorarioType(), $entity, array(
            'action' => $this->generateUrl('controlhorario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new ControlHorario entity.
     *
     */
    public function newAction() {
        $entity = new ControlHorario();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:ControlHorario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ControlHorario entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ControlHorario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlHorario entity.');
        }

        $editForm = $this->createEditForm($entity);


        return $this->render('AppBundle:ControlHorario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a ControlHorario entity.
     *
     * @param ControlHorario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ControlHorario $entity) {
        $form = $this->createForm(new ControlHorarioType(), $entity, array(
            'action' => $this->generateUrl('controlhorario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Edits an existing ControlHorario entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ControlHorario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlHorario entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($this->verificarControlesHorarios($entity)) {
                $usuario = $this->getUser();

                $entity->setUsuarioCargaEgreso($usuario);

                $em->flush();
                $this->sessionManager->addFlash('msgOk', 'Carga de fecha de egreso exitosa.');
                return $this->redirect($this->generateUrl('controlhorario'));
            } else {
                $this->sessionManager->addFlash('msgError', 'La fecha de ingreso no puede ser mayor a la fecha de Egreso.');
            }
        }

        $this->sessionManager->addFlash('msgError', 'No ha sido posible cargar la fecha de egreso exitosa.');
        return $this->render('AppBundle:ControlHorario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /*
     *  Generación de contenido archivo excel Informe de Cuentas por pagar
     */

    private function excelControlHorario($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Usuario');
        $sheet->setCellValue('B1', 'Fecha de Ingreso');
        $sheet->setCellValue('C1', 'Fecha de Egreso');
        $sheet->setCellValue('D1', 'Usuario Carga Fecha de Ingreso');
        $sheet->setCellValue('E1', 'Usuario Carga Fecha de Egreso');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('F1', 'Unidad de Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity->getUsuario());
            $fechaIngreso = $entity->getFechaIngreso();
            if (isset($fechaIngreso)) {
                $sheet->setCellValue('B' . $fila, date_format($entity->getFechaIngreso(), "d-m-Y HH:mm"));
            }
            $fechaEgreso = $entity->getFechaEgreso();
            if (isset($fechaEgreso)) {
                $sheet->setCellValue('C' . $fila, date_format($entity->getFechaEgreso(), "d-m-Y HH:mm"));
            }
            $sheet->setCellValue('D' . $fila, $entity->getUsuarioCargaIngreso());
            $sheet->setCellValue('E' . $fila, $entity->getUsuarioCargaEgreso());
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('F' . $fila, $entity->getUsuario()->getUnidadNegocio());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
            $sheet->getColumnDimension('F')->setAutoSize(true);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':E' . $fila)->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        return $spreadsheet;
    }

}
