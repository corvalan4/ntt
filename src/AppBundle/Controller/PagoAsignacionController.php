<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\ClienteProveedor;
use AppBundle\Form\PagoAsignacionType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


/**
 * PagoAsignacion controller.
 *
 */
class PagoAsignacionController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all PagoAsignacion entities.
     *
     */
    public function indexAction($id, Request $request, $page = 1) {

        $filtros = $request->query->all();
               
        $em = $this->getDoctrine()->getManager();
        $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
        $filtros['clienteProveedor'] = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
        
        $entities = $em->getRepository('AppBundle:PagoAsignacion')->filter($filtros);
        
        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(20);
        $paginador->setCurrentPage($page);
        
        return $this->render('AppBundle:PagoAsignacion:index.html.twig', array(
                    'clienteProveedor' => $clienteProveedor,
                    'movimientoscc' => $paginador
        ));
    }

    /**
     * Creates a new PagoAsignacion entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new PagoAsignacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pagoasignacion_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:PagoAsignacion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PagoAsignacion entity.
     *
     * @param PagoAsignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PagoAsignacion $entity) {
        $form = $this->createForm(new PagoAsignacionType(), $entity, array(
            'action' => $this->generateUrl('pagoasignacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PagoAsignacion entity.
     *
     */
    public function newAction() {
        $entity = new PagoAsignacion();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:PagoAsignacion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PagoAsignacion entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:PagoAsignacion:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PagoAsignacion entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:PagoAsignacion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a PagoAsignacion entity.
     *
     * @param PagoAsignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PagoAsignacion $entity) {
        $form = $this->createForm(new PagoAsignacionType(), $entity, array(
            'action' => $this->generateUrl('pagoasignacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing PagoAsignacion entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pagoasignacion_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:PagoAsignacion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PagoAsignacion entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:PagoAsignacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pagoasignacion'));
    }

    /**
     * Creates a form to delete a PagoAsignacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pagoasignacion_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Informe de Movimientos
     *
     */
    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $medio = $request->get('medio');
        $cliente = $request->get('cliente');
        $proveedor = $request->get('proveedor');
        $tipoMovimiento = $request->get('tipoMovimiento');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entidades = $em->getRepository('AppBundle:PagoAsignacion')->filtroBusquedaInformeMovimientos($fechaDesde, $fechaHasta, $cliente, $proveedor, $medio, $tipoMovimiento, $unidadNegocio);
        $entities = [];
        if ($medio == 'cheque') {
            foreach ($entidades as $entidad) {
                if ($entidad->getDocumento()['tipo'] != 'compra' &&
                        $entidad->getDocumento()['tipo'] != 'factura' &&
                        $entidad->getDocumento()['tipo'] != 'notaCreditoDebito' &&
                        $entidad->getDocumento()['tipo'] != 'facturaproveedor') {
                    if (count($entidad->getDocumento()['entity']->getCheques()) > 0) {
                        $entities[] = $entidad;
                    }
                }
            }
        } else {
            $entities = $entidades;
        }

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelMovimientos($entities, $medio);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Movimientos');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:PagoAsignacion:informe.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /**
     * Informe de Ingresos/Egresos
     *
     */
    public function ingresoEgresoAction(Request $request, $page = 1) {
        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $medio = $request->get('medio');
        $cliente = $request->get('cliente');
        $proveedor = $request->get('proveedor');
        $tipoMovimiento = $request->get('tipoMovimiento');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $em = $this->getDoctrine()->getManager();

        $entidades = $em->getRepository('AppBundle:PagoAsignacion')->filtroBusquedaInformeIngresoEgreso($fechaDesde, $fechaHasta, $cliente, $proveedor, $medio, $tipoMovimiento, $unidadNegocio);

        if (!isset($tipoMovimiento) || $tipoMovimiento == '') {
            $totalIngreso = $em->getRepository('AppBundle:PagoAsignacion')->totalInformeIngreso($fechaDesde, $fechaHasta, $cliente, $medio, $unidadNegocio);
            $totalEgreso = $em->getRepository('AppBundle:PagoAsignacion')->totalInformeEgreso($fechaDesde, $fechaHasta, $proveedor, $medio, $unidadNegocio);
        }
        if ($tipoMovimiento == 'cobranza') {
            $totalIngreso = $em->getRepository('AppBundle:PagoAsignacion')->totalInformeIngreso($fechaDesde, $fechaHasta, $cliente, $medio, $unidadNegocio);
            $totalIngreso = $totalIngreso + $em->getRepository('AppBundle:Cheque')->totalInformeCobranza($fechaDesde, $fechaHasta, $cliente, $unidadNegocio);
            $totalEgreso = 0;
        }
        if ($tipoMovimiento == 'pago') {
            $totalEgreso = $em->getRepository('AppBundle:PagoAsignacion')->totalInformeEgreso($fechaDesde, $fechaHasta, $proveedor, $medio, $unidadNegocio);
            $totalEgreso = $totalEgreso + $em->getRepository('AppBundle:Cheque')->totalInformePagos($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);
            $totalIngreso = 0;
        }

        $entities = [];

        if ($medio == 'cheque') {
            foreach ($entidades as $entidad) {
                if (count($entidad->getDocumento()['entity']->getCheques()) > 0) {
                    $entities[] = $entidad;
                }
            }
        } else {
            $entities = $entidades;
        }

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {

            $spreadSheet = $this->excelIngresoEgreso($entities, $totalIngreso, $totalEgreso, $medio);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe Ingreso - Egreso');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:PagoAsignacion:informeIngresoEgreso.html.twig', array(
                    'entities' => $paginador,
                    'totalIngreso' => $totalIngreso,
                    'totalEgreso' => $totalEgreso
        ));
    }

    /*
     * Generación de contenido archivo excel Informe de Ingreso -Egreso
     */

    private function excelIngresoEgreso($entities, $totalIngreso, $totalEgreso, $medioSelect) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Cliente/Proveedor');
        $sheet->setCellValue('D1', 'Medios');
        $sheet->setCellValue('E1', 'Cheques');
        $sheet->setCellValue('F1', 'Unidad Negocio');
        $sheet->setCellValue('G1', 'Importe');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity->getDocumento()['entity']);
            $sheet->setCellValue('C' . $fila, $entity->getDocumento()['entity']->getClienteProveedor());

            if ($medioSelect != 'cheque' && sizeof($entity->getDocumento()['entity']->getMedios()) > 0) {
                foreach ($entity->getDocumento()['entity']->getMedios() as $medio) {
                    $sheet->setCellValue('D' . $fila, $medio->getMedio()->getDescripcion() . ' - ' . $medio->getImporte());
                }
            }
            $ch = '';
            if (($medioSelect == 'cheque' || $medioSelect == '') && $entity->getDocumento()['entity']->getCheques() !== NULL) {
                foreach ($entity->getDocumento()['entity']->getCheques() as $cheque) {
                    if ($cheque->getEstado() != 'E') {
                        if ($ch == '') {
                            $ch = $ch . $cheque . ' - ' . $cheque->getImporte();
                        } else {
                            $ch = $ch . ' / ' . $cheque . ' - ' . $cheque->getImporte();
                        }
                    }
                }
                $sheet->setCellValue('E' . $fila, $ch);
            }
            $sheet->setCellValue('F' . $fila, $entity->getDocumento()['entity']->getUnidadNegocio());
            $sheet->setCellValue('G' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('F' . $fila, 'Total');
        $sheet->setCellValue('G' . $fila, round($totalIngreso - $totalEgreso, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('F' . $fila . ':G' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);


        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Generación de contenido archivo excel Informe de Ingreso -Egreso
     */

    private function excelMovimientos($entities, $medioSelect) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Cliente/Proveedor');
        $sheet->setCellValue('D1', 'Medios');
        $sheet->setCellValue('E1', 'Cheques');
        $sheet->setCellValue('F1', 'Unidad Negocio');
        $sheet->setCellValue('G1', 'Importe');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity->getDocumento()['entity']);
            $sheet->setCellValue('C' . $fila, $entity->getDocumento()['entity']->getClienteProveedor());

            if ($entity->getDocumento()['tipo'] != 'compra' &&
                    $entity->getDocumento()['tipo'] != 'factura' &&
                    $entity->getDocumento()['tipo'] != 'notaCreditoDebito' &&
                    $entity->getDocumento()['tipo'] != 'facturaproveedor') {

                if ($medioSelect != 'cheque' && sizeof($entity->getDocumento()['entity']->getMedios()) > 0) {
                    foreach ($entity->getDocumento()['entity']->getMedios() as $medio) {
                        $sheet->setCellValue('D' . $fila, $medio->getMedio()->getDescripcion() . ' - ' . $medio->getImporte());
                    }
                }
                $ch = '';
                if (($medioSelect == 'cheque' || $medioSelect == '') && $entity->getDocumento()['entity']->getCheques() !== NULL) {
                    foreach ($entity->getDocumento()['entity']->getCheques() as $cheque) {
                        if ($cheque->getEstado() != 'E') {
                            if ($ch == '') {
                                $ch = $ch . $cheque . ' - ' . $cheque->getImporte();
                            } else {
                                $ch = $ch . ' / ' . $cheque . ' - ' . $cheque->getImporte();
                            }
                        }
                    }
                    $sheet->setCellValue('E' . $fila, $ch);
                }
            }
            $sheet->setCellValue('F' . $fila, $entity->getDocumento()['entity']->getUnidadNegocio());
            $sheet->setCellValue('G' . $fila, $entity->getImporte());
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }

}
