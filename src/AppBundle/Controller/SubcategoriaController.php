<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Subcategoria;
use AppBundle\Form\SubcategoriaType;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Zend\Json\Json;

/**
 * Subcategoria controller.
 *
 */
class SubcategoriaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Subcategoria entities.
     *
     */
    public function indexAction() {


        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Subcategoria')->findAll();

        return $this->render('AppBundle:Subcategoria:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Lists all Subcategoria entities.
     *
     */
    public function listadojsonAction(Request $request) {
        $id_categoria = $request->get("id_categoria");
        $response = array();
        if (empty($id_categoria)) {
            return new Response(json_encode($response, true));
        }
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Subcategoria')->findBy(array('categoria' => $id_categoria));
        foreach ($entities as $entity) {
            $response[] = array('id' => $entity->getId(), 'descripcion' => $entity->getDescripcion());
        }

        return new Response(json_encode($response, true));
    }

    /**
     * Creates a new Subcategoria entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Subcategoria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('subcategoria'));
        }

        return $this->render('AppBundle:Subcategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Subcategoria entity.
     *
     * @param Subcategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Subcategoria $entity) {
        $form = $this->createForm(new SubcategoriaType(), $entity, array(
            'action' => $this->generateUrl('subcategoria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary btn-xs', 'onclick' => 'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Subcategoria entity.
     *
     */
    public function newAction() {

        $entity = new Subcategoria();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Subcategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Subcategoria entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Subcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subcategoria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Subcategoria:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Subcategoria entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Subcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subcategoria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Subcategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Subcategoria entity.
     *
     * @param Subcategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Subcategoria $entity) {
        $form = $this->createForm(new SubcategoriaType(), $entity, array(
            'action' => $this->generateUrl('subcategoria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Subcategoria entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Subcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subcategoria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Modificacion realizada.');
            return $this->redirect($this->generateUrl('subcategoria'));
        }

        return $this->render('AppBundle:Subcategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Subcategoria entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Subcategoria')->find($id);
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('subcategoria'));
    }

    /**
     * Creates a form to delete a Subcategoria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('subcategoria_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }
    /*
     * Render Select
     */
    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $subcategorias = $em->getRepository('AppBundle:Subcategoria')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:Subcategoria:select.html.twig', array(
                    'subcategorias' => $subcategorias
        ));
    }

}
