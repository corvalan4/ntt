<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Afip\Exceptionhandler;
use AppBundle\Afip\WSAA;
use AppBundle\Afip\WSFEV1;

use AppBundle\Entity\Documento;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Cheque;
use AppBundle\Entity\Retencion;
use AppBundle\Entity\ProductoDocumento;
use AppBundle\Entity\MovimientoCC;
use AppBundle\Entity\NumeracionRecibo;
use AppBundle\Form\DocumentoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Ps\PdfBundle\Annotation\Pdf;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Documento controller.
 *
 */
class DocumentoController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;	

    /**
     * Lists all Documento entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Documento')->findAll();

        return $this->render('AppBundle:Documento:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function filtroAction(Request $request, $place, $page)
    {
        if(!isset($page)){
            $page = 1;
        }

			$em = $this->getDoctrine()->getManager();
			
			$this->sessionManager->setSession('unidaddenegocio', $request->get('unidadnegocio'));
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
            $tipodoc = '';
			if($place == 'compras'){
                $tipodoc = 'F';
			}
			if($place == 'cobranzas'){
                $tipodoc = 'R';
			}
			if($place == 'gastos'){
                $tipodoc = 'G';
			}
			if($place == 'pagos'){
                $tipodoc = 'RP';
			}
            
            if($request->get('fechaDesde')=='' and $request->get('fechaHasta')=='' and $request->get('listado')==''){
    			$desde = new \DateTime($this->sessionManager->getSession('fechaDesde'));
    			$hasta = new \DateTime($this->sessionManager->getSession('fechaHasta'));
    			$listado = $this->sessionManager->getSession('listado');
            }else{
                if($request->get('fechaDesde')=='' and $request->get('fechaHasta')==''){
        			$desde = new \DateTime('1/1/1996');
        			$hasta = new \DateTime('NOW');
                }else{
        			$desde = new \DateTime($request->get('fechaDesde'));
        			$hasta = new \DateTime();
                }

    			$listado = $request->get('listado');
                $this->sessionManager->setSession('fechaDesde', $request->get('fechaDesde'));
                $this->sessionManager->setSession('fechaHasta', $request->get('fechaHasta'));
                $this->sessionManager->setSession('listado', $request->get('listado'));
            }

			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$documentos = $em->getRepository('AppBundle:Documento')->getPorFechas($this->getUser()->getUnidadNegocio(), $tipodoc, $desde, $hasta, $listado);
			}else{
				if($request->get('unidadnegocio')!='0'){
					$documentos = $em->getRepository('AppBundle:Documento')->getPorFechas($request->get('unidadnegocio'), $tipodoc, $desde, $hasta, $listado);
				}else{
					$documentos = $em->getRepository('AppBundle:Documento')->getPorFechas(NULL, $tipodoc, $desde, $hasta, $listado);
				}
			}

            $adapter = new ArrayAdapter($documentos);
            $paginador = new Pagerfanta($adapter);
    		$paginador->setMaxPerPage(300);
    		$paginador->setCurrentPage($page);

			if($place == 'compras'){
				if(!$this->isGranted('ROLE_SUPER_ADMIN')){
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv($this->getUser()->getUnidadNegocio());
				}else{
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv();
				}
		
				return $this->render('AppBundle:Documento:compras.html.twig', array(
					'entities' => $paginador,
					'unidades' => $unidades,
					'clientesProveedores' => $clientesProveedores,
				));
			}
			if($place == 'cobranzas'){
				if(!$this->isGranted('ROLE_SUPER_ADMIN')){
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllCli($this->getUser()->getUnidadNegocio());
				}else{
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllCli();
				}
		
				return $this->render('AppBundle:Documento:cobranzas.html.twig', array(
					'entities' => $paginador,
					'unidades' => $unidades,
					'clientesProveedores' => $clientesProveedores,
				));
			}
			if($place == 'gastos'){
				$tiposGasto = $em->getRepository('AppBundle:TipoGasto')->getAllActivas();
		
				return $this->render('AppBundle:Documento:gastos.html.twig', array(
					'entities' => $paginador,
					'unidades' => $unidades,
					'tiposGasto' => $tiposGasto,
				));
			}
			if($place == 'pagos'){
				if(!$this->isGranted('ROLE_SUPER_ADMIN')){
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv($this->getUser()->getUnidadNegocio());
				}else{
					$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv();
				}
				return $this->render('AppBundle:Documento:pagos.html.twig', array(
					'entities' => $paginador,
					'unidades' => $unidades,
					'clientesProveedores' => $clientesProveedores,
				));
			}

    }

    public function comprasAction($page)
    {


        if(!isset($page)){
            $page = 1;
        }

		$em = $this->getDoctrine()->getManager();
        $this->sessionManager->setSession('fechaDesde', '');
        $this->sessionManager->setSession('fechaHasta', '');
        $this->sessionManager->setSession('listado', '');

		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv($this->getUser()->getUnidadNegocio());
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas($this->getUser()->getUnidadNegocio(), 'F');
		}else{
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv();
			$entities = $em->getRepository('AppBundle:Documento')->findBy(array('tipoDocumento'=>'F'));
		}

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(300);
		$paginador->setCurrentPage($page);

		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->getAllActivas();

		return $this->render('AppBundle:Documento:compras.html.twig', array(
			'entities' => $paginador,
			'unidades' => $unidades,
			'clientesProveedores' => $clientesProveedores,
		));
    }

    public function cobranzasAction($page)
    {

        $this->sessionManager->setSession('fechaDesde', '');
        $this->sessionManager->setSession('fechaHasta', '');
        $this->sessionManager->setSession('listado', '');

        if(!isset($page)){
            $page = 1;
        }

		$em = $this->getDoctrine()->getManager();

		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllCli($this->getUser()->getUnidadNegocio());
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas($this->getUser()->getUnidadNegocio(),'R');
		}else{
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllCli();
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas(NULL, 'R');
		}

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(50);
		$paginador->setCurrentPage($page);

		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

		return $this->render('AppBundle:Documento:cobranzas.html.twig', array(
			'entities' => $paginador,
			'unidades' => $unidades,
			'clientesProveedores' => $clientesProveedores,
		));
    }

    public function pagosAction($page)
    {

        $this->sessionManager->setSession('fechaDesde', '');
        $this->sessionManager->setSession('fechaHasta', '');
        $this->sessionManager->setSession('listado', '');
        if(!isset($page)){
            $page = 1;
        }

		$em = $this->getDoctrine()->getManager();

		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv($this->getUser()->getUnidadNegocio());
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas($this->getUser()->getUnidadNegocio(),'RP');
		}else{
			$clientesProveedores = $em->getRepository('AppBundle:ClienteProveedor')->getAllProv();
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas(Null,'RP');
		}
		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(300);
		$paginador->setCurrentPage($page);

		return $this->render('AppBundle:Documento:pagos.html.twig', array(
			'entities' => $paginador,
			'unidades' => $unidades,
			'clientesProveedores' => $clientesProveedores,
		));
    }

    public function gastosAction($page)
    {

        $this->sessionManager->setSession('fechaDesde', '');
        $this->sessionManager->setSession('fechaHasta', '');
        $this->sessionManager->setSession('listado', '');

        if(!isset($page)){
            $page = 1;
        }

		$em = $this->getDoctrine()->getManager();

		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas($this->getUser()->getUnidadNegocio(), 'G');
		}else{
			$entities = $em->getRepository('AppBundle:Documento')->getAllActivas(NULL, 'G');
		}
		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

		$tiposGasto = $em->getRepository('AppBundle:TipoGasto')->getAllActivas();

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(300);
		$paginador->setCurrentPage($page);

		return $this->render('AppBundle:Documento:gastos.html.twig', array(
			'entities' => $paginador,
			'unidades' => $unidades,
			'tiposGasto' => $tiposGasto,
		));
    }

    /**
     * Creates a new Documento entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Documento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $tipodoc = $entity->getTipoDocumento();

		$em = $this->getDoctrine()->getManager();
		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
		$tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$entity->setFechaRegistracion(new \DateTime());            

        if ($form->isValid()) {
 			$movimintocc = new MovimientoCC();
						
			if($entity->getTipoDocumento()!='G'){
				$entity->setTipoGasto(NULL);
			}else{
				if(is_null($entity->getTipoGasto())){
					$this->sessionManager->addFlash('msgError','Debe seleccionar un Tipo de Gasto.');
					return $this->render('AppBundle:Documento:new.html.twig', array(
						'unidades' => $unidades,
						'entity' => $entity,
						'form'   => $form->createView(),
					));
					
				}
			}

			if($entity->getTipoDocumento()!='R' and $entity->getTipoDocumento()!='RP' and $entity->getTipoDocumento()!='C' and $entity->getTipoDocumento()!='D' and $entity->getTipoDocumento()!='RP' and $entity->getTipoDocumento()!='A'){
				$fecha = new \DateTime($request->get('fechaDocumento'));
			}else{
				$fecha = new \DateTime();			
			}
			
			if($entity->getTipoDocumento()=='F'){
        		$entity->setNrocomprobante($request->get('nrocomprobante'));
			}
            
			if($entity->getTipoDocumento()!='G' && $entity->getTipoDocumento()!='A' && $entity->getTipoDocumento()!='D' && $entity->getTipoDocumento()!='C'){
				$cliPro = $em->getRepository('AppBundle:ClienteProveedor')->find($request->get('clienteProveedor'));
				$movimintocc->setClienteProveedor($cliPro);            
			}
			if($entity->getTipoDocumento()=='D' or $entity->getTipoDocumento()=='C'){
				$cliPro = $em->getRepository('AppBundle:ClienteProveedor')->find($request->get('clienteproveedor'));
			}
			
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
			}else{
				if($entity->getTipoDocumento()!='G' and $entity->getTipoDocumento()!='A'){
					$unidad = $cliPro->getUnidadNegocio();			
				}else{
					$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
				}
			}

			$movimintocc->setUnidadNegocio($unidad);			
       		$contador = $request->get('contador');		
   			if($entity->getTipoDocumento()=='D' or $entity->getTipoDocumento()=='C'){
                $idsucursal = $request->get('sucursal');
            	if(isset($idsucursal)){
                	$sucursal = $em->getRepository('AppBundle:Sucursal')->find($idsucursal);
                    if(!is_null($sucursal)){
                        $entity->setSucursal($sucursal);
                    }
            	}
        		$entity->setImporte($request->get('resultadoFinal'));
        		$entity->setImporteiva($request->get('resultadoiva'));
				$movimintocc->setClienteProveedor($cliPro);            
        		for($x = 0; $x <= $contador; $x++){
        			$desc = $request->get('descripcion'.$x);
        			
        			if($desc != '' and !is_null($desc) ){
        				$productodocumento = new ProductoDocumento();
    					$productodocumento->setDocumento($entity);				
    					$productodocumento->setDescripcion($desc);
    					$productodocumento->setPrecio($request->get('precio'.$x));				
    					$productodocumento->setConiva($request->get('coniva'.$x));				
    					$productodocumento->setCantidad($request->get('cantidad'.$x));				
            			$em->persist($productodocumento);
                        $entity->addProductosDocumento($productodocumento);
        			}
        		}	
        		if(count($entity->getProductosDocumento())<1){
                    $tipodoc = $entity->getTipoDocumento();
                    if($tipodoc=='D'){
            			$this->sessionManager->addFlash('msgError','La Nota de Debito debe tener al menos un item.');
                    }else{
            			$this->sessionManager->addFlash('msgError','La Nota de Credito debe tener al menos un item.');
                    }
        
        			return $this->redirect($this->generateUrl('documento_new', array('tipo'=>$tipodoc,'idcliprov'=>$cliPro->getId())));
        		}
                $wsaa = new WSAA('./'); 
                
                // Si la fecha de expiracion es menor a hoy, obtengo un nuevo Ticket de Acceso.
                if($wsaa->get_expiration() < date("Y-m-d h:m:i")) {
                    if ($wsaa->generar_TA()) {
                    } else {
            			$this->sessionManager->addFlash('msgError','Error al obtener el TA.');
                    }
                };
                
                $wsfe = new WSFEV1('./');
                 
                // Carga el archivo TA.xml
                $wsfe->openTA();
                  
                // devuelve el cae
                $ptovta = 4;
        
                if($entity->getTipoDocumento()=='C'){
                    $nc='SI';        
                }else{
                    $nc='NO';        
                }
                if($cliPro->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
                    $tipofactura = 'A';
                }else{
                    $tipofactura = 'B';
                }        
                switch ($tipofactura) {
                    case 'A': 
        				if($nc==''){
        					$tipocbte = 1;
        				}else{
        					if($nc=='SI'){
        						$tipocbte = 3;
        					}else{
        						$tipocbte = 2;
        					}
        				}
                        break;
                    case 'B': 
        				if($nc==''){
        					$tipocbte = 6;
        				}else{
        					if($nc=='SI'){
        						$tipocbte = 8;
        					}else{
        						$tipocbte = 7;
        					}
        				}
        				break;
                }

                // Ultimo comprobante autorizado, a este le sumo uno para procesar el siguiente.
                $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta);
                
                //Armo array con valores hardcodeados de la factura.
                $regfac['concepto'] = 1; 					        # 1: productos, 2: servicios, 3: ambos
    
                if($cliPro->getCondicioniva()->getDescripcion()=='Consumidor Final'){
                    $regfac['tipodocumento'] = 99;		                # 80: CUIT, 96: DNI, 99: Consumidor Final
                }else{
                    $regfac['tipodocumento'] = 80;		                # 80: CUIT, 96: DNI, 99: Consumidor Final
                }        
                $regfac['numerodocumento'] = $cliPro->getDni();	    # 0 para Consumidor Final (<$1000)
                $regfac['cuit'] = str_replace("-", "", $cliPro->getCuit());
                $regfac['capitalafinanciar'] = 0;			        # subtotal de conceptos no gravados
                if($cliPro->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
                    $regfac['importetotal'] = $entity->getImporte();	# total del comprobante
                    $regfac['importeiva'] = $entity->getImporteiva();			# subtotal neto sujeto a IVA
                    $regfac['importeneto'] = $entity->getImporte()-$entity->getImporteiva();
                }else{
                    $regfac['importetotal'] = $entity->getImporte();	# total del comprobante
                    $regfac['importeiva'] = 0;
                    $regfac['importeneto'] = 0;			# subtotal neto sujeto a IVA
                    $regfac['capitalafinanciar'] = $entity->getImporte();
                } 
           
                $regfac['imp_trib'] = 1.0;
                $regfac['imp_op_ex'] = 0.0;
                $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
                $regfac['fecha_venc_pago'] = date('Ymd');
                
                // Armo con la factura los parametros de entrada para el pedido 
                $params = $wsfe->armadoFacturaUnica(
                						$tipofactura,
                						$ptovta,    // el punto de venta
                						$nc,
                						$regfac     // los datos a facturar
                				);
                
                //Solicito el CAE
                $cae = $wsfe->solicitarCAE($params);
    			$this->sessionManager->setSession('CAE',json_encode($cae) );
    
                if($cae->FECAESolicitarResult->FeCabResp->Resultado=='A' or $cae->FECAESolicitarResult->FeCabResp->Resultado=='P'){
            		$entity->setNrocomprobante($regfac['nrofactura']);
            		$entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
                    $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
            
                }else{
                    if($cae->FECAESolicitarResult->FeCabResp->Resultado=='R'){
              			$this->sessionManager->addFlash('msgError','Error al dar de alta la Nota: '. json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) );
                    }

        			return $this->redirect($this->generateUrl('documento_new', array('tipo'=>$tipodoc,'idcliprov'=>$cliPro->getId())));
                }
			}

			$entity->setFecha($fecha);
			$em->persist($entity);
            $em->flush();

            if($entity->getTipoDocumento()=='R'){
                $entity->setEfectivo($request->get('efectivo'));
    			$entity->setFecha(new \DateTime($request->get('fechaDocumento')));             
                if($request->get('auto')!='true'){
        			$entity->setFecha(new \DateTime($request->get('fechaDocumento')));             
                }else{
                    $nrorecibo = $em->getRepository('AppBundle:NumeracionRecibo')->findOneBy(array('unidadNegocio'=>$unidad->getId()));
                    if(is_null($nrorecibo)){
                        $nrorecibo = new NumeracionRecibo();
                        $nrorecibo->setUnidadNegocio($unidad);
                        $nrorecibo->setNrorecibo(1);
                        $em->persist($nrorecibo);
                        $nrorec = $nrorecibo->getNrorecibo();
                    }else{
                        $nrorec = $nrorecibo->getNrorecibo() + 1;
                    }
                    $entity->setNrocomprobante('000'.$nrorec);
                }

                for($i = 0; $i<6; $i++){
                    if($request->get('banco'.$i) != 0 and $request->get('tipocheque'.$i) 
                    and $request->get('importe'.$i)!='' and $request->get('importe'.$i)>0){

                        $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco'.$i));
                        $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque'.$i));

                        $cheque = new Cheque();
                        $cheque->setImporte($request->get('importe'.$i));
                        $cheque->setFirmantes($request->get('firmantes'.$i));
                        $cheque->setCuit($request->get('cuit'.$i));
                        $cheque->setFechacobro(new \DateTime($request->get('fechacobro'.$i)));
                        $cheque->setFechaemision(new \DateTime($request->get('fechaemision'.$i)));
                        $cheque->setNrocheque($request->get('nrocheque'.$i));
                        $cheque->setBanco($banco);
                        $cheque->setTipocheque($tipocheque);
                        $cheque->setDocumento($entity);
                        $cheque->setUnidadNegocio($unidad);
                        $em->persist($cheque); 
                        $em->flush();
                    }
                }
                for($i = 1; $i<=count($tiporetenciones); $i++){
                    if($request->get('retencionimporte'.$i)!='' and $request->get('retencionimporte'.$i)>0){
                        $retencion = new Retencion();
                        $tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->findOneBy(array('descripcion'=>$request->get('retencion'.$i)));
                        $retencion->setImporte($request->get('retencionimporte'.$i));
                        $retencion->setTiporetencion($tiporetencion);
                        $retencion->setDocumento($entity);
                        $retencion->setUnidadNegocio($unidad);
                        $em->persist($retencion);
                        $em->flush();
                    }
                }   
            }
            if($entity->getTipoDocumento()=='RP'){
    			$entity->setFecha(new \DateTime($request->get('fechaDocumento')));             
                $entity->setEfectivo($request->get('efectivo'));
                $this->sessionManager->setSession('gasto', $request->get('gasto'));
                if($request->get('gasto')){
                    $entity->setTipoDocumento('G');
                    $entity->setTipoGasto(NULL);
                    $em->flush();
                }
                if($request->get('auto')!='true'){
        			$entity->setFecha(new \DateTime($request->get('fechaDocumento')));             
                }else{
                    $nrorecibo = $em->getRepository('AppBundle:NumeracionRecibo')->findOneBy(array('unidadNegocio'=>$unidad->getId()));
                    if(is_null($nrorecibo)){
                        $nrorecibo = new NumeracionRecibo();
                        $nrorecibo->setUnidadNegocio($unidad);
                        $nrorecibo->setNrorecibo(1);
                        $em->persist($nrorecibo);
                        $nrorec = $nrorecibo->getNrorecibo();
                    }else{
                        $nrorec = $nrorecibo->getNrorecibo() + 1;
                    }
                    $entity->setNrocomprobante('000'.$nrorec);
                }
                $cheques = $request->get('selectcheques');
                $this->sessionManager->setSession('cheques', $cheques); 
                for($i = 0; $i<count($cheques); $i++){
                    $chequeinactivo = $em->getRepository('AppBundle:Cheque')->find($cheques[$i]);
                    $chequeinactivo->setEstado('U');
                    $chequeinactivo->setPago($entity);
                    $em->flush();
                }

                for($i = 1; $i<=count($tiporetenciones); $i++){
                    if($request->get('retencionimporte'.$i)!='' and $request->get('retencionimporte'.$i)>0){
                        $retencion = new Retencion();
                        $tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->findOneBy(array('descripcion'=>$request->get('retencion'.$i)));
                        $retencion->setImporte($request->get('retencionimporte'.$i));
                        $retencion->setTiporetencion($tiporetencion);
                        $retencion->setDocumento($entity);
                        $retencion->setUnidadNegocio($unidad);
                        $em->persist($retencion);
                        $em->flush();
                    }
                }   
            }
			$movimintocc->setDocumento($entity);            
			$movimintocc->setTipoDocumento($entity->getTipoDocumento());            
			$movimintocc->setMoneda($entity->getMoneda());            
			$em->persist($movimintocc);
            $em->flush();
			$entity->setMovimientocc($movimintocc);
         	$em->flush();
	
            
     	    if($entity->getTipoDocumento()=="F"){
				$this->sessionManager->addFlash('msgOk','Compra registrada.');
				return $this->redirect($this->generateUrl('documento_compras'));
			}
     	    if($entity->getTipoDocumento()=="R"){
				$this->sessionManager->addFlash('msgOk','Cobranza registrada.');
				return $this->redirect($this->generateUrl('documento_cobranzas'));
			}
     	    if($entity->getTipoDocumento()=="RP"){
				$this->sessionManager->addFlash('msgOk','Pago registrado.');
				return $this->redirect($this->generateUrl('documento_pagos'));
			}
     	    if($entity->getTipoDocumento()=="G"){
				$this->sessionManager->addFlash('msgOk','Gasto registrado.');
				return $this->redirect($this->generateUrl('documento_gastos'));
			}
    	    if($entity->getTipoDocumento()=="A"){
				$this->sessionManager->addFlash('msgOk','Ajuste registrado.');
				return $this->redirect($this->generateUrl('movimientocc_admcaja'));
			}
    	    if($entity->getTipoDocumento()=="D"){
				$this->sessionManager->addFlash('msgOk','Nota de Debito registrada.');
				return $this->redirect($this->generateUrl('clienteproveedor_cc', array( 'id'=> $cliPro->getId() )));
			}
    	    if($entity->getTipoDocumento()=="C"){
				$this->sessionManager->addFlash('msgOk','Nota de Credito registrada.');
				return $this->redirect($this->generateUrl('clienteproveedor_cc', array( 'id'=> $cliPro->getId() )));
			}
			
        }
		
 	    if($entity->getTipoDocumento()=="R"){
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas($this->getUser()->getUnidadNegocio());
				$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
				$productos = $em->getRepository('AppBundle:Producto')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				if($tipo=='F'){
					$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas($idunidad);
					$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($idunidad);
					$productos = $em->getRepository('AppBundle:Producto')->getAllActivas($idunidad);
				}else{
					$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas();
				}
			}
			$bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
			$tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
			return $this->render('AppBundle:Documento:newRC.html.twig', array(
				'entity'           => $entity,
				'unidades'         => $unidades,
				'bancos'           => $bancos,
				'tiporetenciones'  => $tiporetenciones,
				'tipocheques'      => $tipocheques,
				'form'             => $form->createView(),
				'clientesProveedores' => $cliProvs)
			);
        }else{
    		return $this->render('AppBundle:Documento:new.html.twig', array(
                'entity'   => $entity,
                'form'     => $form->createView(),
    			'unidades' => $unidades,
            ));
        }
    }

    /**
     * Creates a form to create a Documento entity.
     *
     * @param Documento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Documento $entity)
    {
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Documento entity.
     *
     */
    public function newAction(Request $request, $tipo)
    {

			$em = $this->getDoctrine()->getManager();
			$entity = new Documento();
			$entity->setTipoDocumento($tipo);
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			$form   = $this->createCreateForm($entity);
			$idcliprov = $request->get('idcliprov');
            if(isset($idcliprov)){
				$clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($idcliprov);
            }
			$idunidad = $request->get('unidad');
			
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas($this->getUser()->getUnidadNegocio());
				$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
				$productos = $em->getRepository('AppBundle:Producto')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				if($tipo=='F'){
					$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas($idunidad);
					$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($idunidad);
					$productos = $em->getRepository('AppBundle:Producto')->getAllActivas($idunidad);
				}else{
					$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas();
				}
			}
			
			if($tipo=='F'){    
				return $this->render('AppBundle:Documento:facturaproveedor.html.twig', array(
					'entity' => $entity,
					'unNeg' => $unidad,
					'productos' => $productos,	
					'clientesProveedores' => $cliProvs)
				);
			}else{	
    			if($tipo=='R'){
        			$bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        			$tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        			$tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
    				return $this->render('AppBundle:Documento:newRC.html.twig', array(
    					'entity'           => $entity,
    					'unidades'         => $unidades,
    					'bancos'           => $bancos,
    					'tiporetenciones'  => $tiporetenciones,
    					'tipocheques'      => $tipocheques,
    					'form'             => $form->createView(),
    					'clientesProveedores' => $cliProvs)
    				);
                }else{
        			if($tipo=='RP'){
            			$tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
            			$cheques = $em->getRepository('AppBundle:Cheque')->findBy(array('estado'=>'A'),array('fechacobro'=>'ASC'));
        				return $this->render('AppBundle:Documento:newP.html.twig', array(
        					'entity'           => $entity,
        					'unidades'         => $unidades,
        					'tiporetenciones'  => $tiporetenciones,
        					'cheques'          => $cheques,
        					'form'             => $form->createView(),
        					'clientesProveedores' => $cliProvs)
        				);
                    }else{
	        			if($tipo=='D'){
							if($clienteProveedor->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
								$tipofactura = 'A';
							}else{ 
								$tipofactura = 'B';
							}        
            				return $this->render('AppBundle:Documento:newD.html.twig', array(
            					'entity'           => $entity,
            					'tipofactura'      => $tipofactura,
            					'unidades'         => $unidades,
            					'form'             => $form->createView(),
            					'clienteProveedor' => $clienteProveedor)
            				);
                        }else{
    	        			if($tipo=='C'){
								if($clienteProveedor->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
									$tipofactura = 'A';
								}else{ 
									$tipofactura = 'B';
								}        
                				return $this->render('AppBundle:Documento:newC.html.twig', array(
                					'entity'           => $entity,
                					'tipofactura'      => $tipofactura,
                					'unidades'         => $unidades,
                					'form'             => $form->createView(),
                					'clienteProveedor' => $clienteProveedor)
                				);
                            }else{
                                return $this->render('AppBundle:Documento:new.html.twig', array(
                					'entity' => $entity,
                					'unidades'   => $unidades,
                					'form'   => $form->createView(),
                					'clientesProveedores' => $cliProvs)
                				);
                            }
                        }
                    }
                }
                
			}

    }

    /**
     * Finds and displays a Documento entity.
     *
     */
    public function showAction($id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Documento')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Documento entity.');
			}
			
			if($entity->getTipoDocumento()=='F'){
				return $this->render('AppBundle:Documento:showFactProv.html.twig', array(
					'entity'      => $entity,
				));
			}else{
    			if($entity->getTipoDocumento()=='D'){
                    if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
        				return $this->render('AppBundle:Documento:showA.html.twig', array(
        					'entity'      => $entity,
        				));
                    }else{
        				return $this->render('AppBundle:Documento:showB.html.twig', array(
        					'entity'      => $entity,
        				));
                    }     			
                }else{
        			if($entity->getTipoDocumento()=='C'){
                        if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
            				return $this->render('AppBundle:Documento:showA.html.twig', array(
            					'entity'      => $entity,
            				));
                        }else{
            				return $this->render('AppBundle:Documento:showB.html.twig', array(
            					'entity'      => $entity,
            				));
                        }     			
        			}else{
                        if($entity->getTipoDocumento()=='R'){
            				return $this->render('AppBundle:Documento:showR.html.twig', array(
            					'entity'      => $entity,
            				));
                        }else{
                            if($entity->getTipoDocumento()=='RP' or $entity->getTipoDocumento()=='G'){
                				return $this->render('AppBundle:Documento:showP.html.twig', array(
                					'entity'      => $entity,
                				));
                            }else{
                				return $this->render('AppBundle:Documento:show.html.twig', array(
                					'entity'      => $entity,
                				));
                            }        			 
                        }        			 
        			}
                }
			}
            			

    }

    public function imprimirAction($id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Documento')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Documento entity.');
			}
	
			$pdfGenerator = $this->get('siphoc.pdf.generator');
			$pdfGenerator->setName('RECIBO CLIENTE '.$entity->getId().'.pdf');
			return $pdfGenerator->downloadFromView(
				'AppBundle:Documento:imprimir.html.twig', array(
					'entity' => $entity,)
				);

    }

    /**
     * Displays a form to edit an existing Documento entity.
     *
     */
    public function editAction($id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Documento')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Documento entity.');
			}
	
			$editForm = $this->createEditForm($entity);

			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				$cliProvs = $em->getRepository('AppBundle:ClienteProveedor')->getAllActivas();
			}
	
			return $this->render('AppBundle:Documento:edit.html.twig', array(
				'entity'      => $entity,
				'edit_form'   => $editForm->createView(),
				'clientesProveedores' => $cliProvs
			));

    }

    /**
    * Creates a form to edit a Documento entity.
    *
    * @param Documento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Documento $entity)
    {
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Documento entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
			if($entity->getTipoDocumento()!='G'){
				$entity->setTipoGasto(NULL);
			}

			$fecha = new \DateTime($request->get('fechaDocumento'));

            if($entity->getTipoDocumento()=='F' and $entity->getFechaRegistracion()<$fecha){
				$this->sessionManager->addFlash('msgError','La Fecha no puede ser mayor a la de registracion.');
				return $this->render('AppBundle:Documento:edit.html.twig', array(
					'entity'      => $entity,
					'edit_form'   => $editForm->createView(),
				));
			}else{
				if($entity->getTipoDocumento()=='F'){$entity->setFecha($fecha);}
				
				$em->flush();
				
				if($entity->getTipoDocumento()=="F"){
					$this->sessionManager->addFlash('msgOk','Factura modificada.');
					return $this->redirect($this->generateUrl('documento_compras'));
				}
				if($entity->getTipoDocumento()=="R"){
					$this->sessionManager->addFlash('msgOk','Recibo modificada.');
					return $this->redirect($this->generateUrl('documento_cobranzas'));
				}
				if($entity->getTipoDocumento()=="RP"){
					$this->sessionManager->addFlash('msgOk','Recibo modificado.');
					return $this->redirect($this->generateUrl('documento_pagos'));
				}
				if($entity->getTipoDocumento()=="G"){
					$this->sessionManager->addFlash('msgOk','Gasto modificado.');
					return $this->redirect($this->generateUrl('documento_gastos'));
				}			
			}

        }

        return $this->render('AppBundle:Documento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    public function borrarAction(Request $request, $id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Documento')->find($id);
			$stock = '';		
			$entity->setEstado('B');
			$entity->getMovimientocc()->setEstado('B');
			if($entity->getTipoDocumento()=='F'){
				foreach($entity->getProductosDocumento() as $prodDoc){
					$stock = $prodDoc->getProducto()->getStock() - $prodDoc->getCantidad();
					if($stock<0){$stock=0;}
					$prodDoc->getProducto()->setStock($stock);
				}		
			}
			$em->flush();
			
			if($entity->getTipoDocumento()=="F"){
				$this->sessionManager->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_compras'));
			}
			if($entity->getTipoDocumento()=="R"){
				$this->sessionManager->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_cobranzas'));
			}
			if($entity->getTipoDocumento()=="G"){
				$this->sessionManager->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_gastos'));
			}
			if($entity->getTipoDocumento()=="RP"){
				$this->sessionManager->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_pagos'));
			}

    }

    /**
     * Deletes a Documento entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Documento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Documento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('documento'));
    }

    public function reciboproveedorAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Documento')->find($id);
        $entity->setNroreciboproveedor($request->get('nroreciboproveedor'));
        $entity->setFechareciboproveedor(new \DateTime($request->get('fecharecibo')));
        
        $em->flush();

		$this->sessionManager->addFlash('msgOk','Nro. de recibo entregado por el proveedor, cargado con exito.');
		return $this->redirect($this->generateUrl('documento_pagos'));
    }

    /**
     * Creates a form to delete a Documento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('documento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr'=> array('class'=>'btn')))
            ->getForm()
        ;
    }
    public function informeComprasAction()
    {

			$em = $this->getDoctrine()->getManager();
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			return $this->render('AppBundle:Documento:filtro.html.twig', array(
				'tipo'=>'compras',
				'unidades'=>$unidades
			));

    }
    public function informeCobranzasAction()
    {

			$em = $this->getDoctrine()->getManager();
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			return $this->render('AppBundle:Documento:filtro.html.twig', array(
				'tipo'=>'cobranzas',
				'unidades'=>$unidades
			));

    }
    public function informePagosAction()
    {

			$em = $this->getDoctrine()->getManager();
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			return $this->render('AppBundle:Documento:filtro.html.twig', array(
				'tipo'=>'pagos',
				'unidades'=>$unidades
			));

    }
    public function informeVentasAction()
    {

			$em = $this->getDoctrine()->getManager();
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			return $this->render('AppBundle:Documento:filtro.html.twig', array(
				'tipo'=>'ventas',
				'unidades'=>$unidades
			));

    }
    public function informeGastosAction()
    {

			$em = $this->getDoctrine()->getManager();
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
			return $this->render('AppBundle:Documento:filtro.html.twig', array(
				'tipo'=>'gastos',
				'unidades'=>$unidades
			));

    }


    public function informesAction(Request $request)
    {

			$desde = new \DateTime( $request->get('fechaDesde') );
			$hasta = new \DateTime( $request->get('fechaHasta') );
			$saldoP = 0;
			$saldoD = 0;
			$porcP = 0;
			$porcD = 0;
	
			$em = $this->getDoctrine()->getManager();
			// ask the service for a Excel5
			$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
	
			$phpExcelObject->getProperties()->setCreator("Willy Jhons")
			   ->setLastModifiedBy("Willy Jhons")
			   ->setTitle("Informe")
			   ->setSubject("Willy Jhons");
	
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A1', 'Fecha: '.date('d-m-Y'))
			   ->setCellValue('A2', 'Periodo')
			   ->setCellValue('B2', 'Desde: '.$desde->format('d-m-Y'))
			   ->setCellValue('C2', 'Hasta: '.$hasta->format('d-m-Y'));
			   				
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$documentos = $em->getRepository('AppBundle:Documento')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				$documentos = $em->getRepository('AppBundle:Documento')->getAllActivas($request->get('unidadnegocio'));
			}

			$count = 4;
			if($request->get('agrupado')=='1'){	
				if($request->get('tipo')=='ventas'){
					
					if(!$this->isGranted('ROLE_SUPER_ADMIN')){
						$ventas = $em->getRepository('AppBundle:Factura')->getAllActivas($this->getUser()->getUnidadNegocio());
					}else{
						$ventas = $em->getRepository('AppBundle:Factura')->getAllActivas($request->get('unidadnegocio'));
					}
		
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Ventas');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Ventas ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Ventas (u$s)');			
					
					foreach($ventas as $venta){
						if($desde < $venta->getFecha() and $venta->getFecha() < $hasta ){
							$importeFC = 0;
							if($venta->getMovimientoCC()->getMoneda() == '1' ){
								foreach ($venta->getProductosFactura() as $prodFact){
									$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
								}
								
								if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
									$desc = $importeFC*($venta->getDescuento()/100);																	
									$importeFC = $importeFC - $desc;																				
								}
			
								$importeFC = $importeFC - $venta->getBonificacion();																									
								$saldoP = $saldoP + $importeFC;
							
							}else{
								foreach ($venta->getProductosFactura() as $prodFact){
									$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
								}
								
								if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
									$desc = $importeFC*($venta->getDescuento()/100);																	
									$importeFC = $importeFC - $desc;																				
								}
								
								$importeFC = $importeFC - $venta->getBonificacion();																									
								$saldoD = $saldoD + $importeFC;
							
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($ventas as $venta){
						if($desde < $venta->getFecha() and $venta->getFecha() < $hasta ){
							$importeFC = 0;
		
							foreach ($venta->getProductosFactura() as $prodFact){
								$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
							}
		
							if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
								$desc = $importeFC*($venta->getDescuento()/100);																	
								$importeFC = $importeFC - $desc;																				
							}
		
							$importeFC = $importeFC - $venta->getBonificacion();																									
		
							if($fechaAnt == ''){
								$fechaAnt = $venta->getFecha()->format('d-m-Y');
			
								if($venta->getMovimientoCC()->getMoneda() == '1' ){
									$valorAntP = $valorAntP + $importeFC;	
									$porcP = ($valorAntP * 100) / $saldoP;
								}else{
									$valorAntD = $valorAntD + $importeFC;
									$porcD = ($valorAntD * 100) / $saldoD;
								}
							}else{						
								if($fechaAnt == $venta->getFecha()->format('d-m-Y')){
									$fechaAnt = $venta->getFecha()->format('d-m-Y');
		
									if($venta->getMovimientoCC()->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $importeFC;
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $importeFC;
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{		
									$phpExcelObject->setActiveSheetIndex(0)
									   ->setCellValue('A'.$count, $fechaAnt)
									   ->setCellValue('B'.$count, round($porcP, 2))
									   ->setCellValue('C'.$count, $valorAntP)
									   ->setCellValue('D'.$count, round($porcD, 2))
									   ->setCellValue('E'.$count, $valorAntD);		
									$valorAntD = 0;
									$valorAntP = 0;
									$porcP = 0;
									$porcD = 0;
									if($venta->getMovimientoCC()->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $importeFC;
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $importeFC;
										$porcD = ($valorAntD * 100) / $saldoD;
									}
									$fechaAnt = $venta->getFecha()->format('d-m-Y');
									$count = $count + 1;
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				if($request->get('tipo')=='compras'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Compras');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Compras ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Compras (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="F"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="F"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				if($request->get('tipo')=='cobranzas'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Cobranzas');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Cobranzas ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Cobranzas (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="R"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="R"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
		
				if($request->get('tipo')=='pagos'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Pagos');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Pagos ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Pagos (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="RP"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="RP"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				
				if($request->get('tipo')=='gastos'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Gastos ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Gastos (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="G"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="G"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
			}else{
	// AGRUPAMIENTO MES
	
				if($request->get('tipo')=='ventas'){
		
					$ventas = $em->getRepository('AppBundle:Factura')->findAll();
		
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Ventas');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Ventas ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Ventas (u$s)');			
					
					foreach($ventas as $venta){
						if($desde < $venta->getFecha() and $venta->getFecha() < $hasta ){
							$importeFC = 0;
							if($venta->getMovimientoCC()->getMoneda() == '1' ){
								foreach ($venta->getProductosFactura() as $prodFact){
									$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
								}
								
								if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
									$desc = $importeFC*($venta->getDescuento()/100);																	
									$importeFC = $importeFC - $desc;																				
								}
			
								$importeFC = $importeFC - $venta->getBonificacion();																									
								$saldoP = $saldoP + $importeFC;
							
							}else{
								foreach ($venta->getProductosFactura() as $prodFact){
									$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
								}
								
								if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
									$desc = $importeFC*($venta->getDescuento()/100);																	
									$importeFC = $importeFC - $desc;																				
								}
								
								$importeFC = $importeFC - $venta->getBonificacion();																									
								$saldoD = $saldoD + $importeFC;
							
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($ventas as $venta){
						if($desde < $venta->getFecha() and $venta->getFecha() < $hasta ){
							$importeFC = 0;
		
							foreach ($venta->getProductosFactura() as $prodFact){
								$importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC; 
							}
		
							if($venta->getDescuento() != 0 and $venta->getDescuento() != ''){
								$desc = $importeFC*($venta->getDescuento()/100);																	
								$importeFC = $importeFC - $desc;																				
							}
		
							$importeFC = $importeFC - $venta->getBonificacion();																									
		
							if($fechaAnt == ''){
								$fechaAnt = $venta->getFecha()->format('m-Y');
			
								if($venta->getMovimientoCC()->getMoneda() == '1' ){
									$valorAntP = $valorAntP + $importeFC;	
									$porcP = ($valorAntP * 100) / $saldoP;
								}else{
									$valorAntD = $valorAntD + $importeFC;
									$porcD = ($valorAntD * 100) / $saldoD;
								}
							}else{						
								if($fechaAnt == $venta->getFecha()->format('m-Y')){
									$fechaAnt = $venta->getFecha()->format('m-Y');
		
									if($venta->getMovimientoCC()->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $importeFC;
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $importeFC;
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{		
									$phpExcelObject->setActiveSheetIndex(0)
									   ->setCellValue('A'.$count, $fechaAnt)
									   ->setCellValue('B'.$count, round($porcP, 2))
									   ->setCellValue('C'.$count, $valorAntP)
									   ->setCellValue('D'.$count, round($porcD, 2))
									   ->setCellValue('E'.$count, $valorAntD);		
									$valorAntD = 0;
									$valorAntP = 0;
									$porcP = 0;
									$porcD = 0;
									if($venta->getMovimientoCC()->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $importeFC;
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $importeFC;
										$porcD = ($valorAntD * 100) / $saldoD;
									}
									$fechaAnt = $venta->getFecha()->format('m-Y');
									$count = $count + 1;
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				if($request->get('tipo')=='compras'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Compras');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Compras ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Compras (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="F"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						$this->sessionManager->setSession('MES', $doc->getFechaRegistracion()->format('m-Y'));
						if($doc->getTipoDocumento()=="F"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				if($request->get('tipo')=='cobranzas'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Cobranzas');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Cobranzas ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Cobranzas (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="R"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="R"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
		
				if($request->get('tipo')=='pagos'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Pagos');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Pagos ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Pagos (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="RP"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="RP"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}
				
				
				if($request->get('tipo')=='gastos'){
					$phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A3', 'Dia/Mes')
					   ->setCellValue('B3', '% del total del periodo')
					   ->setCellValue('C3', 'Gastos ($)')
					   ->setCellValue('D3', '% del total del periodo')
					   ->setCellValue('E3', 'Gastos (u$s)');			
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="G"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($doc->getMoneda() == '1' ){
									$saldoP = $saldoP + $doc->getImporte();
								}else{
									$saldoD = $saldoD + $doc->getImporte();
								}
							}
						}
					}
		
					$fechaAnt = '';
					$valorAntP = 0;
					$valorAntD = 0;
					foreach($documentos as $doc){
						if($doc->getTipoDocumento()=="G"){
							if($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta ){
								if($fechaAnt == ''){
									$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
									if($doc->getMoneda() == '1' ){
										$valorAntP = $valorAntP + $doc->getImporte();
										$porcP = ($valorAntP * 100) / $saldoP;
									}else{
										$valorAntD = $valorAntD + $doc->getImporte();
										$porcD = ($valorAntD * 100) / $saldoD;
									}
								}else{						
									if($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')){
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
									}else{		
										$phpExcelObject->setActiveSheetIndex(0)
										   ->setCellValue('A'.$count, $fechaAnt)
										   ->setCellValue('B'.$count, round($porcP, 2))
										   ->setCellValue('C'.$count, $valorAntP)
										   ->setCellValue('D'.$count, round($porcD, 2))
										   ->setCellValue('E'.$count, $valorAntD);		
										$valorAntD = 0;
										$valorAntP = 0;
										$porcP = 0;
										$porcD = 0;
										if($doc->getMoneda() == '1' ){
											$valorAntP = $valorAntP + $doc->getImporte();
											$porcP = ($valorAntP * 100) / $saldoP;
										}else{
											$valorAntD = $valorAntD + $doc->getImporte();
											$porcD = ($valorAntD * 100) / $saldoD;
										}
										$fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
										$count = $count + 1;
									}
								}
							}
						}
					}
					$phpExcelObject->setActiveSheetIndex(0)
					   ->setCellValue('A'.$count, $fechaAnt)
					   ->setCellValue('B'.$count, round($porcP, 2))
					   ->setCellValue('C'.$count, $valorAntP)
					   ->setCellValue('D'.$count, round($porcD, 2))
					   ->setCellValue('E'.$count, $valorAntD);		
					$count = $count + 1;
				}		
			}
			
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A'.$count, 'TOTALES')
			   ->setCellValue('B'.$count, '100%')
			   ->setCellValue('C'.$count, '$'.$saldoP)
			   ->setCellValue('D'.$count, '100%')
			   ->setCellValue('E'.$count, 'u$s'.$saldoD);		
	
			$phpExcelObject->getActiveSheet()->getStyle('A3:E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
	
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$phpExcelObject->setActiveSheetIndex(0);
			$phpExcelObject->getActiveSheet()->getStyle('A'.$count.':E'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
			$phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			$phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
			$phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
			// create the response
			$response = $this->get('phpexcel')->createStreamedResponse($writer);
			// adding headers
			$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
	
			if($request->get('tipo')=='compras'){
				$response->headers->set('Content-Disposition', 'attachment;filename=InformeCompras.xls');
			}
			
			if($request->get('tipo')=='cobranzas'){
				$response->headers->set('Content-Disposition', 'attachment;filename=InformeCobranzas.xls');
			}
			
			if($request->get('tipo')=='pagos'){
				$response->headers->set('Content-Disposition', 'attachment;filename=InformePagos.xls');
			}
			
			if($request->get('tipo')=='ventas'){
				$response->headers->set('Content-Disposition', 'attachment;filename=InformeVentas.xls');
			}
			
			
			if($request->get('tipo')=='gastos'){
				$response->headers->set('Content-Disposition', 'attachment;filename=InformeGastos.xls');
			}
			
			$response->headers->set('Pragma', 'public');
			$response->headers->set('Cache-Control', 'maxage=1');
			
			return $response;

    }
	
	
    public function facturaproveedorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Documento();
		$movimientocc = new MovimientoCC();
		$entity->setTipoGasto(NULL);
		$valor = 0;
		$fecha = new \DateTime($request->get('fechaDocumento'));
		$fechareg = new \DateTime('NOW');
		$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));			
		$cliPro = $em->getRepository('AppBundle:ClienteProveedor')->find($request->get('proveedor'));

		$movimientocc->setUnidadNegocio($unidad);			
		$movimientocc->setClienteProveedor($cliPro);            
		$movimientocc->setDocumento($entity);
		$movimientocc->setTipoDocumento('F');
		$movimientocc->setMoneda($request->get('moneda'));

		$entity->setIibbcap($request->get('iibbcap'));
		$entity->setIibbprov($request->get('iibbprov'));
		$entity->setIvacap($request->get('ivacap'));
		$entity->setIvaprov($request->get('ivaprov'));
		$entity->setNrocomprobante($request->get('nrocomprobante'));
        
		$entity->setMoneda($request->get('moneda'));
		$entity->setDescuento($request->get('descuento'));
		$entity->setFecha($fecha);
		$entity->setFechaRegistracion($fechareg);
		$entity->setBonificacion($request->get('bonificacion'));
		$entity->setTipoDocumento('F');
        $entity->setImporte($valor);

		$entity->setMovimientocc($movimientocc);

		$contador = $request->get('contador');		
		
		for($x = 0; $x <= $contador; $x++){
			$codigo = $request->get('codigo'.$x);
			$desc = $request->get('descripcion'.$x);
			$precio = $request->get('precio'.$x);
			$cantidad = $request->get('cantidad'.$x);
			
			if(!is_null($codigo) and $codigo!='' and $desc != '' and !is_null($desc) and $precio != '' and !is_null($precio)and $cantidad != '' and !is_null($cantidad) ){
				$productoDocumento = new ProductoDocumento();
				if($codigo=='000'){
					$producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo));					
					$productoDocumento->setProducto($producto);
					$productoDocumento->setDocumento($entity);				
					$productoDocumento->setPrecio($request->get('precio'.$x));				
					$productoDocumento->setCantidad($request->get('cantidad'.$x));				
					$entity->addProductosDocumento($productoDocumento);
				}else{
					$producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio'=>$unidad));					
					
					if(is_null($producto)){
						$producto = new Producto();

						$categoriasubcategoria = $em->getRepository('AppBundle:Categoriasubcategoria')->find(1);
						if(!is_null($categoriasubcategoria)){
							$producto->setCategoriasubcategoria($categoriasubcategoria);
						}
						$producto->setCodigo($codigo);
						$producto->setDescripcion($desc);
						$producto->setUnidadNegocio($unidad);
						$producto->setMoneda($request->get('moneda'));
						$producto->setStock($request->get('cantidad'.$x));
						$em->persist($producto);
						$producto->setCosto($request->get('precio'.$x));					
					}else{
						$stock = $producto->getStock() + $request->get('cantidad'.$x);
						$producto->setStock($stock);
						if($producto->getMoneda()==$request->get('moneda')){
							$producto->setCosto($request->get('precio'.$x));					
						}
					}

					$productoDocumento->setProducto($producto);
					$productoDocumento->setDocumento($entity);
					$productoDocumento->setPrecio($request->get('precio'.$x));			
					$productoDocumento->setCantidad($request->get('cantidad'.$x));
					
					$entity->addProductosDocumento($productoDocumento);
					$em->persist($productoDocumento);
				}

				$valor = $valor + ($precio*$cantidad);
			}
		}	
		
		if(count($entity->getProductosDocumento())==0){
			$this->sessionManager->addFlash('msgError','No puede generar una factura sin productos.');
			return $this->redirect($this->generateUrl('documento_compras'));
		}

		$em->persist($entity);
		$em->persist($movimientocc);
		$em->flush();
        
        if($entity->getDescuento()!='' and $entity->getDescuento()>0){
            $valor = $valor - ($entity->getDescuento() * $valor / 100);
        }
        if($entity->getBonificacion()!='' and $entity->getBonificacion()>0){
            $valor = $valor - $entity->getBonificacion(); 
        }
        
		$valorfinal = $request->get('importetotal');
		if(isset($valorfinal) and $valorfinal!=''){
            $valoriva = $valorfinal - $valor;
            $entity->setImporte($valorfinal);
            $entity->setImporteiva($valoriva);
		}else{
            $entity->setImporte($valor); 
		}
		$em->flush();

		$this->sessionManager->addFlash('msgOk','Compra registrada.');
		return $this->redirect($this->generateUrl('documento_show', array('id'=>$entity->getId())));
    }
    public function calcularetencionAction(Request $request)
    {	
        $em = $this->getDoctrine()->getManager();
		$idretencion = $request->get('data');
		$idproveedor = $request->get('data2');
		$totalneto = $request->get('data3');
        
		$tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->find($idretencion);			
		$proveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($idproveedor);			
		$response = '';

        $iibb = strrpos($tiporetencion->getDescripcion(), "IIBB");
        if($iibb != -1){
    		$resultado = $em->getRepository('AppBundle:Documento')
                            ->calculaganancias($idproveedor);			                
            $resultado = $resultado + $totalneto;
            if($resultado>$tiporetencion->getImporte()){
                $resta = $resultado - $tiporetencion->getImporte();
                $response = ($resta * $proveedor->getPorcentaje())/100;
            }
        }

        $iva = strrpos($tiporetencion->getDescripcion(), "IVA");
        if($iva != -1 ){
    		$resultado = $em->getRepository('AppBundle:Documento')
                            ->calculaganancias($idproveedor);			                
            $resultado = $resultado + $totalneto;
            if($resultado>$tiporetencion->getImporte()){
                $resta = $resultado - $tiporetencion->getImporte();
                $response = ($resta * $proveedor->getPorcentajeiva())/100;
            }
        }

        $gan = strrpos($tiporetencion->getDescripcion(), "GANANCIAS");
        if($gan != -1){
    		$resultado = $em->getRepository('AppBundle:Documento')
                            ->calculaganancias($idproveedor, $proveedor->getUnidadNegocio()->getId());			                
            if($resultado<$tiporetencion->getImporte()){
                $resultado = $resultado + $totalneto;
                if($resultado>$tiporetencion->getImporte()){
                    $resta = $resultado - $tiporetencion->getImporte();
                    $response = ($resta * $tiporetencion->getPorcentaje())/100;
                }
            }else{
                $response = ($totalneto * $tiporetencion->getPorcentaje())/100;
            }
        }
		return new Response($response);		
    }
}
