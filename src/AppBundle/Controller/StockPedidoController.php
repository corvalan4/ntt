<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\StockPedido;
use AppBundle\Form\StockPedidoType;

/**
 * StockPedido controller.
 *
 */
class StockPedidoController extends Controller
{

    /**
     * Lists all StockPedido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:StockPedido')->findAll();

        return $this->render('AppBundle:StockPedido:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new StockPedido entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new StockPedido();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stockpedido_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:StockPedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a StockPedido entity.
     *
     * @param StockPedido $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StockPedido $entity)
    {
        $form = $this->createForm(new StockPedidoType(), $entity, array(
            'action' => $this->generateUrl('stockpedido_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new StockPedido entity.
     *
     */
    public function newAction()
    {
        $entity = new StockPedido();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:StockPedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a StockPedido entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StockPedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPedido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:StockPedido:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing StockPedido entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StockPedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPedido entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:StockPedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a StockPedido entity.
    *
    * @param StockPedido $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(StockPedido $entity)
    {
        $form = $this->createForm(new StockPedidoType(), $entity, array(
            'action' => $this->generateUrl('stockpedido_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing StockPedido entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StockPedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPedido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('stockpedido_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:StockPedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a StockPedido entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:StockPedido')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StockPedido entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stockpedido'));
    }

    /**
     * Creates a form to delete a StockPedido entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stockpedido_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
