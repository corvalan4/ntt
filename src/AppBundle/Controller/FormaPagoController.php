<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\FormaPago;
use AppBundle\Form\FormaPagoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * FormaPago controller.
 *
 */
class FormaPagoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all FormaPago entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:FormaPago')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));

        return $this->render('AppBundle:FormaPago:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new FormaPago entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new FormaPago();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('formapago'));
        }

        return $this->render('AppBundle:FormaPago:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a FormaPago entity.
     *
     * @param FormaPago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FormaPago $entity) {
        $form = $this->createForm(new FormaPagoType(), $entity, array(
            'action' => $this->generateUrl('formapago_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new FormaPago entity.
     *
     */
    public function newAction() {
        $entity = new FormaPago();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:FormaPago:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FormaPago entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:FormaPago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormaPago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:FormaPago:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FormaPago entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:FormaPago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormaPago entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:FormaPago:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a FormaPago entity.
     *
     * @param FormaPago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(FormaPago $entity) {
        $form = $this->createForm(new FormaPagoType(), $entity, array(
            'action' => $this->generateUrl('formapago_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing FormaPago entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:FormaPago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormaPago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('formapago'));
        }

        return $this->render('AppBundle:FormaPago:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FormaPago entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:FormaPago')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FormaPago entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('formapago'));
    }

    /**
     * Creates a form to delete a FormaPago entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('formapago_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /*
     * Render Select
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $formapagos = $em->getRepository('AppBundle:FormaPago')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:FormaPago:select.html.twig', array(
                    'formapagos' => $formapagos
        ));
    }

}
