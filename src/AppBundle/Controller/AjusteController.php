<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Ajuste;
use AppBundle\Entity\MovimientoCaja;
use AppBundle\Form\AjusteType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Ajuste controller.
 *
 */
class AjusteController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Ajuste entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Ajuste')->findAll();

        return $this->render('AppBundle:Ajuste:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Ajuste entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Ajuste();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $idCaja = $request->get('idCaja');
            $caja = $em->getRepository('AppBundle:Caja')->find($idCaja);            
            $movimientoCaja = new MovimientoCaja();
            $movimientoCaja->setImporte($entity->getImporte());
            $movimientoCaja->setCaja($caja);
            $movimientoCaja->setAjuste($entity);
            $em->persist($movimientoCaja);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ajuste_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Ajuste:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Ajuste entity.
     *
     * @param Ajuste $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Ajuste $entity) {
        $form = $this->createForm(new AjusteType(), $entity, array(
            'action' => $this->generateUrl('ajuste_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-success pull-right')));

        return $form;
    }

    /**
     * Displays a form to create a new Ajuste entity.
     *
     */
    public function newAction(Request $request, $id) {
        $entity = new Ajuste();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Ajuste:new.html.twig', array(
                    'entity' => $entity,
                    'id' => $id,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Ajuste entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Ajuste')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ajuste entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Ajuste:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Ajuste entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Ajuste')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ajuste entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Ajuste:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Ajuste entity.
     *
     * @param Ajuste $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Ajuste $entity) {
        $form = $this->createForm(new AjusteType(), $entity, array(
            'action' => $this->generateUrl('ajuste_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Ajuste entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Ajuste')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ajuste entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ajuste_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Ajuste:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Ajuste entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Ajuste')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ajuste entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ajuste'));
    }

    /**
     * Creates a form to delete a Ajuste entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('ajuste_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
