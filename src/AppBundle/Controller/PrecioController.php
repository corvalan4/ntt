<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Precio;
use AppBundle\Form\PrecioType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Precio controller.
 *
 */
class PrecioController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;	

    /**
     * Lists all Precio entities.
     *
     */
    public function indexAction()
    {

			$em = $this->getDoctrine()->getManager();
	
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$entities = $em->getRepository('AppBundle:Precio')->findAllOrderedByLista($this->getUser()->getUnidadNegocio());
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				$entities = $em->getRepository('AppBundle:Precio')->findAllOrderedByLista();
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas();	
			}
			
			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();	

			$this->sessionManager->setSession('nombreProducto', '');		
			$this->sessionManager->setSession('idProducto', '');		
			$this->sessionManager->setSession('codigoProducto', '');		
	
			return $this->render('AppBundle:Precio:index.html.twig', array(
				'entities' => $entities,
				'unidades' => $unidades,
				'listaPrecios' => $listaPrecios,
			));

    }

    public function filtroAction(Request $request)
    {


		$em = $this->getDoctrine()->getManager();

		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

		$producto = NULL;
		
		if($request->get('codigo')==''){
			$entities = $em->getRepository('AppBundle:Precio')->getAllByUnidad($request->get('unidadnegocio'));				
			$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas();	
			return $this->render('AppBundle:Precio:index.html.twig', array(
				'entities' => $entities,
				'unidades' => $unidades,
				'listaPrecios' => $listaPrecios,
			));
		}	

		$this->sessionManager->setSession('codigo', $request->get('codigo'));					
		
		if($request->get('codigo')!='000' and $this->getUser()->getUnidadNegocio()!=''){
			$producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio'=>$this->getUser()->getUnidadNegocio()));
		}else{
			if($this->getUser()->getUnidadNegocio()==''){
				$producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio'=>$request->get('unidadnegocio')));
			}			
		}

		$this->sessionManager->setSession('nombreProducto', $producto->getDescripcion());					
		$this->sessionManager->setSession('codigoProducto', $producto->getCodigo());					
		
		if(!is_null($producto)){
			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$entities = $em->getRepository('AppBundle:Precio')->findByProducto($producto->getId(), $this->getUser()->getUnidadNegocio());
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				$entities = $em->getRepository('AppBundle:Precio')->findByProducto($producto->getId());
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas();	
			}
			
			if(count($entities)>1){
				return $this->render('AppBundle:Precio:index.html.twig', array(
					'entities' => $entities,
					'unidades' => $unidades,
					'listaPrecios' => $listaPrecios,
				));
			}
			if(count($entities)==1){
				foreach($entities as $entidad)
				{
					$idPrecio = $entidad->getId();
				}
				return $this->redirect($this->generateUrl('precio_edit', array('id' => $idPrecio)));
			}
			if(count($entities)<1){
				$this->sessionManager->setSession('nombreProducto', $producto->getDescripcion());		
				$this->sessionManager->setSession('idProducto', $producto->getId());		
				$this->sessionManager->setSession('codigoProducto', $producto->getCodigo());		
				return $this->redirect($this->generateUrl('precio_new'));
			}
		
		}else{
			$this->sessionManager->addFlash('msgWarn','No se han encontrado resultados.');		
			return $this->redirect($this->generateUrl('precio'));
		}
    }

    /**
     *
     */
    public function filtroLAction(Request $request)
    {		

			$em = $this->getDoctrine()->getManager();

			if(!$this->isGranted('ROLE_SUPER_ADMIN')){
				$entities = $em->getRepository('AppBundle:Precio')->findByLista($request->get('listado'));
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas($this->getUser()->getUnidadNegocio());
			}else{
				$entities = $em->getRepository('AppBundle:Precio')->findByLista($request->get('listado'));
				$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas();	
			}

			$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
	
			return $this->render('AppBundle:Precio:index.html.twig', array(
				'entities' => $entities,
				'unidades' => $unidades,
				'listaPrecios' => $listaPrecios,
			));

    }
	

    /**
     *
     */
    public function prodEditAction($id)
    {

		
		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository('AppBundle:Precio')->findByProducto($id);
		$producto = $em->getRepository('AppBundle:Producto')->find($id);

		if(count($entities)<1){
			$this->sessionManager->setSession('nombreProducto', $producto->getDescripcion());		
			$this->sessionManager->setSession('idProducto', $producto->getId());		
			$this->sessionManager->setSession('codigoProducto', $producto->getCodigo());		
			return $this->redirect($this->generateUrl('precio_new'));
		}

		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
		$listaPrecios = $em->getRepository('AppBundle:ListaPrecio')->getAllActivas();
		
		return $this->render('AppBundle:Precio:index.html.twig', array(
			'entities' => $entities,
			'unidades' => $unidades,
			'listaPrecios' => $listaPrecios,
		));
    }

    /**
     * Creates a new Precio entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Precio();
        $form = $this->createCreateForm($entity, $request->get('unidadnegocio'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

			$this->sessionManager->addFlash('msgOk', 'Precio dado de alta. Cod: '.$entity->getProducto()->getCodigo());
            return $this->redirect($this->generateUrl('precio'));
        }

        return $this->render('AppBundle:Precio:new.html.twig', array(
            'entity' => $entity,
			'unidad' => $request->get('unidadnegocio'),
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Precio entity.
     *
     * @param Precio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Precio $entity, $unidad = '')
    {
		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$unidad = $this->getUser()->getUnidadNegocio();
		}
        
		$form = $this->createForm(new PrecioType($unidad), $entity, array(
            'action' => $this->generateUrl('precio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Precio entity.
     *
     */
    public function newAction(Request $request)
    {

            $em = $this->getDoctrine()->getManager();

			$entity = new Precio();
			$form   = $this->createCreateForm($entity, $request->get('unidadnegocio'));

			return $this->render('AppBundle:Precio:new.html.twig', array(
				'entity' => $entity,
				'unidad' => $request->get('unidadnegocio'),
				'form'   => $form->createView(),
			));

    }

    /**
     * Finds and displays a Precio entity.
     *
     */
    public function showAction($id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Precio')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Precio entity.');
			}
	
			$deleteForm = $this->createDeleteForm($id);
	
			return $this->render('AppBundle:Precio:show.html.twig', array(
				'entity'      => $entity,
				'delete_form' => $deleteForm->createView(),
			));

    }

    /**
     * Displays a form to edit an existing Precio entity.
     *
     */
    public function editAction($id)
    {

			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('AppBundle:Precio')->find($id);
	
			$editForm = $this->createEditForm($entity, $entity->getListaPrecio()->getUnidadNegocio()->getId());
	
			return $this->render('AppBundle:Precio:edit.html.twig', array(
				'entity'      => $entity,
				'edit_form'   => $editForm->createView(),
			));

    }

    /**
    * Creates a form to edit a Precio entity.
    *
    * @param Precio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Precio $entity, $unidad = '')
    {
        $form = $this->createForm(new PrecioType($unidad), $entity, array(
            'action' => $this->generateUrl('precio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing Precio entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Precio')->find($id);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $request->get('unidadnegocio'));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

			$this->sessionManager->addFlash('msgOk', 'Se ha modificado el precio. Cod: '.$entity->getProducto()->getCodigo());
            return $this->redirect($this->generateUrl('precio'));
        }

        return $this->render('AppBundle:Precio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Precio entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('AppBundle:Precio')->find($id);

		$em->remove($entity);
		$em->flush();

        return $this->redirect($this->generateUrl('precio'));
    }

    /**
     * Creates a form to delete a Precio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('precio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr'=> array('class'=>'btn')))
            ->getForm()
        ;
    }
}
