<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * Usuario controller.
 *
 */
class UsuarioController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Usuario entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_VENDEDOR')) {
            $entities = new ArrayCollection();
            $entities->add($this->getUser());
        }
        if ($this->isGranted('ROLE_ADMIN')) {
            $entities = $em->getRepository('AppBundle:Usuario')->findBy(array('unidadNegocio' => $this->getUser()->getUnidadNegocio()));
        }
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $entities = $em->getRepository('AppBundle:Usuario')->findBy([], ['apellidonombre' => 'ASC']);
        }

        return $this->render('AppBundle:Usuario:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Usuario entity.
     *
     */
    public function createAction(Request $request) {

        $entity = new Usuario();

        $form = $this->createCreateForm($entity);
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $form->add('roles', 'choice', array(
                'choices' => $this->getExistingRoles(),
                'data' => $entity->getRoles(),
                'label' => 'Roles',
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'mapped' => true,
            ));
        }

        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            if (empty($entity->getPassword())) {
                $entity->setPassword(uniqid());
            }

            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updatePassword($entity);

            if (in_array('ROLE_SUPER_ADMIN', $entity->getRoles())) {
                $entity->setUnidadNegocio(NULL);
            }
            if (!$this->isGranted('ROLE_SUPER_ADMIN') and $this->isGranted('ROLE_ADMIN')) {
                $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
                $entity->setUnidadNegocio($unidad);
            }

            $em->persist($entity);
            try {
                $em->flush();
            } catch (\Exception $e) {
                    
                $this->sessionManager->addFlash('msgError', 'No ha sido posible crear el usuario. Verifique el e-mail y nombre de usuario ya no se encuentren en uso.');
                return $this->render('AppBundle:Usuario:new.html.twig', array(
                            'entity' => $entity,
                            'form' => $form->createView(),
                ));
            }

            $this->sessionManager->addFlash('msgOk', 'Usuario creado exitosamente.');

            return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render('AppBundle:Usuario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuario $entity) {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    public function getExistingRoles() {
        $roleHierarchy = $this->container->getParameter('security.role_hierarchy.roles');
        $roles = array_keys($roleHierarchy);

        foreach ($roles as $role) {
            $theRoles[$role] = $role;
        }
        return $theRoles;
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     */
    public function newAction() {

        $entity = new Usuario();
        $form = $this->createCreateForm($entity);
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $form->add('roles', 'choice', array(
                'choices' => $this->getExistingRoles(),
                'data' => $entity->getRoles(),
                'label' => 'Roles',
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'mapped' => true,
            ));
        }

        return $this->render('AppBundle:Usuario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Usuario entity.
     *
     */
    public function showAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Usuario:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $editForm->add('roles', 'choice', array(
                'choices' => $this->getExistingRoles(),
                'data' => $entity->getRoles(),
                'label' => 'Roles',
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'mapped' => true,
            ));
        }
        return $this->render('AppBundle:Usuario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Usuario $entity) {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $editForm = $this->createEditForm($entity);
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $editForm->add('roles', 'choice', array(
                'choices' => $this->getExistingRoles(),
                'data' => $entity->getRoles(),
                'label' => 'Roles',
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'mapped' => true,
            ));
        }

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updatePassword($entity);

            if (in_array('ROLE_SUPER_ADMIN', $entity->getRoles())) {
                $entity->setUnidadNegocio(NULL);
            }
            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Usuario modificado exitosamente.');

            return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render('AppBundle:Usuario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Usuario entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Usuario')->find($id);
        $entity->setEstado('B');
        $entity->setEnabled(0);
        $em->flush();
        $this->sessionManager->addFlash('msgOk', 'Usuario deshabilitado exitosamente.');
        return $this->redirect($this->generateUrl('usuario'));
    }

    public function activarAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Usuario')->find($id);
        $entity->setEstado('A');
        $entity->setEnabled(1);
        $em->flush();
        $this->sessionManager->addFlash('msgOk', 'Usuario habilitado exitosamente.');
        return $this->redirect($this->generateUrl('usuario'));
    }

    /**
     * Creates a form to delete a Usuario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('usuario_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn')))
                        ->getForm()
        ;
    }

    /*
     * Render Select Vendedor
     */

    public function renderSelectVendedorAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $vendedores = $em->getRepository('AppBundle:Usuario')->getUsersByRol('ROLE_VENDEDOR', $estado);
        return $this->render('AppBundle:Usuario:select.html.twig', array(
                    'vendedores' => $vendedores
        ));
    }

}
