<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Entity\ListaPrecio;
use AppBundle\Entity\Precio;
use AppBundle\Entity\Producto;

class DefaultController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

	public function indexAction()
	{
		return $this->render('AppBundle:Default:index.html.twig');
	}

	public function generarAction()
	{
		$hoy = new \DateTime('NOW');

		$pdfGenerator = $this->get('siphoc.pdf.generator');
		$pdfGenerator->setName('imprimirPrueba'.$hoy->format('Y-m-d H:i:s').'.pdf');
		return $pdfGenerator->downloadFromView('AppBundle:Default:imprimir.html.twig');
	}


	public function restClaveAction(Request $request)
	{
		return $this->render('AppBundle:Default:olvideClave.html.twig');
	}

	public function olvideClaveAction(Request $request)
	{
		$email = $request->get('usuario');
		$em = $this->getDoctrine()->getManager();

		$usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('email' => $email));
		if(!is_null($usuario)){
			$mail = $usuario->getClave();
			if($mail!='' and isset($mail)){
				//crear mail y mandar
				$message = \Swift_Message::newInstance()
					->setSubject('Recuperacion de clave')
					->setFrom('info@sistemastitanio.com.ar')
					->setTo($usuario->getMail())
					->setBody(
						$this->renderView('AppBundle:Default:email.txt.twig', array('login' => $usuario->getUsername(), 'pass' => ""))
					);
				$this->get('mailer')->send($message);

				//mensaje ok
				$this->sessionManager->addFlash('msgOk','Su contraseña ha sido enviada a su dirección de correo electrónico.');
			}else{
				$this->sessionManager->addFlash('msgOk','No pudimos reestablecer su clave, ya que no tiene mail.');
			}

		}else{
			$this->sessionManager->addFlash('msgErr','Usuario inexistente.');
		}
		return $this->redirect($this->generateUrl('joyas_joyas_homepage'));

	}

	public function cargaMasivaAction()
	{
		$em = $this->getDoctrine()->getManager();
		$unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

		return $this->render('AppBundle:Default:cargamasiva.html.twig', array(
			'unidades' => $unidades,
		));
	}

	public function importarArchivosAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
		$uploaddir = $this->get('kernel')->getRootDir()."/../titanio/uploads/documents/";
		// Cell caching ::
		$cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		\PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
		$cA = 0;
		$filename = '';
		$a = $request->get('desde');
		$b = $a + 1;
		$existe = 's';
		$hayAlgo = 'NO';

		if($a == 1){
			if($this->isGranted('ROLE_SUPER_ADMIN')){
				$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
			}else{
				$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
			}

			$this->sessionManager->setSession('unidad', $unidad->getId());

			$name = trim($_FILES['archivo']['name']);
			$filesize = $_FILES['archivo']['size'];
			$uploadfile = $uploaddir . $name;
			$files = $request->files;
			$uploadedFile = $files->get('archivo');

			if ($uploadedFile->move($uploaddir, $name)){
			}else{
				$this->sessionManager->addFlash('msgError', 'No se pudo cargar el archivo.');
			}
		}else{
			$this->sessionManager->setSession('unidad', $request->get('unidad'));
			$unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
			$this->sessionManager->setSession('unidadimp', $request->get('unidadimp'));
			$uploadfile = $this->sessionManager->getSession('uploadfile');
		}

		if (!file_exists($uploadfile)) {
			$this->sessionManager->addFlash('msgError', 'No se pudo migrar el archivo.');
			return $this->redirect($this->generateUrl('joyas_joyas_cargamasiva'));
		}

		ini_set('memory_limit', '-1');
		$inputFileType = 'Excel2007';
		$sheetname = 'Hoja1';
		$objReader =  \PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setLoadSheetsOnly($sheetname);
		$objPHPExcel = $objReader->load($uploadfile);
		$vacio = false;

		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			for($i = $a; $i<=$b; $i++) {
				$rowIndex = $i;
				if ($rowIndex == 1){
					for($numlet=1;$numlet<5;$numlet++){
						if($numlet==1){$letra='G';}
						if($numlet==2){$letra='H';}
						if($numlet==3){$letra='I';}
						if($numlet==4){$letra='J';}

						$descLista = $worksheet->getCell($letra.'1')->getCalculatedValue();

						if(!isset($descLista) or $descLista==''){
							break;
						}
						$listaPrecio = $em->getRepository('AppBundle:ListaPrecio')->findOneBy(array('descripcion' => $descLista, 'unidadNegocio'=>$unidad->getId()));

						if(is_null($listaPrecio)){
							$listaPrecio = new ListaPrecio();
							$listaPrecio->setDescripcion($descLista);
							$listaPrecio->setUnidadNegocio($unidad);

							$cellMoneda = $worksheet->getCell('F2');
							if($cellMoneda->getCalculatedValue()=='us'){
								$listaPrecio->setMoneda('2');
							}else{
								$listaPrecio->setMoneda('1');
							}
							$em->persist($listaPrecio);
							$em->flush();
							$this->sessionManager->setSession('lista'.$listaPrecio->getDescripcion(), $listaPrecio);
						}
					}
				}
				if (!$vacio && $rowIndex!=1 ){
					for($columnas=1;$columnas<11;$columnas++){
						if($columnas==1){$letracont='A';}
						if($columnas==2){$letracont='B';}
						if($columnas==3){$letracont='C';}
						if($columnas==4){$letracont='D';}
						if($columnas==5){$letracont='E';}
						if($columnas==6){$letracont='F';}
						if($columnas==7){$letracont='G';}
						if($columnas==8){$letracont='H';}
						if($columnas==9){$letracont='I';}
						if($columnas==10){$letracont='J';}

						if ($columnas == 1){
							$categoriasubcategoria = null;
							$id=$worksheet->getCell($letracont . $rowIndex)->getCalculatedValue();
							if(isset($id) or $id!='')
							{
								$categoriasubcategoria = $em->getRepository('AppBundle:Categoriasubcategoria')->findOneBy(array('id' => $id));
							}
						}
						if ($columnas == 2){
							$codigo=$worksheet->getCell($letracont . $rowIndex)->getCalculatedValue();
							if($codigo=='' or !isset($codigo)){
								break;
							}
							$producto = $em->getRepository('AppBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio'=>$unidad->getId()));
							if(is_null($producto)){
								$producto = new Producto();
								$producto->setUnidadNegocio($unidad);
								$producto->setCodigo($codigo);
								if(!is_null($categoriasubcategoria)){
									$producto->setCategoriasubcategoria($categoriasubcategoria);
								}
								$existe = 'n';
							}else{
								if(!is_null($categoriasubcategoria)){
									$producto->setCategoriasubcategoria($categoriasubcategoria);
								}
							}
						}
						if ($columnas == 3){
							$stock=str_replace(',', '.', $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue());
							$producto->setStock($stock);
						}
						if ($columnas == 4){
							$producto->setCosto(str_replace(',', '.', $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue()));
						}
						if ($columnas == 5){
							$producto->setDescripcion($worksheet->getCell($letracont . $rowIndex)->getCalculatedValue());
						}
						if ($columnas == 6){
							if(strtoupper($worksheet->getCell($letracont . $rowIndex)->getCalculatedValue())=='US'){
								$producto->setMoneda('2');
							}else{
								$producto->setMoneda('1');
							}
							$em->persist($producto);
							$em->flush();
						}
						if ($columnas > 6){
							if(trim($worksheet->getCell($letracont . $rowIndex)->getCalculatedValue())!=''){
								$precio = new Precio();
								$precio->setValor(str_replace(',', '.', $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue()));
								$precio->setProducto($producto);

								$cellLista = $worksheet->getCell($letracont.'1');
								$listaPrecio = $em->getRepository('AppBundle:ListaPrecio')->findOneBy(array('descripcion' => $cellLista->getCalculatedValue(), 'unidadNegocio'=>$unidad->getId()));
								$precio->setListaPrecio($listaPrecio);

								$em->persist($precio);

								if($existe == 's'){
									$precioDos = $em->getRepository('AppBundle:Precio')->findOneBy(array('producto' => $producto->getId(), 'listaPrecio' => $listaPrecio->getId()));
									if(!is_null($precioDos)){
										$em->remove($precioDos);
									}
								}
							}
							$em->flush();
						}
					}
				}

				$control = $worksheet->getCell('B'.$i)->getCalculatedValue();
				if(!isset($control) or $control==''){
					$vacio = true;
				}

				$cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
				$deleted = $cacheDriver->deleteAll();
			}
			// ENVIAR DATOS
			if(!$vacio){
				$this->sessionManager->setSession('desde', $b);
				$this->sessionManager->setSession('fin', 'no');
				$this->sessionManager->setSession('uploadfile', $uploadfile);
				$this->sessionManager->setSession('unidad', $unidad->getId());
				$this->sessionManager->addFlash('msgWarn', 'Se ha actualizado desde la linea '.$a.' hasta la linea '.$b.'. Y continuando...');
				return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
			}else{
				$this->sessionManager->addFlash('msgOk', 'Carga satisfactora. Ultima linea: '.$i);
				$this->sessionManager->setSession('desde', '');
				$this->sessionManager->setSession('fin', '');
				$this->sessionManager->setSession('uploadfile', '');
			}
		}
		return $this->redirect($this->generateUrl('joyas_joyas_cargamasiva'));
	}
}

function decryptIt( $q ) {
	$cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
	$qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
	return( $qDecoded );
}
?>
