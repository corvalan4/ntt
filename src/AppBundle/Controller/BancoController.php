<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Banco;
use AppBundle\Form\BancoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Banco controller.
 *
 */
class BancoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Banco entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Banco')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));

        return $this->render('AppBundle:Banco:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Banco entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Banco();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('banco'));
        }

        return $this->render('AppBundle:Banco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Banco entity.
     *
     * @param Banco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Banco $entity) {
        $form = $this->createForm(new BancoType(), $entity, array(
            'action' => $this->generateUrl('banco_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Banco entity.
     *
     */
    public function newAction() {
        $entity = new Banco();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Banco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Banco entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Banco:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Banco entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Banco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Banco entity.
     *
     * @param Banco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Banco $entity) {
        $form = $this->createForm(new BancoType(), $entity, array(
            'action' => $this->generateUrl('banco_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Edits an existing Banco entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('banco'));
        }

        return $this->render('AppBundle:Banco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Banco entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Banco')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Banco entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('banco'));
    }

    /**
     * Creates a form to delete a Banco entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('banco_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /*
     * Render Select
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado' => $estado), array('descripcion' => 'ASC'));
        return $this->render('AppBundle:Banco:select.html.twig', array(
                    'bancos' => $bancos
        ));
    }

}
