<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Diseno;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Diseno controller.
 *
 */
class DisenoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    public function predictivaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $string = '<ul>';       
        if (strlen($request->get('valor')) > 1) {
            $entities = $em->getRepository('AppBundle:Diseno')->predictiva($request->get('valor'));

            foreach ($entities as $entity) {              

                $string .= '<li class=suggest-element id=' . $entity->getId();
                $string .= '><a href="#" id=predictivaelemento' . $entity->getId() .
                        ' data=' . $entity->getId() . ' > COD: ' .
                        $entity->getProducto()->getCodigo() . ' - ' . $entity->getProducto()->getNombre() . ' -  ' . $entity->getDescripcion();
            }
        }
        $string = $string . '</ul>';
        return new Response($string);
    }

}
