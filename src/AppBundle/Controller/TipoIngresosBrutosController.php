<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoIngresosBrutos;
use AppBundle\Form\TipoIngresosBrutosType;

/**
 * TipoIngresosBrutos controller.
 *
 */
class TipoIngresosBrutosController extends Controller
{

    /**
     * Lists all TipoIngresosBrutos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoIngresosBrutos')->findAll();

        return $this->render('AppBundle:TipoIngresosBrutos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoIngresosBrutos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoIngresosBrutos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoingresosbrutos_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:TipoIngresosBrutos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoIngresosBrutos entity.
     *
     * @param TipoIngresosBrutos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoIngresosBrutos $entity)
    {
        $form = $this->createForm(new TipoIngresosBrutosType(), $entity, array(
            'action' => $this->generateUrl('tipoingresosbrutos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoIngresosBrutos entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoIngresosBrutos();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoIngresosBrutos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoIngresosBrutos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIngresosBrutos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoIngresosBrutos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoIngresosBrutos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIngresosBrutos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoIngresosBrutos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoIngresosBrutos entity.
    *
    * @param TipoIngresosBrutos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoIngresosBrutos $entity)
    {
        $form = $this->createForm(new TipoIngresosBrutosType(), $entity, array(
            'action' => $this->generateUrl('tipoingresosbrutos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoIngresosBrutos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoIngresosBrutos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoIngresosBrutos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoingresosbrutos_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:TipoIngresosBrutos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoIngresosBrutos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoIngresosBrutos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoIngresosBrutos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoingresosbrutos'));
    }

    /**
     * Creates a form to delete a TipoIngresosBrutos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoingresosbrutos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
