<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\CuentaBancaria;
use AppBundle\Entity\MovimientoCC;
use AppBundle\Entity\MovimientoBancario;
use AppBundle\Entity\Cheque;
use AppBundle\Form\MovimientoCCType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * MovimientoCC controller.
 *
 */
class MovimientoCCController extends Controller
{

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all MovimientoCC entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:MovimientoCC')->findAll();

        return $this->render('AppBundle:MovimientoCC:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function irimpuestosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        return $this->render('AppBundle:MovimientoCC:irimpuestos.html.twig', array(
            'unidades' => $unidades,
            'tipo'     => $request->get('tipo')
        ));
    }

    public function informeimpuestosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tipo = $request->get('tipo');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $unidad = $this->getUser()->getUnidadNegocio();
        }else{
            $unidad = $request->get('unidadnegocio');
        }

        if($tipo=='ivacompras'){
            $documentos = $em->getRepository('AppBundle:Documento')->informeIvaCompras($fechaDesde, $fechaHasta, $unidad);
            return $this->render('AppBundle:MovimientoCC:ivacompras.html.twig', array(
                'documentos' => $documentos,
            ));
        }
        if($tipo=='ivaventas'){
            $ventas = $em->getRepository('AppBundle:Factura')->informeIvaVentas($fechaDesde, $fechaHasta, $unidad);
            $documentos = $em->getRepository('AppBundle:Documento')->informeIvaVentas($fechaDesde, $fechaHasta, $unidad);
            return $this->render('AppBundle:MovimientoCC:ivaventas.html.twig', array(
                'documentos' => $documentos,
                'ventas' => $ventas,
            ));
        }
        if($tipo=='retenciones'){
            $documentos = $em->getRepository('AppBundle:Documento')->informeRetenciones($fechaDesde, $fechaHasta, $unidad);
            return $this->render('AppBundle:MovimientoCC:retenciones.html.twig', array(
                'documentos' => $documentos,
            ));
        }
        if($tipo=='citiventas'){
            return $this->citiventas($fechaDesde, $fechaHasta, $unidad);
        }
        if($tipo=='citicompras'){
            return $this->citicompras($fechaDesde, $fechaHasta, $unidad);
        }
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        return $this->render('AppBundle:MovimientoCC:irimpuestos.html.twig', array(
            'unidades' => $unidades,
            'tipo'     => $request->get('tipo')
        ));
    }

    public function citiventas($fechaDesde, $fechaHasta, $unidad){
        $em = $this->getDoctrine()->getManager();
        $facturas = $em->getRepository('AppBundle:MovimientoCC')->citiventas($fechaDesde, $fechaHasta, $unidad);
        $documentos = $em->getRepository('AppBundle:MovimientoCC')->citiventasdoc($fechaDesde, $fechaHasta, $unidad);

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("NTT")
            ->setLastModifiedBy("NTT")
            ->setTitle("CITI VENTAS")
            ->setSubject("NTT");

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Fecha desde:'.$fechaDesde)
            ->setCellValue('B1', 'Fecha hasta: '.$fechaHasta);

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Campo')
            ->setCellValue('B2', 'Fecha Comprob.')
            ->setCellValue('C2', 'Tipo Comprob.')
            ->setCellValue('D2', 'Pto. Venta')
            ->setCellValue('E2', 'Nro. Comprob.')
            ->setCellValue('F2', 'Nro. Comprob. Hasta')
            ->setCellValue('G2', 'Codigo Doc. Comprador')
            ->setCellValue('H2', 'Nro. Doc. Comprador')
            ->setCellValue('I2', 'Apellido y Nombre del Comprador')
            ->setCellValue('J2', 'Importe Total de la Operacion')
            ->setCellValue('K2', 'Importe total de Conceptos que no Integran el Precio Neto Gravado')
            ->setCellValue('L2', 'Percepcion a no Categorizados')
            ->setCellValue('M2', 'Importe de Operaciones Exentas')
            ->setCellValue('N2', 'Importe de percepciones o Pagos a Cuenta de Impuestos Nacionales')
            ->setCellValue('O2', 'Importe de percepciones de Ingresos Brutos')
            ->setCellValue('P2', 'Importe de percepciones de Impuestos Municipales')
            ->setCellValue('Q2', 'Importe Impuestos Internos')
            ->setCellValue('R2', 'Codigo de Moneda')
            ->setCellValue('S2', 'Tipo de Cambio')
            ->setCellValue('T2', 'Cantidad de Alicuotas de IVA')
            ->setCellValue('U2', 'Codigo de Operacion')
            ->setCellValue('V2', 'Otros Tributos')
            ->setCellValue('W2', 'Fecha de Vencimiento de Pago')
            ->setCellValue('X2', 'Importe Neto Gravado')
            ->setCellValue('Y2', 'Alicuota de IVA')
            ->setCellValue('Z2', 'IVA Liquidado')
            ->setCellValue('AA2', 'Importacion Ventas')
            ->setCellValue('AB2', 'Importacion Alicuotas')
            ->setCellValue('AA1', 'REEMPLAZAR //// por el signo =, tocando "ctrl + b" o buscar, y yendo a la solapa de "Reemplazar"')
        ;

        $count = 3;
        foreach ($facturas as $entity){
            $tipocmb = '';
            if($entity->getFactura()){
                if($entity->getFactura()->getTipofactura()=='A'){
                    $tipocmb = '001';
                }else{
                    $tipocmb = '006';
                }
            }else{
                if($entity->getTipoDocumento()=='D'){
                    if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Inscripto'){
                        $tipocmb = '002';
                    }else{
                        $tipocmb = '007';
                    }
                }
                if($entity->getTipoDocumento()=='C'){
                    if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Inscripto'){
                        $tipocmb = '003';
                    }else{
                        $tipocmb = '008';
                    }
                }
            }
            if($tipocmb == '001'){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('B'.$count, $entity->getFactura()->getFecha()->format('d/m/Y'))
                    ->setCellValue('E'.$count, $entity->getFactura()->getNrofactura())
                    ->setCellValue('F'.$count, $entity->getFactura()->getNrofactura())
                    ->setCellValue('J'.$count, $entity->getFactura()->getImporte())
                    ->setCellValue('W'.$count, $entity->getFactura()->getFechavtocae())
                    ->setCellValue('X'.$count, round($entity->getFactura()->getImporte()/1.21, 2))
                    ->setCellValue('Y'.$count, 21)
                    ->setCellValue('Z'.$count, round($entity->getFactura()->getImporte()-($entity->getFactura()->getImporte()/1.21), 2))
                ;
            }else{
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('B'.$count, $entity->getFactura()->getFecha()->format('d/m/Y'))
                    ->setCellValue('E'.$count, $entity->getFactura()->getNrofactura())
                    ->setCellValue('F'.$count, $entity->getFactura()->getNrofactura())
                    ->setCellValue('J'.$count, $entity->getFactura()->getImporte())
                    ->setCellValue('W'.$count, $entity->getFactura()->getFechavtocae())
                    ->setCellValue('X'.$count, $entity->getFactura()->getImporte())
                    ->setCellValue('Y'.$count, 0)
                    ->setCellValue('Z'.$count, 0)
                ;
            }

            if($entity->getClienteProveedor()->getCuit()){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('G'.$count, '80')
                    ->setCellValue('H'.$count, str_replace('-', '', $entity->getClienteProveedor()->getCuit()));
            }else{
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('G'.$count, '96')
                    ->setCellValue('H'.$count, str_replace(' ', '', $entity->getClienteProveedor()->getDni()));

            }

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$count, $count)
                ->setCellValue('C'.$count, $tipocmb)
                ->setCellValue('D'.$count, '004')
                ->setCellValue('I'.$count, $entity->getClienteProveedor()->getRazonSocial())
                ->setCellValue('K'.$count, 0)
                ->setCellValue('L'.$count, 0)
                ->setCellValue('M'.$count, 0)
                ->setCellValue('N'.$count, 0)
                ->setCellValue('O'.$count, 0)
                ->setCellValue('P'.$count, 0)
                ->setCellValue('Q'.$count, 0)
                ->setCellValue('R'.$count, 'PES')
                ->setCellValue('S'.$count, 1)
                ->setCellValue('T'.$count, 1)
                ->setCellValue('U'.$count, 'O')
                ->setCellValue('V'.$count, 0)
                ->setCellValue('W'.$count, '')
                ->setCellValue('AA'.$count, '////TEXTO(B'.$count.';"yyyymmdd")&TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&TEXTO(F'.$count.';"####################00000000000000000000")&TEXTO(G'.$count.';"##00")&TEXTO(H'.$count.';"####################00000000000000000000")&I'.$count.'&REPETIR(" ";30-LARGO(I'.$count.'))&TEXTO(J'.$count.'*100;"###############000000000000000")&TEXTO(K'.$count.'*100;"###############000000000000000")&TEXTO(L'.$count.'*100;"###############000000000000000")&TEXTO(M'.$count.'*100;"###############000000000000000")&TEXTO(N'.$count.'*100;"###############000000000000000")&TEXTO(O'.$count.'*100;"###############000000000000000")&TEXTO(P'.$count.'*100;"###############000000000000000")&TEXTO(Q'.$count.'*100;"###############000000000000000")&TEXTO(R'.$count.';"###000")&TEXTO(S'.$count.'*1000000;"##########0000000000")&TEXTO(T'.$count.';"#0")&TEXTO(U'.$count.';"#0")&TEXTO(V'.$count.';"###############000000000000000")&TEXTO(W'.$count.';"yyyymmdd")')
                ->setCellValue('AB'.$count, '////TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&TEXTO(X'.$count.'*100;"###############000000000000000")&SI(Y'.$count.'=21;"0005";SI(Y'.$count.'=27;"0006";SI(Y'.$count.'=0;"0003";SI(Y'.$count.'=5;"0008";SI(Y'.$count.'=2,5;"0009";SI(Y'.$count.'=10,5;"0004";SI(Y'.$count.'="Exento";"0002";SI(Y'.$count.'="No Gravado";"0001"))))))))&TEXTO(Z'.$count.'*100;"###############000000000000000")')
            ;
            $count++;
        }

        foreach ($documentos as $entity){
            $tipocmb = '';
            if($entity->getTipoDocumento()=='D'){
                if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Inscripto'){
                    $tipocmb = '002';
                }else{
                    $tipocmb = '007';
                }
            }
            if($entity->getTipoDocumento()=='C'){
                if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Inscripto'){
                    $tipocmb = '003';
                }else{
                    $tipocmb = '008';
                }
            }

            if($entity->getDocumento()->getImporteiva()>0){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('B'.$count, $entity->getDocumento()->getFecha()->format('d/m/Y'))
                    ->setCellValue('E'.$count, $entity->getDocumento()->getNrocomprobante())
                    ->setCellValue('F'.$count, $entity->getDocumento()->getNrocomprobante())
                    ->setCellValue('J'.$count, $entity->getDocumento()->getImporte())
                    ->setCellValue('W'.$count, $entity->getDocumento()->getFechavtocae())
                    ->setCellValue('X'.$count, round($entity->getDocumento()->getImporte()-$entity->getDocumento()->getImporteiva(), 2))
                    ->setCellValue('T'.$count, 1)
                    ->setCellValue('Y'.$count, 21)
                    ->setCellValue('Z'.$count, $entity->getDocumento()->getImporteiva())
                ;
            }else{
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('B'.$count, $entity->getDocumento()->getFecha()->format('d/m/Y'))
                    ->setCellValue('E'.$count, $entity->getDocumento()->getNrocomprobante())
                    ->setCellValue('F'.$count, $entity->getDocumento()->getNrocomprobante())
                    ->setCellValue('J'.$count, $entity->getDocumento()->getImporte())
                    ->setCellValue('W'.$count, $entity->getDocumento()->getFechavtocae())
                    ->setCellValue('X'.$count, $entity->getDocumento()->getImporte())
                    ->setCellValue('T'.$count, 0)
                    ->setCellValue('Y'.$count, 0)
                    ->setCellValue('Z'.$count, 0)
                ;
            }

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$count, $count)
                ->setCellValue('C'.$count, $tipocmb)
                ->setCellValue('D'.$count, '004')
                ->setCellValue('G'.$count, '80')
                ->setCellValue('H'.$count, str_replace('-', '', $entity->getClienteProveedor()->getCuit()))
                ->setCellValue('I'.$count, $entity->getClienteProveedor()->getRazonSocial())
                ->setCellValue('K'.$count, 0)
                ->setCellValue('L'.$count, 0)
                ->setCellValue('M'.$count, 0)
                ->setCellValue('N'.$count, 0)
                ->setCellValue('O'.$count, 0)
                ->setCellValue('P'.$count, 0)
                ->setCellValue('Q'.$count, 0)
                ->setCellValue('R'.$count, 'PES')
                ->setCellValue('S'.$count, 1)
                ->setCellValue('U'.$count, 'O')
                ->setCellValue('V'.$count, 0)
                ->setCellValue('AA'.$count, '////TEXTO(B'.$count.';"yyyymmdd")&TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&TEXTO(F'.$count.';"####################00000000000000000000")&TEXTO(G'.$count.';"##00")&TEXTO(H'.$count.';"####################00000000000000000000")&I'.$count.'&REPETIR(" ";30-LARGO(I'.$count.'))&TEXTO(J'.$count.'*100;"###############000000000000000")&TEXTO(K'.$count.'*100;"###############000000000000000")&TEXTO(L'.$count.'*100;"###############000000000000000")&TEXTO(M'.$count.'*100;"###############000000000000000")&TEXTO(N'.$count.'*100;"###############000000000000000")&TEXTO(O'.$count.'*100;"###############000000000000000")&TEXTO(P'.$count.'*100;"###############000000000000000")&TEXTO(Q'.$count.'*100;"###############000000000000000")&TEXTO(R'.$count.';"###000")&TEXTO(S'.$count.'*1000000;"##########0000000000")&TEXTO(T'.$count.';"#0")&TEXTO(U'.$count.';"#0")&TEXTO(V'.$count.';"###############000000000000000")&TEXTO(W'.$count.';"yyyymmdd")')
                ->setCellValue('AB'.$count, '////TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&TEXTO(X'.$count.'*100;"###############000000000000000")&SI(Y'.$count.'=21;"0005";SI(Y'.$count.'=27;"0006";SI(Y'.$count.'=0;"0003";SI(Y'.$count.'=5;"0008";SI(Y'.$count.'=2,5;"0009";SI(Y'.$count.'=10,5;"0004";SI(Y'.$count.'="Exento";"0002";SI(Y'.$count.'="No Gravado";"0001"))))))))&TEXTO(Z'.$count.'*100;"###############000000000000000")')
            ;
            $count++;
        }

        $phpExcelObject->getActiveSheet()->getStyle('A2:AB2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');

        $phpExcelObject->getActiveSheet()->setTitle('Citi Ventas');

        $phpExcelObject->setActiveSheetIndex(0);

        $phpExcelObject->getActiveSheet()->getColumnDimension('AA')->setWidth(60);
        $phpExcelObject->getActiveSheet()->getColumnDimension('AB')->setWidth(60);
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=CitiVentas.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    public function citicompras($fechaDesde, $fechaHasta, $unidad){
        $em = $this->getDoctrine()->getManager();
        $documentos = $em->getRepository('AppBundle:MovimientoCC')->citicompras($fechaDesde, $fechaHasta, $unidad);

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("NTT")
            ->setLastModifiedBy("NTT")
            ->setTitle("CITI COMPRAS")
            ->setSubject("NTT");

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Fecha desde:'.$fechaDesde)
            ->setCellValue('B1', 'Fecha hasta: '.$fechaHasta);

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Campo')
            ->setCellValue('B2', 'Fecha Comprob.')
            ->setCellValue('C2', 'Tipo Comprob.')
            ->setCellValue('D2', 'Pto. Venta')
            ->setCellValue('E2', 'Nro. Comprob.')
            ->setCellValue('F2', 'Nro. Despacho de Importacion')
            ->setCellValue('G2', 'Codigo Doc. Vendedor')
            ->setCellValue('H2', 'Nro. Doc. Vendedor')
            ->setCellValue('I2', 'Apellido y Nombre del Vendedor')
            ->setCellValue('J2', 'Importe Total de la Operacion')
            ->setCellValue('K2', 'Importe total de Conceptos que no Integran el Precio Neto Gravado')
            ->setCellValue('L2', 'Importe de Operaciones Exentas')
            ->setCellValue('M2', 'Importe de Percepcion de IVA')
            ->setCellValue('N2', 'Importe de percepciones o Pagos a Cuenta de Otros Impuestos Nacionales')
            ->setCellValue('O2', 'Importe de percepciones de Ingresos Brutos')
            ->setCellValue('P2', 'Importe de percepciones de Impuestos Municipales')
            ->setCellValue('Q2', 'Importe Impuestos Internos')
            ->setCellValue('R2', 'Codigo de Moneda')
            ->setCellValue('S2', 'Tipo de Cambio')
            ->setCellValue('T2', 'Cantidad de Alicuotas de IVA')
            ->setCellValue('U2', 'Codigo de Operacion')
            ->setCellValue('V2', 'Credito Fiscal Computable')
            ->setCellValue('W2', 'Otros Tributos')
            ->setCellValue('X2', 'CUIT Emisor/Corredor')
            ->setCellValue('Y2', 'Denominacion del Emisor/Corredor')
            ->setCellValue('Z2', 'IVA Comision')
            ->setCellValue('AA2', 'Importe Neto Gravado')
            ->setCellValue('AB2', 'Alicuota de IVA')
            ->setCellValue('AC2', 'Impuesto IVA Liquidado')
            ->setCellValue('AD2', 'Importacion Compras')
            ->setCellValue('AE2', 'Importacion Alicuotas')
            ->setCellValue('AD1', 'REEMPLAZAR //// por el signo =, tocando "ctrl + b" o buscar, y yendo a la solapa de "Reemplazar"')
        ;

        $count = 3;
        foreach ($documentos as $entity){
            $tipocmb = '';
            if($entity->getTipoDocumento()=='G'){
                if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Inscripto'){
                    $tipocmb = '001';
                }else{
                    if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Monotributo'){
                        $tipocmb = '011';
                    }else{
                        $tipocmb = '006';
                    }
                }
            }

            if($entity->getTipoDocumento()=='F'){
                if($entity->getClienteProveedor()->getCondicionIva()=='Responsable Monotributo'){
                    $tipocmb = '011';
                }else{
                    $tipocmb = '006';
                }
            }

            if($entity->getDocumento()->getImporteiva()>0){
                $tipocmb = '001';
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('AA'.$count, round($entity->getDocumento()->getImporte()-$entity->getDocumento()->getImporteiva(), 2))
                    ->setCellValue('AB'.$count, 21)
                    ->setCellValue('AC'.$count, $entity->getDocumento()->getImporteiva())
                    ->setCellValue('T'.$count, 1)
                ;
            }else{
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('AA'.$count, 0)
                    ->setCellValue('AB'.$count, 0)
                    ->setCellValue('AC'.$count, 0)
                    ->setCellValue('T'.$count, 0)
                ;
            }

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$count, $count)
                ->setCellValue('B'.$count, $entity->getDocumento()->getFecha()->format('d/m/Y'))
                ->setCellValue('C'.$count, $tipocmb)
                ->setCellValue('D'.$count, '')
                ->setCellValue('E'.$count, $entity->getDocumento()->getNrocomprobante())
                ->setCellValue('F'.$count, '')
                ->setCellValue('G'.$count, 80)
                ->setCellValue('H'.$count, str_replace('-', '', $entity->getClienteProveedor()->getCuit()))
                ->setCellValue('I'.$count, $entity->getClienteProveedor()->getRazonSocial())
                ->setCellValue('J'.$count, $entity->getDocumento()->getImporte())
                ->setCellValue('K'.$count, 0)
                ->setCellValue('L'.$count, 0)
                ->setCellValue('M'.$count, $entity->getDocumento()->getIvacap() + $entity->getDocumento()->getIvaprov())
                ->setCellValue('N'.$count, 0)
                ->setCellValue('O'.$count, $entity->getDocumento()->getIibbcap() + $entity->getDocumento()->getIibbprov())
                ->setCellValue('P'.$count, 0)
                ->setCellValue('Q'.$count, 0)
                ->setCellValue('R'.$count, 'PES')
                ->setCellValue('S'.$count, 1)
                ->setCellValue('U'.$count, 'O')
                ->setCellValue('V'.$count, $entity->getDocumento()->getImporteiva())
                ->setCellValue('W'.$count, 0)
                ->setCellValue('AD'.$count, '////TEXTO(B'.$count.';"yyyymmdd")&TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&F'.$count.'&REPETIR(" ";16-LARGO(F'.$count.'))&TEXTO(G'.$count.';"##00")&TEXTO(H'.$count.';"####################00000000000000000000")&I'.$count.'&REPETIR(" ";30-LARGO(I'.$count.'))&TEXTO(J'.$count.'*100;"###############000000000000000")&TEXTO(K'.$count.'*100;"###############000000000000000")&TEXTO(L'.$count.'*100;"###############000000000000000")&TEXTO(M'.$count.'*100;"###############000000000000000")&TEXTO(N'.$count.'*100;"###############000000000000000")&TEXTO(O'.$count.'*100;"###############000000000000000")&TEXTO(P'.$count.'*100;"###############000000000000000")&TEXTO(Q'.$count.'*100;"###############000000000000000")&TEXTO(R'.$count.';"###000")&TEXTO(S'.$count.'*1000000;"##########0000000000")&TEXTO(T'.$count.';"#0")&TEXTO(U'.$count.';"#0")&TEXTO(V'.$count.'*100;"###############000000000000000")&TEXTO(W'.$count.';"###############000000000000000")&TEXTO(X'.$count.';"###########00000000000")&Y'.$count.'&REPETIR(" ";30-LARGO(Y'.$count.'))&TEXTO(Z'.$count.'*100;"###############000000000000000")')
                ->setCellValue('AE'.$count, '////TEXTO(C'.$count.';"###000")&TEXTO(D'.$count.';"#####00000")&TEXTO(E'.$count.';"####################00000000000000000000")&TEXTO(G'.$count.';"##00")&TEXTO(H'.$count.';"####################00000000000000000000")&TEXTO(AA'.$count.'*100;"###############000000000000000")&SI(AB'.$count.'=21;"0005";SI(AB'.$count.'=27%;"0006";SI(AB'.$count.'=0;"0003";SI(AB'.$count.'=5,5;"0008";SI(AB'.$count.'=2,5;"0009";SI(AB'.$count.'=10,5;"0004";SI(AB'.$count.'="Exento";"0002";SI(AB'.$count.'="No Gravado";"0001"))))))))&TEXTO(AC'.$count.'*100;"###############000000000000000")')
            ;
            $count++;
        }

        $phpExcelObject->getActiveSheet()->getStyle('A2:AE2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');

        $phpExcelObject->getActiveSheet()->setTitle('Citi Compras');

        $phpExcelObject->setActiveSheetIndex(0);

        $phpExcelObject->getActiveSheet()->getColumnDimension('AE')->setWidth(60);
        $phpExcelObject->getActiveSheet()->getColumnDimension('AD')->setWidth(60);
        $phpExcelObject->getActiveSheet()->getColumnDimension('AA')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('AB')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=CitiCompras.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    public function chequesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(array('estado'=>'A'));
        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado'=>'A'));
        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $entities = $em->getRepository('AppBundle:Cheque')
                ->findBy(array('unidadNegocio'=>$this->getUser()->getUnidadNegocio()));
        }else{
            $entities = $em->getRepository('AppBundle:Cheque')->findAll();
        }

        return $this->render('AppBundle:MovimientoCC:cheques.html.twig', array(
            'entities' => $entities,
            'unidades' => $unidades,
            'cuentas' => $cuentas,
        ));
    }

    /**
     *
     */
    public function admcajaAction()
    {

        $this->sessionManager->setSession('cajade', '');
        $em = $this->getDoctrine()->getManager();
        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas($this->getUser()->getUnidadNegocio());
        }else{
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas();
        }
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        return $this->render('AppBundle:MovimientoCC:admcaja.html.twig', array(
            'entities' => $entities,
            'unidades' => $unidades,
        ));
    }

    public function filtroAction(Request $request)
    {

        $this->sessionManager->setSession('cajade', '');

        $em = $this->getDoctrine()->getManager();
        if($request->get('unidadnegocio')!='0'){
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
            $this->sessionManager->setSession('cajade', $unidad->getDescripcion());
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas($request->get('unidadnegocio'));
        }else{
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas();
        }

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        return $this->render('AppBundle:MovimientoCC:admcaja.html.twig', array(
            'entities' => $entities,
            'unidades' => $unidades,
        ));

    }

    public function imprimirAction()
    {


        $em = $this->getDoctrine()->getManager();
        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas($this->getUser()->getUnidadNegocio());
        }else{
            $entities = $em->getRepository('AppBundle:MovimientoCC')->getAllActivas();
        }

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Willy Jhons")
            ->setLastModifiedBy("Willy Jhons")
            ->setTitle("Informe")
            ->setSubject("Willy Jhons");

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Administracion de Caja')
            ->setCellValue('D1', 'Pesos ($)')
            ->setCellValue('G1', 'Dolar(u$s)');

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Fecha')
            ->setCellValue('B2', 'Movimiento')
            ->setCellValue('C2', 'Debe')
            ->setCellValue('D2', 'Haber')
            ->setCellValue('E2', 'Saldo')
            ->setCellValue('F2', 'Debe')
            ->setCellValue('G2', 'Haber')
            ->setCellValue('H2', 'Saldo');

        $count = 3;
        $suma = 0;

        $pesosD = 0;
        $pesosH = 0;
        $dolaresD = 0;
        $dolaresH = 0;
        $tipo = '';

        foreach ($entities as$entity){
            if ($entity->getTipoDocumento() == 'R' or $entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'G' or $entity->getTipoDocumento() == 'A'){

                if ($entity->getTipoDocumento() == 'R'){
                    $tipo = 'RECIBO CLIENTE'.$entity->getId();
                }
                if ($entity->getTipoDocumento() == 'RP'){
                    $tipo = 'RECIBO PROV'. $entity->getId();
                }
                if ($entity->getTipoDocumento() == 'G' ){
                    $tipo = 'GASTO' .$entity->getId();
                }
                if ($entity->getTipoDocumento() == 'A' ){
                    $tipo = 'AJUSTE'.$entity->getId();
                }

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$count,$entity->getDocumento()->getFechaRegistracion()->format('d-m-Y'))
                    ->setCellValue('B'.$count, $tipo);

                if ($entity->getTipoDocumento() == 'R') {
                    if ($entity->getMonedaStr() == 'ARG') {
                        $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('C'.$count, '$ '.$entity->getDocumento()->getImporte())
                            ->setCellValue('E'.$count, '$ '.$operacion);
                    }else{
                        $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('F'.$count, 'U$S '.$entity->getDocumento()->getImporte())
                            ->setCellValue('H'.$count, 'U$S '.$operacion);
                    }
                }
                if($entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'G'){
                    if ($entity->getMonedaStr() == 'ARG'){
                        $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('D'.$count, '$ '.$entity->getDocumento()->getImporte())
                            ->setCellValue('E'.$count, '$ '.$operacion);
                    }else{
                        $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('G'.$count, 'U$S '.$entity->getDocumento()->getImporte())
                            ->setCellValue('H'.$count, 'U$S '.$operacion);
                    }
                }
                if ($entity->getTipoDocumento() == 'A'){
                    if ($entity->getMonedaStr() == 'ARG'){
                        if ($entity->getDocumento()->getImporte() <0){
                            $pesosH = $entity->getDocumento()->getImporte() * (-1) + $pesosH;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('D'.$count, '$ '. $entity->getDocumento()->getImporte() * (-1))
                                ->setCellValue('E'.$count, '$ '.$operacion);
                        }else{
                            $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('C'.$count, '$ '. $entity->getDocumento()->getImporte())
                                ->setCellValue('E'.$count, '$ '.$operacion);
                        }
                    }else{
                        if ($entity->getDocumento()->getImporte() <0){
                            $dolaresH = $entity->getDocumento()->getImporte() * (-1) + $dolaresH;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('G'.$count, 'U$S '.$entity->getDocumento()->getImporte()*(-1))
                                ->setCellValue('H'.$count, 'U$S '.$operacion);
                        }else{
                            $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('F'.$count, 'U$S '.$entity->getDocumento()->getImporte())
                                ->setCellValue('H'.$count, 'U$S '.$operacion);
                        }
                    }
                }
                $count++;
            }
        }

        $sumaPesos = $pesosD - $pesosH;
        $sumaDolares = $dolaresD - $dolaresH;

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('D'.$count, 'SALDO CAJA $ '.round($sumaPesos, 2))
            ->setCellValue('G'.$count, 'SALDO CAJA U$S '.round($sumaDolares, 2));

        $phpExcelObject->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


        $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->getStyle('D'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getStyle('G'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=Administracion Caja.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;

    }

    /**
     * Creates a new MovimientoCC$entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MovimientoCC();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocc_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:MovimientoCC:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MovimientoCC$entity.
     *
     * @param MovimientoCC $entity The$entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MovimientoCC $entity)
    {
        $form = $this->createForm(new MovimientoCCType(), $entity, array(
            'action' => $this->generateUrl('movimientocc_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MovimientoCC$entity.
     *
     */
    public function newAction()
    {
        $entity = new MovimientoCC();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:MovimientoCC:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MovimientoCC$entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:MovimientoCC:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing MovimientoCC$entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:MovimientoCC:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a MovimientoCC$entity.
     *
     * @param MovimientoCC $entity The$entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MovimientoCC $entity)
    {
        $form = $this->createForm(new MovimientoCCType(), $entity, array(
            'action' => $this->generateUrl('movimientocc_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing MovimientoCC$entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocc_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:MovimientoCC:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a MovimientoCC$entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:MovimientoCC')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('movimientocc'));
    }

    /**
     * Creates a form to delete a MovimientoCC$entity by id.
     *
     * @param mixed $id The$entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('movimientocc_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }

    public function cuentascobrarAction()
    {

        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();

        return $this->render('AppBundle:MovimientoCC:filtro.html.twig', array(
            'tipo'=>'cobrar',
            'unidades'=>$unidades
        ));

    }

    public function crearchequeAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio());
        }else{
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
        }
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));

        return $this->render('AppBundle:MovimientoCC:nuevocheque.html.twig', array(
            'tipocheques'=>$tipocheques,
            'cuentas'=>$cuentas,
            'bancos'=>$bancos,
            'unidad'=>$unidad
        ));
    }


    public function nuevochequeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));

        $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque'));
        $cheque = new Cheque();
        $cheque->setImporte($request->get('importe'));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setUnidadNegocio($unidad);
        $cheque->setCuentaorigen($cuenta);
        $em->persist($cheque);
        $em->flush();

        $this->sessionManager->addFlash('msgOk','Cheque registrado correctamente.');
        return $this->redirect($this->generateUrl('movimientocc_cheques'));
    }

    public function editarchequeAction(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Cheque')->find($id);
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $cuentas = $em->getRepository('AppBundle:CuentaBancaria')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));

        return $this->render('AppBundle:MovimientoCC:editarcheque.html.twig', array(
            'tipocheques'=>$tipocheques,
            'entity'=>$entity,
            'bancos'=>$bancos,
            'cuentas'=>$cuentas,
        ));
    }

    public function depositarAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
        if($request->get('idcuenta') != 0){
            $cuentabancaria = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('idcuenta'));

            $cheque->setEstado('D');
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cuentabancaria);
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $em->persist($movimiento);
            $cheque->setMovimiento($movimiento);
            $cuentabancaria->setMovimiento($movimiento);
            $em->flush();

            $this->sessionManager->addFlash('msgOk','Cheque Depositado correctamente.');
        }else{
            $this->sessionManager->addFlash('msgWarn','Debe seleccionar Cuenta Bancaria.');
        }
        return $this->redirect($this->generateUrl('movimientocc_cheques'));
    }

    public function conciliarAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
        $cheque->setConciliacion('SI');
        $cheque->setEstado('C');
        if($cheque->getCuentaorigen()){
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cheque->getCuentaorigen());
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $em->persist($movimiento);
            $cheque->setMovimiento($movimiento);
            $cheque->getCuentaorigen()->setMovimiento($movimiento);
            $em->flush();
        }
        $em->flush();

        $this->sessionManager->addFlash('msgOk','Cheque Conciliado correctamente.');
        return $this->redirect($this->generateUrl('movimientocc_cheques'));
    }

    public function hacereditarchequeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('AppBundle:Cheque')->find($request->get('idcheque'));

        $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('AppBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque'));

        $cheque->setImporte($request->get('importe'));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setCuentaorigen($cuenta);

        $em->flush();

        $this->sessionManager->addFlash('msgOk','Cheque editado correctamente.');
        return $this->redirect($this->generateUrl('movimientocc_cheques'));
    }

    public function cuentaspagarAction()
    {

        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        return $this->render('AppBundle:MovimientoCC:filtro.html.twig', array(
            'tipo'=>'pagar',
            'unidades'=>$unidades
        ));

    }

    public function informeCuentasAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Willy Jhons")
            ->setLastModifiedBy("Willy Jhons")
            ->setTitle("Informe")
            ->setSubject("Willy Jhons");
        if($request->get('listado')=="1"){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fecha: '.date('d-m-Y'))
                ->setCellValue('C1', 'Moneda: ARG');
        }else{
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fecha'.date('d-m-Y'))
                ->setCellValue('C1', 'Moneda: USD');
        }

        if($request->get('tipo')=='cobrar'){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A2', 'CLIENTE')
                ->setCellValue('B2', 'FECHA ULTIMA COMPRA')
                ->setCellValue('C2', 'IMPORTE ULTIMA COMPRA')
                ->setCellValue('D2', 'FECHA ULTIMO PAGO')
                ->setCellValue('E2', 'IMPORTE ULTIMO PAGO')
                ->setCellValue('F2', 'SALDO ACTUAL');

            if(!$this->isGranted('ROLE_SUPER_ADMIN')){
                $resultados = $em->getRepository('AppBundle:MovimientoCC')->getCuentasPorCobrar($request->get('listado'), $this->getUser()->getUnidadNegocio());
            }else{
                $resultados = $em->getRepository('AppBundle:MovimientoCC')->getCuentasPorCobrar($request->get('listado'), $request->get('unidadnegocio'));
            }

            $count = 3;
            $suma = 0;
            foreach($resultados as $r){

                $pesosD = 0;
                $pesosH = 0;
                $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($r["id"]);
                foreach($clienteProveedor->getMovimientoscc() as $mov){
                    if($mov->getEstado()=='A' and $mov->getMoneda()==$request->get('listado')){
                        if($mov->getTipoDocumento()=="R" or $mov->getTipoDocumento()=="C"){
                            $pesosH = $pesosH + $mov->getDocumento()->getImporte();
                            $this->sessionManager->setSession($mov->getDocumento()->getImporte(),$mov->getDocumento()->getImporte());
                        }else{
                            if($mov->getTipoDocumento()=="FC"){
                                $importeFC = $mov->getFactura()->getImporte();
                                $pesosD = $pesosD + $importeFC;
                            }else{
                                if($mov->getTipoDocumento()=="D"){
                                    $pesosD = $pesosD + $mov->getDocumento()->getImporte();
                                }
                            }
                        }
                    }
                }
                $this->sessionManager->setSession('debe',$pesosD);
                $this->sessionManager->setSession('haber',$pesosH);

                $saldo = $pesosD - $pesosH;

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$count, $r["razonSocial"])
                    ->setCellValue('B'.$count, $r["fechaCompra"])
                    ->setCellValue('C'.$count, $r["importeCompra"])
                    ->setCellValue('D'.$count, $r["fechaPago"])
                    ->setCellValue('E'.$count, $r["importePago"])
                    ->setCellValue('F'.$count, $saldo);

                $suma = $suma + $saldo;
                $count = $count + 1;
            }

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('E'.$count, 'TOTAL')
                ->setCellValue('F'.$count, $suma);

            $phpExcelObject->getActiveSheet()->getStyle('A2:F2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


            $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);
            $phpExcelObject->getActiveSheet()->getStyle('E'.$count.':F'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeCuentasCobrar.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');

            return $response;
        }else{

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A2', 'PROVEEDOR')
                ->setCellValue('B2', 'FECHA ULTIMA COMPRA')
                ->setCellValue('C2', 'IMPORTE ULTIMA COMPRA')
                ->setCellValue('D2', 'FECHA ULTIMO PAGO')
                ->setCellValue('E2', 'IMPORTE ULTIMO PAGO')
                ->setCellValue('F2', 'SALDO ACTUAL');

            if(!$this->isGranted('ROLE_SUPER_ADMIN')){
                $resultados = $em->getRepository('AppBundle:MovimientoCC')->getCuentasPorPagar($request->get('listado'), $this->getUser()->getUnidadNegocio());
            }else{
                $resultados = $em->getRepository('AppBundle:MovimientoCC')->getCuentasPorPagar($request->get('listado'), $request->get('unidadnegocio'));
            }

            $count = 3;
            $suma = 0;
            foreach($resultados as $r){

                $pesosD = 0;
                $pesosH = 0;
                $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($r["id"]);
                foreach($clienteProveedor->getMovimientoscc() as $mov){
                    if($mov->getEstado()=='A' and $mov->getMoneda()==$request->get('listado')){
                        if($mov->getTipoDocumento()=="F" or $mov->getTipoDocumento()=="D"){
                            $pesosH = $pesosH + $mov->getDocumento()->getImporte();
                        }else{
                            if($mov->getTipoDocumento()=="RP" or $mov->getTipoDocumento()=="C"){
                                $pesosD = $pesosD + $mov->getDocumento()->getImporte();
                            }
                        }
                    }
                }

                $saldo = $pesosD - $pesosH;

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$count, $r["razonSocial"])
                    ->setCellValue('B'.$count, $r["fechaCompra"])
                    ->setCellValue('C'.$count, $r["importeCompra"])
                    ->setCellValue('D'.$count, $r["fechaPago"])
                    ->setCellValue('E'.$count, $r["importePago"])
                    ->setCellValue('F'.$count, $saldo);

                $suma = $suma + $saldo;
                $count = $count + 1;
            }

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('E'.$count, 'TOTAL')
                ->setCellValue('F'.$count, $suma);

            $phpExcelObject->getActiveSheet()->getStyle('A2:F2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


            $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);
            $phpExcelObject->getActiveSheet()->getStyle('E'.$count.':F'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeCuentasPagar.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');

            return $response;
        }

    }

    public function cambiarestadoAction(Request $request){
        $estado = $request->get('estado');
        $id = $request->get('id');
        if(isset($estado)){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Cheque')->find($id);
            $entity->setEstado($estado);
            $em->flush();
            $this->sessionManager->addFlash('msgOk','Accion realizada.');
        }
        return $this->redirect($this->generateUrl('movimientocc_cheques'));
    }
}
