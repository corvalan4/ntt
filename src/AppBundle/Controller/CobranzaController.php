<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Cobranza;
use AppBundle\Entity\CobranzaFactura;
use AppBundle\Entity\Retencion;
use AppBundle\Entity\Caja;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\MedioDocumento;
use AppBundle\Entity\MovimientoCaja;
use AppBundle\Entity\Cheque;
use AppBundle\Form\CobranzaType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Cobranza controller.
 *
 */
class CobranzaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Cobranza entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $desde = $request->get('desde');
        if (empty($desde)) {
            $desde = new \DateTime('NOW -30 days');
        } else {
            $desde = new \DateTime($request->get('desde'));
        }

        $hasta = $request->get('hasta');
        if (empty($hasta)) {
            $hasta = new \DateTime('NOW +1 days');
        } else {
            $hasta = new \DateTime($request->get('hasta'));
        }
        $descripcion = $request->get('descripcion');
        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Cobranza')->filtro($desde, $hasta, $descripcion, $unidadNegocio);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(20);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Cobranza:index.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /**
     * Creates a new Cobranza entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Cobranza();
        $form = $this->createCreateForm($entity);
        
        $form
                ->add('facturas', 'entity', array(
                    'class' => 'AppBundle:Factura',
                    'label' => 'Facturas',
                    'mapped' => false,
                    'multiple' => true
        ));
        $form->handleRequest($request);
        
        
        $idClienteProveedor = $request->get('clienteProveedor');
        $cliente = $em->getRepository('AppBundle:ClienteProveedor')->find($idClienteProveedor);
        $entity->setClienteProveedor($cliente);

        $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));

        $facturasAsociadas = $form['facturas']->getData();

        $idfactura = $request->get('idfactura');
        $factura = null;
        if (!empty($idfactura)) {
            $factura = $em->getRepository('AppBundle:Factura')->find($idfactura);
        }

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
            $entity->setUnidadnegocio($unidad);
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $request->get('unidad'), 'estado' => 'A'));
        } else {
            $entity->setUnidadnegocio($this->getUser()->getUnidadNegocio());
            $caja = $em->getRepository('AppBundle:Caja')->findOneBy(array('unidadNegocio' => $this->getUser()->getUnidadNegocio(), 'estado' => 'A'));
        }

        $importemedios = 0;

        foreach ($medios as $medio) {
            $importemedio = $request->get('importemedio' . $medio->getId());
            $numerocupon = $request->get('numerocupon' . $medio->getId());
            $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
            $cuotas = $request->get('cuotas' . $medio->getId());
            if (!empty($importemedio)) {
                $mediodocumento = new MedioDocumento();
                $mediodocumento->setMedio($medio);
                $mediodocumento->setCobranza($entity);
                $mediodocumento->setImporte($importemedio);
                $mediodocumento->setNumerolote((int) $numerotransaccion);
                $mediodocumento->setNumerocupon((int) $numerocupon);
                $mediodocumento->setCuotas((int) $cuotas);
                $em->persist($mediodocumento);
                $importemedios += $importemedio;
                if ($medio->getDescripcion() == 'Efectivo') {
                    $medioEfectivo = $mediodocumento;
                }
            }
        }

        $importecheques = 0;
        for ($i = 0; $i < 6; $i++) {
            if ($request->get('banco' . $i) != 0 and $request->get('tipocheque' . $i)
                    and!empty($request->get('importe' . $i)) and $request->get('importe' . $i) > 0) {

                $banco = $em->getRepository('AppBundle:Banco')->find($request->get('banco' . $i));
                $tipocheque = $em->getRepository('AppBundle:TipoCheque')->find($request->get('tipocheque' . $i));
                $importecheque = $request->get('importe' . $i);
                $cheque = new Cheque();
                $cheque->setImporte($importecheque);
                $cheque->setFirmantes($request->get('firmantes' . $i));
                $cheque->setCuit($request->get('cuit' . $i));
                $cheque->setFechacobro(new \DateTime($request->get('fechacobro' . $i)));
                $cheque->setFechaemision(new \DateTime($request->get('fechaemision' . $i)));
                $cheque->setNrocheque($request->get('nrocheque' . $i));
                $cheque->setBanco($banco);
                $cheque->setTipocheque($tipocheque);
                $cheque->setCobranza($entity);
                $cheque->setUnidadNegocio($unidad);
                $em->persist($cheque);
                $importecheques += $importecheque;
            }
        }

        $importeretenciones = 0;

        $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array(), array('descripcion' => 'ASC'));
        for ($i = 1; $i <= count($tiporetenciones); $i++) {
            if ($request->get('retencionimporte' . $i) != '' and $request->get('retencionimporte' . $i) > 0) {
                $retencion = new Retencion();
                $tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->findOneBy(array('descripcion' => $request->get('retencion' . $i)));
                $improteretencion = $request->get('retencionimporte' . $i);
                $retencion->setImporte($improteretencion);
                $retencion->setTiporetencion($tiporetencion);
                $retencion->setCobranza($entity);
                $retencion->setUnidadNegocio($unidad);
                $em->persist($retencion);
                $importeretenciones += $improteretencion;
            }
        }
        $importe = $importeretenciones + $importemedios + $importecheques;
        $entity->setImporte($importe);

        if ($form->isValid()) {
            $em->persist($entity);
            //Se agrega el movimiento de Caja
            if (!isset($caja)) {
                $caja = $this->crearCaja($entity->getUnidadNegocio());
            }
            if (isset($medioEfectivo)) {
                //Siempre debe existir una caja activa
                $movimientoCaja = new MovimientoCaja();
                $movimientoCaja->setImporte($medioEfectivo->getImporte());
                $movimientoCaja->setCaja($caja);
                $movimientoCaja->setCobranza($entity);
                $em->persist($movimientoCaja);
            }

            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setCobranza($entity);
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setImporte($importe);
            $em->persist($pagoAsignacion);
            $em->flush();

            if (count($facturasAsociadas) > 0) {
                foreach ($facturasAsociadas as $facturasAsociada) {
                    $cobranzafactura = new CobranzaFactura();
                    $cobranzafactura->setCobranza($entity);
                    $cobranzafactura->setImporte($importe);
                    $cobranzafactura->setFactura($facturasAsociada);

                    $em->persist($cobranzafactura);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('cobranza_show', array('id' => $entity->getId())));
            }

            if ($factura) {
                $cobranzafactura = new CobranzaFactura();
                $cobranzafactura->setCobranza($entity);
                $cobranzafactura->setImporte($importe);
                $cobranzafactura->setFactura($factura);

                $em->persist($cobranzafactura);
                $em->flush();
                return $this->redirect($this->generateUrl('cobranza_show', array('id' => $entity->getId())));
            }

            return $this->redirect($this->generateUrl('cobranza'));
        }

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array(), array('descripcion' => 'ASC'));
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array(), array('descripcion' => 'ASC'));

        return $this->render('AppBundle:Cobranza:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'medios' => $medios,
                    'bancos' => $bancos,
                    'tipocheques' => $tipocheques,
                    'factura' => $factura,
                    'tiporetenciones' => $tiporetenciones,
                    'form' => $form->createView())
        );
    }

    /**
     * Creates a form to create a Cobranza entity.
     *
     * @param Cobranza $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cobranza $entity) {
        $form = $this->createForm(new CobranzaType(), $entity, array(
            'action' => $this->generateUrl('cobranza_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Cobranza entity.
     *
     */
    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Cobranza();
        $entity->setFecha(new \DateTime("NOW"));

        $idfactura = $request->get('idfactura');
        $idfacturas = $request->get('idfacturas');

        $factura = null;
        if (!empty($idfactura)) {
            $factura = $em->getRepository('AppBundle:Factura')->find($idfactura);

            if ($factura) {
                $entity->setFecha($factura->getFecha());
                $entity->setClienteProveedor($factura->getClienteProveedor());
                $entity->setUnidadnegocio($factura->getUnidadnegocio());
            }
        }

        $totalFacturas = 0;
        $arrFacturas = array();
        if (!empty($idfacturas)) {
            foreach ($idfacturas as $idfactura) {
                $facturas = $em->getRepository('AppBundle:Factura')->find($idfactura);
                if ($facturas) {
                    $arrFacturas[] = $facturas;
                    $entity->setClienteProveedor($facturas->getClienteProveedor());
                    $totalFacturas += $facturas->getImporte();
                }
            }
            $entity->setClienteProveedor($facturas->getClienteProveedor());
            $entity->setFecha(new \DateTime("NOW"));
        }

        $form = $this->createCreateForm($entity);
        if (count($arrFacturas) > 0) {
            $form
                    ->add('facturas', 'entity', array(
                        'class' => 'AppBundle:Factura',
                        'label' => 'Facturas',
                        'mapped' => false,
                        'data' => $arrFacturas,
                        'multiple' => true,
                        'query_builder' => function (\AppBundle\Entity\FacturaRepository $repository) use ($idfacturas) {
                            return $repository->createQueryBuilder('c')
                                    ->where('c.id IN (?1)')
                                    ->setParameter(1, $idfacturas);
                        },
                        'choice_label' => function ($factura) {
                            return strip_tags($factura);
                        }
            ));
        } else {
            $form
                    ->add('facturas', 'choice', array(
                        'choices' => array(),
                        'mapped' => false,
                        'required' => false)
            );
        }

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
        $bancos = $em->getRepository('AppBundle:Banco')->findBy(array(), array('descripcion' => 'ASC'));
        $tipocheques = $em->getRepository('AppBundle:TipoCheque')->findBy(array(), array('descripcion' => 'ASC'));
        $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array(), array('descripcion' => 'ASC'));

        return $this->render('AppBundle:Cobranza:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'medios' => $medios,
                    'bancos' => $bancos,
                    'tipocheques' => $tipocheques,
                    'tiporetenciones' => $tiporetenciones,
                    'factura' => $factura,
                    'facturas' => $arrFacturas,
                    'totalFacturas' => $totalFacturas,
                    'form' => $form->createView())
        );
    }

    /**
     * Finds and displays a Cobranza entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Cobranza:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cobranza entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Cobranza:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Cobranza entity.
     *
     * @param Cobranza $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cobranza $entity) {
        $form = $this->createForm(new CobranzaType(), $entity, array(
            'action' => $this->generateUrl('cobranza_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Edits an existing Cobranza entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cobranza_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Cobranza:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cobranza entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Cobranza')->find($id);

        if ($entity->getMovimientoCaja()) {
            $em->remove($entity->getMovimientoCaja());
        }

        foreach ($entity->getCobranzasfactura() as $cobranzaFactura) {
            $em->remove($cobranzaFactura);
        }

        foreach ($entity->getPagosAsignacionCobranza() as $pagosAsignacionCobranza) {
            $em->remove($pagosAsignacionCobranza);
        }

        foreach ($entity->getMedios() as $medios) {
            $em->remove($medios);
        }

        foreach ($entity->getRetenciones() as $retenciones) {
            $em->remove($retenciones);
        }

        foreach ($entity->getCheques() as $cheque) {
            $cheque->setCobranza(null);
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('cobranza'));
    }

    /**
     * Creates a form to delete a Cobranza entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cobranza_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $medio = $request->get('medio');
        $cliente = $request->get('cliente');
        $medioEntity = null;
        if (isset($medio)) {
            $medioEntity = $em->getRepository('AppBundle:Medio')->find($medio);
        }

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entidades = $em->getRepository('AppBundle:Cobranza')->filtroBusquedaInformeCobranza($fechaDesde, $fechaHasta, $cliente, $medio, $unidadNegocio);
        $entities = [];
        if ($medio == 'cheque') {
            foreach ($entidades as $entidad) {
                if (count($entidad->getCheques()) > 0) {
                    $entities[] = $entidad;
                }
            }
            $total = $em->getRepository('AppBundle:Cheque')->totalInformeCobranza($fechaDesde, $fechaHasta, $cliente, $unidadNegocio);
        } else if ($medio != '') {
            $total = $em->getRepository('AppBundle:MedioDocumento')->totalInformeCobranza($fechaDesde, $fechaHasta, $medio, $cliente, $unidadNegocio);
            $entities = $entidades;
        } else {
            $total = $em->getRepository('AppBundle:Cobranza')->totalInformeCobranza($fechaDesde, $fechaHasta, $medio, $cliente, $unidadNegocio);
            $entities = $entidades;
        }
        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities, $total, $medio);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Cobranza');
        }
        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Cobranza:informe.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total,
                    'medioEntity' => $medioEntity
        ));
    }

    public function getUltimaCobranzaAction(Request $request) {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $cobranza = $em->getRepository('AppBundle:Cobranza')->getUltimaCobranza($id);
        $respuesta['fecha'] = '-';
        $respuesta['importe'] = '-';
        $respuesta['unidadNegocio'] = '-';
        if (isset($id)) {
            $saldo = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
            $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);

            $respuesta['saldo'] = round($clienteProveedor->getSaldo() + $saldo['cobranzas'] + $saldo['notacredito'] - $saldo['notadebito'] - $saldo['facturas'] + $saldo['facturasproveedor'] - $saldo['gastos'] - $saldo['pagos'], 2);
        }

        if (isset($cobranza) && !empty($cobranza)) {
            $respuesta['fecha'] = date_format($cobranza[0][0]->getFecha(), 'Y-m-d');
            $respuesta['importe'] = $cobranza[0][0]->getImporte();
            $respuesta['unidadNegocio'] = $cobranza[0][0]->getUnidadNegocio()->getDescripcion();
        }

        return new JsonResponse($respuesta);
    }

    private function crearCaja($unidadNegocio) {
        $em = $this->getDoctrine()->getManager();
        $caja = new Caja();
        $caja->setUnidadNegocio($unidadNegocio);
        $em->persist($caja);
        $em->flush();
        return $caja;
    }

    /*
     * Generación de contenido archivo excel Informe de Ingreso -Egreso
     */

    private function excelInforme($entities, $total, $medioSelect) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Medios');
        $sheet->setCellValue('E1', 'Cheques');
        $sheet->setCellValue('F1', 'Unidad Negocio');
        $sheet->setCellValue('G1', 'Importe total de Cobranza');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
            $sheet->setCellValue('B' . $fila, $entity);
            $sheet->setCellValue('C' . $fila, $entity->getClienteProveedor());

            if ($medioSelect != 'cheque' && sizeof($entity->getMedios()) > 0) {
                foreach ($entity->getMedios() as $medio) {
                    $sheet->setCellValue('D' . $fila, $medio->getMedio()->getDescripcion() . ' - ' . $medio->getImporte());
                }
            }
            $ch = '';
            if (($medioSelect == 'cheque' || $medioSelect == '') && $entity->getCheques() !== NULL) {
                foreach ($entity->getCheques() as $cheque) {
                    if ($cheque->getEstado() != 'E') {
                        if ($ch == '') {
                            $ch = $ch . $cheque . ' - ' . $cheque->getImporte();
                        } else {
                            $ch = $ch . ' / ' . $cheque . ' - ' . $cheque->getImporte();
                        }
                    }
                }
                $sheet->setCellValue('E' . $fila, $ch);
            }
            $sheet->setCellValue('F' . $fila, $entity->getUnidadNegocio());
            $sheet->setCellValue('G' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('F' . $fila, 'Total');
        $sheet->setCellValue('G' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('F' . $fila . ':G' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);


        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }

}
