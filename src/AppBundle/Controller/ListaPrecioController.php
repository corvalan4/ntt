<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\ListaPrecio;
use AppBundle\Form\ListaPrecioType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * ListaPrecio controller.
 *
 */
class ListaPrecioController extends Controller
{
    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all ListaPrecio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:ListaPrecio')->findAll();

        return $this->render('AppBundle:ListaPrecio:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ListaPrecio entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ListaPrecio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->addFlash("msgOk", "Lista creada exitosamente.");
            return $this->redirect($this->generateUrl('listaprecio'));
        }

        return $this->render('AppBundle:ListaPrecio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ListaPrecio entity.
     *
     * @param ListaPrecio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ListaPrecio $entity)
    {
        $form = $this->createForm(new ListaPrecioType(), $entity, array(
            'action' => $this->generateUrl('listaprecio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=>['class'=>'btn btn-primary']));

        return $form;
    }

    /**
     * Displays a form to create a new ListaPrecio entity.
     *
     */
    public function newAction()
    {
        $entity = new ListaPrecio();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:ListaPrecio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ListaPrecio entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:ListaPrecio:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ListaPrecio entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:ListaPrecio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ListaPrecio entity.
    *
    * @param ListaPrecio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ListaPrecio $entity)
    {
        $form = $this->createForm(new ListaPrecioType(), $entity, array(
            'action' => $this->generateUrl('listaprecio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=>['class'=>'btn btn-primary']));

        return $form;
    }
    /**
     * Edits an existing ListaPrecio entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->addFlash("msgOk", "Lista creada exitosamente.");
            return $this->redirect($this->generateUrl('listaprecio'));
        }

        return $this->render('AppBundle:ListaPrecio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ListaPrecio entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ListaPrecio')->find($id);
        $entity->setEstado("B");
        $em->flush();

        return $this->redirect($this->generateUrl('listaprecio'));
    }
    /**
     * Deletes a ListaPrecio entity.
     *
     */
    public function activarAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ListaPrecio')->find($id);
        $entity->setEstado("A");
        $em->flush();

        return $this->redirect($this->generateUrl('listaprecio'));
    }

    /**
     * Creates a form to delete a ListaPrecio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('listaprecio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
