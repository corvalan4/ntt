<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TipoRetencion;
use AppBundle\Form\TipoRetencionType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * TipoRetencion controller.
 *
 */
class TipoRetencionController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;	

    /**
     * Lists all TipoRetencion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion' => 'ASC'));

        return $this->render('AppBundle:TipoRetencion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoRetencion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoRetencion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tiporetencion'));
        }

        return $this->render('AppBundle:TipoRetencion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoRetencion entity.
     *
     * @param TipoRetencion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoRetencion $entity)
    {
        $form = $this->createForm(new TipoRetencionType(), $entity, array(
            'action' => $this->generateUrl('tiporetencion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-primary btn-xs', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new TipoRetencion entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoRetencion();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:TipoRetencion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoRetencion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoRetencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoRetencion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoRetencion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoRetencion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoRetencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoRetencion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:TipoRetencion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoRetencion entity.
    *
    * @param TipoRetencion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoRetencion $entity)
    {
        $form = $this->createForm(new TipoRetencionType(), $entity, array(
            'action' => $this->generateUrl('tiporetencion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing TipoRetencion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:TipoRetencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoRetencion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tiporetencion'));
        }

        return $this->render('AppBundle:TipoRetencion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoRetencion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:TipoRetencion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoRetencion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tiporetencion'));
    }

    /**
     * Creates a form to delete a TipoRetencion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tiporetencion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
