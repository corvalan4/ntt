<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\Diseno;
use AppBundle\Entity\Precio;
use AppBundle\Entity\Stock;
use AppBundle\Entity\UnidadNegocio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Producto;
use AppBundle\Form\ProductoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
// IMPORTACION NECESARIA PARA ARCHIVOS
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @DI\Inject("productoconsignacionservice")
     */
    public $productoConsignacionService;

    /**
     * Lists all Producto entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filter = array(
            'codigo' => $request->get('codigo'),
            'descripcion' => $request->get('descripcion'),
            'categoria' => $request->get('categoria'),
            'subcategoria' => $request->get('subcategoria'),
            'proveedor' => $request->get('proveedor'),
            'medida' => $request->get('medida'),
        );

        if ($this->isGranted("ROLE_ADMIN")) {
            $entities = $em->getRepository('AppBundle:Producto')->findByFilter($filter);
        } else {
            $filter['listaprecio'] = $this->getUser()->getUnidadNegocio()->getListaPrecio() ? $this->getUser()->getUnidadNegocio()->getListaPrecio()->getId() : "";
            $entities = $em->getRepository('AppBundle:Producto')->findByFilterSeller($filter);
        }
        $categorias = $em->getRepository('AppBundle:Categoria')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
        $proveedores = $em->getRepository('AppBundle:ClienteProveedor')->findBy(array('estado' => 'A'), array('razonSocial' => 'ASC'));

        
         //Verifico si se presiono el botón exportar excel
         
        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excel($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Control Horario');
        }

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        if ($this->isGranted("ROLE_ADMIN")) {
            $page = 'index.html.twig';
        } else {
            $page = 'index_vendedor.html.twig';
        }
        return $this->render('AppBundle:Producto:' . $page, array(
                    'entities' => $paginador,
                    'categorias' => $categorias,
                    'proveedores' => $proveedores,
        ));
    }

    public function predictivaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $string = '<ul>';
        if (strlen($search) >= 2) {
            $productos = $em->getRepository('AppBundle:Producto')->predictiva($search);
            foreach ($productos as $prod) {
                $string = $string . '<li class=suggest-element codigo=' . $prod->getCodigo() . ' id=' . $prod->getId() . '><a href="#" data=' . $prod->getId() . ' id=producto' . $prod->getId() . '>' . $prod->getNombre() . ' ( ' . $prod->getCodigo() . ' )';
            }
        }
        $response = $string;
        return new Response($response);
    }

    public function predictivaUnidadAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $idUnidad = $request->get('idunidad');
        $string = '<ul>';
        if (strlen($search) >= 2) {
            $stocks = $em->getRepository('AppBundle:Stock')->predictiva($search, $idUnidad);
            foreach ($stocks as $prod) {
                $string = $string . '<li class=suggest-element codigo=' . $prod->getProducto()->getCodigo() . ' id=' . $prod->getId() . '><a href="#" data=' . $prod->getId() . ' id=producto' . $prod->getId() . '>' . $prod->getProducto()->getNombre() . " - N° Serie " . $prod->getSerie();
            }
        }
        $response = $string;
        return new Response($response);
    }

    public function listadoAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Producto')->findAll();

        return $this->render('AppBundle:Producto:listado.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function listadocategoriaAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Producto')->listadocategoria($this->sessionManager->getSession('iddependencia'));

        $listado = new ArrayCollection();

        foreach ($entities as $entity) {
            $categoria = $em->getRepository('AppBundle:Categoria')->find($entity['id']);
            $entidad = array('stock' => $entity['stock'], "nombrecategoria" => $categoria->getNombre());
            $listado->add($entidad);
        }

        return $this->render('AppBundle:Producto:listadocategoria.html.twig', array(
                    'entities' => $listado,
        ));
    }

    /**
     * Creates a new Producto entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Producto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $listas = $em->getRepository('AppBundle:ListaPrecio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        $usuario = $this->getUser();
        $entity->setUsuario($usuario);

        if ($form->isValid()) {
            $entity->setSlug($this->sessionManager->getSlug($entity->getCodigo() . " " . $entity->getNombre()));
            $stockInicial = $form['stock_inicial']->getData();
            if(!empty($stockInicial) and $stockInicial > 0){
                $unidadNegocio = $em->getRepository(UnidadNegocio::class)->findOneBy(['estado' => 'A']);

                $stock = new Stock();
                $stock->setProducto($entity);
                $stock->setStock($stockInicial);
                $stock->setCosto(0);
                $stock->setRealizo($this->getUser());
                $stock->setUnidadnegocio($unidadNegocio ? $unidadNegocio : null);
                $em->persist($stock);

                $balance = $this->sessionManager->newBalance($stock, $this->getUser(), 0, $stockInicial);
                $em->persist($balance);
            }

            foreach ($listas as $lista) {
                $precio_lista = $request->get('lista_id_' . $lista->getId());
                if (!empty($precio_lista)) {
                    $precio = new Precio();
                    $precio->setValor($precio_lista);
                    $precio->setProducto($entity);
                    $precio->setListaprecio($lista);
                    $em->persist($precio);
                }
            }
            $em->persist($entity);
            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Producto creado con exito.');
            return $this->redirect($this->generateUrl('producto_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Producto:new.html.twig', array(
                    'entity' => $entity,
                    'listas' => $listas,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Producto entity.
     *
     * @param Producto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Producto $entity) {
        $form = $this->createForm(new ProductoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('producto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-primary btn-xs', "style" => 'float:right; margin-right:1em;')));

        return $form;
    }

    /**
     * Displays a form to create a new Producto entity.
     *
     */
    public function newAction() {
        $entity = new Producto();
        $form = $this->createCreateForm($entity);
        $em = $this->getDoctrine()->getManager();
        $listas = $em->getRepository('AppBundle:ListaPrecio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        return $this->render('AppBundle:Producto:new.html.twig', array(
                    'entity' => $entity,
                    'listas' => $listas,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Producto entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        return $this->render('AppBundle:Producto:show.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
        ));
    }

    private function checkStock($productos, $cantidad, $unidad) {
        $em = $this->getDoctrine()->getManager();

        foreach ($productos as $producto) {
            $filter = ["producto_id" => $producto->getId(), "unidad_id" => $unidad];
            $stockdisponible = $em->getRepository('AppBundle:Balance')->balancePorProducto($filter);
            if ($stockdisponible < $cantidad)
                return false;
        }

        return true;
    }

    /**
     * Finds and displays a Producto entity.
     *
     */
    public function pasajeStockAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $cantidad = $request->get('cantidad');
        $unidad = $this->isGranted('ROLE_SUPER_ADMIN') ? $request->get('unidad') : $this->getUser()->getUnidadNegocio()->getId();

        if (empty($cantidad)) {
            $this->addFlash("msgError", "Debe indicar una cantidad.");
            return $this->redirectToRoute('producto_show', array('id' => $id));
        }

        $pasajerealizado = true;

        if (!$this->checkStock($entity->getProductos(), $cantidad, $unidad)) {
            $this->addFlash("msgError", "No todos los productos que lo conforman tienen el stock necesario");
            return $this->redirectToRoute('producto_show', array('id' => $id));
        }
        $costo = 0;

        foreach ($entity->getProductos() as $producto) {
            $cantidadactual = $cantidad;
            if ($producto->getStock() > 0 and $producto->getStock() >= $cantidadactual) {
                $elementostocks = $em->getRepository('AppBundle:Stock')->findBy(array('producto' => $producto->getId(), 'unidadnegocio' => $unidad), array('fecha' => 'ASC'));

                foreach ($elementostocks as $elementostock) {
                    if ($cantidadactual > 0 and $cantidadactual <= $elementostock->getStock()) {
                        $stock = $elementostock->getStock() - $cantidadactual;
                        $additional = ["description" => "Pasaje a Producto Compuesto"];
                        $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $cantidad, \AppBundle\Entity\Balance::TYPE_NEGATIVE, $additional);
                        $em->persist($balance);
                        $elementostock->setStock($stock);
                        $cantidadactual = 0;
                    } else {
                        if ($cantidadactual > 0 and $elementostock->getStock() > 0) {
                            $cantidadactual = $cantidadactual - $elementostock->getStock();
                            $additional = ["description" => "Pasaje a Producto Compuesto"];
                            $balance = $this->sessionManager->registerBalance($elementostock, $this->getUser(), 0, $elementostock->getStock(), Balance::TYPE_NEGATIVE, $additional);
                            $em->persist($balance);
                            $elementostock->setStock(0);
                        }
                    }
                }
            } else {
                $this->addFlash("msgError", "No hay stock suficiente para realizar el pasaje de stock en el producto: <br/>" . $producto);
                $pasajerealizado = false;
                break;
            }
        }

        if ($pasajerealizado) {
            $this->addFlash("msgOk", "Pasaje realizado exitosamente.");
            $unidadNegocio = $em->getRepository('AppBundle:UnidadNegocio')->find($unidad);

            $stockCompuesto = new Stock();
            $stockCompuesto->setDiseno($entity->getDisenos()[0]);
            $stockCompuesto->setProducto($entity);
            $stockCompuesto->setStock($cantidad);
            $stockCompuesto->setCosto(0);
            $stockCompuesto->setUnidadnegocio($unidadNegocio);
            $em->persist($stockCompuesto);

            $balance = $this->sessionManager->newBalance($stockCompuesto, $this->getUser(), 0, $cantidad, \AppBundle\Entity\Balance::TYPE_POSITIVE);

            $em->persist($balance);
            $em->flush();
        }

        return $this->redirectToRoute('producto_show', array('id' => $id));
    }

    public function productoDisenoDeleteAction($id_diseno) {
        $em = $this->getDoctrine()->getManager();

        $diseno = $em->getRepository('AppBundle:Diseno')->find($id_diseno);
        $diseno->setEstado("E");
        $em->flush();

        $this->sessionManager->addFlash('msgError', 'Diseño eliminado.');

        return $this->redirectToRoute("producto_show", ['id' => $diseno->getProducto()->getId()]);
    }

    /**
     * Displays a form to edit an existing Producto entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $listas = $em->getRepository('AppBundle:ListaPrecio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);
        $listas_precio = [];
        foreach ($listas as $lista) {
            $listas_precio[$lista->getId()] = ["id" => $lista->getId(), "descripcion" => $lista->getDescripcion(), "valor" => ""];
        }
        foreach ($entity->getPrecios() as $precio) {
            $listas_precio[$precio->getListaprecio()->getId()]["valor"] = $precio->getValor();
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('AppBundle:Producto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'listas' => $listas_precio,
        ));
    }

    /**
     * Creates a form to edit a Producto entity.
     *
     * @param Producto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Producto $entity) {
        $form = $this->createForm(new ProductoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('producto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn btn-primary btn-xs', "style" => 'float:right; margin-right:1em;')));

        return $form;
    }

    /**
     * Edits an existing Producto entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $listas = $em->getRepository('AppBundle:ListaPrecio')->findBy(['estado' => 'A'], ['descripcion' => 'ASC']);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setSlug($this->sessionManager->getSlug($entity->getCodigo() . " " . $entity->getNombre()));
            foreach ($listas as $lista) {
                $precio = $em->getRepository('AppBundle:Precio')->findOneBy(['producto' => $entity->getId(), 'listaprecio' => $lista->getId()]);
                $precio_lista = $request->get('lista_id_' . $lista->getId());
                if (!empty($precio_lista)) {
                    if (!$precio) {
                        $precio = new Precio();
                    }

                    $precio->setValor($precio_lista);
                    $precio->setProducto($entity);
                    $precio->setListaprecio($lista);
                    $em->persist($precio);
                }
            }

            $this->sessionManager->addFlash('msgOk', 'Producto editado con exito.');
            $em->flush();

            return $this->redirect($this->generateUrl('producto'));
        }

        $listas_precio = [];

        foreach ($listas as $lista) {
            $listas_precio[$lista->getId()] = ["id" => $lista->getId(), "descripcion" => $lista->getDescripcion(), "valor" => ""];
        }
        foreach ($entity->getPrecios() as $precio) {
            $listas_precio[$precio->getListaprecio()->getId()]["valor"] = $precio->getValor();
        }
        return $this->render('AppBundle:Producto:edit.html.twig', array(
                    'entity' => $entity,
                    'listas' => $listas_precio,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Producto entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $entity->setEstado("E");
        $em->flush();

        return $this->redirect($this->generateUrl('producto'));
    }

    public function activarAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Producto')->find($id);
        $entity->setEstado("A");
        $em->flush();

        return $this->redirect($this->generateUrl('producto'));
    }

    /**
     * Creates a form to delete a Producto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('producto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Lists all Balance entities.
     *
     */
    public function informeVentasAction(Request $request, $page = 1) {

        if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
            $unidadNegocio = $this->getUser()->getUnidadnegocio()->getId();
        } else {
            $unidadNegocio = $request->get('unidadNegocio');
        }

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta') . ' +1 day');
        }

        $categoria = $request->get('categoria');
        $subcategoria = $request->get('subcategoria');
        $codigo = $request->get('codigo');
        $nombre = $request->get('nombre');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Producto')->filtroBusquedaInformeVentas($fechaDesde, $fechaHasta, $codigo, $nombre, $categoria, $subcategoria, $unidadNegocio);
        $total = $em->getRepository('AppBundle:Producto')->sumInformeVentas($fechaDesde, $fechaHasta, $codigo, $nombre, $categoria, $subcategoria, $unidadNegocio);

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Producto:informeVentas.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total[0]
        ));
    }

    /*
     * Render Select 
     */

    public function renderSelectAction(Request $request) {
        $estado = $request->get('estado');
        $em = $this->getDoctrine()->getManager();

        $productos = $em->getRepository('AppBundle:Producto')->findBy(array('estado' => $estado), array('nombre' => 'ASC'));

        return $this->render('AppBundle:Producto:select.html.twig', array(
                    'productos' => $productos,
        ));
    }

    /*
     * Informe de General de Stock
     */

    public function informeGeneralStockAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();

        $filtros['unidadNegocio'] = null;
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadNegocio();
        }

        $productos = $em->getRepository('AppBundle:Producto')->filtroBusquedaInformeGeneralStock($filtros);
        $entities = [];
        $datetime = new \DateTime();
        $fechaDesde = new \DateTime($datetime->format("Y") . "/01/01 00:00:00");
        $fechaHasta = new \DateTime($datetime->format("Y") . "/12/31 23:59:00");


        $total = 0;

        foreach ($productos as $producto) {
            $p['producto'] = $producto;

            $p['stockDeposito'] = $em->getRepository('AppBundle:Stock')->stockByUnidadAndIdProduct($filtros['unidadNegocio'], $producto->getId());

            if (!$p['stockDeposito']) {
                $p['stockDeposito'] = 0;
            }
            $p['stockReservados'] = $em->getRepository('AppBundle:Stock')->stockReservadosByUnidadAndIdProduct($filtros['unidadNegocio'], $producto->getId());
            if (!$p['stockReservados']) {
                $p['stockReservados'] = 0;
            }
            $p['stockNoConforme'] = $em->getRepository('AppBundle:Stock')->stockNoConformidadByUnidadAndIdProduct($filtros['unidadNegocio'], $producto->getId());
            if (!$p['stockNoConforme']) {
                $p['stockNoConforme'] = 0;
            }
            $p['stockConsignados'] = $em->getRepository('AppBundle:ProductoConsignacion')->stockByUnidadAndIdProduct($filtros['unidadNegocio'], $producto->getId());
            if (!$p['stockConsignados']) {
                $p['stockConsignados'] = 0;
            }

            $p['pendientesFacturar'] = $em->getRepository('AppBundle:Stock')->countPendientesFacturacion($filtros['unidadNegocio'], $producto->getId()) + $em->getRepository('AppBundle:Stock')->countPendientesFacturacionDesdeRemito($filtros['unidadNegocio'], $producto->getId());                     

            if (!$p['pendientesFacturar']) {
                $p['pendientesFacturar'] = 0;
            }

            $totalVentas = $em->getRepository('AppBundle:ProductoFactura')->ventasByUnidadAndIdProduct($filtros['unidadNegocio'], $producto->getId(), $fechaDesde, $fechaHasta);
            if (!$totalVentas) {
                $p['ventas'] = 0;
            }

            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                if ($filtros['unidadNegocio'] == '') {
                    $p['unidadNegocio'] = 'Todas';
                } else {
                    $p['unidadNegocio'] = $em->getRepository('AppBundle:UnidadNegocio')->find($filtros['unidadNegocio']);
                }
            } else {
                $p['unidadNegocio'] = $em->getRepository('AppBundle:UnidadNegocio')->find($filtros['unidadNegocio']);
            }

            if (isset($filtros['stockMasUno']) && $filtros['stockMasUno'] != '') {
                if ($filtros['stockMasUno'] == 1) {
                    if ($p['stockDeposito'] >= 1) {
                        $entities[] = $p;
                    }
                } else {
                    if ($p['stockDeposito'] == 0) {
                        $entities[] = $p;
                    }
                }
            } else {
                $entities[] = $p;
            }
        }

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcelInformeGeneralStock($entities);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de General de Stock');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Producto:informeGeneralStock.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     *  Generación de contenido archivo excel Informe General de Stock
     */

    private function exportarExcelInformeGeneralStock($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Código');
        $sheet->setCellValue('B1', 'Stock Depósito');
        $sheet->setCellValue('C1', 'Stock Reservados');
        $sheet->setCellValue('D1', 'Stock No Conforme');
        $sheet->setCellValue('E1', 'Stock Consignación');
        $sheet->setCellValue('F1', 'Total');
        $sheet->setCellValue('G1', 'Pendientes a Facturar');
        $sheet->setCellValue('H1', 'Ventas ' . date("Y"));
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('I1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity['producto']->getCodigo());
            $sheet->setCellValue('B' . $fila, $entity['stockDeposito']);
            $sheet->setCellValue('C' . $fila, $entity['stockReservados']);
            $sheet->setCellValue('D' . $fila, $entity['stockNoConforme']);
            $sheet->setCellValue('E' . $fila, $entity['stockConsignados']);
            $sheet->setCellValue('F' . $fila, $entity['stockDeposito'] + $entity['stockConsignados']);
            $sheet->setCellValue('G' . $fila, $entity['pendientesFacturar']);
            $sheet->setCellValue('H' . $fila, $entity['ventas']);

            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('I' . $fila, $entity['unidadNegocio']);
            }

            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     *  Generación de contenido archivo excel de Productos
     */

    private function excel($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Código');
        $sheet->setCellValue('B1', 'Categoría');
        $sheet->setCellValue('C1', 'Subcategoría');
        $sheet->setCellValue('D1', 'Nombre');
        $sheet->setCellValue('E1', 'Precio');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('F1', 'Stock');
        }
        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, $entity->getCodigo());
            if ($entity->getSubcategoria()) {
                if ($entity->getSubcategoria()) {
                    $sheet->setCellValue('B' . $fila, $entity->getSubcategoria()->getCategoria());
                }
                $sheet->setCellValue('C' . $fila, $entity->getSubcategoria());
            }
            $sheet->setCellValue('D' . $fila, $entity->getNombre());
            $sheet->setCellValue('E' . $fila, $entity->getPrecio());
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('F' . $fila, $entity->getStock());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
            $sheet->getColumnDimension('F')->setAutoSize(true);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':E' . $fila)->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Predictiva de productos
     */

    public function predictivaSelect2Action(Request $request) {

        if (strlen($request->get('search')) < 3) {
            return new JsonResponse([]);
        }
        $response = ['items' => []];
        $em = $this->getDoctrine()->getManager();
        $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad'), 'trazable' => 1];
        $entities = $em->getRepository('AppBundle:Producto')->findByFilterWithStock($filter);

        foreach ($entities as $entity) {
            $response['items'][] = [
                'id' => $entity->getId(),
                'id_prod' => $entity->getId(),
                'precio' => $entity->getPrecio(),
                'name' => $entity->getNombre(),
                'full_name' => $entity->getNombre(),
                'text' => $entity->getNombre(),
            ];
        }
        $response['total_count'] = sizeof($entities);

        return new JsonResponse($response);
    }

    /*
     * Predictiva de productos
     */

    public function predictivaSelect3Action(Request $request) {

        if (strlen($request->get('search')) < 3) {
            return new JsonResponse([]);
        }
        $response = ['items' => []];
        $em = $this->getDoctrine()->getManager();
        $filter = ['search' => $request->get('search'), 'idunidad' => $request->get('idunidad')];

        $entities = $em->getRepository('AppBundle:Producto')->findByFilterProducts($filter);

        foreach ($entities as $entity) {
            $response['items'][] = [
                'id' => $entity->getId(),
                'id_prod' => $entity->getId(),
                'precio' => $entity->getPrecio(),
                'name' => $entity->getNombre(),
                'full_name' => $entity->getNombre(),
                'text' => $entity->getNombre(),
            ];
        }
        $response['total_count'] = sizeof($entities);

        return new JsonResponse($response);
    }

    /**
     * Lists all Stock entities.
     *
     */
    public function informeVencidosAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $filtros = $request->query->all();


        if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
            $filtros['unidadNegocio'] = $this->getUser()->getUnidadnegocio()->getId();
        } else {
            $filtros['unidadNegocio'] = $request->get('unidadNegocio');
        }

        $entities = $em->getRepository('AppBundle:Stock')->filtroBusquedaInformeProductosVencidos($filtros);

        /*
         * Verifico si se presiono el botón exportar excel
         */

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->exportarExcelInformeVencidos($entities, $filtros);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe productos vencer/vencidos');
        }


        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Producto:informeVencidos.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Exportación a excel de Informe de Vencidos
     */

    private function exportarExcelInformeVencidos($entities, $filtros) {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Código');
        $sheet->setCellValue('B1', 'Fecha de vencimiento');
        $sheet->setCellValue('C1', 'Lote');
        $sheet->setCellValue('D1', 'Serie');
        $sheet->setCellValue('E1', 'Depósito');
        $sheet->setCellValue('F1', 'Banco');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $sheet->setCellValue('G1', 'Unidad Negocio');
        }

        $fila = 2;
        foreach ($entities as $entity) {

            $productoConsignacion = $this->productoConsignacionService->getProductoConsignacionByStockInClient($entity->getId());

            $sheet->setCellValue('A' . $fila, $entity->getProducto()->getCodigo());
            $sheet->setCellValue('B' . $fila, $entity->getVencimiento() ? $entity->getVencimiento()->format("d/m/Y") : '');
            $sheet->setCellValue('C' . $fila, $entity->getLote());
            $sheet->setCellValue('D' . $fila, $entity->getSerie());
            if (!$productoConsignacion) {
                $sheet->setCellValue('E' . $fila, $entity->getDeposito());
            } else {
                $sheet->setCellValue('F' . $fila, $productoConsignacion->getConsignacion()->getClienteProveedor());
            }
            if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                $sheet->setCellValue('G' . $fila, $entity->getUnidadNegocio());
            }
            $fila++;
        }

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }

}
