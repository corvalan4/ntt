<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Factura;
use AppBundle\Entity\Iva;
use AppBundle\Form\FacturaProveedorGastoType;
use AppBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Gasto controller.
 *
 */
class GastoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Gasto entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $filtros = $request->query->all();
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $filtros['unidad'] = $request->get('unidadNegocio');
        } else {
            $filtros['unidad'] = $this->getUser()->getUnidadNegocio()->getId();
        }
        $filtros['esGasto'] = true;

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Factura')->findByFilter($filtros);

        $tiposGasto = $em->getRepository('AppBundle:TipoGasto')->findBy(array('estado' => 'A'), array('nombre' => 'ASC'));

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Gasto:index.html.twig', array(
                    'entities' => $paginador,
                    'tiposGasto' => $tiposGasto,
        ));
    }

    /**
     * Creates a new Gasto.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $entity->setTipo("FP");
        $mes = $form->get("mes")->getData();
        $anio = $form->get("anio")->getData();
        $entity->setMesanio($mes . "/" . $anio);

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        } else {
            $this->sessionManager->addFlash("msgError", "Se debe seleccionar un Proveedor.");
            return $this->render('AppBundle:Gasto:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        }
        $total = (float) $request->get("total");
        $entity->setImporte($total);

        if ($form->isValid()) {
            $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            $tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            $importeiva105 = $request->get("importeiva105");
            if (!empty($importeiva21) && $importeiva21 > 0) {
                $iva = new Iva();
                if ($tipoIva21) {
                    $iva->setTipoIva($tipoIva21);
                }
                $iva->setFactura($entity);
                $iva->setValor(round($importeiva21, 2));
                $iva->setImporteFactura($total);
                $em->persist($iva);
            }
            if (!empty($importeiva105) && $importeiva105 > 0) {
                $iva = new Iva();
                if ($importeiva105) {
                    $iva->setTipoIva($tipoIva105);
                }
                $iva->setFactura($entity);
                $iva->setValor(round($importeiva105, 2));
                $iva->setImporteFactura($total);
                $em->persist($iva);
                $entity->addIva($iva);
            }
            $puntoVta = $request->get("puntoventa");
            if (!$puntoVta) {
                $puntoVta = 0;
            }

            $entity->setPtovta($puntoVta);
            $entity->setNrofactura($request->get("nrofactura"));

            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setFactura($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($entity->getImporte());
            $em->persist($pagoAsignacion);

            $em->persist($entity);

            $em->flush();
            $this->sessionManager->addFlash("msgOk", "Gasto creado exitosamente.");


            return $this->redirect($this->generateUrl('gasto'));
        }

        $this->sessionManager->addFlash("msgError", "Error al dar de alta el gasto.");
        return $this->render('AppBundle:gasto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Gasto entity.
     *
     */
    /* public function createAction(Request $request) {
      $entity = new Gasto();
      $form = $this->createCreateForm($entity);
      $form->handleRequest($request);

      $em = $this->getDoctrine()->getManager();
      $error = false;
      $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
      $proveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($request->get('clienteProveedor'));
      if (!isset($proveedor)) {
      $this->sessionManager->addFlash('msgError', 'No se ha seleccionado un proveedor.');
      $error = true;
      } else {
      $entity->setClienteProveedor($proveedor);

      if ($this->isGranted('ROLE_SUPER_ADMIN')) {
      $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($request->get('unidad'));
      if ($unidad) {
      $entity->setUnidadnegocio($unidad);
      } else {
      $this->sessionManager->addFlash('msgError', 'No se ha seleccionado una unidad de negocio.');
      $error = true;
      }
      } else {
      $entity->setUnidadnegocio($this->getUser()->getUnidadNegocio());
      }
      }
      if (!$error) {

      $importemedios = 0;
      foreach ($medios as $medio) {
      $importemedio = $request->get('importemedio' . $medio->getId());
      $numerocupon = $request->get('numerocupon' . $medio->getId());
      $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
      $cuotas = $request->get('cuotas' . $medio->getId());
      if (!empty($importemedio)) {
      $mediodocumento = new MedioDocumento();
      $mediodocumento->setMedio($medio);
      $mediodocumento->setGasto($entity);
      $mediodocumento->setImporte($importemedio);
      $mediodocumento->setNumerolote((int) $numerotransaccion);
      $mediodocumento->setNumerocupon((int) $numerocupon);
      $mediodocumento->setCuotas((int) $cuotas);
      $em->persist($mediodocumento);
      $importemedios += $importemedio;
      }
      }

      $importeRetenciones = 0;

      $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
      for ($i = 1; $i <= count($tiporetenciones); $i++) {
      if ($request->get('retencionimporte' . $i) != '' and $request->get('retencionimporte' . $i) > 0) {
      $retencion = new Retencion();
      $tiporetencion = $em->getRepository('AppBundle:TipoRetencion')->findOneBy(array('descripcion' => $request->get('retencion' . $i)));
      $retencion->setImporte($request->get('retencionimporte' . $i));
      $retencion->setTiporetencion($tiporetencion);
      $retencion->setGasto($entity);
      $em->persist($retencion);
      $importeRetenciones += $request->get('retencionimporte' . $i);
      }
      }
      $importe = $importeRetenciones + $importemedios;
      $entity->setImporte($importe);

      $chequesId = $request->get('selectcheques');

      if ($form->isValid()) {
      $em->persist($entity);
      if (is_array($chequesId)) {
      foreach ($chequesId as $chequeId) {
      $cheque = $em->getRepository('AppBundle:Cheque')->find($chequeId);
      if ($cheque->getEstado() == 'U') {
      $this->sessionManager->addFlash('msgError', 'Hay al menos un cheque que ya ha sido utilizado con anterioridad.');
      return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
      }
      $cheque->setEstado('U');
      $cheque->setGasto($entity);
      $em->persist($cheque);
      $em->flush();
      $importe = $importe + $cheque->getImporte();
      $cuentaOrigen = $cheque->getCuentaorigen();
      if (isset($cuentaOrigen)) {
      $movimiento = new MovimientoBancario();
      $movimiento->setCheque($cheque);
      $movimiento->setCuentabancaria($cheque->getCuentaorigen());
      $movimiento->setTipoMovimiento('CH');
      $movimiento->setValor($cheque->getImporte());
      $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
      $movimiento->setEstado('U');
      $em->persist($movimiento);
      $em->flush();
      }
      }
      }
      // Se asigna Pago.
      $pagoAsignacion = new PagoAsignacion();
      $pagoAsignacion->setGasto($entity);
      $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
      $pagoAsignacion->setFecha($entity->getFecha());
      $pagoAsignacion->setImporte($importe);
      $em->persist($pagoAsignacion);
      $em->flush();
      $entity->setImporte($importe);
      $em->persist($entity);
      $em->flush();
      return $this->redirect($this->generateUrl('gasto_show', array('id' => $entity->getId())));
      }
      }
      $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
      $cheques = $em->getRepository('AppBundle:Cheque')->findBy(array('estado' => 'A'), array('fechacobro' => 'ASC'));

      return $this->render('AppBundle:Gasto:new.html.twig', array(
      'entity' => $entity,
      'medios' => $medios,
      'tiporetenciones' => $tiporetenciones,
      'cheques' => $cheques,
      'form' => $form->createView(),
      'origen' => 'gasto'
      )
      );
      }
     */

    /**
     * Creates a form to create a Gasto entity.
     *
     * @param Gasto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Factura $entity) {
        $form = $this->createForm(new FacturaProveedorGastoType(), $entity, array(
            'action' => $this->generateUrl('gasto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new Factura entity.
     *
     */
    public function newAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }

        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Gasto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Gasto entity.
     *
     */
    /* public function newAction() {
      $em = $this->getDoctrine()->getManager();
      $entity = new Gasto();
      $entity->setFecha(new \DateTime("NOW"));
      $form = $this->createCreateForm($entity);

      $medios = $em->getRepository('AppBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));
      $tiporetenciones = $em->getRepository('AppBundle:TipoRetencion')->findBy(array('estado' => 'A'), array('descripcion' => 'ASC'));
      $cheques = $em->getRepository('AppBundle:Cheque')->findBy(array('estado' => 'A'), array('fechacobro' => 'ASC'));

      return $this->render('AppBundle:Gasto:new.html.twig', array(
      'entity' => $entity,
      'medios' => $medios,
      'tiporetenciones' => $tiporetenciones,
      'cheques' => $cheques,
      'form' => $form->createView(),
      'origen' => 'gasto')
      );
      } */

    /**
     * Finds and displays a Gasto entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Gasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gasto entity.');
        }

        return $this->render('AppBundle:Gasto:show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Factura')->find($id);
        if ($entity->getMesanio() == '/') {
            $entity->setMesanio($entity->getFecha()->format('m/Y'));
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        return $this->render('AppBundle:Gasto:edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $editForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Gasto entity.
     *
     */
    /*    public function editAction($id) {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('AppBundle:Gasto')->find($id);

      if (!$entity) {
      throw $this->createNotFoundException('Unable to find Gasto entity.');
      }

      $editForm = $this->createEditForm($entity);
      $deleteForm = $this->createDeleteForm($id);

      return $this->render('AppBundle:Gasto:edit.html.twig', array(
      'entity' => $entity,
      'edit_form' => $editForm->createView(),
      'delete_form' => $deleteForm->createView(),
      ));
      }
     */

    /**
     * Creates a form to edit a Gasto entity.
     *
     * @param Gasto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Factura $entity) {
        $form = $this->createForm(new FacturaProveedorGastoType(), $entity, array(
            'action' => $this->generateUrl('gasto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Edits an existing Gasto entity.
     *
     */
    /*  public function updateAction(Request $request, $id) {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('AppBundle:Gasto')->find($id);

      if (!$entity) {
      throw $this->createNotFoundException('Unable to find Gasto entity.');
      }

      $deleteForm = $this->createDeleteForm($id);
      $editForm = $this->createEditForm($entity);
      $editForm->handleRequest($request);

      if ($editForm->isValid()) {
      $em->flush();

      return $this->redirect($this->generateUrl('gasto_edit', array('id' => $id)));
      }

      return $this->render('AppBundle:Gasto:edit.html.twig', array(
      'entity' => $entity,
      'edit_form' => $editForm->createView(),
      'delete_form' => $deleteForm->createView(),
      ));
      } */

    /**
     * Edits an existing Factura entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $importetotal = round($request->get("importetotal"), 2);
        $entity->setImporte($importetotal);

        if ($editForm->isValid()) {
            $tipoIva21 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            $tipoIva105 = $em->getRepository('AppBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            $importeiva105 = $request->get("importeiva105");

            if (!empty($importeiva21) and $importeiva21 > 0) {
                $iva21 = $em->getRepository('AppBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva21, 'factura' => $entity));
                if (!$iva21) {
                    $iva21 = new Iva();
                    if ($tipoIva21) {
                        $iva21->setTipoIva($tipoIva21);
                    }
                    $iva21->setFactura($entity);
                    $em->persist($iva21);
                    $entity->addIva($iva21);
                }
                $iva21->setValor(round($importeiva21, 2));
                $iva21->setImporteFactura($importetotal);
            }
            if (!empty($importeiva105) and $importeiva105 > 0) {
                $iva105 = $em->getRepository('AppBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva105, 'factura' => $entity));
                if (!$iva105) {
                    $iva105 = new Iva();
                    if ($importeiva105) {
                        $iva105->setTipoIva($tipoIva105);
                    }
                    $iva105->setFactura($entity);

                    $em->persist($iva105);
                    $entity->addIva($iva105);
                }
                $iva105->setValor(round($importeiva21, 2));
                $iva105->setImporteFactura($importetotal);
            }

            $em->persist($entity);

            $em->flush();
            $this->sessionManager->addFlash("msgOk", "Gasto editado.");

            return $this->redirect($this->generateUrl('gasto'));
        }

        $this->sessionManager->addFlash("msgError", "Error al editar el gasto.");

        return $this->render('AppBundle:Gasto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     */
    public function deleteAction($id) {
        //Ver esto
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Factura')->find($id);

        $iva = $em->getRepository('AppBundle:Iva')->findOneBy(['factura' => $id]);
        $movimiento = $em->getRepository('AppBundle:PagoAsignacion')->findOneBy(['factura' => $id]);
        $em->remove($iva);
        $em->remove($movimiento);
        $em->remove($entity);

        $em->flush();

        $this->sessionManager->addFlash("msgOk", "Factura de gasto eliminada con éxito.");

        return $this->redirect($this->generateUrl('gasto'));
    }

    /**
     * Deletes a Gasto entity.
     *
     */
    /* public function deleteAction(Request $request, $id) {
      $form = $this->createDeleteForm($id);
      $form->handleRequest($request);

      if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('AppBundle:Gasto')->find($id);

      if (!$entity) {
      throw $this->createNotFoundException('Unable to find Gasto entity.');
      }

      $em->remove($entity);
      $em->flush();
      }

      return $this->redirect($this->generateUrl('gasto'));
      } */


    public function informeAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        if (empty($request->get('fechaDesde'))) {
            $fechaDesde = new \DateTime('NOW -30 days');
        } else {
            $fechaDesde = new \DateTime($request->get('fechaDesde'));
        }

        if (empty($request->get('fechaHasta'))) {
            $fechaHasta = new \DateTime('NOW +1 days');
        } else {
            $fechaHasta = new \DateTime($request->get('fechaHasta'));
        }

        $proveedor = $request->get('proveedor');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $entities = $em->getRepository('AppBundle:Factura')->filtroBusquedaInformeGasto($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);
        $total = $em->getRepository('AppBundle:Factura')->totalInformeGasto($fechaDesde, $fechaHasta, $proveedor, $unidadNegocio);

        //Verifico si se presiono el botón exportar excel        

        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelInforme($entities, $total);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Gastos');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Gasto:informe.html.twig', array(
                    'entities' => $paginador,
                    'total' => $total
        ));
    }

    public function informeCuentasPorPagarAction(Request $request, $page = 1) {
        $em = $this->getDoctrine()->getManager();

        $proveedor = $request->get('proveedor');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidadNegocio = $request->get('unidadNegocio');
        } else {
            $unidadNegocio = $this->getUser()->getUnidadNegocio();
        }

        $facturas = $em->getRepository('AppBundle:Gasto')->getUltimoGasto($proveedor, $unidadNegocio);

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $spreadSheet = $this->excelCuentasPorPagar($facturas);
            return $this->sessionManager->exportarExcel($spreadSheet, 'Informe de Cuentas por Pagar - Gastos');
        }

        $paginador = new Pagerfanta(new ArrayAdapter($facturas));
        $paginador->setMaxPerPage(30);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Gasto:informeCuentasPorPagar.html.twig', array(
                    'entities' => $paginador
        ));
    }

    /*
     * Generación de contenido archivo excel Informe de Ingreso -Egreso
     */

    private function excelInforme($entities, $total) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->setCellValue('B1', 'Documento');
        $sheet->setCellValue('C1', 'Tipo de Gasto');
        $sheet->setCellValue('D1', 'Proveedor');
        $sheet->setCellValue('E1', 'Unidad Negocio');
        $sheet->setCellValue('F1', 'Importe ($)');

        $fila = 2;
        foreach ($entities as $entity) {
            $sheet->setCellValue('A' . $fila, date_format($entity->getFecha(), "d/m/Y"));
             $nro = !empty($entity->getNrofactura()) ? $entity->getNrofactura() : $entity->getId();
            $facturaNro= "Factura" . $entity->getTipofactura() . " #" . str_pad($nro, 5, "0", STR_PAD_LEFT) . " - Pto." . $entity->getPtovta();            
            $sheet->setCellValue('B' . $fila, $facturaNro);
            $sheet->setCellValue('C' . $fila, $entity->getTipoGasto());
            $sheet->setCellValue('D' . $fila, $entity->getClienteProveedor());
            $sheet->setCellValue('E' . $fila, $entity->getUnidadNegocio());
            $sheet->setCellValue('F' . $fila, $entity->getImporte());
            $fila++;
        }

        //Totales
        $sheet->setCellValue('E' . $fila, 'Total');
        $sheet->setCellValue('F' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
       

        return $spreadsheet;
    }

    /*
     *  Generación de contenido archivo excel Informe de Cuentas por pagar
     */

    private function excelCuentasPorPagar($entities) {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set cell name and merge cells
        $sheet->setCellValue('A1', 'Unidad Negocio');
        $sheet->setCellValue('B1', 'Razón Social');
        $sheet->setCellValue('C1', 'Fecha Última Compra ($)');
        $sheet->setCellValue('D1', 'Fecha Último Pago');
        $sheet->setCellValue('E1', 'Último Pago ($)');
        $sheet->setCellValue('F1', 'Saldo Actual ($)');

        $fila = 2;
        $total = 0;
        foreach ($entities as $entity) {
            $cobranza = $this->getUltimoPago($entity[0]->getClienteProveedor()->getId());
            $sheet->setCellValue('A' . $fila, $entity[0]->getUnidadNegocio());
            $sheet->setCellValue('B' . $fila, $entity[0]->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, date_format($entity[0]->getFecha(), "d-m-Y"));
            $sheet->setCellValue('D' . $fila, (new \DateTime($cobranza['fecha']))->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $cobranza['importe']);
            $sheet->setCellValue('F' . $fila, $cobranza['saldo']);
            $total = $total + $cobranza['saldo'];
            $fila++;
        }

        //Totales
        $sheet->setCellValue('E' . $fila, 'Total');
        $sheet->setCellValue('F' . $fila, round($total, 2));
        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }

    /*
     * Obtengo último pago
     */

    public function getUltimoPago($id) {

        $em = $this->getDoctrine()->getManager();
        $pago = $em->getRepository('AppBundle:Pago')->getUltimoPago($id);

        $respuesta['fecha'] = '';
        $respuesta['importe'] = 0;

        if (isset($id)) {
            $saldo = $em->getRepository('AppBundle:PagoAsignacion')->findSaldosByClienteProveedor($id);
            $clienteProveedor = $em->getRepository('AppBundle:ClienteProveedor')->find($id);
            $respuesta['saldo'] = round($clienteProveedor->getSaldo() + $saldo['cobranzas'] + $saldo['notacredito'] + $saldo['notadebito'] - $saldo['facturas'] + $saldo['facturasproveedor'] - $saldo['gastos'] - $saldo['pagos'], 2);
        }

        if (isset($pago) && !empty($pago)) {
            $respuesta['fecha'] = date_format($pago[0][0]->getFecha(), 'Y-m-d');
            $respuesta['importe'] = $pago[0][0]->getImporte();
        }

        return $respuesta;
    }

}
