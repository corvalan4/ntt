<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\PagoAsignacion;
use AppBundle\Entity\Stock;
use AppBundle\Entity\Sucursal;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Presupuesto;
use AppBundle\Entity\ProductoPresupuesto;
use AppBundle\Form\PresupuestoType;
use AppBundle\Services\SessionManager;
use AppBundle\Services\PresupuestoService;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Presupuesto controller.
 *
 */
class PresupuestoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * @var SessionManager
     * @DI\Inject("presupuestoservice")
     */
    public $presupuestoService;

    /**
     * Lists all Presupuesto entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {

        $em = $this->getDoctrine()->getManager();

        $unidad = !$this->isGranted('ROLE_SUPER_ADMIN') ? $this->getUser()->getUnidadNegocio()->getId() : '';
        $fecha_desde = $request->get('fechaDesde') ?? '';
        $fecha_hasta = $request->get('fechaHasta') ?? '';
        $cliente = $request->get('cliente') ?? '';
        $estado = $request->get('estado') ?? '';
        $id = $request->get('id') ?? '';
        $filter = ['cliente' => $cliente, 'unidad' => $unidad, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'id' => $id, 'estado' => $estado];

        $unidades = $em->getRepository('AppBundle:UnidadNegocio')->findBy([], ['descripcion' => 'asc']);

        $entities = $em->getRepository('AppBundle:Presupuesto')->findByFilter($filter);

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(50);
        $paginador->setCurrentPage($page);

        return $this->render('AppBundle:Presupuesto:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
        ));
    }

    /**
     * Creates a new Presupuesto entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Presupuesto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

        $hayProductos = false;

        if ($form->isValid()) {

            $file = $entity->getAdjunto();

            if (!empty($file)) {
                $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();
                $entity->setAdjunto($fileName);
            }
            for ($i = 1; $i <= 20; $i++) {
                $idproducto = $request->get("idprod_elemento$i");
                $cantidad = (float) $request->get("cantidad_elemento$i");

                $precio = floatval($request->get("precio$i"));

                $descripcion = $request->get("descripcion_elemento$i");
                
                $renglon = $request->get("renglon_elemento$i");

                if (!empty($descripcion) && !empty($precio)) {
                    if (!$hayProductos) {
                        $hayProductos = true;
                    }
                    $producto = $em->getRepository('AppBundle:Producto')->find($idproducto);
                    $productopresupuesto = new ProductoPresupuesto();
                    $productopresupuesto->setProducto($producto);
                    $productopresupuesto->setDescripcion($descripcion);
                    $productopresupuesto->setPrecio($precio);
                    $productopresupuesto->setRenglon($renglon);
                    $productopresupuesto->setCantidad($cantidad);
                    $productopresupuesto->setPresupuesto($entity);

                    $em->persist($productopresupuesto);
                }
            }

            $entity->setImporte(floatval($request->get('total')));
            $this->sessionManager->addFlash('msgOk', 'Se ha creado un nuevo Presupuesto.');
            $em->persist($entity);
            $em->flush();
            if (!empty($file)) {
                $directory = $this->getParameter('directory_upload') . $entity->getId();
                if (!file_exists($directory)) {
                    mkdir($directory, 0777, true);
                }

                // moves the file to the directory where brochures are stored
                $file->move(
                        $directory,
                        $fileName
                );
            }
            return $this->redirect($this->generateUrl('presupuesto'));
        }
        $this->sessionManager->addFlash('msgError', 'No ha sido posible crear el presupuesto. Verifique los datos.');
        return $this->redirectToRoute('presupuesto_new');
    }

    /**
     * Creates a form to create a Presupuesto entity.
     *
     * @param Presupuesto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Presupuesto $entity) {
        $form = $this->createForm(new PresupuestoType(), $entity, array(
            'action' => $this->generateUrl('presupuesto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Confirmar Presupuesto', 'attr' => array('class' => 'btn btn-success  btn-xs', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new Presupuesto entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $entity = new Presupuesto();

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $unidad = $em->getRepository('AppBundle:UnidadNegocio')->find($this->getUser()->getUnidadNegocio()->getId());
            $entity->setUnidadNegocio($unidad);
        }
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:Presupuesto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a Presupuesto entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        return $this->render('AppBundle:Presupuesto:show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Presupuesto entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('AppBundle:Presupuesto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a Presupuesto entity.
     *
     * @param Presupuesto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Presupuesto $entity) {
        $form = $this->createForm(new PresupuestoType(true), $entity, array(
            'action' => $this->generateUrl('presupuesto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Confirmar Presupuesto', 'attr' => array('class' => 'btn btn-success  btn-xs', 'style' => 'float:right;')));


        return $form;
    }

    /**
     * Edits an existing Presupuesto entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $sucursalstr = $editForm->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('AppBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

        $hayProductos = false;
        if ($editForm->isValid()) {

            $file = $request->files->get('adjunto');

            if (!empty($file)) {

                $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();
                $entity->setAdjunto($fileName);
            }

            //Recorro los anteriores para verificar si tienen algun cambio
            foreach ($entity->getProductosPresupuesto() as $productoPresupuesto) {
                $cantidad = floatval($request->get("cantidad_pp" . $productoPresupuesto->getid()));
                $precio = floatval($request->get("precio_pp" . $productoPresupuesto->getid()));
                $descripcion = $request->get("descripcion_pp" . $productoPresupuesto->getid());
                $renglon = $request->get("renglon_pp" . $productoPresupuesto->getid());

                $productoPresupuesto->setRenglon($renglon);
                $productoPresupuesto->setCantidad($cantidad);
                $productoPresupuesto->setPrecio($precio);
                $productoPresupuesto->setDescripcion($descripcion);

                $em->persist($productoPresupuesto);
            }

            //Agrego los nuevos si es que hay
            for ($i = 1; $i <= 20; $i++) {

                $idproducto = $request->get("idprod_elemento$i");
                $cantidad = (float) $request->get("cantidad_elemento$i");

                $precio = floatval($request->get("precio$i"));

                $descripcion = $request->get("descripcion_elemento$i");
                
                $renglon = $request->get("renglon_elemento$i");

                if (!empty($descripcion) && !empty($precio)) {
                    if (!$hayProductos) {
                        $hayProductos = true;
                    }
                    $producto = $em->getRepository('AppBundle:Producto')->find($idproducto);
                    $productopresupuesto = new ProductoPresupuesto();

                    $productopresupuesto->setRenglon($renglon);
                    $productopresupuesto->setProducto($producto);
                    $productopresupuesto->setDescripcion($descripcion);
                    $productopresupuesto->setPrecio($precio);
                    $productopresupuesto->setCantidad($cantidad);

                    $productopresupuesto->setPresupuesto($entity);

                    $em->persist($productopresupuesto);
                }
            }

            $entity->setImporte(floatval($request->get('total')));
            //Se quita a pedido del cliente para que no salga impreso
            //$this->sessionManager->addFlash('msgOk', 'Se ha editado el Presupuesto.');
            $em->persist($entity);
            $em->flush();
            if (!empty($file)) {
                $directory = $this->getParameter('directory_upload') . $entity->getId();
                if (!file_exists($directory)) {

                    mkdir($directory, 0777, true);
                }

                // moves the file to the directory where brochures are stored
                $file->move(
                        $directory,
                        $fileName
                );
            }
            return $this->redirectToRoute('presupuesto_show', array('id' => $entity->getId()));
        }
        return $this->render('AppBundle:Presupuesto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName() {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * Eliminar adjunto.
     *
     */
    public function eliminarAdjuntoAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }
        $file = $this->getParameter('directory_upload') . $entity->getId() . '/' . $entity->getAdjunto();
        unlink($file);
        $entity->setAdjunto(null);

        $em->flush();
        $this->sessionManager->addFlash('msgOk', 'Se ha eliminado el archivo adjunto.');
        return $this->redirectToRoute('presupuesto_edit', array(
                    'id' => $id
        ));
    }

    /**
     * Eliminar adjunto.
     *
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }
        $entity->setEstado(Presupuesto::PRESUPUESTO_ANULADO);
        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Se ha eliminado el presupuesto seleccionado.');
        return $this->redirectToRoute('presupuesto');
    }

    /**
     * Muestra la pantalla de presupuesto a remitir
     *
     */
    public function remitirAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        return $this->render('AppBundle:Presupuesto:remitir.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Genera un remito a partir de un presupuesto
     *
     */
    public function generarRemitoAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Presupuesto')->find($id);

        //Recorro los productods 
        foreach ($entity->getProductosPresupuesto() as $productoPresupuesto) {
            $aceptado = $request->get("aceptado_pp" . $productoPresupuesto->getid());
            
            $renglon =$request->get("renglon_pp" . $productoPresupuesto->getid());
            $descripcion =$request->get("descripcion_pp" . $productoPresupuesto->getid());
            $cantidad =$request->get("cantidad_pp" . $productoPresupuesto->getid());
            $precio =$request->get("precio_pp" . $productoPresupuesto->getid());
            
            $productoPresupuesto->setRenglon($renglon);
            $productoPresupuesto->setDescripcion($descripcion);
            $productoPresupuesto->setCantidad($cantidad);
            $productoPresupuesto->setprecio($precio);            
            if ($aceptado == 'on') {
                $productoPresupuesto->setEstado(ProductoPresupuesto::PRODUCTO_PRESUPUESTO_ACEPTADO);
            } else {
                $productoPresupuesto->setEstado(ProductoPresupuesto::PRODUCTO_PRESUPUESTO_NO_ACEPTADO);
            }
            $em->persist($productoPresupuesto);
        }

        $em->persist($entity);
        $em->flush();

        $remitoOficial = $this->presupuestoService->remitirPresupuesto($entity,$request->get('nroRemito'),$request->get('observacion'));
        if ($remitoOficial) {
            $entity->setEstado(Presupuesto::PRESUPUESTO_FACTURARLO);
            $entity->setRemitoOficial($remitoOficial);
            $em->persist($entity);
            $em->flush();
            //$this->sessionManager->addFlash('msgOk', 'Se ha remitido el Presupuesto.');
            //Se saca a pedido del cliente al igual que en presupuesto
            return $this->redirectToRoute('remitooficial_show', array('id' => $remitoOficial->getId()));
        } else {
            $this->sessionManager->addFlash('msgError', 'No se podido remitir el Presupuesto.');
            return $this->redirectToRoute('presupuesto');
        }
    }

}
